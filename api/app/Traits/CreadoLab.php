<?php

namespace App\Traits;

use Exception;

trait CreadoLab
{
    public $url = "https://scoring-br.credolab.com/v5.0/";
    public $username = "admin@pagadores.com";
    public $password = "68TJq32Ww";

    public function credolab_AccessToken($return = false)
    {
        $url = $this->url . "account/login";
        $username = $this->username;
        $password = $this->password;

        try {

            $curl = curl_init($url);
            curl_setopt_array($curl, array(
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => json_encode(["userEmail" => $username, 'password' => $password]),
                CURLOPT_HTTPHEADER => array(
                    'Accept: application/json',
                    'Content-Type: application/json'
                ),
            ));

            $response = curl_exec($curl);
            if (curl_errno($curl)) {
                return false;
            }
            curl_close($curl);

            $result = json_decode($response, true);
            return $result['access_token'];

        } catch (Exception $e) {
            return false;
        }
    }

    public function credolab_Datasets($referenceNumber = null)
    {
        $url = $this->url . "datasets/" . $referenceNumber . '/datasetinsight';

        $accessToken = $this->credolab_AccessToken();
        if($accessToken){
            $curl = curl_init($url);
            curl_setopt_array($curl, array(
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => array(
                    'Accept: application/json',
                    'Content-Type: application/json',
                    "Authorization: Bearer " . $accessToken,
                ),
            ));

            $response = curl_exec($curl);
            if (curl_errno($curl)) {
                return false;
            }
            curl_close($curl);

            $result = json_decode($response, true);
            return $result;
        }
        return false;
    }
}
