<?php

namespace App\Traits;

use DB;
use \App\Models\Deudas;
use \App\Models\Homologacionbancostemplaris;
use \App\Models\Usuarios;
use SoapClient;

trait Templaris
{
    //public $templarisServiceUrl = "http://13.89.48.155";
    public $templarisServiceUrl = "https://templaris.net";
    public $templarisServiceUsername = "testpagadores";
    public $templarisServicePassword = "Pagadores";

    public function tokenTemplaris()
    {
        $uri = $this->templarisServiceUrl . '/templarissrl/token.php';
        $token = base64_encode("$this->templarisServiceUsername:$this->templarisServicePassword");

        $curl = curl_init($uri);
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode(["grant_type" => "client_credentials"]),
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json",
                "Authorization: Basic {$token}"
            ),
        ));

        $response = curl_exec($curl);
        if (curl_errno($curl)) {
            echo 'Error:' . curl_error($curl);
        }
        curl_close($curl);

        $result = json_decode($response, true);
        if (isset($result['access_token']) && $result['access_token']) {
            return $result['access_token'];
        }
        return false;
        //echo json_encode($result);die();
    }

    public function customerAccountsTemplaris($cedula = null)
    {
        /* $fechaActual = date('Y-m-d 23:59:59');
        $fechaConsulta = date('Y-m-d 00:00:00');
        $fechaConsulta = date('Y-m-d 00:00:00', strtotime('-90 days', strtotime($fechaConsulta)));        
        $deudaRegistrada = Deudas::where('cedula' , '=', $cedula)
                            ->where('updated_at', '<=', $fechaActual)
                            ->where('updated_at', '>=', $fechaConsulta)
                            ->where('manual_debt_add', 0)
                            ->where('origen', '=', 'templaris')
                            ->get()
                            ->first(); */
        //echo json_encode($deudaRegistrada);die();
        //if (!$deudaRegistrada) {
            $token = $this->tokenTemplaris();
            $uri = $this->templarisServiceUrl . '/templarissrl/api.php/customer_accounts/' . $cedula;

            $homologacionbancos = Homologacionbancostemplaris::get()->toArray();

            if ($token) {
                $curl = curl_init($uri);
                curl_setopt_array($curl, array(
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_CUSTOMREQUEST => "GET",
                    CURLOPT_HTTPHEADER => array(
                        "Content-Type: application/json",
                        "Authorization: Bearer {$token}"
                    ),
                ));
                $response = curl_exec($curl);
                if (curl_errno($curl)) {
                    echo 'Error:' . curl_error($curl);
                }
                curl_close($curl);
                $result = json_decode($response, true);
                //echo json_encode($result);die();
                if (isset($result['data']['result']) && !empty($result['data']['result'])) {
                    foreach ($result['data']['result'] as $cuenta) {

                        $moneda = $cuenta['currency'];
                        $account_number = $cuenta['original_account_number'];
                        //$monto_deuda = $cuenta['new_debt_amount'];
                        $monto_deuda = $cuenta['original_offer_payoff_amount'];
                        $product_type = $cuenta['product_type'];
                        $banco_deuda = $cuenta['Creditor_id'];

                        $tkl_creditor_id = 0;
                        if ($cuenta['Creditor_id']) {
                            foreach ($homologacionbancos as $homologacionbanco) {
                                if ($homologacionbanco['buro_afiliado'] == $cuenta['Creditor_id']) {
                                    $tkl_creditor_id = $homologacionbanco['tkl_creditor_id'];
                                }
                            }
                        }

                        if ($moneda == 'USA') {
                            $moneda = 'USD';
                        } elseif ($moneda == 'DOM') {
                            $moneda = 'DOP';
                        }

                        $deudaRegistrada = Deudas::where('cedula', '=', $cedula)
                                            ->where('moneda', '=', $moneda)
                                            ->where('account_number', '=', $account_number)
                                            //->where('monto_deuda', '>=', $monto_deuda)
                                            ->where('banco_deuda', '=', $banco_deuda)
                                            ->where('origen', '=', 'templaris')
                                            ->get()
                                            ->first();
                        //echo json_encode([$deudaRegistrada, $cedula, $moneda, $account_number, $monto_deuda]);die();

                        if (!$deudaRegistrada) {
                            DB::table('deudas')->insertGetId([
                                'fecha'        => date('Y-m-d'),
                                'monto_deuda'  => $monto_deuda,
                                'product_resolution_id'   => $product_type,
                                'tipo_deuda'   => $product_type,
                                'banco_deuda'  => $banco_deuda,
                                'creditor_id'  => $tkl_creditor_id,
                                 'activo'       => 0,
                                 'estatus'      => 0,
                                // 'n_cuotas'     => 0,
                                // 'm_cuota'      => 0,
                                // 'fecha_apertura' => null,
                                // 'cuentaid'     => null,
                                'account_number' => $account_number,
                                'cedula'       => $cedula,
                                'moneda'       => $moneda,
                                'manual_debt_add' => false,
                                'origen' => 'templaris',
                                'updated_at' => date('Y-m-d H:i:s'),
                                'modified' => date('Y-m-d H:i:s')
                            ]);
                        } else {
                            $deuda = Deudas::find($deudaRegistrada->id);
                            $deuda->creditor_id = $tkl_creditor_id;
                            $deuda->save();
                        }
                    }
                    return true;
                } else {
                    /* DB::table('deudas')->insertGetId([
                        'fecha'        => date('Y-m-d'),
                        'monto_deuda'  => 0,
                        'product_resolution_id'   => 0,
                        'tipo_deuda'   => 0,
                        'banco_deuda'  => 0,
                        'creditor_id'  => 0,
                        'activo'       => 1,
                        'estatus'      => 1,
                        'cedula'       => $cedula,
                        'manual_debt_add' => false,
                        'origen' => 'templaris',
                        'updated_at' => date('Y-m-d H:i:s'),
                        'modified' => date('Y-m-d H:i:s')
                    ]); */
                }
            }
        // } else {
            $deudaRegistradas = Deudas::where('cedula' , '=', $cedula)
                            ->where('origen', '=', 'templaris')
                            ->get()
                            ->toArray();
            $homologacionbancos = Homologacionbancostemplaris::get()->toArray();
            foreach ($deudaRegistradas as $deudaRegistrada) {
                $tkl_creditor_id = 0;
                if ($deudaRegistrada['banco_deuda']) {
                    foreach ($homologacionbancos as $homologacionbanco) {
                        if ($homologacionbanco['buro_afiliado'] == $deudaRegistrada['banco_deuda']) {
                            $tkl_creditor_id = $homologacionbanco['tkl_creditor_id'];
                        }
                    }
                }
                $deuda = Deudas::find($deudaRegistrada['id']);
                $deuda->creditor_id = $tkl_creditor_id;
                $deuda->save();
            }
        // }
        return false;
    }
}