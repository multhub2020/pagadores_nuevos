<?php

namespace App\Traits;

use DB;
use \App\Models\Deudas;
use \App\Models\Homologacionbancos;
use \App\Models\Usuarios;
use Exception;
use SoapClient;

trait DataCredito
{
    public $webServiceUrl = "https://testdatacredito.caltec.do/dcrservice_wsTemplaris.asmx?WSDL";
    public $webServiceUsername = "LUIS.MARTE.TEMPLARIS";
    public $webServicePassword = "LM22300914342lm";

    //public $webServiceUrl = "https://datacredito.caltec.do/dcrservice_ws.asmx?WSDL";
    //public $webServiceUsername = "PRUEBA.WEBSERVICES@DATACREDITO.INFO";
    //public $webServicePassword = "Aa123456789";
    public $webServiceTipoId = "C";

    public function getXmlToDb($idSearch = null, $tipoId = null, $usuarioid = null)
    {
        $fechaActual = date('Y-m-d 23:59:59');
        $fechaConsulta = date('Y-m-d 00:00:00');
        $fechaConsulta = date('Y-m-d 00:00:00', strtotime('-90 days', strtotime($fechaConsulta)));
        $deudaRegistrada = Deudas::where('cedula', '=', $idSearch)
            ->where('updated_at', '<=', $fechaActual)
            ->where('updated_at', '>=', $fechaConsulta)
            ->where('manual_debt_add', 0)
            ->where('origen', '=', 'datacredito')
            ->get()
            ->first();
        if (!$deudaRegistrada) {

            try {

                if ($tipoId) {
                    $this->webServiceTipoId = $tipoId;
                }

                $client = new SoapClient(
                    $this->webServiceUrl,
                    array(
                        'exceptions' => true,
                        'cache_wsdl' => WSDL_CACHE_NONE,
                        'encoding' => 'utf-8'
                    )
                );

                $params = array(
                    "Username" => $this->webServiceUsername,
                    "Password" => $this->webServicePassword,
                    "TipoId" => $this->webServiceTipoId,
                    "Id" => $idSearch,
                );


                $result = $client->GetXml($params);
                $status = $result->GetXmlResult;

                $xml = simplexml_load_string($status);
                $json  = json_encode($xml);
                $xmlArr = json_decode($json, true);

                $cedula = $xmlArr['Individuo']['CedulaNueva'];
                //$cedula = '11111111111';

                $homologacionbancos = Homologacionbancos::get()->toArray();

                if (isset($xmlArr['DesgloseCreditos']['Producto'])) {
                    foreach ($xmlArr['DesgloseCreditos']['Producto'] as $producto) {
                        /**
                         * 1 = Trajeta de Credito
                         * 2 = Prestamos
                         * 3 = Telecomunicaciones - No lo usamos
                         */
                        if (in_array($producto['@attributes']['ProductoId'], [1, 2])) {
                            foreach ($producto['Cuenta'] as $cuenta) {
                                /*$deuda = new Deudas;
                            $deuda->cedula = $cedula;
                            $deuda->monto_deuda = $cuenta['TotalAdeudado'];
                            $deuda->tipo_deuda = $producto['@attributes']['ProductoId'];
                            $deuda->banco_deuda = $cuenta['Afiliado'];
                            $deuda->identifica_banco = '';
                            $deuda->fecha = date('Y-m-d');
                            $deuda->activo = 1;
                            $deuda->estatus = 0;
                            $deuda->n_cuotas = $cuenta['TotalAtraso'];
                            $deuda->m_cuota = $cuenta['Cuota'];
                            $deuda->save();*/

                                $TotalAdeudado = $cuenta['TotalAdeudado'];
                                if (empty($TotalAdeudado)) {
                                    $TotalAdeudado = 0;
                                }

                                $TotalAtraso = $cuenta['TotalAtraso'];
                                if (empty($TotalAtraso)) {
                                    $TotalAtraso = 0;
                                }

                                $Cuota = $cuenta['Cuota'];
                                if (empty($Cuota)) {
                                    $Cuota = 0;
                                }

                                $EstatusUltimo = $cuenta['EstatusUltimo'];
                                if (empty($EstatusUltimo)) {
                                    $EstatusUltimo = 0;
                                }

                                $tkl_creditor_id = 0;
                                if ($cuenta['Afiliado']) {
                                    foreach ($homologacionbancos as $homologacionbanco) {
                                        if ($homologacionbanco['buro_afiliado'] == $cuenta['Afiliado']) {
                                            $tkl_creditor_id = $homologacionbanco['tkl_creditor_id'];
                                        }
                                    }
                                }

                                $fechaApertura = '01-' . $cuenta['FechaApertura'];
                                $fechaApertura = date('Y-m-d', strtotime($fechaApertura));

                                if ($TotalAdeudado && $cuenta['Estatus'] == 1 && $EstatusUltimo == 'CASTIGADA') {
                                    $deudaRegistrada = Deudas::where('cedula', '=', $cedula)
                                        ->where('fecha_apertura', '=', $fechaApertura)
                                        ->where('cuentaid', '=', $cuenta['@attributes']['CuentaId'])
                                        ->where('origen', '=', 'datacredito')
                                        ->get()
                                        ->first();

                                    $moneda = $cuenta['Moneda'];
                                    if ($moneda == 'US') {
                                        $moneda = 'USD';
                                    } elseif ($moneda == 'RD') {
                                        $moneda = 'DOP';
                                    }

                                    if (!$deudaRegistrada) {
                                        DB::table('deudas')->insertGetId([
                                            'fecha'        => date('Y-m-d'),
                                            'monto_deuda'  => $TotalAdeudado,
                                            'product_resolution_id'   => $producto['@attributes']['ProductoId'],
                                            'tipo_deuda'   => $producto['@attributes']['ProductoId'],
                                            'banco_deuda'  => $cuenta['Afiliado'],
                                            'creditor_id'  => $tkl_creditor_id,
                                            'activo'       => 0,
                                            'estatus'      => 0,
                                            'n_cuotas'     => $TotalAtraso,
                                            'm_cuota'      => $Cuota,
                                            'cedula'       => $cedula,
                                            'fecha_apertura' => $fechaApertura,
                                            'cuentaid'     => $cuenta['@attributes']['CuentaId'],
                                            'moneda'       => $moneda,
                                            'manual_debt_add' => false,
                                            'origen' => 'datacredito',
                                            'updated_at' => date('Y-m-d H:i:s'),
                                            'modified' => date('Y-m-d H:i:s')
                                        ]);
                                    } else {
                                        $deuda = Deudas::find($deudaRegistrada->id);
                                        $deuda->creditor_id = $tkl_creditor_id;
                                        $deuda->save();
                                    }
                                }
                            }
                        }
                    }
                } else {
                    DB::table('deudas')->insertGetId([
                        'fecha'        => date('Y-m-d'),
                        'monto_deuda'  => 0,
                        'product_resolution_id'   => 0,
                        'tipo_deuda'   => 0,
                        'banco_deuda'  => 0,
                        'creditor_id'  => 0,
                        'activo'       => 1,
                        'estatus'      => 1,
                        'cedula'       => $cedula,
                        'manual_debt_add' => false,
                        'origen' => 'datacredito',
                        'updated_at' => date('Y-m-d H:i:s'),
                        'modified' => date('Y-m-d H:i:s')
                    ]);
                }

                if (isset($xmlArr['Individuo']) && $usuarioid) {
                    $individuo = $xmlArr['Individuo'];

                    $sexo = $individuo['Sexo'];
                    if (empty($sexo)) {
                        $sexo = '';
                    }

                    $nacionalidad = $individuo['Nacionalidad'];
                    if (empty($nacionalidad)) {
                        $nacionalidad = '';
                    }

                    $estadoCivil = $individuo['EstadoCivil'];
                    if (empty($estadoCivil)) {
                        $estadoCivil = '';
                    }

                    $lugarNacimiento = $individuo['LugarNacimiento'];
                    if (empty($lugarNacimiento)) {
                        $lugarNacimiento = '';
                    }

                    $fechaNacimiento = $individuo['FechaNacimiento'];
                    if (empty($fechaNacimiento)) {
                        $fechaNacimiento = '';
                    }
                    $fechaNacimiento = date('Y-m-d', strtotime($fechaNacimiento));

                    DB::table('usuarios')->insertGetId([
                        'sexo'  => $sexo,
                        'nacionalidad'  => $nacionalidad,
                        'estadocivil'   => $estadoCivil,
                        'lugarnacimiento'   => $lugarNacimiento,
                        'fechanacimiento' => $fechaNacimiento
                    ]);
                    $usuario = Usuarios::find($usuarioid);
                    $usuario->sexo = $sexo;
                    $usuario->nacionalidad = $nacionalidad;
                    $usuario->estadocivil = $estadoCivil;
                    $usuario->lugarnacimiento = $lugarNacimiento;
                    $usuario->save();
                }
            } catch (Exception $e) {
                //return $e->getMessage();
                return false;
            }
        }

        return true;
    }
}
