<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use \Illuminate\Support\Facades\Config;

class EmailSend extends Mailable
{
    use Queueable, SerializesModels;

    public $subject = 'Comprobante de Pago';
    public $msg;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($msg)
    {
        $this->msg = $msg;

        $driver = 'smtp';
        $host = 'smtp.gmail.com';
        $puerto = '587';
        $email = 'servicio@pagadores.com';
        $password = 'Paga2021';
        
        Config::set('mail.driver', $driver);
        Config::set('mail.host', $host);
        Config::set('mail.username', $email);
        Config::set('mail.password', $password);
        Config::set('mail.port', $puerto);
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.comprobantes')
                    ->attach(storage_path('imagenes') . '/' . $this->msg['imagen']);
    }
}
