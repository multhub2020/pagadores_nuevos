<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class StatusTkl extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'status:tkl';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Actualiza el status de tkl en la tabla de deudas';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $deudas = \DB::table('deudas')
            ->select(['id', 'aplicacionid'])
            ->where('aplicacionid', '>', 0)
            ->get();
        //echo json_encode($deudas);

        $url = "http://23.96.19.56/";
        $app_env = env('APP_ENV', 'production');
        if($app_env == 'production'){
            $url = "http://40.114.75.243/";
        }

        $headers = [
            'Content-Type: application/json',
            'Accept: application/json',
        ];
        $data = [
            "grant_type" => "client_credentials",
            "scope" => "*",
            "client_secret" => "UOH8qdFoWgIY28mBf5RQi7xj7kXC0l622jz8tMzz",
            "client_id" => "2",
        ];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url . 'oauth/token');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_FAILONERROR, TRUE);
        $result = curl_exec($ch);
        curl_close($ch);

        $result = json_decode($result, true);
        if (isset($result['access_token'])) {
            $token = $result['access_token'];

            $headers = [
                'Content-Type: application/json',
                'Accept: application/json',
                'Authorization: Bearer ' . $token,
            ];

            foreach ($deudas as $deuda) {
                try {
                    $ch2 = curl_init();
                    curl_setopt($ch2, CURLOPT_URL,  $url . 'api/applications/' . $deuda->aplicacionid);
                    curl_setopt($ch2, CURLOPT_HTTPHEADER, $headers);
                    curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
                    $result2 = json_decode(curl_exec($ch2), true);
                    curl_close($ch2);

                    if(isset($result2['data']['status'])){
                        $status_tkl = $result2['data']['status'];

                        $thisDeuda = \DB::table('deudas')
                        ->where('id', $deuda->id)
                        ->update(['status_tkl' => $status_tkl]);
                    }
                } catch (\Exception $e) {
                    echo 'Error al actualizar el status de la aplicacion ' . $deuda->aplicacionid;
                }
                
            }
        } else {
            echo 'Error al generar el token';
        }
    }
}
