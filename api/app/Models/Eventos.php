<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Eventos extends Model
{
	protected $table= 'eventos';

	
	public function respuestas(){
	    return $this->hasMany('App\Models\Eventosrespuestas','evento_id');
	}

	public function usuarios(){
	    return $this->hasMany('App\Models\Eventosusuarios','evento_id');
	}

}
