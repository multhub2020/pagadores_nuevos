<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Encuentasrespuestas extends Model
{
	protected $table= 'encuentasrespuestas';

	public function encuenta(){
	    return $this->belongsTo('App\Models\Encuentas','encuenta_id');
	}

}
