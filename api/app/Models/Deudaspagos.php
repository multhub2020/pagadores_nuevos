<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Deudaspagos extends Model
{
	protected $table= 'deudaspagos';
	
	public function deudas(){
	    return $this->hasMany('App\Models\Deudas','deuda_id');
	}



}
