<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Eventosusuarios extends Model
{
	protected $table= 'eventosusuarios';

	public function respuestas(){
	    return $this->belongsTo('App\Models\Eventosrespuestas','eventosrespuesta_id');
	}

	public function evento(){
	    return $this->belongsTo('App\Models\Eventos','evento_id');
	}

	public function usuarios(){
	    return $this->belongsTo('App\Models\Usuarios','usuario_id');
	}

}
