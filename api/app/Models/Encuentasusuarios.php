<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Encuentasusuarios extends Model
{
	protected $table= 'encuentasusuarios';

	public function respuestas(){
	    return $this->belongsTo('App\Models\Encuentasrespuestas','encuentasrespuesta_id');
	}

	public function encuesta(){
	    return $this->belongsTo('App\Models\Encuentas','encuenta_id');
	}

	public function usuarios(){
	    return $this->belongsTo('App\Models\Usuarios','usuario_id');
	}

}
