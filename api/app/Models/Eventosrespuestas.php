<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Eventosrespuestas extends Model
{
	protected $table= 'eventosrespuestas';

	public function evento(){
	    return $this->belongsTo('App\Models\Eventos','evento_id');
	}

}
