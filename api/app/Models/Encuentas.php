<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Encuentas extends Model
{
	protected $table= 'encuentas';

	
	public function respuestas(){
	    return $this->hasMany('App\Models\Encuentasrespuestas','encuenta_id');
	}

	public function usuarios(){
	    return $this->hasMany('App\Models\Encuentasusuarios','encuenta_id');
	}

}
