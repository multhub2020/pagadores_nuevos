<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Models\Usuarios;
use \App\Models\Logerrores;
use \App\module;
use \App\Models\Parametros;


use DB;
use Mail;


use App\Http\Controllers\Controller;
use JWTAuth;

class PerfilController extends Controller
{
    
    public function logerroresadd(Request $request){
        $data = $request->json()->all();
        $id_user  = DB::table('logerrores')->insertGetId(['fecha'                => date('Y-m-d'),
                                                          'created'              => date('Y-m-d'),
                                                           'ruta'                => $data['ruta'],
                                                           'error'               => $data['error'],
                                                           ]
                                                        );
        return response()->json([
                              'msg'=>'Nosotros',
                              'id'=>$id_user,
                              'code'=>200                        
                            ],200);
    }




    public function sobrenosotros(Request $request){
        $data = $request->json()->all();
        return response()->json([
                              'msg'=>'Nosotros',
                              'code'=>200                        
                            ],200);
    }
     /*
      *
      *   FUncTION BASE IMAGEN
    */
    public function reducir($foto="", $WIDTH=350, $HEIGHT=350, $QUALITY=100 ) {
          if($foto!=""){
              $foto2 = str_replace("data:image/jpeg;base64,", "", $foto);
              $theme_image_little    = @imagecreatefromstring(base64_decode($foto2));
              if($theme_image_little !== false) {
                    $org_w = imagesx($theme_image_little);
                    $org_h = imagesy($theme_image_little);
                    $image_little           = imagecreatetruecolor($WIDTH, $HEIGHT);
                    imageinterlace($theme_image_little, true);
                    imageinterlace($image_little, true);
                    imagecopyresampled($image_little, $theme_image_little, 0, 0, 0, 0, $WIDTH, $HEIGHT, $org_w, $org_h);
                    ob_start();
                    imagejpeg($image_little, null, $QUALITY);
                    $contents =  ob_get_contents();
                    ob_end_clean();
              }else{
                 return "";
              }
              return "data:image/jpeg;base64,".base64_encode($contents);
          }else{
              return "";
          }
          //return base64_encode($contents);
    }
    public function base64_to_png( $base64_string, $output_file ) {
          $foto = str_replace("data:image/jpeg;base64,", "", $base64_string);
          $ifp  = fopen( $output_file, "wb" ); 
          fwrite( $ifp, base64_decode( $foto) ); 
          fclose( $ifp ); 
          return($output_file);
          //file_put_contents($output_file,base64_decode($base64_string));
    }
    public function miperfilfotoupdate(Request $request){
        $data = $request->json()->all();
        if(isset($data['imagenes'][0])){
            $cantidad_caracteres = strlen($data['imagenes'][0]);
            if($cantidad_caracteres>150){
                $img_reducir1 = $this->reducir($data['imagenes'][0], 1200, 1200, 100);
                $imgpath1  = storage_path('imagenes')."/".$data['usuarioid'].date('Y-m-d-H-i-s')."_1.jpg";
                $imgpath1_ = env('APP_URL_IMAGEN')."/".$data['usuarioid'].date('Y-m-d-H-i-s')."_1.jpg";
                $this->base64_to_png($img_reducir1, $imgpath1);

                $img_reducir2 = $this->reducir($data['imagenes'][0], 250, 250, 100);
                $imgpath2  = storage_path('imagenes')."/".$data['usuarioid'].date('Y-m-d-H-i-s')."_2.jpg";
                $imgpath2_ = env('APP_URL_IMAGEN')."/".$data['usuarioid'].date('Y-m-d-H-i-s')."_2.jpg";
                $this->base64_to_png($img_reducir2, $imgpath2);

                $thum1        = $this->reducir($data['imagenes'][0], 30, 30, 100);
                $thumbnail1_  = storage_path('imagenes')."/".$data['usuarioid'].date('Y-m-d-H-i-s')."th_1.jpg";
                $thumbnail1   = env('APP_URL_IMAGEN')."/".$data['usuarioid'].date('Y-m-d-H-i-s')."th_1.jpg";
                $this->base64_to_png($thum1, $thumbnail1_);
            }else{
                $theme_image_enc_little = "";
                $imgpath1_ = "";
                $imgpath2_ = "";
            }
        }else{
            $theme_image_enc_little = "";
            $imgpath1_ = "";
            $imgpath2_ = "";
        }
        $datos = Usuarios::find($data['usuarioid']);
        if($imgpath1_!=""){
            $datos->foto             = $imgpath1_;
            $datos->foto2            = $imgpath2_;
        }
        $datos->save();
        $datos_new = Usuarios::find($data['usuarioid']);
        return response()->json([
            'datos' =>$datos_new,
            'code'=>200                   
        ],200);
    }//fin function

    public function listarperfil(Request $request){
        $data = $request->json()->all();
        $seguidores  = 0;
        $invitados   = 0;
        $color       = "gold";
        $avatar      = "./assets/icon/avatar.png";
        $datos_usuarios      = Usuarios::where('id', $data['usuarioid'])->get();  
        $invitados           = Usuarios::where('userinvitador_id',   $data['usuarioid'])->count(); 
        $invitados_aceptados = Usuarios::where('userinvitador_id',   $data['usuarioid'])->where('invitado',2)->count(); 
        //CONTAR SCORE
        $seguidores_data  = Usuarios::select('id', 'userinvitador_id', 'genetica')->where('invitado',2)->get(); 
        foreach($seguidores_data  as $key1) {
            $porciones = explode("-", $key1['genetica']);
            foreach ($porciones as $key2) {
                if($data['usuarioid']==$key2){
                    $seguidores++;
                }
            }
        }//fin find1
        //FIN CONTAR SCORE
        if($invitados==0){
            $porcentaje = 0;    
        }else{
            $porcentaje = ($invitados_aceptados*100)/$invitados;    
        }
        
        /*
            Bronce    0-50 referidos registrados, 
            Plata   51-200 referidos registradados   
            Oro  201 - 500 Referidos registrados     
            Platinium 501+  referidos registrados
        */
              if($seguidores<=50){
                    $color = 1;
        }else if($seguidores<=200){
                    $color = 2;
        }else if($seguidores<=500){
                    $color = 3;
        }else if($seguidores>=501){
                    $color = 4;
        }
        return response()->json([
            'datos_usuarios'=>$datos_usuarios,
            'seguidores' => $seguidores,
            'invitados'  => $invitados,
            'color'      => $color,
            'avatar'     => $avatar,
            'porcentaje' => $porcentaje,
            'code'=>200                      
        ],200);
    }//fin function

    public function guardarperfil(Request $request){
    $data   = $request->json()->all();
    $datos1 = Usuarios::find($data['usuarioid']);
    $datos1->nombres          = $data['nombre'];
    $datos1->apellidos        = $data['apellido'];
    $datos1->nombre_apellido  = $data['nombre']." ".$data['apellido'];
    $datos1->save();
    $datos_usuarios = Usuarios::where('id', $data['usuarioid'])->get();  
    return response()->json([
                              'msg'=>'Los datos fueron actualizados',
                              'datos_usuarios'=>$datos_usuarios,
                              'code'=>200                        
                            ],200);
  }//fin function




  public function perfileditar(Request $request){
        $data = $request->json()->all();
        $datos_usuarios  = Usuarios::where('id', $data['usuarioid'])->get();  
        return response()->json([
            'datos_usuarios'=>$datos_usuarios,
            'code'=>200                      
        ],200);
    }//fin function



    public function perfileditarupdate(Request $request){
        $data = $request->json()->all();
        $datos1 = Usuarios::find($data['usuarioid']);
        $datos1->email     = $data['email'];
        $datos1->nombres   = $data['nombres'];
        $datos1->apellidos = $data['apellidos'];
        $datos1->nombre_apellido  = $data['nombres']." ".$data['apellidos'];
        ///////////////////////////////////////////////////////////////////////////////////
        //$datos_aux = Usuarios::find($datos->id);
        /*$data_array = null;
        $headers = [
            'Content-Type: application/json'
        ];
        $data_array = [
                "first_name": $data['nombres'],
                "last_name":  $data['apellidos'],
                "email":      $data['email']
        ];      
        $ch = curl_init();
        curl_setopt( $ch, CURLOPT_URL, 'http://40.114.75.243/api/customers/'.$datos1->customer_id);
        curl_setopt( $ch, CURLOPT_POST, true );
        curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers );
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $data_array ) );
        curl_setopt($ch,  CURLOPT_FAILONERROR, TRUE);
        $result_api    = curl_exec($ch);
        $respuesta_api = json_decode($result_api, true);
        curl_close( $ch );*/
        ///////////////////////////////////////////////////////////////////////////////////
        $datos1->save();
        $datos_usuarios  = Usuarios::where('id', $data['usuarioid'])->get();  
        return response()->json([
                                    'msg'   => 'Los datos fueron guardados',
                                    'datos_usuarios'=>$datos_usuarios,
                                    'code'  => 200
                                ],200);
    }//fin function


 
 

}//Fin class
