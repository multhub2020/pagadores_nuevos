<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Models\Usuarios;
use \App\Models\Depositos;
use \App\module;

use DB;
use Mail;

// Update the path below to your autoload.php,
// see https://getcomposer.org/doc/01-basic-usage.md
//require_once '/path/to/vendor/autoload.php';

use Twilio\Rest\Client;



use App\Http\Controllers\Controller;
use JWTAuth;

class DepositosController extends Controller
{
    public function agregar(Request $request)
    {
        $data = $request->json()->all();

        $usuario = Usuarios::find($data['usuarioid']);

        $imgpath1 = null;
        $imgpath1_ = null;
        if (isset($data['imagen'])) {
            $cantidad_caracteres = strlen($data['imagen']);
            if ($cantidad_caracteres > 150) {
                $img_reducir1 = $this->reducir($data['imagen'], 1200, 1200, 100);
                $imgpath1  = storage_path('imagenes') . "/" . date('Y-m-d-H-i-s') . "_1.jpg";
                $imgpath1_ = env('APP_URL_IMAGEN') . "/" . date('Y-m-d-H-i-s') . "_1.jpg";
                $this->base64_to_png($img_reducir1, $imgpath1);

                $img_reducir2 = $this->reducir($data['imagen'], 250, 250, 100);
                $imgpath2  = storage_path('imagenes') . "/" . date('Y-m-d-H-i-s') . "_2.jpg";
                $imgpath2_ = env('APP_URL_IMAGEN') . "/" . date('Y-m-d-H-i-s') . "_2.jpg";
                $this->base64_to_png($img_reducir2, $imgpath2);

                $thum1        = $this->reducir($data['imagen'], 30, 30, 100);
                $thumbnail1_  = storage_path('imagenes') . "/" . date('Y-m-d-H-i-s') . "th_1.jpg";
                $thumbnail1   = env('APP_URL_IMAGEN') . "/" . date('Y-m-d-H-i-s') . "th_1.jpg";
                $this->base64_to_png($thum1, $thumbnail1_);
            } else {
                $theme_image_enc_little = "";
                $imgpath1_ = "";
                $imgpath2_ = "";
            }
        } else {
            $theme_image_enc_little = "";
            $imgpath1_ = "";
            $imgpath2_ = "";
        }

        $deposito = new Depositos;
        $deposito->cedula = $usuario->ci;
        $deposito->imagen = $imgpath1_;
        $deposito->numero_transaccion = $data['numero_transaccion'];
        $deposito->banco = $data['banco'];
        $deposito->monto = $data['monto'];
        $deposito->customer_id = $data['customer_id'];
        $deposito->application_id = $data['application_id'];
        if ($deposito->save()) {
            $dataEmail = [
                'cedula' => $usuario->ci,
                'nombre' => $usuario->nombre_apellido,
                'email' => $usuario->email,
                'numero_transaccion' => $data['numero_transaccion'],
                'banco' => $data['banco'],
                'monto' => $data['monto'],
                'customer_id' => $data['customer_id'],
                'application_id' => $data['application_id'],
                'imagen' => $imgpath1
            ];
            //Mail::to('comprobantes@pagadores.com')->send(new EmailSend($dataEmail));

            $subject = 'Comprobante de Pago';
            $email   = 'comprobantes@pagadores.com';
            //$imgpath1 = storage_path('imagenes') . "/Seleccion_001.png";
            $imagen  = $imgpath1;
            Mail::send('emails.comprobantes', $dataEmail, function ($message) use ($subject, $email, $imagen) {
                $message->subject($subject);
                $message->to($email);
                if($imagen){
                    $message->attach($imagen);
                }
            });

            return response()->json([
                'error' => false,
                'data' => $deposito,
                'code' => 200
            ], 200);
        }
        return response()->json([
            'error' => true,
            'data' => [],
            'code' => 201
        ], 200);
    }

    public function reducir($foto = "", $WIDTH = 350, $HEIGHT = 350, $QUALITY = 100)
    {
        if ($foto != "") {
            $foto2 = str_replace("data:image/jpeg;base64,", "", $foto);
            $theme_image_little    = @imagecreatefromstring(base64_decode($foto2));
            if ($theme_image_little !== false) {
                $org_w = imagesx($theme_image_little);
                $org_h = imagesy($theme_image_little);
                $image_little           = imagecreatetruecolor($WIDTH, $HEIGHT);
                imageinterlace($theme_image_little, true);
                imageinterlace($image_little, true);
                imagecopyresampled($image_little, $theme_image_little, 0, 0, 0, 0, $WIDTH, $HEIGHT, $org_w, $org_h);
                ob_start();
                imagejpeg($image_little, null, $QUALITY);
                $contents =  ob_get_contents();
                ob_end_clean();
            } else {
                return "";
            }
            return "data:image/jpeg;base64," . base64_encode($contents);
        } else {
            return "";
        }
        //return base64_encode($contents);
    }

    public function base64_to_png($base64_string, $output_file)
    {
        $foto = str_replace("data:image/jpeg;base64,", "", $base64_string);
        $ifp  = fopen($output_file, "wb");
        fwrite($ifp, base64_decode($foto));
        fclose($ifp);
        return ($output_file);
        //file_put_contents($output_file,base64_decode($base64_string));
    }
}