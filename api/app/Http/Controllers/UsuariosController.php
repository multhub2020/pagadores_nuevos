<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Models\Usuarios;
use \App\Models\Tokenpush;
use \App\module;
use \App\Models\Parametros;

use DB;
use Mail;

use Twilio\Rest\Client;

use App\Http\Controllers\Controller;
use Exception;
use JWTAuth;
use App\Traits\CreadoLab;

class UsuariosController extends Controller
{
    use CreadoLab;

    public function verificaregistro2(Request $request){
        $data   = $request->json()->all();
        $cedula = str_replace("-","",$data['cedula']);
        $nombresapellidos = trim($data['nombresapellidos']);
        $code   = 200;
        $msg    = "";
        if(!preg_match("/^[0-9]{11}$/", $cedula)){
            $code = 201;
            $msg = "Formato de cédula incorrecto";
        }else if(strlen($nombresapellidos)<10){
            $code = 201;
            $msg = "Faltan caracteres para el nombre";
         }else if(!preg_match("/^[ a-zA-Z]+$/", $nombresapellidos)){
            $code = 201;
            $msg = "El nombre solo debe tener letras ";
        }
        return response()->json([
            'msg'=>$msg,
            'code'=>$code                        
        ],200);
    }
    public function loginclave(Request $request){
        $data  = $request->json()->all();
        $token = "";
        if(filter_var($data['usuario'], FILTER_VALIDATE_EMAIL)) {
            $msg  = "formato valido de teléfono / email.";
            $code = 200;
        }else{
            $msg  = "formato invalido de teléfono / email.";
            $code = 201;
            if(preg_match("/^[0-9]{10}$/",  $data['usuario'])){
                $msg  = "formato valido de teléfono / email.";
                $code = 200;
            } //check for a pattern of 91-0123456789 
        }
        if($code!=200){
                return response()->json([
                        'msg'=>$msg,
                        'code'=>201                        
                    ],200);
        }else{
                if($data['tipo']==1){
                    $cantidad1 = Usuarios::where('invitado',2)->where('clave',md5($data['clave']) )->where('email',   $data['usuario'])->count(); 
                    $cantidad2 = Usuarios::where('invitado',2)->where('clave',md5($data['clave']) )->where('telefono',$data['usuario'])->count();         
                }else{
                    $cantidad1 = Usuarios::where('invitado',2)->where('email',   $data['usuario'])->count(); 
                    $cantidad2 = Usuarios::where('invitado',2)->where('telefono',$data['usuario'])->count();  
                }
                if($cantidad1 == 0 && $cantidad2 == 0){
                    return response()->json([
                        'msg'=>'Clave incorrecta. ',
                        'code'=>201
                    ],200);
                }else if($cantidad1!=0){
                    if($data['tipo']==1){
                        $usr = Usuarios::where('invitado',2)->where('clave',md5($data['clave']) )->where('email',   $data['usuario'])->first(); 
                    }else{
                        $usr = Usuarios::where('invitado',2)->where('email',   $data['usuario'])->first(); 
                    }
                    return response()->json([
                        'datos'=>$usr->toArray(),
                        'token'=>$token, 
                        'code'=>200                        
                    ],200);
                }else{
                    if($data['tipo']==1){
                        $usr = Usuarios::where('invitado',2)->where('clave',md5($data['clave']) )->where('telefono',   $data['usuario'])->first(); 
                    }else{
                        $usr = Usuarios::where('invitado',2)->where('telefono',   $data['usuario'])->first(); 
                    }
                    return response()->json([
                        'datos'=>$usr->toArray(),
                        'token'=>$token, 
                        'code'=>200                        
                    ],200);
                }
        }//fin code
    }//fin function

    public function loginregistrocedula(Request $request){
        $data   = $request->json()->all();
        $datos1 = Usuarios::find($data['usuarioid']);
        $datos1->ci = str_replace("-","",$data['cedula']);
        $datos1->customer_id = $data['customerid'];
        $datos1->invitado = 2;
        $datos1->save();
        $usr = Usuarios::where('id',$data['usuarioid'])->first(); 
        return response()->json([
                                    'msg'   => 'Los datos fueron guardados',
                                    'code'  => 200,
                                    'datos'=>$usr->toArray(),
                                ],200);
    }//fin function


    public function actualizarpush(Request $request){
        $data = $request->json()->all();
        $datos1 = Usuarios::find($data['u_push']);
        $datos1->push_token = $data['t_push']!=""?$data['t_push']:0;
        $datos1->push_platf = $data['p_push']!=""?$data['p_push']:0;
        $datos1->save();
        return response()->json([
                                'code'=>200                        
                              ],200);
    }//fin function
    public function actualizarpush_pull(Request $request){
        $data = $request->json()->all();
        $cantidad1 = Tokenpush::where('token_push',$data['t_push'])->count(); 
        if($cantidad1==0){
                    $id_user =  DB::table('tokenpush')->insertGetId(['token_push' => $data['t_push'],
                                                                     'platf_push' => $data['p_push'],
                                                                    ]
                                                                   );

        }else{
                    $datos  = Tokenpush::where('token_push',$data['t_push'])->first(); 
                    $datos1 = Tokenpush::find($datos->id);
                    $datos1->token_push = $data['t_push']!=""?$data['t_push']:0;
                    $datos1->platf_push = $data['p_push']!=""?$data['p_push']:0;
                    $datos1->save();
        }
        return response()->json([
                                'code'=>200                        
                              ],200);
    }//fin function
    public function verificar(Request $request){
        $data = $request->json()->all();
         if(!filter_var($data['usuario'], FILTER_VALIDATE_EMAIL)) {
            return response()->json([
                'msg'   => "formato no es valido de email.",
                'code'  => 201
            ],200);
        }else if(!preg_match("/^[0-9]{10}$/",  $data['celular'])){
            return response()->json([
                'msg'   => "formato de celular no es valido.",
                'code'  => 202
            ],200);
        }else{
                $cantidad1 = Usuarios::where('invitado', 2)->where('email',   $data['usuario'])->count(); 
                if($cantidad1 == 0){
                        $cantidad1 = Usuarios::where('invitado', 1)->where('email',   $data['usuario'])->count(); 
                        if($cantidad1==0){
                                $id_user  = DB::table('usuarios')->insertGetId(['fechaingreso'        => date('Y-m-d'),
                                                                                'role_id'             => 2,
                                                                                'email'               => $data['usuario'],
                                                                                'telefono'            => $data['celular'],
                                                                                'userinvitador_id'    => '0',
                                                                                'genetica'            => $data['usuario']
                                                                               ]
                                                                            );
                        }else{
                               $datos   = Usuarios::where('invitado', 1)->where('email',   $data['usuario'])->get()->toArray(); 
                               $id_user = $datos[0]['id'];
                        }
                        return response()->json([
                            'code'=>200,
                            'usuarioid'=>$id_user
                        ],200);
                }else{
                        return response()->json([
                            'msg'=>"El usuario ya se encuentra registrado ",
                            'code'=>201
                        ],200);
                }
        }
    }
    public function verificarcel(Request $request){
        $data = $request->json()->all();
          if(!preg_match("/^[0-9]{10}$/",  $data['celular'])){
            return response()->json([
                'msg'   => "formato de celular no es valido.",
                'code'  => 201
            ],200);
        }else{
                return response()->json([
                    'code'=>200
                ],200);
        }
    }
    public function loginregistro(Request $request){
        $data = $request->json()->all();
        if(!preg_match("/^[0-9]{10}$/",  $data['celular']) && $data['celular']!=""){
                return response()->json([
                    'msg'   => "formato no es valido de celular.",
                    'code'  => 201
                ],200);
        }else{
                ///////////////////////////////////////////////////////////////////////////////////
                $datos = Usuarios::where('id', $data['usuarioid'])->first(); 
                //$datos_aux = Usuarios::find($datos->id);
                /*$data_array = null;
                $headers = [
                    'Content-Type: application/json'
                ];
                $data_array = [
                         "national_id": str_replace("-","",$data['cedula']),
                        "first_name": $data['nombres'],
                        "last_name": $data['apellidos'],
                         "email":  $datos->email,
                         "cellphone": $data['celular'].,
                         "country": "DOM",
                ];      
                $ch = curl_init();
                curl_setopt( $ch, CURLOPT_URL, 'http://40.114.75.243/api/customers' );
                curl_setopt( $ch, CURLOPT_POST, true );
                curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers );
                curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
                curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
                curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, "POST");
                curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $data_array ) );
                curl_setopt($ch,  CURLOPT_FAILONERROR, TRUE);
                $result_api    = curl_exec($ch);
                $respuesta_api = json_decode($result_api, true);
                curl_close( $ch );*/
                ///////////////////////////////////////////////////////////////////////////////////

                //$datos = Usuarios::where('id', $data['usuarioid'])->first(); 
                $codigo_veri_a    = $datos->id*(date('d')*date('m')*date('i'));
                $codigo_veri_a   .= "123456789";
                $codigo_veri      = "";
                $charactersLength = strlen($codigo_veri_a);
                for ($i = 0; $i < 4; $i++) {
                    $codigo_veri .= $codigo_veri_a[rand(0, $charactersLength-1)];
                }
                //AQUI enviar codigo al sms
                // Send an SMS using Twilio's REST API and PHP
                
                

                        //$sid     = "AC8a3c3fdc6f27ea2df70a41548219847b";//"AC8a3c3fdc6f27ea2df70a41548219847b"; // Your Account SID from www.twilio.com/console
                        //$token   = "2c71c43f96b525676e8ee9705d23334a";//"2c71c43f96b525676e8ee9705d23334a"; // Your Auth Token from www.twilio.com/console

                        $sid       = "AC6b675d394ea0f83b871e53b7d69fb067";
                        $token     = "926772c541329498da0b780aa0725c1f";  
                        $telefono2 = "+13126464282";

                        $msg     = "El SMS de la invitación fue enviado";
                        $client  = new Client($sid, $token);
                        $mensaje = "Estimado/a ".$data['celular'].", ingrese el siguiente código de verificación para validar su cuenta: ".$codigo_veri;
                        $message = $client->messages->create(
                                "+1".$data['celular'], // Text this number
                            [
                                'from' => $telefono2, // From a valid Twilio number
                                'body' => $mensaje
                            ]
                        );

                
                if($data['cedula']==""){$data['cedula']='1';}
                $registroid = md5($data['clave']).md5($data['celular']);
                $datos1 = Usuarios::find($datos->id);
                $datos1->clave = md5($data['clave']);
                //$datos1->customer_id = $respuesta_api['data']['results']['id'];
                //$datos1->customer_id = $data['customer_id'];
                /////////// COMENTAR AL QUITAR MENSAJES
                //$datos1->invitado = 2;
                /////////
                $datos1->fechaaceptacion = date('Y-m-d');
                $datos1->ci = str_replace("-","",$data['cedula']);
                $datos1->telefono = $data['celular'];
                $datos1->nombres   = $data['nombres'];
                $datos1->apellidos = $data['apellidos'];
                $datos1->nombre_apellido = $data['nombres']." ".$data['apellidos'];
                $datos1->registrotipo = isset($data['registrotipo'])?$data['registrotipo']:'phone';
                $datos1->role_id = 2;
                $datos1->verificado = 2;
                $datos1->recuperar = 0;
                $datos1->codigo = $codigo_veri;
                $datos1->registroid = $registroid;
                $datos1->save();
                $datos2 = Usuarios::find($datos->id);
                return response()->json([
                                            'msg'   => 'Los datos fueron guardados',
                                            'code'  => 200,
                                            'datos' => $datos2
                                        ],200);
        }//else
    }//fin function

    
    
    public function loginregistrocodigore(Request $request){
        $data = $request->json()->all();
        $cantidad1 = 0;
        $datos = Usuarios::where('id', $data['usuarioid'])->first(); 
        //$datos = Usuarios::where('invitado',1)->where('id', $data['usuarioid'])->first(); 
        $codigo_veri_a    = $datos->id*(date('d')*date('m')*date('i'));
        $codigo_veri_a   .= "123456789";
        $codigo_veri      = "";
        $charactersLength = strlen($codigo_veri_a);
        for ($i = 0; $i < 4; $i++) {
            $codigo_veri .= $codigo_veri_a[rand(0, $charactersLength-1)];
        }
        //AQUI enviar codigo al sms
        // Send an SMS using Twilio's REST API and PHP
        
        //$sid     = "AC8a3c3fdc6f27ea2df70a41548219847b";//"AC8a3c3fdc6f27ea2df70a41548219847b"; // Your Account SID from www.twilio.com/console
        //$token   = "2c71c43f96b525676e8ee9705d23334a";//"2c71c43f96b525676e8ee9705d23334a"; // Your Auth Token from www.twilio.com/console

        $sid       = "AC6b675d394ea0f83b871e53b7d69fb067";
        $token     = "926772c541329498da0b780aa0725c1f";  
        $telefono2 = "+13126464282";

        $client  = new Client($sid, $token);
        $mensaje = "Estimado/a ".$data['celular'].", ingrese el siguiente código de verificación para validar su cuenta: ".$codigo_veri;
        $message = $client->messages->create(
                "+1".$data['celular'], // Text this number
            [
                'from' => $telefono2, // From a valid Twilio number
                'body' => $mensaje
            ]
        );
        $datos1 = Usuarios::find($datos->id);
        $datos1->codigo = $codigo_veri;
        $datos1->save();    
        return response()->json([
                                    'msg'   => "El'código fue reenviado",
                                    'code'  => 200,
                                ],200);
    }//fin function
    public function loginregistrocodigo(Request $request){
        $data = $request->json()->all();
        $cantidad  = Usuarios::where('invitado',1)->where('id',$data['usuarioid'])->where('codigo',$data['codigo'])->count();         
        if($cantidad==0){
            $msj =  'El código es invalido';
            $codigo = 201;
        }else{
            $msj =  'Los datos fueron guardados';
            $datos1 = Usuarios::find($data['usuarioid']);
            //$datos1->invitado = 2;
            $datos1->codigo   = "";
            $datos1->save();
            $codigo = 200;
        }
        return response()->json([
                                    'msg'   => $msj,
                                    'code'  => $codigo
                                ],200);
    }//fin function
    public function loginrecuperar1(Request $request){
        $data = $request->json()->all();
        if(filter_var($data['usuario'], FILTER_VALIDATE_EMAIL)) {
            $msg  = "formato valido de teléfono / email.";
            $code = 200;
        }else{
            $msg  = "formato invalido de teléfono / email.";
            $code = 201;
            if(preg_match("/^[0-9]{10}$/",  $data['usuario'])){
                $msg  = "formato valido de teléfono / email.";
                $code = 200;
            } //check for a pattern of 91-0123456789 
        }
        if($code!=200){
                return response()->json([
                        'msg'=>$msg,
                        'code'=>201                        
                    ],200);
        }else{ 
                $cantidad1 = Usuarios::where('invitado',2)->where('email',   $data['usuario'])->count(); 
                $cantidad2 = Usuarios::where('invitado',2)->where('telefono',$data['usuario'])->count();         
                if($cantidad1 == 0 && $cantidad2 == 0){
                        return response()->json([
                            'msg'=>"El email no se encuentra registrado",
                            'email'=>$data['usuario'],
                            'code'=>201
                        ],200);
                }else if($cantidad1!=0){
                        $datos = Usuarios::where('invitado',2)->where('email',   $data['usuario'])->first(); 
                        
                }else{
                        $datos = Usuarios::where('invitado',2)->where('telefono',   $data['usuario'])->first(); 
                        
                }
                //ENVIAR CODIGO DE RECUPERCACION
                $codigo_veri_a    = $datos->id*(date('d')*date('m')*date('i'));
                $codigo_veri_a   .= "123456789";
                $codigo_veri      = "";
                $charactersLength = strlen($codigo_veri_a);
                for ($i = 0; $i < 4; $i++) {
                    $codigo_veri .= $codigo_veri_a[rand(0, $charactersLength-1)];
                }
                $datos1 = Usuarios::find($datos->id);
                $datos1->codigo_recuperacion      =  $codigo_veri;
                $datos1->save();
                $subject = 'Código de verificación';
                $email   = $datos->email;
                $mensaje = "Estimado/a ".$datos->nombre_apellido." Para restablecer su contraseña, ingrese el siguiente código de verificación cuando se le indique: ".$codigo_veri;
                $d = ['mensaje'=> $mensaje];
                Mail::send('emails.recuperacion', $d, function ($message) use ($subject, $email){
                    $message->subject($subject);
                    $message->to($email);
                });
                return response()->json([
                    'usuario'=>$data['usuario'],
                    'msg'=>'Código de verificación fue enviado a su correo',
                    'code'=>200                      
                ],200);
        }//fin else
    }//fin function
    public function loginrecuperar2(Request $request){
        $data = $request->json()->all();
        $cantidad = Usuarios::where('codigo_recuperacion',$data['codigo_recuperacion'])->count();         
        if($cantidad == 0){
            return response()->json([
                'codigo'=>$data['codigo_recuperacion'],
                'msg'=>"Él código no es válido.",
                'code'=>201
            ],200);
        }else{
            $datos = Usuarios::where('codigo_recuperacion',$data['codigo_recuperacion'])->first(); 
            return response()->json([
                'codigo'=>$data['codigo_recuperacion'],
                'code'=>200                      
            ],200);
        }
    }//fin function
    public function loginrecuperar3(Request $request){
            $data = $request->json()->all();
            $cantidad1 = Usuarios::where('invitado',2)->where('email',   $data['usuario'])->count(); 
            $cantidad2 = Usuarios::where('invitado',2)->where('telefono',$data['usuario'])->count();         
            if($cantidad1!=0){
                    $datos = Usuarios::where('invitado',2)->where('email',   $data['usuario'])->first(); 
            }else{
                    $datos = Usuarios::where('invitado',2)->where('telefono',   $data['usuario'])->first(); 
                    
            }
            $datos1 = Usuarios::find($datos->id);
            $datos1->clave = md5($data['clave']);
            $datos1->codigo_recuperacion = "";
            $datos1->save();
            return response()->json([
                    'msg'=>'La contraseña ha sido modificada con éxito',
                    'code'=>200                      
            ],200);
    }



    public function perfilclaveeditar(Request $request){
            $data = $request->json()->all();
            $cantidad1 = Usuarios::where('id',$data['usuarioid'])->where('clave',   md5($data['claveactual']))->count(); 
            if($cantidad1!=0){
                    $datos1 = Usuarios::find($data['usuarioid']);
                    $datos1->clave = md5($data['clave']);
                    $datos1->codigo_recuperacion = "";
                    $datos1->save();
                    return response()->json([
                            'msg'=>'La contraseña ha sido modificada con éxito',
                            'code'=>200                      
                    ],200);
            }else{
                    return response()->json([
                            'msg'=>'La contraseña actual no es correcta',
                            'code'=>201                      
                    ],200);
            }
    }

    public function credolabcollect(Request $request){
        $data = $request->json()->all();
        $usuario = Usuarios::where('customer_id', $data['customer_id'])->first();
        if($usuario) {
            $credolabcollect = str_replace('"', '', $data['credolabcollect']);
            $credolabcollect = str_replace("'", '', $credolabcollect);
            $usuario = Usuarios::find($usuario->id);
            $usuario->credolabcollect = $credolabcollect;
            $usuario->save();
            return response()->json([
                'msg'=>'El id de credolabcollect ha sido guardado con éxito',
                'code'=>200                      
            ],200);
        } else {
            return response()->json([
                'msg'=>'El customer_id no se encontro',
                'code'=>201
            ],201);
        }
    }

    public function credolabAccessToken($return = false)
    {
        $accessToken = $this->credolab_AccessToken();
        if($accessToken){
            return response()->json([
                'error' => false,
                'access_token'  => $accessToken,
                'code' => 200
            ], 200);
        } else {
            return response()->json([
                'error' => 'Error:' . $accessToken,
                'access_token'  => '',
                'code' => 201
            ], 201);
        }
    }

    public function credolabDatasets(Request $request)
    {
        $data = $request->json()->all();
        $customer_id = $data['customer_id'];
        $usuario = Usuarios::where('customer_id', $customer_id)->first();
        if($usuario && isset($usuario->credolabcollect) && $usuario->credolabcollect) {
            $credolabcollect = $usuario->credolabcollect;
            $datasets = $this->credolab_Datasets($credolabcollect);
            if ($datasets) {
                $usuario = Usuarios::find($usuario->id);
                $usuario->creadolabscores = $datasets['scores'][0]['value'] ?? null;
                $usuario->save();

                return response()->json([
                    'error' => false,
                    'data'  => $datasets,
                    'scores' => $datasets['scores'][0]['value'] ?? null,
                    'code' => 200
                ], 200);
            } else {
                return response()->json([
                    'error' => 'Error:' . $datasets,
                    'data'  => '',
                    'scores' => '',
                    'code' => 201
                ], 201);
            }
        } else {
            return response()->json([
                'error' => true,
                'data'  => '',
                'scores' => '',
                'code' => 201
            ], 201);
        }
    }
}//Fin class
