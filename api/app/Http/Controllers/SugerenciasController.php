<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Models\Informaciones;
use \App\Models\Contactos;
use \App\Models\Sugerencias;
use \App\module;
use \App\Models\Parametros;

use DB;
use Mail;

use Twilio\Rest\Client;

use App\Http\Controllers\Controller;
use JWTAuth;

class SugerenciasController extends Controller
{
    
    public function sugerenciasadd(Request $request){
        $data = $request->json()->all();
        $id_user   =  DB::table('sugerencias')->insertGetId(['usuario_id' => $data['usuarioid'],
        												     'puntaje'    => $data['puntaje'],
        												     'texto'      => $data['texto'],
	                                                    ]
	                                                   );
        return response()->json([
            'msg'=>"Sugerencia Enviada.¡Gracias!",
            'code'=>200                      
        ],200);
    }//fin function

    

 

}//Fin class
