<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Models\Usuarios;
use \App\Models\Publicidades;
use \App\Models\Parametros;
use \App\Models\Deudas;
use \App\module;


use DB;
use Mail;
use DateTime;

use Twilio\Rest\Client;

use App\Http\Controllers\Controller;
use JWTAuth;

use App\Traits\DataCredito;
use App\Traits\Templaris;
use Exception;

class MenuController extends Controller
{
    use DataCredito;
    use Templaris;

    public $arreglo = array(); // Array para almacenar los registros padres e hijos

    private function obtenerChildren2($user_ids = null)
    {
        $seguidores  = Usuarios::select('id', 'userinvitador_id')->whereIn('userinvitador_id', $user_ids)->get();
        $aux = $seguidores->toArray();
        if ($aux) {
            foreach ($aux as $value) {
                $hijos[] = $value['id'];
            }
            $this->arreglo = array_merge($this->arreglo, $hijos);
            $this->obtenerChildren2($hijos);
        }
    }

    public function obtenerChildren3(array &$elements, $userinvitador_id = 0)
    {
        $branch = array();
        foreach ($elements as $key => $element) {
            if ($element['userinvitador_id'] == $userinvitador_id) {
                $children = $this->obtenerChildren3($elements, $key);
                if ($children) {
                    $element['children'] = $children;
                }
                $branch[$key] = $element;
                unset($elements[$key]);
            }
        }
        return $branch;
    }

    public function obtenerChildren($input = array())
    {
        $tree = [];
        $branches = [];
        while (!empty($input)) {
            $beforeCount = count($input);
            foreach ($input as $id => $item) {
                $pid = $item['userinvitador_id'];
                if (isset($branches[$pid])) {
                    $branches[$pid]['children'][$id] = $item;
                    $branches[$id] = $branches[$pid]['children'][$id];
                    unset($input[$id]);
                }
            }
            if ($beforeCount === count($input)) {
                $firstItem = array_shift($input);
                $id = $firstItem['id'];
                $tree[$id] = $firstItem;
                $branches[$id] = &$tree[$id];
            }
        }
        return $branches;
    } //fin function

    public function listarmenu(Request $request)
    {
        $data = $request->json()->all();

        $consultarBuro = false;
        if(isset($data['consultarBuro'])){
            $consultarBuro = $data['consultarBuro'];
        }        

        $parametros     = Parametros::where('activo', 1)->get();
        $parametros1    = $parametros->toArray();
        $parametroemail = $parametros1[0]['emailtexto'];
        $parametrosms   = $parametros1[0]['sms'];
        $parametrovideo = $parametros1[0]['video'];
        $datos_menus = array();
        $versionapp  = "1.0.5";
        $seguidores  = 0;
        $invitados   = 0;
        $color       = "gold";
        $avatar      = "./assets/icon/avatar.png";
        $datos_publi = Publicidades::where('activo', 1)->where('tipo', 1)->get();
        $invitados   = Usuarios::where('userinvitador_id',   $data['usuarioid'])->count();
        //CONTAR SCORE
        //$seguidores_data  = Usuarios::select('id', 'userinvitador_id')->where('invitado',2)->where('userinvitador_id',   $data['usuarioid'])->get(); 
        $seguidores_data  = Usuarios::select('id', 'userinvitador_id', 'genetica')->where('invitado', 2)->get();
        foreach ($seguidores_data  as $key1) {
            $porciones = explode("-", $key1['genetica']);
            foreach ($porciones as $key2) {
                if ($data['usuarioid'] == $key2) {
                    $seguidores++;
                }
            }
        } //fin find1
        //FIN CONTAR SCORE
        /*
            Bronce    0-50 referidos registrados, 
            Plata   51-200 referidos registradados   
            Oro  201 - 500 Referidos registrados     
            Platinium 501+  referidos registrados
        */
        if ($seguidores <= 50) {
            $color = 1;
        } else if ($seguidores <= 200) {
            $color = 2;
        } else if ($seguidores <= 500) {
            $color = 3;
        } else if ($seguidores >= 501) {
            $color = 4;
        }
        $datosusauriodeudas  = Usuarios::where('id', $data['usuarioid'])->get()->toArray();
        $cedulausuariodeudas = isset($datosusauriodeudas[0]['ci']) ? $datosusauriodeudas[0]['ci'] : 0;

        if ($consultarBuro) {
            try {
                //$this->getXmlToDb($cedulausuariodeudas, "C", $data['usuarioid']); //consultar deudas buro de deudas
                $this->customerAccountsTemplaris($cedulausuariodeudas);
            } catch (Exception $e) {
            }
        }
        $datosusauriodeudas  = Usuarios::where('id', $data['usuarioid'])->get()->first();

        $datosdeudas         = Deudas::where('estatus', 0)->where('cedula', $cedulausuariodeudas)->orderBy('created', 'asc')->get()->toArray();
        $contar = 0;
        foreach ($datosdeudas as $key) {
            $datosdeudas[$contar]['monto_deuda'] = number_format($key['monto_deuda']);

            $fechaa = $key['fecha'];
            $fechab = date("Y-m-d");

            $fecha1= new DateTime($fechaa);
            $fecha2= new DateTime($fechab);
            $diff = $fecha1->diff($fecha2);

            $datosdeudas[$contar]['diasmora'] = $diff->days;

            $datosdeudas[$contar]['id_supervisor'] = 0;
            if ($key['origen'] == 'templaris') {
                $homologacionTemplaris = \App\Models\Homologacionbancostemplaris::where('tkl_creditor_id', $key['creditor_id'])->get()->first();
                if (isset($homologacionTemplaris->id_supervisor) && $homologacionTemplaris->id_supervisor) {
                    $datosdeudas[$contar]['id_supervisor'] = $homologacionTemplaris->id_supervisor;
                }
            }

            $contar++;
        }
        return response()->json([
            'datosdeudas' => $datosdeudas,
            'datos_publi' => $datos_publi,
            'seguidores' => $seguidores,
            'invitados'  => $invitados,
            'color'      => $color,
            'avatar'     => $avatar,
            'versionapp' => $versionapp,
            'parametrovideo' => $parametrovideo,
            'datos_usuarios' => $datosusauriodeudas,
            'code' => 200
        ], 200);
    } //fin 


    public function listarmenudeudas(Request $request)
    {
        $data = $request->json()->all();
        $parametros  = Parametros::where('activo', 1)->get();
        $parametros1  = $parametros->toArray();
        $parametroemail = $parametros1[0]['emailtexto'];
        $parametrosms   = $parametros1[0]['sms'];
        $parametrovideo = $parametros1[0]['video'];
        $datos_menus = array();
        $versionapp  = "1.0.5";
        $seguidores  = 0;
        $invitados   = 0;
        $color       = "gold";
        $avatar      = "./assets/icon/avatar.png";
        $datos_publi = Publicidades::where('activo', 1)->where('tipo', 1)->get();
        $invitados   = Usuarios::where('userinvitador_id',   $data['usuarioid'])->count();
        //CONTAR SCORE
        //$seguidores_data  = Usuarios::select('id', 'userinvitador_id')->where('invitado',2)->where('userinvitador_id',   $data['usuarioid'])->get(); 
        $seguidores_data  = Usuarios::select('id', 'userinvitador_id', 'genetica')->where('invitado', 2)->get();
        foreach ($seguidores_data  as $key1) {
            $porciones = explode("-", $key1['genetica']);
            foreach ($porciones as $key2) {
                if ($data['usuarioid'] == $key2) {
                    $seguidores++;
                }
            }
        } //fin find1
        //FIN CONTAR SCORE
        /*
            Bronce    0-50 referidos registrados, 
            Plata   51-200 referidos registradados   
            Oro  201 - 500 Referidos registrados     
            Platinium 501+  referidos registrados
        */
        if ($seguidores <= 50) {
            $color = 1;
        } else if ($seguidores <= 200) {
            $color = 2;
        } else if ($seguidores <= 500) {
            $color = 3;
        } else if ($seguidores >= 501) {
            $color = 4;
        }
        $datosusauriodeudas  = Usuarios::where('id', $data['usuarioid'])->get()->toArray();
        $cedulausuariodeudas = isset($datosusauriodeudas[0]['ci']) ? $datosusauriodeudas[0]['ci'] : 0;

        try {
            //$this->getXmlToDb($cedulausuariodeudas, "C", $data['usuarioid']); //consultar deudas buro de deudas
            $this->customerAccountsTemplaris($cedulausuariodeudas);
        } catch (Exception $e) {
        }

        $seleccionarray = array();
        $checkbox = explode(",", $data['checkbox']);
        foreach ($checkbox as $key) {
            if ($key != 0) {
                $seleccionarray[] = $key;
            }
        }
        $datosdeudas = Deudas::where('estatus', 0)->whereIn('id', $seleccionarray)->orderBy('created', 'asc')->limit(1)->get()->toArray();
        //$datosdeudas = Deudas::where('estatus',0)->where('cedula', $cedulausuariodeudas)->orderBy('created', 'asc')->get()->limit(1)->toArray(); 
        $contar = 0;
        foreach ($datosdeudas as $key) {
            $datosdeudas[$contar]['monto_deuda']  = number_format($key['monto_deuda']);
            $datosdeudas[$contar]['monto_deuda2'] = $key['monto_deuda'];
            $datosdeudas[$contar]['id_supervisor'] = 0;
            if ($key['origen'] == 'templaris') {
                $homologacionTemplaris = \App\Models\Homologacionbancostemplaris::where('tkl_creditor_id', $key['creditor_id'])->get()->first();
                if (isset($homologacionTemplaris->id_supervisor) && $homologacionTemplaris->id_supervisor) {
                    $datosdeudas[$contar]['id_supervisor'] = $homologacionTemplaris->id_supervisor;
                }
            }
            $contar++;
        }
        return response()->json([
            'datosdeudas' => $datosdeudas,
            'datos_publi' => $datos_publi,
            'seguidores' => $seguidores,
            'invitados'  => $invitados,
            'color'      => $color,
            'avatar'     => $avatar,
            'versionapp' => $versionapp,
            'parametrovideo' => $parametrovideo,
            'code' => 200
        ], 200);
    } //fin function


    public function menuinvitacion(Request $request)
    {
        $data = $request->json()->all();
        $parametros  = Parametros::where('activo', 1)->get();
        $parametros1  = $parametros->toArray();
        $parametroemail = $parametros1[0]['emailtexto'];
        $parametrosms   = $parametros1[0]['sms'];
        $data['email']    = "";
        $data['telefono'] = "";
        $invitado_aux = $data['invitado'];
        $mensaje_  = "Las invitaciones fueron enviadas ";
        $mensaje2_ = "";
        $mensaje2 = "";
        $code_ = '200';
        if (filter_var($invitado_aux, FILTER_VALIDATE_EMAIL)) {
            $msg  = "formato valido de teléfono / email.";
            $code = 200;
            $op   = 1;
            $data['email']    = $invitado_aux;
            $data['invitado'] = $invitado_aux;
        } else {
            $invitado1 = str_replace("-", "",  $data['invitado']);
            $invitado2 = str_replace("(", "",  $invitado1);
            $invitado3 = str_replace(")", "",  $invitado2);
            $invitado4 = str_replace(" ", "",  $invitado3);
            $invitado5 = str_replace("+1", "", $invitado4);
            $data['invitado'] = $invitado5;
            $msg  = "formato invalido de teléfono / email.";
            $code = 201;
            if (preg_match("/^[0-9]{10}$/",  $data['invitado'])) {
                $msg  = "formato valido de teléfono / email.";
                $code = 200;
                $op   = 2;
                $data['telefono'] = $data['invitado'];
            } //check for a pattern of 91-0123456789 
        }
        if ($code == 200) {
            $cantidad1 = Usuarios::where('email',   $data['invitado'])->count();
            $cantidad2 = Usuarios::where('telefono', $data['invitado'])->count();
            if ($cantidad1 != 0 || $cantidad2 != 0) {
                $msg = "El Email / Télefono ya se encuentra registrado ";
                $code = 201;
            } else {
                if ($op == 1) { //ENVIO DE CORREO AL INVITADO
                    $msg = "El Email de la invitación fue enviado";
                    $subject = 'Invitación app Pagadores';
                    $email   = $data['invitado'];
                    $mensaje = "Estimado/a " . $data['invitado'] . ", " . $parametroemail;
                    $d = ['mensaje' => $mensaje];
                    Mail::send('emails.invitacion', $d, function ($message) use ($subject, $email) {
                        $message->subject($subject);
                        $message->to($email);
                    });
                } else { //ENVIO DE MSJ AL INVITADO
                    $msg     = "El SMS de la invitación fue enviado";
                    //$sid     = "AC8a3c3fdc6f27ea2df70a41548219847b"; //"AC8a3c3fdc6f27ea2df70a41548219847b"; // Your Account SID from www.twilio.com/console
                    //$token   = "2c71c43f96b525676e8ee9705d23334a"; //"2c71c43f96b525676e8ee9705d23334a"; // Your Auth Token from www.twilio.com/console

                    $sid       = "AC6b675d394ea0f83b871e53b7d69fb067";
                    $token     = "926772c541329498da0b780aa0725c1f";  
                    $telefono2 = "+13126464282";

                    $client  = new Client($sid, $token);
                    $mensaje = "Estimado/a " . $data['invitado'] . ", " . $parametrosms;
                    try {
                        $message = $client->messages->create(
                            "+1" . $data['invitado'], // Text this number
                            [
                                'from' => $telefono2, // From a valid Twilio number
                                'body' => $mensaje
                            ]
                        );
                    } catch (\Twilio\Exceptions\RestException $e) {
                        $mensaje2 = ", +1" . $data['invitado'];
                    }
                } //fin else
                if ($mensaje2 == "") {
                    $datos1   = Usuarios::find($data['usuarioid']);
                    if (isset($datos1->genetica)) {
                        $genetica = $datos1->genetica == '' ? $data['usuarioid'] : $datos1->genetica . '-' . $data['usuarioid'];
                    } else {
                        $genetica = $data['usuarioid'];
                    }
                    $id_user  = DB::table('usuarios')->insertGetId([
                        'fechaingreso'        => date('Y-m-d'),
                        'role_id'             => 2,
                        'email'               => $data['email'],
                        'telefono'            => $data['telefono'],
                        'userinvitador_id'    => $data['usuarioid'],
                        'genetica'            => $genetica
                    ]);
                } else {
                    $code = 201;
                    $msg  = " Número de teléfono (" . $mensaje2 . ") no es un número de teléfono móvil (celular)";
                }
            }
        } //fin if
        return response()->json([
            'code' => $code,
            'msg' => $msg,
            'telefono' => $data['telefono']
        ], 200);
    } //fin function



    public function listartop(Request $request)
    {
        $data = $request->json()->all();
        $usr1  = Usuarios::where('top20', 2)->where('invitado', 2)->orderBy('genetica', 'asc')->get();
        $variable1 = $usr1->toArray();
        $variable2 = array();
        $posi = 0;
        $contador = 0;
        $pos = 0;
        $seguidomayor = 0;
        $seguidores_data  = Usuarios::select('id', 'userinvitador_id', 'genetica')->where('invitado', 2)->get();
        foreach ($variable1 as $key1) {
            $activa = 0;
            $porciones   = explode("-", $key1['genetica']);
            $nivel_count = count($porciones);
            $seguidores  = 0;
            foreach ($seguidores_data  as $key2) {
                $porciones2 = explode("-", $key2['genetica']);
                foreach ($porciones2 as $key3) {
                    if ($key1['id'] == $key3) {
                        $seguidores++;
                    }
                }
            } //fin find1
            if ($seguidores <= 50) {
                $color = 1;
            } else if ($seguidores <= 200) {
                $color = 2;
            } else if ($seguidores <= 500) {
                $color = 3;
            } else if ($seguidores >= 501) {
                $color = 4;
            }
            $variable1[$posi]['color']      = $color;
            $variable1[$posi]['seguidores'] = $seguidores;
            $variable1[$posi]['nivel']      = 'Nivel ' . $nivel_count;
            $variable1[$posi]['activa']     = '2';
            $posi++;
        }
        $var1_auxiliar = $variable1;
        $bandera = 0;
        $contar2 = 0;
        for ($i = 0; $i < $posi; $i++) {
            $posicion_bandera = 0;
            $bandera          = 0;
            $j = 0;
            //foreach($var1_auxiliar as $key1) {
            for ($xx = 0; $xx < $posi; $xx++) {
                if ($var1_auxiliar[$xx]['seguidores'] >= $bandera && $var1_auxiliar[$xx]['activa'] == '2') {
                    $bandera = $var1_auxiliar[$xx]['seguidores'];
                    $posicion_bandera = $j;
                }
                $j++;
            }
            if (isset($var1_auxiliar[$posicion_bandera])) {
                $variable2[$contar2] = $var1_auxiliar[$posicion_bandera];
                $variable2[$contar2]['numero'] = $contar2 + 1;
                $contar2++;
                $var1_auxiliar[$posicion_bandera]['activa'] = '1';
            }
            //$var1_auxiliar = $variable1;

        }
        //print_r( $var1_auxiliar);
        //print_r( $variable2);
        return response()->json([
            'datos' => $variable2,
            'code' => 200
        ], 200);
    }

    public function datacredito(Request $request)
    {
        $data = $request->json()->all();
        $datosusauriodeudas  = Usuarios::where('id', $data['usuarioid'])->get()->toArray();
        $cedulausuariodeudas = isset($datosusauriodeudas[0]['ci']) ? $datosusauriodeudas[0]['ci'] : 0;
        try {
            $this->getXmlToDb($cedulausuariodeudas, "C", $data['usuarioid']); //consultar deudas buro de deudas
        } catch (Exception $e) {
        }
    }
}//Fin class