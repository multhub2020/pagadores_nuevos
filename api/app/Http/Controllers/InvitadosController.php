<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Models\Usuarios;
use \App\Models\Parametros;
use \App\module;

use DB;
use Mail;

// Update the path below to your autoload.php,
// see https://getcomposer.org/doc/01-basic-usage.md
//require_once '/path/to/vendor/autoload.php';

use Twilio\Rest\Client;



use App\Http\Controllers\Controller;
use JWTAuth;

class InvitadosController extends Controller
{
    
    public function reenviarinvitacion(Request $request){
        $data = $request->json()->all();
        $usr1  = Usuarios::where('invitado',1)->where('id', $data['usuarioidpuntual'])->get(); 
        $parametros  = Parametros::where('activo',1)->get(); 
        $parametros1  = $parametros->toArray(); 
        $parametroemail = $parametros1[0]['emailtexto'];
        $parametrosms   = $parametros1[0]['sms'];
        $msg      = "Mensaje no fue enviado";
        $mensaje_  = "La invitación fue enviada ";
        $mensaje2_ = "";
        $mensaje2 = "";
        $code_ = '200';
        foreach ($usr1 as $key) {
            $code = 0;
            if(filter_var($key['email'], FILTER_VALIDATE_EMAIL)) {
                    $code = 1;
            }else{
                if(preg_match("/^[0-9]{10}$/",  $key['telefono'])){
                    $code = 2;
                } 
            }
                  if($code==1){
                                    $msg     = "El Email de la invitación fue enviado";
                                    $subject = 'Invitación app Pagadores';
                                    $email   = $key['email'];
                                    $mensaje = "Estimado/a ".$key['email'].", ".$parametroemail;
                                    $d = ['mensaje'=> $mensaje];
                                    Mail::send('emails.invitacion', $d, function ($message) use ($subject, $email){
                                        $message->subject($subject);
                                        $message->to($email);
                                    });
            }else if($code==2){
                                    // Send an SMS using Twilio's REST API and PHP
                                    //$sid     = "AC8a3c3fdc6f27ea2df70a41548219847b";//"AC8a3c3fdc6f27ea2df70a41548219847b"; // Your Account SID from www.twilio.com/console
                                    //$token   = "2c71c43f96b525676e8ee9705d23334a";//"2c71c43f96b525676e8ee9705d23334a"; // Your Auth Token from www.twilio.com/console

                                    $sid       = "AC6b675d394ea0f83b871e53b7d69fb067";
                                    $token     = "926772c541329498da0b780aa0725c1f";  
                                    $telefono2 = "+13126464282";

                                    $msg     = "El SMS de la invitación fue enviado";
                                    $client  = new Client($sid, $token);
                                    $mensaje = "Estimado/a ".$key['telefono'].", ".$parametrosms;
                                    try {
                                            $message = $client->messages->create(
                                                    "+1".$key['telefono'], // Text this number
                                                [
                                                    'from' => $telefono2, // From a valid Twilio number
                                                    'body' => $mensaje
                                                ]
                                            );
                                    }catch(\Twilio\Exceptions\RestException $e){        
                                        $mensaje2 = ", +1".$key['telefono'];
                                        
                                    }
            }
        }
        //$data['usuarioid']
        //$data['usuarioidpuntual']
        if($mensaje2!=""){
            $mensaje_ = " Número de teléfono (".$mensaje2.") no es un número de teléfono móvil (celular)";
        }
        return response()->json([
                        'msj'=>$mensaje_,
                        'code'=>200                       
                    ],200);
    }

    public function reenviarinvitacionall(Request $request){
        $data = $request->json()->all();
        $usr1  = Usuarios::where('invitado',1)->where('userinvitador_id', $data['usuarioid'])->get(); 
        $msg   = "Mensaje no fue enviado";
        $mensaje_  = "Las invitaciones fueron enviadas ";
        $mensaje2_ = "";
        $mensaje2 = "";
        $code_ = '200';
        $parametros  = Parametros::where('activo',1)->get(); 
        $parametros1  = $parametros->toArray(); 
        $parametroemail = $parametros1[0]['emailtexto'];
        $parametrosms   = $parametros1[0]['sms'];
        foreach ($usr1 as $key) {
            $code = 0;
            if(filter_var($key['email'], FILTER_VALIDATE_EMAIL)) {
                    $code = 1;
            }else{
                if(preg_match("/^[0-9]{10}$/",  $key['telefono'])){
                    $code = 2;
                } 
            }
                  if($code==1){
                                    $msg     = "El Email de la invitación fue enviado";
                                    $subject = 'Invitación app Pagadores';
                                    $email   = $key['email'];
                                    $mensaje = "Estimado/a ".$key['email'].", ".$parametroemail;
                                    $d = ['mensaje'=> $mensaje];
                                    Mail::send('emails.invitacion', $d, function ($message) use ($subject, $email){
                                        $message->subject($subject);
                                        $message->to($email);
                                    });
            }else if($code==2){
                                    // Send an SMS using Twilio's REST API and PHP
                                    //$sid      = "AC8a3c3fdc6f27ea2df70a41548219847b";//"AC8a3c3fdc6f27ea2df70a41548219847b"; // Your Account SID from www.twilio.com/console
                                    //$token   = "2c71c43f96b525676e8ee9705d23334a";//"2c71c43f96b525676e8ee9705d23334a"; // Your Auth Token from www.twilio.com/console

                                    $sid       = "AC6b675d394ea0f83b871e53b7d69fb067";
                                    $token     = "926772c541329498da0b780aa0725c1f";  
                                    $telefono2 = "+13126464282";

                                    $msg     = "El SMS de la invitación fue enviado";
                                    $client  = new Client($sid, $token);
                                    $mensaje = "Estimado/a ".$key['telefono'].", ".$parametrosms;
                                    try {
                                            $message = $client->messages->create(
                                                    "+1".$key['telefono'], // Text this number
                                                [
                                                    'from' => $telefono2, // From a valid Twilio number
                                                    'body' => $mensaje
                                                ]
                                            );
                                    }catch(\Twilio\Exceptions\RestException $e){        
                                        $mensaje2 = ", +1".$key['telefono'];
                                        
                                    }
            }
        }
        if($mensaje2!=""){
            $mensaje_ = " Número de teléfono (".$mensaje2.") no es un número de teléfono móvil (celular)";
        }
        return response()->json([
                        'msj'=>$mensaje_,
                        'code'=>200                       
                    ],200);
    }


    public function listarinvitados(Request $request){
        $data  = $request->json()->all();
        $usr   = Usuarios::where('userinvitador_id', $data['usuarioid'])->get(); 
        $usr2  = Usuarios::where('invitado',1)->where('userinvitador_id', $data['usuarioid'])->get(); 
        return response()->json([
                        'datos'=>$usr,
                        'datos2'=>$usr2,
                        'code'=>200                       
                    ],200);
    }

    public function listarseguidores(Request $request){
        $data = $request->json()->all();
        $usr1  = Usuarios::where('invitado',2)->orderBy('genetica', 'asc')->get(); 
        $variable1 = $usr1->toArray();
        
        $usr2  = Usuarios::where('invitado',2)->orderBy('genetica', 'asc')->where('id',   $data['usuarioid'])->get(); 
        $variable2  = $usr2->toArray();
        $nombressub = $variable2[0]['nombre_apellido'];
        $posi = 0;
        foreach($variable1 as $key1) {
                $activa = 0;
                $porciones = explode("-", $key1['genetica']);
                $nivel_count = count($porciones);
                foreach ($porciones as $key2) {
                    if($data['usuarioid']==$key2){
                        $activa++;
                    }
                }
                if($activa!=0){
                    $seguidores = 0;
                    $seguidores_data  = Usuarios::select('id', 'userinvitador_id')->where('invitado',2)->where('userinvitador_id',   $key1['id'])->get(); 
                    foreach($seguidores_data  as $key3) {
                        $seguidores++;
                    }
                            if($seguidores<=50){
                                $color = 1;
                    }else if($seguidores<=200){
                                $color = 2;
                    }else if($seguidores<=500){
                                $color = 3;
                    }else if($seguidores>=501){
                                $color = 4;
                    }
                    $variable1[$posi]['color']      = $color;
                    $variable1[$posi]['seguidores'] = $seguidores;
                    $variable1[$posi]['nivel']      = 'Nivel '.$nivel_count;
                    $variable1[$posi]['activa']     = '2';
                }else{
                    //unset($variable1[$posi]);
                    $variable1[$posi]['activa']     = '1';    
                }
                $posi++;
        }
        return response()->json([
                        'datos1'=>$variable1,
                        'nombressub' => $nombressub,
                        'code'=>200                       
                    ],200);
    }

    public function listarseguidoresfiltro(Request $request){
        $data = $request->json()->all();
        //$data['puntaje']
        //$data['nivel']
        $usr1  = Usuarios::where('invitado',2)->orderBy('genetica', 'asc')->get(); 
        $variable1 = $usr1->toArray();
        $usr2  = Usuarios::where('invitado',2)->orderBy('genetica', 'asc')->where('id',   $data['usuarioid'])->get(); 
        $variable2 = $usr2->toArray();
        $nombressub = $variable2[0]['nombre_apellido'];
        $posi = 0;
        foreach($variable1 as $key1) {
                $activa = 0;
                $porciones   = explode("-", $key1['genetica']);
                $nivel_count = count($porciones);
                foreach ($porciones as $key2) {
                    if($data['usuarioid']==$key2){
                        $activa++;
                    }
                }
                if($activa!=0){
                    $seguidores = 0;
                    $seguidores_data  = Usuarios::select('id', 'userinvitador_id')->where('invitado',2)->where('userinvitador_id',   $key1['id'])->get(); 
                    foreach($seguidores_data  as $key3) {
                        $seguidores++;
                    }
                            if($seguidores<=50){
                                $color = 1;
                    }else if($seguidores<=200){
                                $color = 2;
                    }else if($seguidores<=500){
                                $color = 3;
                    }else if($seguidores>=501){
                                $color = 4;
                    }else{
                                $color = 4;
                    }
                    if($data['puntaje']==5){
                        $data['puntaje'] = 4;
                    }
                    $variable1[$posi]['color']      = $color;
                    $variable1[$posi]['seguidores'] = $seguidores;
                    $variable1[$posi]['nivel']      = 'Nivel '.$nivel_count;
                    //$data['compara']
                          if($data['compara']==1 && $data['nivel']<$nivel_count && $color>=$data['puntaje']){
                                $variable1[$posi]['activa']     = '2';
                    }else if($data['compara']==2 && $data['nivel']==$nivel_count && $color>=$data['puntaje']){
                                $variable1[$posi]['activa']     = '2';
                    }else if($data['compara']==3 && $data['nivel']>$nivel_count && $color>=$data['puntaje']){
                                $variable1[$posi]['activa']     = '2';                                
                    }else{
                                $variable1[$posi]['activa']     = '1';
                    }
                }else{
                    //unset($variable1[$posi]);
                    $variable1[$posi]['activa']     = '1';    
                }
                $posi++;
        }
        return response()->json([
                        'datos1'=>$variable1,
                        'nombressub'=>$nombressub,
                        'code'=>200                       
                    ],200);
    }

    public function sms($tipo=1){
        $tipo=1;
        $telefono = "+18496528020";
        if($tipo==1){

                                //$sid   = "AC8a3c3fdc6f27ea2df70a41548219847b";//"AC8a3c3fdc6f27ea2df70a41548219847b"; // Your Account SID from www.twilio.com/console
                                //$token = "2c71c43f96b525676e8ee9705d23334a";//"2c71c43f96b525676e8ee9705d23334a"; // Your Auth Token from www.twilio.com/console

                                $sid       = "AC6b675d394ea0f83b871e53b7d69fb067";
                                $token     = "926772c541329498da0b780aa0725c1f";  
                                $telefono2 = "+13126464282";

                                $client = new Client($sid, $token);
                                try {
                                        $message = $client->messages->create(
                                                 $telefono,
                                            [
                                                //'from' => "+447476545562",
                                                'from' => $telefono2,
                                                'body' => "Hola"
                                            ]
                                        );

                                }catch(\Twilio\Exceptions\RestException $e){        
                                    print_r($e);
                                    
                                }
        }else{
                                $token      = "AAAAb64Jkyc:APA91bEpeNCSxSnurLWXf0yJaORJfE3SEUe6PInDm_oR0eIGSUvLCj8fxRIRJ9BmfWWdW2FG8VPy2UTS69k3xCxCk4U_FLDx1T62FBZxq83IKzcU4UzIqb2-tdMRkvKLxglCu27GBrtu";
                                $to_android = "fj52ga0g9Zc:APA91bFMrQWSONSSdkBB_-mEEEQnW0HaM1YOYaeUenTX1OTdinog5BfHP6i7d7WkXPdOBtLb6REk8HIpbNAtlw--3UJUgvX0I_RfPLvFg43B6jl88kP610MHaYsql1ajq_heIN_LDiwg";
                                $titulo     = "Titulo";
                                $mensaje    = "Mensaje";
                                $data       = null;
                                $headers = [
                                    "Authorization:key=".$token." ",
                                    'Content-Type: application/json'
                                ];
                                $data = [
                                    'to' => $to_android,
                                    'data' => [
                                        'body'   => $mensaje,
                                        'title'  => $titulo,
                                    ]
                                ];
                        try {
                                $ch = curl_init();
                                curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
                                //curl_setopt($ch, CURLOPT_PROXY, "https://fcm.googleapis.com/fcm/send"); //your proxy url
                                //curl_setopt($ch, CURLOPT_PROXYPORT, "80"); // your proxy port number
                                //curl_setopt( $ch,CURLOPT_URL, 'http://www.google.com' );
                                //curl_setopt( $ch,CURLOPT_URL, 'https://gcm-http.googleapis.com/gcm/send' );
                                curl_setopt( $ch,CURLOPT_POST, true );
                                curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
                                curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
                                curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
                                curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $data ) );
                                curl_setopt($ch, CURLOPT_FAILONERROR, TRUE);
                                //curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
                                //curl_setopt($ch, CURLOPT_PORT, 9710);
                                $result = curl_exec($ch);
                                if($errno = curl_errno($ch)) {
                                    $error_message = $errno." - ".curl_strerror($errno);
                                   // echo "cURL error ({$errno}):\n {$error_message}";
                                }else{
                                    $error_message = "";
                                }
                                
                                curl_close( $ch );
                                return response()->json([
                                                'msg'=>$error_message,
                                                'v' => 'Curl: ', function_exists('curl_version') ? 'Enabled' : 'Disabled'
                                            ],200);

                        } catch (Exception $e) {
                                echo 'Excepción capturada: ',  $e->getMessage(), "\n";
                        }                        
        }//fin else
    }
    public function loginregistroinvitador(Request $request){
        $data = $request->json()->all();
        if(filter_var($data['usuario'], FILTER_VALIDATE_EMAIL)) {
            $msg  = "formato valido de teléfono / email.";
            $code = 200;
        }else{
            $msg  = "formato invalido de teléfono / email.";
            $code = 201;
            if(preg_match("/^[0-9]{10}$/",  $data['usuario'])){
                $msg  = "formato valido de teléfono / email.";
                $code = 200;
            } //check for a pattern of 91-0123456789 
        }
        if($code!=200){
                return response()->json([
                        'msg'=>$msg,
                        'code'=>201                        
                    ],200);
        }else{
                $cantidad1 = Usuarios::where('invitado',1)->where('email',   $data['usuario'])->count();         
                $cantidad2 = Usuarios::where('invitado',1)->where('telefono',$data['usuario'])->count();         
                if($cantidad1 == 0 && $cantidad2 == 0 ){
                        $cantidad1_gmail = Usuarios::where('invitado',2)->where('email',   $data['usuario'])->count();  
                        if($cantidad1_gmail!=0 && $data['gmail']=='2'){
                                $usr      = Usuarios::where('invitado',2)->where('email', $data['usuario'])->first(); 
                                $email    = $data['usuario'];
                                $telefono = "";
                                return response()->json([
                                    'datos'=>$usr->toArray(),
                                    'code'=>202                     
                                ],200);
                        }else{
                                return response()->json([
                                    'code' =>201,
                                    'msg'  =>'El email ó Teléfono no esta invitado'
                                ],200);
                        }
                }else{
                    if($cantidad1 != 0){
                             $email    = $data['usuario'];
                             $telefono = "";
                    }else{
                            $telefono = $data['usuario'];
                            $email    = "";
                    }
                    return response()->json([
                        'usuario'=>$data['usuario'],
                        'email'=>$email,
                        'telefono'=>$telefono,
                        'code'=>200                     
                    ],200);
                }//fin else
        }//fin else
    }//fin function


    public function recordatorio(){
        $usr      = Usuarios::where('invitado',2)->get(); 
        $contar  = 0;
        foreach ($usr as $key) { $contar++;
            ///AVISAR AL USUARIO POR NOTIFICACIONES//
            $token = "AAAAb64Jkyc:APA91bEpeNCSxSnurLWXf0yJaORJfE3SEUe6PInDm_oR0eIGSUvLCj8fxRIRJ9BmfWWdW2FG8VPy2UTS69k3xCxCk4U_FLDx1T62FBZxq83IKzcU4UzIqb2-tdMRkvKLxglCu27GBrtu";//"738083342753";//TOKEN DE SENSER ID
            $not   = "";//"DATABASE OBJECT NOTIFICATION";
            //Datos/
            $to_android = $key['push_token'];//$usuario[0]['User']['push_token'];//Datos del usuario
            $to_ios     = $key['push_token'];//$usuario[0]['User']['push_token'];//Datos del usuario
            $platform   = $key['push_platf'];//Datos del usuario
            $titulo     = "¡Aviso de invitacion!";
            $mensaje    = "Pagadores te recuerda que invites a 3 o más personas a través de SMS, Email, Whatsapp";
            $data       = null;
            $headers = [
                "Authorization:key=".$token." ",
                'Content-Type: application/json'
            ];
            if($platform === 'ios') {
                $data = [
                    'to' => $to_ios,
                    'notification' => [
                        'body'   => $mensaje,
                        'title'  => $titulo,
                    ],
                    "data" => [// aditional data for iOS
                        "extra-key" => "extra-value",
                    ],
                    'notId' =>$not,//unique id for each notification
                ];
            } elseif ($platform === 'android') {
                $data = [
                    'to' => $to_android,
                    'data' => [
                        'body'   => $mensaje,
                        'title'  => $titulo,
                    ]
                ];
            }
            $ch = curl_init();
            curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
            //curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
            curl_setopt( $ch,CURLOPT_POST, true );
            curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
            curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
            curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
            curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $data ) );
            curl_setopt($ch, CURLOPT_FAILONERROR, TRUE);
            $result = curl_exec($ch);
            curl_close( $ch );
        }
        return response()->json([
                        'code'=>200,
                        'contar'=>$contar
        ],200);
    }//fin function


    public function invitaciones(){
        $usr      = Usuarios::where('invitado',2)->get(); 
        $contar   = 0;
        foreach ($usr as $key) {
            $usr2      = Usuarios::where('invitado',1)->where('userinvitador_id', $key['id'])->count(); 
            if($usr2!=0){$contar++;
                        ///AVISAR AL USUARIO POR NOTIFICACIONES//
                        $token = "AAAAb64Jkyc:APA91bEpeNCSxSnurLWXf0yJaORJfE3SEUe6PInDm_oR0eIGSUvLCj8fxRIRJ9BmfWWdW2FG8VPy2UTS69k3xCxCk4U_FLDx1T62FBZxq83IKzcU4UzIqb2-tdMRkvKLxglCu27GBrtu";//"738083342753";//TOKEN DE SENSER ID
                        $not   = "";//"DATABASE OBJECT NOTIFICATION";
                        //Datos/
                        $to_android = $key['push_token'];//$usuario[0]['User']['push_token'];//Datos del usuario
                        $to_ios     = $key['push_token'];//$usuario[0]['User']['push_token'];//Datos del usuario
                        $platform   = $key['push_platf'];//Datos del usuario
                        $titulo     = "¡Aviso de invitacion!";
                        $mensaje    = "Pagadores te recuerda que tienes personas invitadas sin registrarse en Pagadores";
                        $data       = null;
                        $headers = [
                            "Authorization:key=".$token." ",
                            'Content-Type: application/json'
                        ];
                        if($platform === 'ios') {
                            $data = [
                                'to' => $to_ios,
                                'notification' => [
                                    'body'   => $mensaje,
                                    'title'  => $titulo,
                                ],
                                "data" => [// aditional data for iOS
                                    "extra-key" => "extra-value",
                                ],
                                'notId' =>$not,//unique id for each notification
                            ];
                        } elseif ($platform === 'android') {
                            $data = [
                                'to' => $to_android,
                                'data' => [
                                    'body'   => $mensaje,
                                    'title'  => $titulo,
                                ]
                            ];
                        }
                        $ch = curl_init();
                        curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
                        //curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
                        curl_setopt( $ch,CURLOPT_POST, true );
                        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
                        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
                        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
                        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $data ) );
                        curl_setopt($ch, CURLOPT_FAILONERROR, TRUE);
                        $result = curl_exec($ch);
                        curl_close( $ch );
            }//fin if
        }//fin function
        return response()->json([
                        'code'=>200,
                        'contar2'=>$contar
        ],200);
    }//fin function



}//Fin class
