<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Models\Usuarios;
use \App\Models\Publicidades;
use \App\Models\Parametros;
use \App\Models\Deudas;
use \App\Models\Productoresolucios;
use \App\module;


use DB;
use Mail;

use Twilio\Rest\Client;

use App\Http\Controllers\Controller;
use JWTAuth;

class DeudasController extends Controller
{

    public function agregadeuda1(Request $request)
    {
        $data = $request->json()->all();
        $datosusauriodeudas  = Usuarios::where('id', $data['usuarioid'])->get()->toArray();
        $cedulausuariodeudas = isset($datosusauriodeudas[0]['ci']) ? $datosusauriodeudas[0]['ci'] : 0;
        $id_user  = DB::table('deudas')->insertGetId(
            [
                'fecha'        => date('Y-m-d'),
                'monto_deuda'  => $data['deuda'],
                'tipo_deuda'   => $data['producto'],
                'product_resolution_id' => $data['producto'],
                'banco_deuda'  => $data['banco'],
                'identifica_banco'  => $data['identifica_banco'],
                'creditor_id'  => $data['creditor_id'],
                'moneda'       => $data['moneda'],
                'estatus'         => 0,
                'manual_debt_add' => 2,
                'cedula'          => $cedulausuariodeudas
            ]
        );
        return response()->json([
            'code' => 200
        ], 200);
    }

    public function deudasadd(Request $request)
    {
        $data = $request->json()->all();
        $checkbox_re  = $data['checkbox_re'];
        $checkbox_mon = $data['checkbox_mon'];
        $checkbox_cuo = $data['checkbox_cuo'];
        $checkbox_pro = $data['checkbox_pro'];
        $checkbox = explode(",", $data['checkbox']);
        $contar = 0;
        foreach ($checkbox as $key) {
            if ($key != 0) {
                $checkbox_array[$contar]['id']  = $key;
                $checkbox_array[$contar]['re']  = $checkbox_re[$contar];
                $checkbox_array[$contar]['mon'] = $checkbox_mon[$contar];
                $checkbox_array[$contar]['cuo'] = $checkbox_cuo[$contar];
                $checkbox_array[$contar]['pro'] = $checkbox_pro[$contar];
                $datosdeudas = Deudas::where('id', $key)->get()->toArray();
                $checkbox_array[$contar]['montdeuda'] = $datosdeudas[0]['monto_deuda'];
                $contar++;
            }
        }
        //$checkbox_re = explode(",", $data['checkbox_re']);
        /*foreach ($checkbox_re as $key) {
            if($key!=0){
                $checkbox_re_array[] = $key;
        }*/

        //$checkbox_mon = explode(",", $data['checkbox_mon']);
        /*foreach ($checkbox_mon as $key) {
            if($key!=0){
                $checkbox_mon_array[] = $key;
            }
        }*/

        //$checkbox_cuo = explode(",", $data['checkbox_cuo']);
        /*foreach ($checkbox_cuo as $key) {
            if($key!=0){
                $checkbox_cuo_array[] = $key;
            }
        }*/

        //$checkbox_pro = explode(",", $data['checkbox_pro']);
        /*foreach ($checkbox_pro as $key) {
            if($key!=0){
                $checkbox_pro_array[] = $key;
            }
        }*/
        foreach ($checkbox_array as $key) {
            $datos1 = Deudas::find($key['id']);
            if ($key['re'] == 1) {
                $datos1->ofrecer_saldo  = $key['mon'];
            } else {
                $datos1->n_cuotas = $key['cuo'];
                $datos1->m_cuota  = (
                    (
                        ($key['montdeuda'] - $key['mon'])
                        *
                        0.25)
                    +
                    ($key['montdeuda'] - $key['mon'])) / $key['cuo'];
                $datos1->pago_inicial  = $key['mon'];
            }
            //$datos1->estatus = 1;
            $datos1->save();
        }
        return response()->json([
            'code' => 200
        ], 200);
    }

    public function deudassolicitudenviarcuotas(Request $request)
    {
        $data = $request->json()->all();
        $checkbox_re  = $data['checkbox_re'];
        $pago_inicial = $data['pago_inicial'];
        $checkbox_mon = $data['checkbox_mon'];
        $checkbox_cuo = $data['checkbox_cuo'];
        $datos1 = Deudas::find($data['id']);
        if ($checkbox_re[0] == 1) {
            $datos1->m_cuota  = $checkbox_mon[0];
        } else {
            $datos1->n_cuotas = $checkbox_cuo[0];
        }
        $datos1->pago_inicial  = $pago_inicial;
        //$datos1->estatus = 1;
        $datos1->save();

        $datos2 = Productoresolucios::where('app', 2)->get()->toArray();

        return response()->json([
            'code' => 200,
            'productresolutionid' => $datos2[0]['tk']
        ], 200);
    }




    public function listaplication(Request $request)
    {
        $data = $request->json()->all();
        $datosdeudas = Deudas::orderBy('id', 'desc')->where('aplicacionid', $data['aplicacionid'])->get()->toArray();
        return response()->json([
            'contar' => $datosdeudas,
            'code' => 200
        ], 200);
    }



    
    public function deudasaplication(Request $request)
    {
        $data = $request->json()->all();
        $datos1  = Deudas::find($data['id']);
        $datos1->aplicacionid = $data['aplicacionid'];
        $datos1->estatus = 1;
        $datos1->credolabscores = $data['credolabscores'];
        $datos1->save();
        return response()->json([
            'code' => 200
        ], 200);
    }

    public function deudassolicitudenviar(Request $request)
    {
        $data = $request->json()->all();
        $checkbox_id  = $data['checkbox_id'];
        $checkbox_mon = $data['checkbox_mon'];
        $datos1       = Deudas::find($checkbox_id);
        $datos1->pago_inicial = $checkbox_mon;
        //$datos1->estatus      = 1;
        $datos1->save();
        $datos2 = Productoresolucios::where('app', 1)->get()->toArray();
        return response()->json([
            'code' => 200,
            'productresolutionid' => $datos2[0]['tk']
        ], 200);
    }

    public function deudaslista(Request $request)
    {
        $data = $request->json()->all();
        $checkbox  = $data['checkbox'];
        $seleccionarray = array();
        $checkbox = explode(",", $data['checkbox']);
        foreach ($checkbox as $key) {
            if ($key != 0) {
                $seleccionarray[] = $key;
            }
        }
        $datosdeudas = Deudas::where('estatus', 0)->whereIn('id', $seleccionarray)->count();
        return response()->json([
            'contar' => $datosdeudas,
            'code' => 200
        ], 200);
    }


    public function localizardeudaid(Request $request)
    {
        $data = $request->json()->all();
        $checkbox    = $data['checkbox'];
        $datosdeudas = Deudas::where('id', $data['checkbox'])->get()->toArray();
        $contar = 0;
        foreach ($datosdeudas as $key) {
            //$datosdeudas[$contar]['monto_deuda']  = number_format($key['monto_deuda']);
            $datosdeudas[$contar]['monto_deuda2'] = $key['monto_deuda'];

            $datosdeudas[$contar]['id_supervisor'] = 0;
            if ($key['origen'] == 'templaris') {
                $homologacionTemplaris = \App\Models\Homologacionbancostemplaris::where('tkl_creditor_id', $key['creditor_id'])->get()->first();
                if (isset($homologacionTemplaris->id_supervisor) && $homologacionTemplaris->id_supervisor) {
                    $datosdeudas[$contar]['id_supervisor'] = $homologacionTemplaris->id_supervisor;
                }
            }

            $contar++;
        }
        return response()->json([
            'datosdeudas' => $datosdeudas,
            'code' => 200
        ], 200);
    }
}