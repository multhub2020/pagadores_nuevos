<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Models\Usuarios;
use \App\Models\Parametros;
use \App\module;

use DB;
use Mail;

// Update the path below to your autoload.php,
// see https://getcomposer.org/doc/01-basic-usage.md
//require_once '/path/to/vendor/autoload.php';

use Twilio\Rest\Client;



use App\Http\Controllers\Controller;
use JWTAuth;

class AgilpaysController extends Controller
{
    /**Desarrollo */
    /* public $site_id = '8b0b8cf1eb402ew7a00';
    public $merchant_key = 'MTQ4NDI3MjM3';
    public $session_id = 'ABCDEF';
    public $payment_api_url = 'https://stage.agilpay.net/WebApi/APaymentTokenApi/'; */

    /**Produccion */
    public $site_id = 'wVCmT5k2BZv9U3tFtGab';
    public $merchant_key = 'FMef2nNsT6EqJPyLBvJN';
    public $session_id = '123';
    public $payment_api_url = 'https://www.agilpay.net/api/';

    public function gethash(Request $request)
    {
        $data = $request->json()->all();
        $contenthash = '';
        $accountNumber = isset($data['AccountNumber']) ? $data['AccountNumber'] : '';
        $amount = isset($data['Amount']) ? $data['Amount'] : '';
        $currency = isset($data['Currency']) ? $data['Currency'] : '840';
        $customerName = isset($data['CustomerName']) ? str_replace(' ', '', $data['CustomerName']) : '';
        $customerID = isset($data['CustomerId']) ? $data['CustomerId'] : '';
        $accountType = isset($data['AccountType']) ? $data['AccountType'] : '1';
        $accountToken = isset($data['AccountToken']) ? $data['AccountToken'] : '';
        if (isset($data['type'])) {
            if ($data['type'] == 'Authorize') {
                $contenthash = $this->site_id . $this->session_id . $this->merchant_key . $accountNumber . $amount . $currency;
            } else if ($data['type'] == 'AuthorizeToken') {
                $contenthash = $this->site_id . $this->session_id . $this->merchant_key . $accountToken . $amount . $currency;
            } else if ($data['type'] == 'RegisterToken') {
                $contenthash = $this->site_id . $this->session_id . $accountNumber . $customerName . $customerID . $accountType;
            }
        }

        $curl = curl_init();
        curl_setopt_array($curl, array(
        CURLOPT_URL => $this->payment_api_url . "GetHash?contenthash=" . $contenthash,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json",
            "SiteId: " . $this->site_id
        ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);

        return response()->json([
            'hash'=> str_replace('"', '', $response),
            'error' => $response && strpos($response, 'Message') === false ? false : true,
            'code'=>200
        ],200);
    }

    private function _gethash($data = [])
    {
        $contenthash = '';
        $accountNumber = isset($data['AccountNumber']) ? $data['AccountNumber'] : '';
        $amount = isset($data['Amount']) ? $data['Amount'] : '';
        $currency = isset($data['Currency']) ? $data['Currency'] : '840';
        $customerName = isset($data['CustomerName']) ? str_replace(' ', '', $data['CustomerName']) : '';
        $customerID = isset($data['CustomerId']) ? $data['CustomerId'] : '';
        $accountType = isset($data['AccountType']) ? $data['AccountType'] : '1';
        $accountToken = isset($data['AccountToken']) ? $data['AccountToken'] : '';
        if (isset($data['type'])) {
            if ($data['type'] == 'Authorize') {
                $contenthash = $this->site_id . $this->session_id . $this->merchant_key . $accountNumber . $amount . $currency;
            } else if ($data['type'] == 'AuthorizeToken') {
                $contenthash = $this->site_id . $this->session_id . $this->merchant_key . $accountToken . $amount . $currency;
            } else if ($data['type'] == 'RegisterToken') {
                $contenthash = $this->site_id . $this->session_id . $accountNumber . $customerName . $customerID . $accountType;
            }
        }

        $curl = curl_init();
        curl_setopt_array($curl, array(
        //CURLOPT_URL => $this->payment_api_url . "GetHash?contenthash=" . $contenthash,
        CURLOPT_URL => $this->payment_api_url . "Auth/GetHash?contenthash=" . $contenthash,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json",
            "SiteId: " . $this->site_id
        ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);

        return $response;
    }

    public function gethash2()
    {
        $data['type'] = 'Authorize';
        $data['AccountNumber'] = '4709190638740477';
        $data['Amount'] = '100';
        $data['Currency'] = '840';

        $response = $this->_gethash($data);
        return response()->json([
            'hash'=> str_replace('"', '', $response),
            'error' => $response && strpos($response, 'Message') === false ? false : true,
            'code'=>200
        ],200);
    }

    public function registertoken(Request $request)
    {
        $data = $request->json()->all();
        $data['type'] = 'RegisterToken';

        $messageHash = $this->_gethash($data);
        if (!$messageHash || strpos($messageHash, 'Message') === true) {
            return response()->json([
                'message'=> 'Message hash failure',
                'error' => true,
                'code'=>200
            ],200);
        }
        $messageHash = str_replace('"', '', $messageHash);

        $MerchantKey = $this->merchant_key;
        $AccountNumber = isset($data['AccountNumber']) ? $data['AccountNumber'] : '';
        $ExpirationMonth = isset($data['ExpirationMonth']) ? $data['ExpirationMonth'] : '';
        $ExpirationYear = isset($data['ExpirationYear']) ? $data['ExpirationYear'] : '';
        $CustomerName = isset($data['CustomerName']) ? $data['CustomerName'] : '';
        $IsDefault = isset($data['IsDefault']) ? $data['IsDefault'] : false;
        $CustomerId = isset($data['CustomerId']) ? $data['CustomerId'] : '';
        $AccountType = isset($data['AccountType']) ? $data['AccountType'] : '';
        $CustomerEmail = isset($data['CustomerEmail']) ? $data['CustomerEmail'] : '';
        $ZipCode = isset($data['ZipCode']) ? $data['ZipCode'] : '';

        $curl = curl_init();
        curl_setopt_array($curl, array(
        CURLOPT_URL => $this->payment_api_url . "RegisterToken",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS =>"{     \r\n\t\"MerchantKey\": \"$MerchantKey\", \r\n    \"AccountNumber\": \"$AccountNumber\", \r\n    \"ExpirationMonth\": \"$ExpirationMonth\", \r\n    \"ExpirationYear\": \"$ExpirationYear\",  \r\n    \"CustomerName\": \"$CustomerName\",  \r\n    \"IsDefault\": $IsDefault,  \r\n    \"CustomerId\": \"$CustomerId\",\r\n    \"AccountType\": \"$AccountType\", \r\n    \"CustomerEmail\": \"$CustomerEmail\", \r\n    \"ZipCode\": \"$ZipCode\" \r\n}\r\n",
        CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json",
            "SessionId: " . $this->session_id,
            "SiteId: " . $this->site_id,
            "MessageHash: " . $messageHash
        ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);

        if (!$response || strpos($response, 'Message') === true) {
            return response()->json([
                'message'=> 'RegisterToken failure',
                'error' => true,
                'code'=>200
            ],200);
        }

        $response = json_decode($response);
        //$response = $response->Account;
        return response()->json([
            '$messageHash' => $messageHash,
            'data' => $response,
            'error' => false,
            'code'=>200
        ],200);
    }

    public function authorizepay(Request $request)
    {
        $data = $request->json()->all();
        $data['type'] = 'Authorize';

        $messageHash = $this->_gethash($data);
        if (!$messageHash || strpos($messageHash, 'Message') === true) {
            return response()->json([
                'message'=> 'Message hash failure',
                'error' => true,
                'code'=>200
            ],200);
        }
        $messageHash = str_replace('"', '', $messageHash);

        $MerchantKey = $this->merchant_key;
        $AccountNumber = isset($data['AccountNumber']) ? $data['AccountNumber'] : '';
        $ExpirationMonth = isset($data['ExpirationMonth']) ? $data['ExpirationMonth'] : '';
        $ExpirationYear = isset($data['ExpirationYear']) ? $data['ExpirationYear'] : '';
        $CustomerName = isset($data['CustomerName']) ? $data['CustomerName'] : '';
        $IsDefault = isset($data['IsDefault']) ? $data['IsDefault'] : false;
        $CustomerId = isset($data['CustomerId']) ? $data['CustomerId'] : '';
        $AccountType = isset($data['AccountType']) ? $data['AccountType'] : '';
        $CustomerEmail = isset($data['CustomerEmail']) ? $data['CustomerEmail'] : '';
        $ZipCode = isset($data['ZipCode']) ? $data['ZipCode'] : '';
        $Amount = isset($data['Amount']) ? $data['Amount'] : '';
        $Currency = isset($data['Currency']) ? $data['Currency'] : '840';
        $Tax = isset($data['Tax']) ? $data['Tax'] : '0';
        $Invoice = isset($data['Invoice']) ? $data['Invoice'] : '0';
        $Transaction_Detail = isset($data['Transaction_Detail']) ? $data['Transaction_Detail'] : '';
        $ReserveFunds = isset($data['ReserveFunds']) ? $data['ReserveFunds'] : false;
        $CVV = isset($data['CVV']) ? $data['CVV'] : '';

        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => $this->payment_api_url . "Autorize",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS =>"{\r\n    \"MerchantKey\": \"$MerchantKey\",\r\n    \"AccountNumber\": \"$AccountNumber\",\r\n    \"ExpirationMonth\": \"$ExpirationMonth\",\r\n    \"ExpirationYear\": $ExpirationYear,\r\n    \"CustomerName\": \"$CustomerName\",\r\n    \"IsDefault\": $IsDefault,\r\n    \"CustomerId\": \"$CustomerId\",\r\n    \"AccountType\": \"$AccountType\",\r\n    \"CustomerEmail\": \"$CustomerEmail\",\r\n    \"ZipCode\": \"$ZipCode\",\r\n    \"Amount\": \"$Amount\",\r\n    \"Currency\": \"$Currency\",\r\n    \"Tax\": \"$Tax\",\r\n    \"Invoice\": \"$Invoice\",\r\n    \"Transaction_Detail\": \"$Transaction_Detail\",\r\n    \"ReserveFunds\": $ReserveFunds,\r\n    \"CVV\":\"$CVV\"\r\n}",
          CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json",
            "SessionId: " . $this->session_id,
            "SiteId: " . $this->site_id,
            "MessageHash: " . $messageHash
          ),
        ));
        $response = curl_exec($curl); 
        curl_close($curl);

        if (!$response || strpos($response, 'Message') === true) {
            return response()->json([
                'message'=> 'Autorize failure',
                'error' => true,
                'code'=>200
            ],200);
        }
        
        $response = json_decode($response);
        //$response = $response->Account;
        return response()->json([
            'data' => $response,
            'error' => false,
            'code'=>200
        ],200);
    }

    private function _authorizepay($data = [])
    {
        $data['type'] = 'Authorize';

        $messageHash = $data['messageHash'];
        $MerchantKey = $this->merchant_key;
        $AccountNumber = isset($data['AccountNumber']) ? $data['AccountNumber'] : '';
        $ExpirationMonth = isset($data['ExpirationMonth']) ? $data['ExpirationMonth'] : '';
        $ExpirationYear = isset($data['ExpirationYear']) ? $data['ExpirationYear'] : '';
        $CustomerName = isset($data['CustomerName']) ? $data['CustomerName'] : '';
        $IsDefault = isset($data['IsDefault']) ? $data['IsDefault'] : false;
        $CustomerId = isset($data['CustomerId']) ? $data['CustomerId'] : '';
        $AccountType = isset($data['AccountType']) ? $data['AccountType'] : '';
        $CustomerEmail = isset($data['CustomerEmail']) ? $data['CustomerEmail'] : '';
        $ZipCode = isset($data['ZipCode']) ? $data['ZipCode'] : '';
        $Amount = isset($data['Amount']) ? $data['Amount'] : '';
        $Currency = isset($data['Currency']) ? $data['Currency'] : '840';
        $Tax = isset($data['Tax']) ? $data['Tax'] : '0';
        $Invoice = isset($data['Invoice']) ? $data['Invoice'] : '0';
        $Transaction_Detail = isset($data['Transaction_Detail']) ? $data['Transaction_Detail'] : '';
        $ReserveFunds = isset($data['ReserveFunds']) ? $data['ReserveFunds'] : false;
        $CVV = isset($data['CVV']) ? $data['CVV'] : '';

        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => $this->payment_api_url . "Payment/Autorize",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS =>"{\r\n    \"MerchantKey\": \"$MerchantKey\",\r\n    \"AccountNumber\": \"$AccountNumber\",\r\n    \"ExpirationMonth\": \"$ExpirationMonth\",\r\n    \"ExpirationYear\": $ExpirationYear,\r\n    \"CustomerName\": \"$CustomerName\",\r\n    \"IsDefault\": $IsDefault,\r\n    \"CustomerId\": \"$CustomerId\",\r\n    \"AccountType\": \"$AccountType\",\r\n    \"CustomerEmail\": \"$CustomerEmail\",\r\n    \"ZipCode\": \"$ZipCode\",\r\n    \"Amount\": \"$Amount\",\r\n    \"Currency\": \"$Currency\",\r\n    \"Tax\": \"$Tax\",\r\n    \"Invoice\": \"$Invoice\",\r\n    \"Transaction_Detail\": \"$Transaction_Detail\",\r\n    \"ReserveFunds\": $ReserveFunds,\r\n    \"CVV\":\"$CVV\"\r\n}",
          CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json",
            "SessionId: " . $this->session_id,
            "SiteId: " . $this->site_id,
            "MessageHash: " . $messageHash
          ),
        ));
        $response = curl_exec($curl); 
        curl_close($curl);

        if (!$response || strpos($response, 'Message') === true) {
            return false;
        }

        return json_decode($response);
    }

    public function authorizetoken(Request $request)
    {
        $data = $request->json()->all();

        $data['type'] = 'Authorize';
        $messageHash = $this->_gethash($data);
        if (!$messageHash || strpos($messageHash, 'Message') === true) {
            return response()->json([
                'message'=> 'Authorize Messagehash failure',
                'error' => true,
                'code'=>200
            ],200);
        }
        $messageHash = str_replace('"', '', $messageHash);

        $data['messageHash'] = $messageHash;
        $authorize = $this->_authorizepay($data);
        if (!isset($authorize->AccountToken)) {
            return response()->json([
                'message'=> 'Authorize failure',
                'error' => true,
                'code'=>200
            ],200);
        }
        $AccountToken = $authorize->AccountToken;

        $data['type'] = 'AuthorizeToken';
        $data['AccountToken'] = $AccountToken;
        $messageHash = $this->_gethash($data);
        if (!$messageHash || strpos($messageHash, 'Message') === true) {
            return response()->json([
                'message'=> 'AuthorizeToken Messagehash failure',
                'error' => true,
                'code'=>200
            ],200);
        }
        $messageHash = str_replace('"', '', $messageHash);

        $MerchantKey = $this->merchant_key;
        $Amount = isset($data['Amount']) ? $data['Amount'] : '';
        $Currency = isset($data['Currency']) ? $data['Currency'] : '840';
        $Tax = isset($data['Tax']) ? $data['Tax'] : '0';
        $Invoice = isset($data['Invoice']) ? $data['Invoice'] : '0';
        $Transaction_Detail = isset($data['Transaction_Detail']) ? $data['Transaction_Detail'] : '';
        $HoldFunds = isset($data['HoldFunds']) ? $data['HoldFunds'] : false;
        $EfectiveDate = isset($data['EfectiveDate']) ? $data['EfectiveDate'] : date('Y-m-d');

        $curl = curl_init();
        curl_setopt_array($curl, array(
        //CURLOPT_URL => $this->payment_api_url . "AutorizeToken",
        CURLOPT_URL => $this->payment_api_url . "Payment5/AutorizeToken",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS =>"{\r\n\t\"MerchantKey\": \"$MerchantKey\",\r\n\t\"AccountToken\": \"$AccountToken\",\r\n\t\"Amount\": \"$Amount\",\r\n\t\"Currency\": \"$Currency\",\r\n\t\"Tax\": \"$Tax\",\r\n\t\"Invoice\": \"$Invoice\",\r\n\t\"Transaction_Detail\": \"$Transaction_Detail\",\r\n\t\"HoldFunds\": $HoldFunds,\r\n\t\"EfectiveDate\": \"$EfectiveDate\"\r\n}\r\n",
        CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json",
            "SessionId: " . $this->session_id,
            "SiteId: " . $this->site_id,
            "MessageHash: " . $messageHash
        ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);

        if (!$response || strpos($response, 'Message') === true) {
            return response()->json([
                'message'=> 'RegisterToken failure',
                'error' => true,
                'code'=>200
            ],200);
        }

        $response = json_decode($response);
        //$response = $response->Account;
        return response()->json([
            '$messageHash' => $messageHash,
            'data' => $response,
            'error' => false,
            'code'=>200
        ],200);
    }

}