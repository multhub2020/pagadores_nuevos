<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Models\Informaciones;
use \App\Models\Contactos;
use \App\Models\Usuarios;
use \App\Models\Parametros;

use \App\Models\Encuentas;
use \App\Models\Encuentasrespuestas;
use \App\Models\Encuentasusuarios;


use \App\Models\Eventos;
use \App\Models\Eventosrespuestas;
use \App\Models\Eventosusuarios;


use \App\Models\Historialpushusuarios;
use \App\Models\Historialpush;
//use \App\Models\Notificaciones;
use \App\Models\Notificaciones2;

use \App\module;

use DB;
use Mail;

use Twilio\Rest\Client;

use App\Http\Controllers\Controller;
use JWTAuth;

class NotificacionesController extends Controller
{
        /*
        *   LISTAR NOTIFICACIONES
        *
        */
		public function listarnotificaciones(Request $request){
        	$data  = $request->json()->all();
        	/////////////////////Encuentas////////////////////////////////
            	$data1 = Encuentas::where('activo',1)->get(); 
            	$array_encuesta = array();
            	foreach ($data1 as $key) {
            		$contar_data1 = Encuentasusuarios::where('activo',1)->where('encuenta_id',  $key['id'])->where('usuario_id',  $data['usuarioid'])->count();
            		if($contar_data1==0){
            			$array_encuesta[]= $key['id'];
            		}
            	}
            	$data1_1 = Encuentas::where('activo',1)->whereIn('id', $array_encuesta)->get(); 
        	/////////////////////FIN Encuentas////////////////////////////////
            ////////////NOTIFICACIONES PUSH///////////
                $data2 = Historialpushusuarios::where('activo',1)->where('usuario_id', $data['usuarioid'])->get();
                $data3 = Historialpush::where('activo',1)->get(); 

            ////////////FIN NOTIFICACIONES PUSH///////////

            /////////////////////Encuentas////////////////////////////////
                $data4 = Eventos::where('activo',1)->get(); 
                $array_envento = array();
                foreach ($data4 as $key4) {
                    $contar_data4 = Eventosusuarios::where('activo',1)->where('evento_id',  $key4['id'])->where('usuario_id',  $data['usuarioid'])->count();
                    if($contar_data4==0){
                        $array_envento[]= $key4['id'];
                    }
                }
                $data2_1 = Eventos::where('activo',1)->whereIn('id', $array_envento)->get(); 
            /////////////////////FIN Encuentas////////////////////////////////

                $notificaciones_all_1 = Notificaciones2::where('activo',1)->orderBy('created', 'desc')->get();
                $notificaciones_all_2 = $notificaciones_all_1->toArray();
                $contar_not = 0;
                foreach ($notificaciones_all_2 as $key) {
                          if($key['tipo']=='1'){
                                $ver_cont = Encuentasusuarios::where('activo',1)->where('encuenta_id',  $key['id'])->where('usuario_id',  $data['usuarioid'])->count();
                                if($ver_cont!=0){
                                    $notificaciones_all_2[$contar_not]['activo']='2';
                                }
                    }else if($key['tipo']=='2'){
                                $ver_cont = Eventosusuarios::where('activo',1)->where('evento_id',  $key['id'])->where('usuario_id',  $data['usuarioid'])->count();
                                if($ver_cont!=0){
                                    $notificaciones_all_2[$contar_not]['activo']='2';
                                }
                    }else if($key['tipo']=='3'){

                    }else if($key['tipo']=='4'){
                             if($key['usuario_id']!=$data['usuarioid']){
                                    $notificaciones_all_2[$contar_not]['activo']='2';
                            }
                    }
                    $contar_not++;
                }
        	return response()->json([
                        'encuestas'=>$data1_1,
                        'eventos'=>$data2_1,
                        'notificaciones'=>$data2,
                        'notificaciones2'=>$data3,
                        'notificaciones_all'=>$notificaciones_all_2,
                        'code'=>200                       
                    ],200);

    	}//fin function
        /*
        *   ENCUESTA NOTIFICACIONES
        *
        */
    	public function encuestalist(Request $request){
        	$data  = $request->json()->all();
            $data1_1 = Encuentas::with('respuestas')->where('activo',1)->where('id', $data['encuestaid'])->get(); 
        	return response()->json([
                        'datos'=>$data1_1,
                        'code'=>200                       
                    ],200);

    	}//fin function
    	public function encuestaadd(Request $request){
        	$data  = $request->json()->all();
            $id_user =  DB::table('encuentasusuarios')->insertGetId(['usuario_id' => $data['usuarioid'],
                                                                     'encuenta_id' => $data['encuestaid'],
                                                                     'encuentasrespuesta_id' => $data['encuestaresid'],
                                                                     'observacion' => $data['encuestaresob'],
                                                                     'created'    => date('Y-m-d'),
                                                                    ]
                                                                   );
        	return response()->json([
                        'msg'=>"La encuesta fue enviada!!",
                        'code'=>200                       
                    ],200);

    	}//fin function



        public function eventolist(Request $request){
            $data  = $request->json()->all();
            $data1_1 = Eventos::with('respuestas')->where('activo',1)->where('id', $data['eventoid'])->get(); 
            return response()->json([
                        'datos'=>$data1_1,
                        'code'=>200                       
                    ],200);

        }//fin function
        public function eventoadd(Request $request){
            $data    = $request->json()->all();
            $data1_1 = Usuarios::where('id', $data['usuarioid'])->get(); 
            $data1_2 = $data1_1->toArray();
            $hijo    = $data1_2[0]['nombre_apellido'];
            $papa    = $data1_2[0]['userinvitador_id'];


            $data2_1 = Eventos::where('id',  $data['eventoid'])->get(); 
            $data2_2 = $data2_1->toArray();
            $evento   = $data2_2[0]['pregunta'];

            $usr = Usuarios::where('id',$papa)->get(); 
            foreach ($usr as $key) { 
                    ///AVISAR AL USUARIO POR NOTIFICACIONES//
                    $token = "AAAAb64Jkyc:APA91bEpeNCSxSnurLWXf0yJaORJfE3SEUe6PInDm_oR0eIGSUvLCj8fxRIRJ9BmfWWdW2FG8VPy2UTS69k3xCxCk4U_FLDx1T62FBZxq83IKzcU4UzIqb2-tdMRkvKLxglCu27GBrtu";//"738083342753";//TOKEN DE SENSER ID
                    $not   = "";//"DATABASE OBJECT NOTIFICATION";
                    //Datos/
                    $to_android = $key['push_token'];//$usuario[0]['User']['push_token'];//Datos del usuario
                    $to_ios     = $key['push_token'];//$usuario[0]['User']['push_token'];//Datos del usuario
                    $platform   = $key['push_platf'];//Datos del usuario
                    $titulo     = "¡Aviso de evento!";
                    $mensaje    = "Pagadores te informa que ".$hijo." solicita ayuda para asistir al evento ".$evento;
                    //echo $mensaje;
                    $data2    = null;
                    $headers = [
                        "Authorization:key=".$token." ",
                        'Content-Type: application/json'
                    ];
                    if($platform === 'ios') {
                        $data2 = [
                            'to' => $to_ios,
                            'notification' => [
                                'body'   => $mensaje,
                                'title'  => $titulo,
                            ],
                            "data" => [// aditional data for iOS
                                "extra-key" => "extra-value",
                            ],
                            'notId' =>$not,//unique id for each notification
                        ];
                    } elseif ($platform === 'android') {
                        $data2 = [
                            'to' => $to_android,
                            'data' => [
                                'body'   => $mensaje,
                                'title'  => $titulo,
                            ]
                        ];
                    }
                    $ch = curl_init();
                    curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
                    //curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
                    curl_setopt( $ch,CURLOPT_POST, true );
                    curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
                    curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
                    curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
                    curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $data2 ) );
                    curl_setopt($ch, CURLOPT_FAILONERROR, TRUE);
                    $result = curl_exec($ch);
                    curl_close( $ch );

                    
            $id_papa =  DB::table('historialpushusuarios')->insertGetId(['usuario_id'  => $papa,
                                                                         'titulo'      => $titulo,
                                                                         'texto'       => $mensaje,
                                                                         'created'     => date('Y-m-d'),
                                                                         ]
                                                                        );
            }            
            $id_user =  DB::table('eventosusuarios')->insertGetId(['usuario_id'           => $data['usuarioid'],
                                                                   'evento_id'            => $data['eventoid'],
                                                                   'eventosrespuesta_id'  => $data['eventoresid'],
                                                                   'observacion'          => $data['eventoresob'],
                                                                   'created'              => date('Y-m-d'),
                                                                    ]
                                                                   );
            return response()->json([
                        'msg'=>"La ayuda fue solicitada!!",
                        'code'=>200                       
                    ],200);

        }//fin function
        public function eventoconf(Request $request){
            $data    = $request->json()->all();
            $id_user =  DB::table('eventosusuarios')->insertGetId(['usuario_id'           => $data['usuarioid'],
                                                                   'evento_id'            => $data['eventoid'],
                                                                   'eventosrespuesta_id'  => "0",
                                                                   'observacion'          => "",
                                                                   'asiste'               => $data['confirma'],
                                                                   'created'              => date('Y-m-d'),
                                                                    ]
                                                                   );
            return response()->json([
                        'msg'=>"Tu asistencia para el evento fue confirmada!!",
                        'code'=>200                       
                    ],200);

        }//fin function
    


}
?>