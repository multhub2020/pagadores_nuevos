<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Models\Informaciones;
use \App\Models\Parametros;
use \App\module;


use \App\Models\Ciudad;
use \App\Models\Provincia;
use \App\Models\Industrias;



use DB;
use Mail;


use App\Http\Controllers\Controller;
use JWTAuth;

class InformacionesController extends Controller
{
    
    public function inforpoliticas(Request $request){
        $data = $request->json()->all();
        $data = Informaciones::where('categoria',$data['categoria'])->first();         
        return response()->json([
            'texto'=>$data->texto,
            'code'=>200                      
        ],200);
    }//fin function

     public function inforcondiciones(Request $request){
        $data = $request->json()->all();
        $data = Informaciones::where('categoria',$data['categoria'])->first();         
        return response()->json([
            'texto'=>$data->texto,
            'code'=>200                      
        ],200);
    }//fin function



    public function listciudad(Request $request){
        $data = $request->json()->all();
        $datas = Ciudad::orderBy('id', 'asc')->get()->toArray();         
        return response()->json([
            'datas'=>$datas,
            'code'=>200                      
        ],200);
    }//fin function

     public function listprovin(Request $request){
        $data = $request->json()->all();
        $datas = Provincia::orderBy('id', 'asc')->get()->toArray();         
        return response()->json([
            'datas'=>$datas,
            'code'=>200                      
        ],200);
    }//fin function

    public function listindust(Request $request){
        $data = $request->json()->all();
        $datas = Industrias::orderBy('id', 'asc')->get()->toArray();         
        return response()->json([
            'datas'=>$datas,
            'code'=>200                      
        ],200);
    }//fin function
 

}//Fin class
