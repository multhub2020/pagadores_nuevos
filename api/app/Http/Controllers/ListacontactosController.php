<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Models\Informaciones;
use \App\Models\Contactos;
use \App\Models\Usuarios;
use \App\Models\Parametros;
use \App\module;

use DB;
use Mail;

use Twilio\Rest\Client;

use App\Http\Controllers\Controller;
use JWTAuth;

class ListacontactosController extends Controller
{
    
    public function listacontactosadd(Request $request){
        $data = $request->json()->all();
        if(isset($data['tipo'])){
            if($data['tipo']==1){
                $deletedRows = Contactos::where('usuario_id', $data['usuarioid'])->delete();   
            }
        }else{
            $deletedRows = Contactos::where('usuario_id', $data['usuarioid'])->delete();    
        }
        //print_r($data['datos']);
        foreach ($data['datos'] as $key) {
            $telefono  = isset($key['phoneNumbers'][0]['value'])?$key['phoneNumbers'][0]['value']:"";
            $email     = isset($key['emails'][0]['value'])?$key['emails'][0]['value']:"";
            $nombres   = isset($key['displayName'])?$key['displayName']:"";
            $telefono2 = str_replace ("-", "", $telefono);
            $telefono3 = str_replace ("(", "", $telefono2);
            $telefono4 = str_replace (")", "", $telefono3);
            $telefono5 = str_replace (" ", "", $telefono4);
            $telefono6 = str_replace ("+1", "", $telefono5);
            $id_user   =  DB::table('contactos')->insertGetId(['usuario_id' => $data['usuarioid'],
                                                               'nombres'    => $nombres,
                                                               'telefono'   => $telefono6,
                                                               'email'      => $email,
                                                              // 'comprobar'  => $data['datos']
                                                            ]
                                                           );
        }
        return response()->json([
            'msg'=>"Los datos fueron guardados",
            'code'=>200                      
        ],200);
    }//fin function

     public function listacontactoslistar(Request $request){
        $data = $request->json()->all();
        
        if(!isset($data['page'])){
                $data2 = Contactos::where('usuario_id',$data['usuarioid'])->where('activo','1')->get(); 
                        $contar = 0;
                        $data1  = array();
                        foreach($data2 as $key) {
                                if($key['email']!=""){
                                    $cantidad1 = Usuarios::where('email',   $key['email'])->count(); 
                                }else{
                                    $cantidad1 = 0;
                                }
                                if($key['telefono']!=""){
                                    $cantidad2 = Usuarios::where('telefono',$key['telefono'])->count(); 
                                }else{
                                    $cantidad2 = 0;
                                }
                                if($cantidad1 != 0 || $cantidad2 != 0){

                                }else{
                                    $data1[$contar]= $key;
                                    $contar++;
                                }
                        }   
                }else{
                        $data1 = Contactos::where('usuario_id',$data['usuarioid'])->where('activo','1')->paginate(20); 
                }
        return response()->json([
            'datos'=>$data1,
            'code'=>200                      
        ],200);
    }//fin function



    public function listacontactossend(Request $request){
        $data = $request->json()->all();
        //$data2    = Contactos::where('usuario_id',$data['usuarioid'])->whereIn('id',$data['datos'])->get(); 
        $data2    = Contactos::whereIn('id',$data['datos'])->get(); 
        //$deletedRows = Contactos::where('usuario_id', $data['usuarioid'])->delete();        
        //print_r($data2);
        $parametros  = Parametros::where('activo',1)->get(); 
        $parametros1  = $parametros->toArray(); 
        $parametroemail = $parametros1[0]['emailtexto'];
        $parametrosms   = $parametros1[0]['sms'];
        $variable1 = $data2->toArray();
        $datos1   = Usuarios::find($data['usuarioid']);
        $mensaje1 = "Las invitaciones fueron enviadas ";
        $mensaje2 = "";
        $code = '200';
        //$genetica = $datos1->genetica==''?$data['usuarioid']:$datos1->genetica.'-'.$data['usuarioid'];
        if(isset($datos1->genetica)){
            $genetica = $datos1->genetica==''?$data['usuarioid']:$datos1->genetica.'-'.$data['usuarioid'];
        }else{
            $genetica = $data['usuarioid'];
        }
        foreach ($variable1 as $key) {
                $code = 0;
                $data['email']    = "";
                $data['telefono'] = "";
                if($key['email']!=""){
                    $cantidad1 = Usuarios::where('email',   $key['email'])->count(); 
                }else{
                    $cantidad1 = 0;
                }
                if($key['telefono']!=""){
                    $cantidad2 = Usuarios::where('telefono',$key['telefono'])->count(); 
                }else{
                    $cantidad2 = 0;
                }
                //echo $cantidad1."---".$cantidad2."<br>";
                if($cantidad1 != 0 || $cantidad2 != 0){
                        $msg = "El Email / T茅lefono ya se encuentra registrado ";
                        $code = 0;
                }else{

                                if(filter_var($key['email'], FILTER_VALIDATE_EMAIL)) {
                                        $code = 1;
                                }else{
                                    if(preg_match("/^[0-9]{10}$/",  $key['telefono'])){
                                        $code = 2;
                                    } 
                                }
                                    if($code==1){
                                                        $data['email'] = $key['email'];
                                                        $msg     = "El Email de la invitaci贸n fue enviado";
                                                        $subject = 'Invitaci贸n app Pagadores';
                                                        $email   = $key['email'];
                                                        $mensaje = "Estimado/a ".$key['email'].", ".$parametroemail;
                                                        $d = ['mensaje'=> $mensaje];
                                                        Mail::send('emails.invitacion', $d, function ($message) use ($subject, $email){
                                                            $message->subject($subject);
                                                            $message->to($email);
                                                        });
                                }else if($code==2){
                                                        $data['telefono'] = $key['telefono'];
                                                        // Send an SMS using Twilio's REST API and PHP
                                                        //$sid     = "AC8a3c3fdc6f27ea2df70a41548219847b";//"AC8a3c3fdc6f27ea2df70a41548219847b"; // Your Account SID from www.twilio.com/console
                                                        //$token   = "2c71c43f96b525676e8ee9705d23334a";//"2c71c43f96b525676e8ee9705d23334a"; // Your Auth Token from www.twilio.com/console
                                                        
                                                        $sid       = "AC6b675d394ea0f83b871e53b7d69fb067";
                                                        $token     = "926772c541329498da0b780aa0725c1f";  
                                                        $telefono2 = "+13126464282";

                                                        $msg     = "El SMS de la invitaci贸n fue enviado";
                                                        $client  = new Client($sid, $token);
                                                        $mensaje = "Estimado/a ".$key['telefono'].", ".$parametrosms;
                                                        try {
                                                                    $message = $client->messages->create(
                                                                            "+1".$key['telefono'], // Text this number
                                                                        [
                                                                            'from' => $telefono2, // From a valid Twilio number
                                                                            'body' => $mensaje
                                                                        ]
                                                                    );
                                                        }catch(\Twilio\Exceptions\RestException $e){        
                                                            $mensaje2 = ", +1".$key['telefono'];
                                                            
                                                        }
                                }
                                if($code!=0){
                                $id_user  = DB::table('usuarios')->insertGetId(['fechaingreso'        => date('Y-m-d'),
                                                                                'role_id'             => 2,
                                                                                'email'               => $data['email'],
                                                                                'telefono'            => $data['telefono'],
                                                                                'userinvitador_id'    => $data['usuarioid'],
                                                                                'genetica'            => $genetica
                                                                               ]
                                                                            );
                                //echo $code."---".$id_user;
                                }
                }//fin else
        }
        if($mensaje2!=""){
            $mensaje1 = " Número de teléfono (".$mensaje2.") no es un número de teléfono móvil (celular)";
            $code = '201';
        }
        return response()->json([
            'msg'=>$mensaje1,
            'code'=>200                      
        ],200);
    }//fin function
 

}//Fin class
