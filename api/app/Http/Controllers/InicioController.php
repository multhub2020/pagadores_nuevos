<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Models\Usuarios;
use \App\Models\Publicidades;
use \App\Models\Parametros;
use \App\Models\Deudas;
use \App\Models\InstruccionAcreedores;
use \App\module;


use DB;
use Mail;

use Twilio\Rest\Client;

use App\Http\Controllers\Controller;
use JWTAuth;

use App\Traits\DataCredito;

class InicioController extends Controller
{
    use DataCredito;


    public function addayudas(Request $request){
        $data = $request->json()->all();
        $id_user  = DB::table('ayudas')->insertGetId([ 'usuario_id'    => $data['usuario_id'],
                                                       'denominacion'  => $data['denominacion']
                                                     ]
                                                    );
        return response()->json([
            'code' => 200
        ], 200);
    }


    public function addcalificaciones(Request $request){
        $data = $request->json()->all();
        $id_user  = DB::table('calificaciones')->insertGetId([ 'usuario_id' => $data['usuario_id'],
                                                               'puntos'     => $data['puntos']
                                                             ]
                                                            );
        return response()->json([
            'code' => 200
        ], 200);
    }




    public function inicio(Request $request)
    {
        //$this->getXmlToDb();
        $data = $request->json()->all();

        //$data['customer']   = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIyIiwianRpIjoiNDNkZWFiODcwNGVkMGNmMzQ4Yjk3NDZkOTBjNzE3ZGQ1MWYwYjFjYzkzN2ZiYjMyMjllNTBkYWI0ZWIwYzViN2IyNjJiYmU0MTZjNGY4ZDkiLCJpYXQiOjE2MjQxNTc2OTIsIm5iZiI6MTYyNDE1NzY5MiwiZXhwIjoxNjI1NDUzNjkyLCJzdWIiOiIiLCJzY29wZXMiOlsiKiJdfQ.sj1qaMeJVx4CYfLBrpaj2B9eeMWgXmftyU9ZiKCgmbAPsLdygjg0-YK7hDLvGPREacwXgAIM3m2l0gsHBdFGyMdx2GwWuPHCKNL2eZcdMTxIq-pv5G0bn9yTKouOsRoU9BYIqoF8VYe-mQdJlawosJh8nrkVyHMX9qBY2LESZNIR6ivmW-V5XeLgzYh0Kgpfn7nLr8XaaQDMf_gYUNFleoR3aPRBdYJi6ynC90FrnE9womK31Yt9DeTUlUUc28RqwQoiyyFmwANEv7EP42X6gd4QsTVpe298tGr5s7_uNNu5kn2gp4qQgxHKuhItUf05bunl8vJCDRoCdEnhDZKOSR4VIW2sPAS62S7bIsQJgrtoxO7Cw576i6sw5k6AE6YhwuRzEG29U-n-UFhL6XcgfgsOIlE4UhnfOo8EoNEN5and-eR7q-DzNeannfAAfLIUiP-w7NJAdEOWEcErsWcg6RjP1V0xN_ZiQedsCR6ZO_eWLwR36_LgWJXrMsQHizDnyiCj0_B9ZDxRcMIvu0TByaLc7jEQuaVxQyjZbhmUTjsUxW-sN9yf84DR7V0U_53i37yo087EU5QWAIBNzD57WriivpaeSn7xUscHMEHoRaw3EJLPAeLtYvEzFi3jVou5HT1wVC_63s2cl-6ySd6QTiwiiWr5Ls2BBbTcdeJd_ZU";

        //$data['customerId'] = "27";

        //$data['usuarioid']  = "107";

        $datosusauriodeudas  = Usuarios::where('id', $data['usuarioid'])->get()->toArray();
        $cedulausuariodeudas = isset($datosusauriodeudas[0]['ci']) ? $datosusauriodeudas[0]['ci'] : 0;

        $datosdeudassinresolver  = Deudas::where('estatus', 0)->where('cedula', $cedulausuariodeudas)->orderBy('created', 'asc')->get()->toArray();
        $cuentadeudassinresolver = count($datosdeudassinresolver);

        $totaldeudaactual  = 0;
        $totaldeudamensual = 0;
        $solicitudespendientes = array();


        ///AVISAR AL USUARIO POR NOTIFICACIONES//
        $token      = $data['customer']; //"738083342753";//TOKEN DE SENSER ID
        $customerid = $data['customerId']; //"738083342753";//TOKEN DE SENSER ID
        $headers = [
            "Authorization: Bearer " . $token,
            'Accept: application/json',
            'Content-Type: application/json'
        ];
        //print_r($headers);
        $result1 = array();
        $result2 = array();
        $result3 = array();
        if (isset($data['tipoaccion'])) {
            if ($data['tipoaccion'] == 2) {
                $url = "http://23.96.19.56/api/";
            } else {
                $url = "https://api1.pagadores.com/api/";
            }
        } else {
            $url = "https://api1.pagadores.com/api/";
        }
        //////////////////////////////////////get_all_creditors///////////////////////////////        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url . 'creditors?limit=1000');
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result1 = json_decode(curl_exec($ch));
        curl_close($ch);
        //print_r($result1);
        /*
                this.provider_usuario.listaplication(

                ---------$datosdeudas = Deudas::where('aplicacionid', $data['aplicacionid'])->get()->toArray();


                this.provider_menu.localizarinstruccion(

                --------creditor_id = $data['creditor_id'];
                        $idiomas    = 'ES';
                        if (isset($data['idiomas'])) {
                            $idiomas = $data['idiomas'];
                        }
                        $instruccionAcreedor = InstruccionAcreedores::where(['acreedor' => $creditor_id, 'idiomas' => $idiomas, 'activo' => 1])->get()->first();

        */
        //////////////////////////////////////get_pending_applications///////////////////////////////        
        $ch2 = curl_init();
        curl_setopt($ch2, CURLOPT_URL,  $url . 'get_pending_applications/' . $customerid . '?limit=1000');
        curl_setopt($ch2, CURLOPT_HTTPHEADER, $headers);
        //curl_setopt($ch2, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
        $result2 = json_decode(curl_exec($ch2));
        curl_close($ch2);
        //echo  $url . 'get_pending_applications/' . $customerid . '?limit=1000';
        //print_r($result2);
        $contra = 0;
        if (isset($result2->results)) {
            foreach ($result2->results as $key) {
                $datos[$contra]['application_id']        = $key->application_id;
                $datos[$contra]['product_type_id']       = $key->product_type_id;
                $datos[$contra]['customer_id']           = $key->customer_id;
                $datos[$contra]['account_number']        = $key->account_number;
                $datos[$contra]['requested_amount']      = $key->requested_amount;
                $datos[$contra]['currency']              = $key->currency;
                $datos[$contra]['purpose']               = isset($key->purpose) ? $key->purpose : '0';
                $datos[$contra]['requested_term']        = $key->requested_term;
                $datos[$contra]['product_resolution_id'] = $key->product_resolution_id;
                $datos[$contra]['creditor_id']           = $key->creditor_id;
                $datos[$contra]['aplication_date']       = $key->aplication_date;
                $datos[$contra]['status']                = $key->status;
                $datos[$contra]['last_status_date']      = $key->last_status_date;
                $datos[$contra]['offer_payoff_amount']   = $key->offer_payoff_amount;
                $datos[$contra]['initial_payment_offer'] = $key->initial_payment_offer;
                $datos[$contra]['installment_pay_num_offer'] = $key->installment_pay_num_offer;
                $datos[$contra]['installment_pay_amt_offer'] = $key->installment_pay_amt_offer;
                $datos[$contra]['initial_pay_creditor_conteroffer']         = $key->initial_pay_creditor_conteroffer;
                $datos[$contra]['installment_pay_creditor_num_conteroffer'] = $key->installment_pay_creditor_num_conteroffer;
                $datos[$contra]['installment_pay_creditor_amt_conteroffer'] = $key->installment_pay_creditor_amt_conteroffer;
                $datos[$contra]['amortization_schedule_type_id']            = isset($key->amortization_schedule_type_id) ? $key->amortization_schedule_type_id : '0';
                $datos[$contra]['purpamortization_schedule_type_idose']     = isset($key->purpamortization_schedule_type_idose) ? $key->purpamortization_schedule_type_idose : '0';
                $datos[$contra]['guarantor_id']          = $key->guarantor_id;
                $datos[$contra]['docu_signed']           = $key->docu_signed;
                $datos[$contra]['docu_signed_date']      = $key->docu_signed_date;
                $datos[$contra]['debt_principal_amount'] = $key->debt_principal_amount;
                $datos[$contra]['contractual_interest_amount']  = $key->contractual_interest_amount;
                $datos[$contra]['delinquent_interest_amount']   = $key->delinquent_interest_amount;
                $datos[$contra]['creditor_offer_payoff_amount'] = $key->creditor_offer_payoff_amount;
                $datos[$contra]['final_agree_debt_amount']      = $key->final_agree_debt_amount;
                $datos[$contra]['debt_Amount']                  = $key->debt_Amount;
                $datos[$contra]['monto_deuda']    = $key->debt_Amount;
                $datos[$contra]['n_cuotas']       = $key->installment_pay_num_offer;
                $datos[$contra]['application_id'] = $key->application_id;
                $datos[$contra]['tipo_deuda']     = $key->product_resolution_id;
                foreach ($result1->results as $key2) {
                    if ($key2->id == $datos[$contra]['creditor_id']) {
                        $datos[$contra]['identifica_banco'] = $key2->identification_number;
                        $datos[$contra]['banco_deuda']      = $key2->name;
                               if (($key->status == 'CAPOS' || $key->status == 'A')  && $key->product_type_id == 1) {
                        } else if (($key->status == 'CAPOS' || $key->status == 'A')  && $key->product_type_id == 2) {
                        } else if ($key->status == 'CO'    || $key->status == 'COL') {
                        } else if ($key->status == 'PP'    && $key->product_type_id == 1) {
                        } else if ($key->status == 'PP'    && $key->product_type_id == 2) {
                        } else if ($key->status == 'LC'    && $key->product_type_id == 2) {
                        } else if ($key->status == 'WSA'   && $key->product_type_id == 2) {
                        } else if ($key->status == 'AwaitingSignatureByTheCustomer') {
                        //} else if ($key->status == 'M') {
                        } else {
                            $datosdeudas = Deudas::orderBy('id', 'desc')->where('aplicacionid', $key->application_id)->get()->toArray();
                            //print_r($datosdeudas);
                            $datos[$contra]['banco_deuda'] = isset($datosdeudas[0]['banco_deuda']) ? $datosdeudas[0]['banco_deuda'] : "";
                        } //fin else
                    } //fin if
                }
                if ($key->status == 'CAPOS' || $key->status == 'PP' || $key->status == 'A') {
                    $instruccionAcreedor = InstruccionAcreedores::where(['acreedor' => $key->creditor_id, 'idiomas' => "ES", 'activo' => 1])->get()->toArray();
                    $datos[$contra]['creditor_instruccion'] = isset($instruccionAcreedor[0]['instruccion']) ? $instruccionAcreedor[0]['instruccion'] : "";
                }
                if (($key->status == 'CAPOS' || $key->status == 'A')   && $key->product_type_id == 1) {
                    $datos[$contra]['status_text'] = 'Pendiente de Pago Convenido';
                } else if (($key->status == 'CAPOS' || $key->status == 'A')   && $key->product_type_id == 2) {
                    $datos[$contra]['status_text'] = 'Aprobada';
                } else if ($key->status == 'CO' || $key->status == 'COL') {
                    $datos[$contra]['status_text'] = 'Contra Oferta';
                } else if ($key->status == 'PP' && $key->product_type_id == 1) {
                    $datos[$contra]['status_text'] = 'Pendiente de Pago Convenido';
                } else if ($key->status == 'PP' && $key->product_type_id == 2) {
                    $datos[$contra]['status_text'] = 'Aprobada';
                } else if ($key->status == 'LC' && $key->product_type_id == 2) {
                    $datos[$contra]['status_text'] = 'Aprobada por el Lender';
                } else if ($key->status == 'WSA' && $key->product_type_id == 2) {
                    $datos[$contra]['status_text'] = 'Pendiente por firma de contrato';
                } else if ($key->status == 'C') {
                    $datos[$contra]['status_text'] = 'Completado';
                } else if ($key->status == 'AwaitingSignatureByTheCustomer') {
                    $datos[$contra]['status_text'] = 'Pendiente por firma de contrato';
                //} else if ($key->status == 'M') {
                  //  $datos[$contra]['status_text'] = 'En proceso';
                } else {
                    $datos[$contra]['status_text'] = 'En proceso';
                }
                if ($key->offer_payoff_amount != 0) {
                    $totaldeudaactual += $key->offer_payoff_amount;
                } else {
                    $totaldeudaactual += $key->initial_payment_offer;
                }
                if ($key->offer_payoff_amount == 0) {
                    $totaldeudamensual += $key->installment_pay_creditor_amt_conteroffer;
                } else {
                    $totaldeudamensual += $key->installment_pay_amt_offer;
                }
                $contra++;
            }
        } else {
            $datos = array();
        }
        //print_r($datos);
        //////////////////////////////////////get_customer_accounts_by_customer///////////////////////////////        

        /*  $data       = null;
        $headers = [
            "Authorization:key=" . $token . " ",
            'Content-Type: application/json'
        ];
        $data = [];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url + 'customer_account/' + $customerid + '/accounts');
        //curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_FAILONERROR, TRUE);
        $result3 = curl_exec($ch);
        curl_close($ch); */
        $cuentaspendientes = [];
        $ch3 = curl_init();
        curl_setopt($ch3, CURLOPT_URL,  $url . 'customer_accounts/' . $customerid . '/accounts');
        curl_setopt($ch3, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch3, CURLOPT_RETURNTRANSFER, true);
        $result3 = json_decode(curl_exec($ch3));
        curl_close($ch3);
        if (isset($result3->results)) {
            foreach ($result3->results as $key => $value) {
                foreach ($value as $key2 => $value2) {
                    $cuentaspendientes[$key][$key2] = $value2;
                }
                foreach ($result1->results as $key3) {
                    if ($key3->id == $cuentaspendientes[$key]['creditor_id']) {
                        $cuentaspendientes[$key]['identifica_banco'] = $key3->identification_number;
                        $cuentaspendientes[$key]['banco_deuda']      = $key3->name;
                    }
                }
            }
        }

        return response()->json([
            'datosdeudassinresolver'  => $datosdeudassinresolver,
            'cuentadeudassinresolver' => $cuentadeudassinresolver,
            'totaldeudaactual'        => $totaldeudaactual,
            'totaldeudamensual'       => $totaldeudamensual,
            'solicitudespendientes'   => $datos,
            'cuentaspendientes'       => $cuentaspendientes,
            'cuentaspendientes2'      => array(),
            'code' => 200
        ], 200);
    } //fin 


    public function get_pending_applications_core(Request $request)
    {
        $data = $request->json()->all();


        ///AVISAR AL USUARIO POR NOTIFICACIONES//
        $token = "AAAAb64Jkyc:APA91bEpeNCSxSnurLWXf0yJaORJfE3SEUe6PInDm_oR0eIGSUvLCj8fxRIRJ9BmfWWdW2FG8VPy2UTS69k3xCxCk4U_FLDx1T62FBZxq83IKzcU4UzIqb2-tdMRkvKLxglCu27GBrtu"; //"738083342753";//TOKEN DE SENSER ID
        $data       = null;
        $headers = [
            "Authorization:key=" . $token . " ",
            'Content-Type: application/json'
        ];
        $data = [
            'to' => $to_ios,
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        //curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_FAILONERROR, TRUE);
        $result = curl_exec($ch);
        curl_close($ch);


        return response()->json([
            // 'solicitudespendientes'=> $solicitudespendientes,

            'code' => 200
        ], 200);
    }

    public function contraoferta(Request $request)
    {
        $data = $request->json()->all();

        $datosdeudas[0] = array(
            'fecha' => "27 Enero, 2019",
            "deuda_inicial" => '80000',
            "pago_inicial" => '8000',
            "ofertaste" => '10000',
            "restante_financiar" => '72000',
            "numero_cuotas" => '12',
            "ofertaste_numero_cuotas" => '12',
            "tasa" => '24',
            "cuota_mensual" => '6000',
            "mensaje" => "Luego de revisar tu propuesta, hemos decidido otorgarte un refinanciamiento para salvar la deuda PR-2014 con Banreserva, bajo los siguientes terminos: "
        );

        return response()->json([
            'datosdeudas' => $datosdeudas,
            'code' => 200
        ], 200);
    } //fin 

    public function aprobarcontraoferta(Request $request)
    {
        $data = $request->json()->all();
        $datos1 = Deudas::find($data['deudaid']);
        $datos1->contraoferta      = 3;
        $datos1->status_aprobacion = 2;
        $identifica_banco = $datos1->identifica_banco;
        $datos1->save();
        return response()->json([
            'nombre' => $identifica_banco,
            'code' => 200
        ], 200);
    }



    public function statusaprobacion(Request $request)
    {
        $data = $request->json()->all();
        $datosdeudas  = Deudas::where('id', $data['deudaid'])->get()->toArray();
        return response()->json([
            'datosdeudas' => $datosdeudas,
            'code' => 200
        ], 200);
    }


    public function firmardeuda(Request $request)
    {
        $data = $request->json()->all();
        $datos1 = Deudas::find($data['deudaid']);
        $datos1->status_aprobacion = 3;
        $identifica_banco = $datos1->identifica_banco;
        $datos1->save();
        $datosdeudas  = Deudas::where('id', $data['deudaid'])->get()->toArray();
        return response()->json([
            'datosdeudas' => $datosdeudas,
            'code' => 200
        ], 200);
    }



    public function pagardeuda(Request $request)
    {
        $data = $request->json()->all();
        $datos1 = Deudas::find($data['deudaid']);
        $datos1->status_aprobacion = 4;
        $identifica_banco = $datos1->identifica_banco;
        $datos1->save();
        $datosdeudas  = Deudas::where('id', $data['deudaid'])->get()->toArray();
        return response()->json([
            'datosdeudas' => $datosdeudas,
            'code' => 200
        ], 200);
    }
}
