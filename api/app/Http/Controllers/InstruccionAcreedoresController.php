<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Models\InstruccionAcreedores;
use \App\module;


use DB;
use Mail;

use Twilio\Rest\Client;

use App\Http\Controllers\Controller;
use JWTAuth;

class InstruccionAcreedoresController extends Controller
{

    public function localizarinstruccion(Request $request)
    {
        $data = $request->json()->all();
        $creditor_id    = $data['creditor_id'];
        $idiomas = 'ES';
        if (isset($data['idiomas'])) {
            $idiomas = $data['idiomas'];
        }
        $instruccionAcreedor = InstruccionAcreedores::where(['acreedor' => $creditor_id, 'idiomas' => $idiomas, 'activo' => 1])->get()->first();
        if ($instruccionAcreedor) {
            return response()->json([
                'error' => false,
                'data' => $instruccionAcreedor,
                'code' => 200
            ], 200);
        }
        return response()->json([
            'error' => true,
            'data' => null,
            'code' => 201
        ], 201);
    }
}
