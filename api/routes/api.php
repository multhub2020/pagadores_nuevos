<?php

use Illuminate\Http\Request;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::post('addayudas',                    ['uses' => 'InicioController@addayudas']);
Route::post('addcalificaciones',            ['uses' => 'InicioController@addcalificaciones']);




//INICIO
Route::post('inicio',                    ['uses' => 'InicioController@inicio']);
Route::post('contraoferta',             ['uses' => 'InicioController@contraoferta']);
Route::post('aprobarcontraoferta',      ['uses' => 'InicioController@aprobarcontraoferta']);
Route::post('statusaprobacion',         ['uses' => 'InicioController@statusaprobacion']);

Route::post('firmardeuda',              ['uses' => 'InicioController@firmardeuda']);
Route::post('pagardeuda',               ['uses' => 'InicioController@pagardeuda']);

//DEUDAS

Route::post('deudasadd',                   ['uses' => 'DeudasController@deudasadd']);
Route::post('deudassolicitudenviar',       ['uses' => 'DeudasController@deudassolicitudenviar']);
Route::post('deudasaplication',            ['uses' => 'DeudasController@deudasaplication']);
Route::post('listaplication',              ['uses' => 'DeudasController@listaplication']);


Route::post('deudaslista',                 ['uses' => 'DeudasController@deudaslista']);
Route::post('localizardeudaid',            ['uses' => 'DeudasController@localizardeudaid']);
Route::post('deudassolicitudenviarcuotas', ['uses' => 'DeudasController@deudassolicitudenviarcuotas']);
Route::post('agregadeuda1',                ['uses' => 'DeudasController@agregadeuda1']);


Route::post('perfilclaveeditar',       ['uses' => 'UsuariosController@perfilclaveeditar']);
Route::post('sobrenosotros',           ['uses' => 'PerfilController@sobrenosotros']);
Route::post('miperfilfotoupdate',      ['uses' => 'PerfilController@miperfilfotoupdate']);


Route::post('logerroresadd',           ['uses' => 'PerfilController@logerroresadd']);




//NOTIFICACIONES
Route::post('listarnotificaciones',  ['uses' => 'NotificacionesController@listarnotificaciones']);
Route::post('encuestaadd',           ['uses' => 'NotificacionesController@encuestaadd']);
Route::post('encuestalist',          ['uses' => 'NotificacionesController@encuestalist']);

Route::post('eventoconf',            ['uses' => 'NotificacionesController@eventoconf']);
Route::post('eventoadd',             ['uses' => 'NotificacionesController@eventoadd']);
Route::post('eventolist',            ['uses' => 'NotificacionesController@eventolist']);



/////////////////////////////////////////

Route::post('listciudad',            ['uses' => 'InformacionesController@listciudad']);
Route::post('listprovin',            ['uses' => 'InformacionesController@listprovin']);
Route::post('listindust',            ['uses' => 'InformacionesController@listindust']);



//USUARIOS
Route::post('loginclave',             ['uses' => 'UsuariosController@loginclave']);
Route::post('actualizarpush',         ['uses' => 'UsuariosController@actualizarpush']);
Route::post('actualizarpush_pull',    ['uses' => 'UsuariosController@actualizarpush_pull']);
Route::post('loginregistro',          ['uses' => 'UsuariosController@loginregistro']);
Route::post('loginregistrocedula',    ['uses' => 'UsuariosController@loginregistrocedula']);

Route::post('loginregistrocodigo',    ['uses' => 'UsuariosController@loginregistrocodigo']);
Route::post('loginregistrocodigore',  ['uses' => 'UsuariosController@loginregistrocodigore']);
Route::post('loginrecuperar1',        ['uses' => 'UsuariosController@loginrecuperar1']);
Route::post('loginrecuperar2',        ['uses' => 'UsuariosController@loginrecuperar2']);
Route::post('loginrecuperar3',        ['uses' => 'UsuariosController@loginrecuperar3']);
Route::post('verificaregistro2',      ['uses' => 'UsuariosController@verificaregistro2']);
Route::post('verificar',              ['uses' => 'UsuariosController@verificar']);
Route::post('verificarcel',           ['uses' => 'UsuariosController@verificarcel']);

//sugerencias
Route::post('sugerenciasadd',         ['uses' => 'SugerenciasController@sugerenciasadd']);

//LISTAR CONTACTOR
Route::post('listacontactoslistar',    ['uses' => 'ListacontactosController@listacontactoslistar']);
Route::post('listacontactosadd',       ['uses' => 'ListacontactosController@listacontactosadd']);
Route::post('listacontactossend',      ['uses' => 'ListacontactosController@listacontactossend']);


//INFORMACION
Route::post('inforpoliticas',         ['uses' => 'InformacionesController@inforpoliticas']);
Route::post('inforcondiciones',       ['uses' => 'InformacionesController@inforcondiciones']);

//MENU
Route::post('listarmenu',         ['uses' => 'MenuController@listarmenu']);
Route::post('listarmenudeudas',   ['uses' => 'MenuController@listarmenudeudas']);

//REGISTRO
Route::post('loginregistroinvitador',  ['uses' => 'InvitadosController@loginregistroinvitador']);


//PERFIL
Route::post('listarperfil',           ['uses' => 'PerfilController@listarperfil']);
Route::post('guardarperfil',          ['uses' => 'PerfilController@guardarperfil']);

Route::post('perfileditar',           ['uses' => 'PerfilController@perfileditar']);
Route::post('perfileditarupdate',     ['uses' => 'PerfilController@perfileditarupdate']);

//INVITACION
Route::post('menuinvitacion',         ['uses' => 'MenuController@menuinvitacion']);
Route::post('listartop',              ['uses' => 'MenuController@listartop']);



//CRON
Route::get('recordatorio',           ['uses' => 'InvitadosController@recordatorio']);
Route::get('invitaciones',           ['uses' => 'InvitadosController@invitaciones']);
Route::get('sms',                     ['uses' => 'InvitadosController@sms']);

Route::post('listarinvitados',        ['uses' => 'InvitadosController@listarinvitados']);
Route::post('listarseguidores',       ['uses' => 'InvitadosController@listarseguidores']);
Route::post('listarseguidoressub',    ['uses' => 'InvitadosController@listarseguidoressub']);
Route::post('listarseguidoresfiltro', ['uses' => 'InvitadosController@listarseguidoresfiltro']);
Route::post('reenviarinvitacion',     ['uses' => 'InvitadosController@reenviarinvitacion']);
Route::post('reenviarinvitacionall',  ['uses' => 'InvitadosController@reenviarinvitacionall']);


Route::group(['middleware' => 'token'], function () {
    Route::post('verificatoken',        ['uses' => 'VerificaController@verificatoken']);
});




//agilpay
Route::post('gethash', ['uses' => 'AgilpaysController@gethash']);
Route::post('gethash2', ['uses' => 'AgilpaysController@gethash2']);
Route::post('authorizepay', ['uses' => 'AgilpaysController@authorizepay']);
Route::post('registertoken', ['uses' => 'AgilpaysController@registertoken']);
Route::post('authorizetoken', ['uses' => 'AgilpaysController@authorizetoken']);

Route::post('depositos',      ['uses' => 'DepositosController@agregar']);
Route::post('localizarinstruccion', ['uses' => 'InstruccionAcreedoresController@localizarinstruccion']);
Route::post('datacredito',      ['uses' => 'MenuController@datacredito']);

Route::post('credolabcollect',       ['uses' => 'UsuariosController@credolabcollect']);
Route::get('credolabAccessToken',       ['uses' => 'UsuariosController@credolabAccessToken']);
Route::post('credolabDatasets',       ['uses' => 'UsuariosController@credolabDatasets']);