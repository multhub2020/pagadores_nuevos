<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Comprobantes de Pago</title>
</head>

<body>
    <p><strong>Nombre</strong>: {{ $nombre }}</p>
    <p><strong>Email</strong>: {{ $email }}</p>
    <p><strong>customer_id</strong>: {{ $customer_id }}</p>
    <p><strong>application_id</strong>: {{ $application_id }}</p>
    <p><strong>numero_transaccion</strong>: {{ $numero_transaccion }}</p>
    <p><strong>banco</strong>: {{ $banco }}</p>
    <p><strong>monto</strong>: {{ $monto }}</p>
</body>

</html>