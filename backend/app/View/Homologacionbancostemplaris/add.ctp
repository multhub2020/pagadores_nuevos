<section class="content-header">
  <h1>
    <?= Configure::read('namesysS'); ?> <small><?php echo __('Homologacionbancostemplaris'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= $this->Html->url('/Dashboard/') ?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
    <li class="active"><?php echo __('Homologacionbancostemplaris'); ?></li>
  </ol>
</section>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title"><?php echo __('Add Homologacionbancostemplari'); ?></h3>
          <hr>
        </div><!-- /.box-header -->
        <div class="box-body">
          <?php echo $this->Form->create('Homologacionbancostemplari', array('class' => 'form-horizontal')); ?>
          <div class='row'>
            <div class='col-md-12'>
              <?php

              echo '<div class="form-group">';
              echo '<label class="control-label col-md-2" for="Homologacionbancoburo_afiliado">buro_afiliado</label>';
              echo '<div class="col-md-9">';
              echo $this->Form->input('buro_afiliado', array('id' => 'Homologacionbancoburo_afiliado', 'div' => false, 'label' => false, 'class' => 'form-control'));
              echo '</div>';
              echo '</div>';

              echo '<div class="form-group">';
              echo '<label class="control-label col-md-2" for="Homologacionbancotkl_creditor_id">tkl_creditor_id</label>';
              echo '<div class="col-md-9">';
              echo $this->Form->input('tkl_creditor_id', array('id' => 'Homologacionbancotkl_creditor_id', 'div' => false, 'label' => false, 'class' => 'form-control', 'type' => 'number'));
              echo '</div>';
              echo '</div>';

              echo '<div class="form-group">';
              echo '<label class="control-label col-md-2" for="Homologacionbancoid_supervisor">id_supervisor</label>';
              echo '<div class="col-md-9">';
              echo $this->Form->input('id_supervisor', array('id' => 'Homologacionbancoid_supervisor', 'div' => false, 'label' => false, 'class' => 'form-control', 'type' => 'number'));
              echo '</div>';
              echo '</div>';
              ?>
            </div>
            <div class="col-md-12">
              <div class="form-group">
                <div class="col-md-12">
                  <?php echo $this->Html->link(__('Volver al listado'), array('action' => 'index')); ?> <input value="Guardar" class="btn btn-primary pull-right" type="submit">
                </div>
              </div>
            </div>
          </div>
          </form>
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->
  </div><!-- /.row -->
</section><!-- /.content -->