<?php
function url_origin($s, $use_forwarded_host = false)
{

	$ssl = (!empty($s['HTTPS']) && $s['HTTPS'] == 'on') ? true : false;
	$sp = strtolower($s['SERVER_PROTOCOL']);
	$protocol = substr($sp, 0, strpos($sp, '/')) . (($ssl) ? 's' : '');

	$port = $s['SERVER_PORT'];
	$port = ((!$ssl && $port == '80') || ($ssl && $port == '443')) ? '' : ':' . $port;

	$host = ($use_forwarded_host && isset($s['HTTP_X_FORWARDED_HOST'])) ? $s['HTTP_X_FORWARDED_HOST'] : (isset($s['HTTP_HOST']) ? $s['HTTP_HOST'] : null);
	$host = isset($host) ? $host : $s['SERVER_NAME'] . $port;

	return $protocol . '://' . $host;
}

function full_url($s, $use_forwarded_host = false)
{
	return url_origin($s, $use_forwarded_host) . $s['REQUEST_URI'];
}
?>
<section class="content-header">
	<h1>
		<?= Configure::read('namesysS'); ?> <small><?php echo __('Depositos'); ?></small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="<?= $this->Html->url('/Dashboard/') ?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
		<li class="active"><?php echo __('Depositos'); ?></li>
	</ol>
</section>
<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title"><?php echo __('Depositos'); ?> Detalles</h3>
					<hr>
				</div><!-- /.box-header -->
				<div class="box-body">
					<dl class="dl-horizontal">
						<dt><?php echo __('Id'); ?></dt>
						<dd>
							<?php echo h($deposito['Deposito']['id']); ?>
							&nbsp;
						</dd>
						<dt><?php echo __('Cedula'); ?></dt>
						<dd>
							<?php echo h($deposito['Deposito']['cedula']); ?>
							&nbsp;
						</dd>
						<dt><?php echo __('Imagen'); ?></dt>
						<dd>
							<?php echo h($deposito['Deposito']['imagen']); ?>
							&nbsp;
							<?php if ($deposito['Deposito']['imagen']) : ?>
								<img src="<?= url_origin($_SERVER) ?>/api/storage/imagenes<?= $deposito['Deposito']['imagen'] ?>" style="width: 100%;" />
							<?php endif; ?>
						</dd>
						<dt><?php echo __('Numero Transaccion'); ?></dt>
						<dd>
							<?php echo h($deposito['Deposito']['numero_transaccion']); ?>
							&nbsp;
						</dd>
						<dt><?php echo __('Banco'); ?></dt>
						<dd>
							<?php echo h($deposito['Deposito']['banco']); ?>
							&nbsp;
						</dd>
						<dt><?php echo __('Monto'); ?></dt>
						<dd>
							<?php echo h($deposito['Deposito']['monto']); ?>
							&nbsp;
						</dd>
						<dt><?php echo __('Customer Id'); ?></dt>
						<dd>
							<?php echo h($deposito['Deposito']['customer_id']); ?>
							&nbsp;
						</dd>
						<dt><?php echo __('Application Id'); ?></dt>
						<dd>
							<?php echo h($deposito['Deposito']['application_id']); ?>
							&nbsp;
						</dd>
						<dt><?php echo __('Created At'); ?></dt>
						<dd>
							<?php echo h($deposito['Deposito']['created_at']); ?>
							&nbsp;
						</dd>
						<dt><?php echo __('Updated At'); ?></dt>
						<dd>
							<?php echo h($deposito['Deposito']['updated_at']); ?>
							&nbsp;
						</dd>
					</dl>
					<p>
						<?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $deposito['Deposito']['id'])); ?>
						|
						<?php echo $this->Html->link(__('Volver al listado'), array('action' => 'index')); ?>
					</p>
				</div>
			</div><!-- /.box -->
		</div><!-- /.col -->
	</div><!-- /.row -->
</section><!-- /.content -->