<?php
function url_origin($s, $use_forwarded_host = false)
{

	$ssl = (!empty($s['HTTPS']) && $s['HTTPS'] == 'on') ? true : false;
	$sp = strtolower($s['SERVER_PROTOCOL']);
	$protocol = substr($sp, 0, strpos($sp, '/')) . (($ssl) ? 's' : '');

	$port = $s['SERVER_PORT'];
	$port = ((!$ssl && $port == '80') || ($ssl && $port == '443')) ? '' : ':' . $port;

	$host = ($use_forwarded_host && isset($s['HTTP_X_FORWARDED_HOST'])) ? $s['HTTP_X_FORWARDED_HOST'] : (isset($s['HTTP_HOST']) ? $s['HTTP_HOST'] : null);
	$host = isset($host) ? $host : $s['SERVER_NAME'] . $port;

	return $protocol . '://' . $host;
}

function full_url($s, $use_forwarded_host = false)
{
	return url_origin($s, $use_forwarded_host) . $s['REQUEST_URI'];
}
?>

<section class="content-header">
	<h1>
		<?= Configure::read('namesysS'); ?> <small><?php echo __('Depositos'); ?></small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="<?= $this->Html->url('/Principal/') ?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
		<li class="active"><?php echo __('Depositos'); ?></li>
	</ol>
</section>
<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title"><?php echo __('Depositos'); ?> Registrados</h3>
					<hr>
				</div><!-- /.box-header -->
				<div class="box-body">
					<p><?php echo $this->Html->link(__('Crear Nuevo'), array('action' => 'add'),  array('class' => 'btn btn-primary')); ?></p>

					<table id="data" class="table table-striped table-bordered responsive nowrap" width="100%" cellspacing="0">
						<thead>
							<tr>
								<th><?php echo __('id'); ?></th>
								<th><?php echo __('cedula'); ?></th>
								<th><?php echo __('imagen'); ?></th>
								<th><?php echo __('numero_transaccion'); ?></th>
								<th><?php echo __('banco'); ?></th>
								<th><?php echo __('monto'); ?></th>
								<th><?php echo __('customer_id'); ?></th>
								<th><?php echo __('application_id'); ?></th>
								<th><?php echo __('created_at'); ?></th>
								<th><?php echo __('updated_at'); ?></th>
								<th class="actions"><?php echo __('Acción'); ?></th>
							</tr>
						</thead>
						<tbody>
							<?php $id_tr = 0; ?><?php foreach ($depositos as $deposito) : ?>
							<?php $id_tr++; ?>
							<tr id="tr_<?php echo $id_tr; ?>">
								<td><?php echo h($deposito['Deposito']['id']); ?>&nbsp;</td>
								<td><?php echo h($deposito['Deposito']['cedula']); ?>&nbsp;</td>
								<td>
									<?php echo h($deposito['Deposito']['imagen']); ?>&nbsp;
									<?php if ($deposito['Deposito']['imagen']) : ?>
										<img src="<?= url_origin($_SERVER) ?>/api/storage/imagenes<?= $deposito['Deposito']['imagen'] ?>" style="max-width: 200px; max-height: 200px;" />
									<?php endif; ?>
								</td>
								<td><?php echo h($deposito['Deposito']['numero_transaccion']); ?>&nbsp;</td>
								<td><?php echo h($deposito['Deposito']['banco']); ?>&nbsp;</td>
								<td><?php echo h($deposito['Deposito']['monto']); ?>&nbsp;</td>
								<td><?php echo h($deposito['Deposito']['customer_id']); ?>&nbsp;</td>
								<td><?php echo h($deposito['Deposito']['application_id']); ?>&nbsp;</td>
								<td><?php echo h($deposito['Deposito']['created_at']); ?>&nbsp;</td>
								<td><?php echo h($deposito['Deposito']['updated_at']); ?>&nbsp;</td>
								<td class="actions">
									<?php $usuario_rol              = $this->Session->read('usuario_rol');
													$usuario_id             = $this->Session->read('usuario_id');
									?> <?php echo $this->Html->link(__('Editar'),   array('action' => 'edit',   $deposito['Deposito']['id']), array('class' => 'btn btn-primary')); ?>
									<?php echo $this->Html->link(__('Ver'),      array('action' => 'view',   $deposito['Deposito']['id']), array('class' => 'btn btn-success')); ?>
									<?php echo $this->Html->link(__('Eliminar'), '#', array('onclick' => "eliminar('tr_" . $id_tr . "', '" . $deposito['Deposito']['id'] . "'  )", 'class' => 'btn btn-danger', 'confirm' => __('Esta seguro que desea eliminar el registro # %s?', $deposito['Deposito']['id']))); ?>
								</td>
							</tr>
						<?php endforeach; ?>
						</tbody>
					</table>
				</div>
			</div><!-- /.box -->
		</div><!-- /.col -->
	</div><!-- /.row -->
</section><!-- /.content -->
<script type="text/javascript">
	//$(document).ready(function() {
	$('#data').DataTable({
		dom: 'Bfrtlip',
		responsive: true,
		buttons: [{
				extend: 'copy',
				title: '<?php echo __('Depositos'); ?>'
			},
			{
				extend: 'csv',
				title: '<?php echo __('Depositos'); ?>'
			},
			{
				extend: 'excel',
				title: '<?php echo __('Depositos'); ?>'
			},
			{
				extend: 'pdf',
				title: '<?php echo __('Depositos'); ?>'
			}, ,
			{
				extend: 'print',
				title: '<?php echo __('Depositos'); ?>'
			}
		],
		"language": {
			"sProcessing": "Procesando...",
			"sLengthMenu": "Mostrar _MENU_ registros",
			"sZeroRecords": "No se encontraron resultados",
			"sEmptyTable": "Ningún dato disponible en esta tabla",
			"sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
			"sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
			"sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
			"sInfoPostFix": "",
			"sSearch": "Buscar:",
			"sUrl": "",
			"sInfoThousands": ",",
			"sLoadingRecords": "Cargando...",
			"oPaginate": {
				"sFirst": "Primero",
				"sLast": "Último",
				"sNext": "Siguiente",
				"sPrevious": "Anterior"
			},
			"oAria": {
				"sSortAscending": ": Activar para ordenar la columna de manera ascendente",
				"sSortDescending": ": Activar para ordenar la columna de manera descendente"
			}
		}
	});

	function eliminar(id, valor) {
		table = $('#data').DataTable();
		table.row('#' + id).remove().draw(false);
		//$.ajax({url:'/Depositos/delete/'+valor,type:"post",data:{},success:function(n){} });
		$.ajax({
			url: '<?= $this->Html->url("/Depositos/delete/") ?>' + valor,
			type: "post",
			data: {},
			success: function(n) {}
		});
	}
	//} );
</script>