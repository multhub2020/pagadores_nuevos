<section class="content-header">
  <h1>
    <?= Configure::read('namesysS'); ?>    <small><?php echo __('Eventos'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= $this->Html->url('/Dashboard/')?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
    <li class="active"><?php echo __('Eventos'); ?></li>
  </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><?php echo __('Add Evento'); ?></h3>
                    <hr>
                </div><!-- /.box-header -->
                <div class="box-body">
					<?php echo $this->Form->create('Evento', array('class'=>'form-horizontal')); ?>
					<div class='row'>
							<div class='col-md-12'>
								<?php
	
echo'<div class="form-group">';	
echo'<label class="control-label col-md-2" for="Eventopregunta">Evento</label>';		
echo'<div class="col-md-9">';			
echo $this->Form->input('pregunta', array('id'=>'Eventopregunta', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
echo '</div>';	
echo '</div>';

	?>

  <!--------- Modulos -------->
<div class="form-group">
<label class="control-label col-md-2" for="Mycarmunicipio2">Respuestas</label>
<div class="col-md-9 col-sm-9 col-xs-9">
    <div class="box box-solid box-default">
        <div class="box-header with-border">
            <h3 class="box-title">Respuesta</h3>
            <div class="box-tools pull-right">
                <button id="newModulosBtn2" class="btn btn-box-tool" data-toggle="tooltip" title="Agregar otro servicio" data-widget="chat-pane-toggle"><i class="glyphicon glyphicon-plus"></i></button>
            </div><!-- /.box-tools -->
        </div><!-- /.box-header -->
        <div class="box-body">
            <div id="modulos-container2">
                <div id="moduloscount2">
                    <div class="form-group tel-con" id="modulo2_0">
                        <label class="control-label col-md-2">Respuesta</label>
                        <div class="col-md-3">
                        <input class="form-control tel-0" id="Modulos2_0__Servicio" name="data[Afiliado][Modulos2][0][denominacion]" onchange="Javascript:manageExt(0);">
                        </div>
                        <div class="col-md-1">
                            <button class="btn btn-danger btn-sm" onclick="return false;"><span class="glyphicon glyphicon-trash"></span></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<script type="text/javascript">
$(document).ready(function() {
 
    $("#newModulosBtn2").click(function (e) {
        var MaxInputs       = 1000;
        var x = $("#modulos-container2 #moduloscount2").length + 1;
        var FieldCount = x-1;
        if(x <= MaxInputs) //max input box allowed
        {
            FieldCount++;
            $("#modulos-container2").append('<div id="moduloscount2">'+
                                           '<div class="form-group tel-con" id="modulo2_'+ FieldCount +'">'+
                                           '<label class="control-label col-md-2">Respuesta</label>'+ 
                                           '<div class="col-md-3">'+
                                           '<input class="form-control tel-0" id="Modulos2_'+ FieldCount +'__Servicio" name="data[Afiliado][Modulos2]['+ FieldCount +'][denominacion]" onchange="Javascript:manageExt(0);">'+
                                             '</div>'+
                                           '<div class="col-md-1">'+ 
                                           '<button class="btn btn-danger btn-sm" onclick="if(confirm(\'Seguro que desea eliminar el registro\')){  $(\'#modulo2_'+ FieldCount +'\').remove(); } return false;"><span class="glyphicon glyphicon-trash"></span></button></div></div></div>');
            x++; //text box increment
        }
        return false;
    });
    
});
</script>
							</div>
							<div class="col-md-12">
								<div class="form-group">
	                                <div class="col-md-12">
	                                    <?php echo $this->Html->link(__('Volver al listado'), array('action' => 'index')); ?>	                                    <input value="Guardar" class="btn btn-primary pull-right" type="submit">
	                                </div>
	                            </div>
                            </div>
					</div></form>                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section><!-- /.content -->
