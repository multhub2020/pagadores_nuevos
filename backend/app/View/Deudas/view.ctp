<section class="content-header">
	<h1>
		<?= Configure::read('namesysS'); ?> <small><?php echo __('Deudas'); ?></small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="<?= $this->Html->url('/Dashboard/') ?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
		<li class="active"><?php echo __('Deudas'); ?></li>
	</ol>
</section>
<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title"><?php echo __('Deudas'); ?> Detalles</h3>
					<hr>
				</div><!-- /.box-header -->
				<div class="box-body">
					<dl class="dl-horizontal">
						<dt><?php echo __('Id'); ?></dt>
						<dd>
							<?php echo h($deuda['Deuda']['id']); ?>
							&nbsp;
						</dd>
						<dt><?php echo __('Cedula'); ?></dt>
						<dd>
							<?php echo h($deuda['Deuda']['cedula']); ?>
							&nbsp;
						</dd>
						<dt><?php echo __('Account Number'); ?></dt>
						<dd>
							<?php echo h($deuda['Deuda']['account_number']); ?>
							&nbsp;
						</dd>
						<dt><?php echo __('Product Type Id'); ?></dt>
						<dd>
							<?php echo h($deuda['Deuda']['product_type_id']); ?>
							&nbsp;
						</dd>
						<dt><?php echo __('Product Resolution Id'); ?></dt>
						<dd>
							<?php echo h($deuda['Deuda']['product_resolution_id']); ?>
							&nbsp;
						</dd>
						<dt><?php echo __('Creditor Id'); ?></dt>
						<dd>
							<?php echo h($deuda['Deuda']['creditor_id']); ?>
							&nbsp;
						</dd>
						<dt><?php echo __('Monto Deuda'); ?></dt>
						<dd>
							<?php echo h($deuda['Deuda']['monto_deuda']); ?>
							&nbsp;
						</dd>
						<dt><?php echo __('Tipo Deuda'); ?></dt>
						<dd>
							<?php echo h($deuda['Deuda']['tipo_deuda']); ?>
							&nbsp;
						</dd>
						<dt><?php echo __('Manual Debt Add'); ?></dt>
						<dd>
							<?php echo h($deuda['Deuda']['manual_debt_add']); ?>
							&nbsp;
						</dd>
						<dt><?php echo __('Banco Deuda'); ?></dt>
						<dd>
							<?php echo h($deuda['Deuda']['banco_deuda']); ?>
							&nbsp;
						</dd>
						<dt><?php echo __('Identifica Banco'); ?></dt>
						<dd>
							<?php echo h($deuda['Deuda']['identifica_banco']); ?>
							&nbsp;
						</dd>
						<dt><?php echo __('Fecha'); ?></dt>
						<dd>
							<?php echo h($deuda['Deuda']['fecha']); ?>
							&nbsp;
						</dd>
						<dt><?php echo __('Activo'); ?></dt>
						<dd>
							<?php echo h($deuda['Deuda']['activo']); ?>
							&nbsp;
						</dd>
						<dt><?php echo __('Estatus'); ?></dt>
						<dd>
							<?php echo h($deuda['Deuda']['estatus']); ?>
							&nbsp;
						</dd>
						<dt><?php echo __('Pago Inicial'); ?></dt>
						<dd>
							<?php echo h($deuda['Deuda']['pago_inicial']); ?>
							&nbsp;
						</dd>
						<dt><?php echo __('Ofrecer Saldo'); ?></dt>
						<dd>
							<?php echo h($deuda['Deuda']['ofrecer_saldo']); ?>
							&nbsp;
						</dd>
						<dt><?php echo __('N Cuotas'); ?></dt>
						<dd>
							<?php echo h($deuda['Deuda']['n_cuotas']); ?>
							&nbsp;
						</dd>
						<dt><?php echo __('M Cuota'); ?></dt>
						<dd>
							<?php echo h($deuda['Deuda']['m_cuota']); ?>
							&nbsp;
						</dd>
						<dt><?php echo __('Dia Cuota'); ?></dt>
						<dd>
							<?php echo h($deuda['Deuda']['dia_cuota']); ?>
							&nbsp;
						</dd>
						<dt><?php echo __('Cuentaid'); ?></dt>
						<dd>
							<?php echo h($deuda['Deuda']['cuentaid']); ?>
							&nbsp;
						</dd>
						<dt><?php echo __('Moneda'); ?></dt>
						<dd>
							<?php echo h($deuda['Deuda']['moneda']); ?>
							&nbsp;
						</dd>
						<dt><?php echo __('Aplicacionid'); ?></dt>
						<dd>
							<?php echo h($deuda['Deuda']['aplicacionid']); ?>
							&nbsp;
						</dd>
						<dt><?php echo __('Status Tkl'); ?></dt>
						<dd>
							<?php echo h($deuda['Deuda']['status_tkl']); ?>
							&nbsp;
						</dd>
						<dt><?php echo __('Fecha Apertura'); ?></dt>
						<dd>
							<?php echo h($deuda['Deuda']['fecha_apertura']); ?>
							&nbsp;
						</dd>
						<dt><?php echo __('Contraoferta'); ?></dt>
						<dd>
							<?php echo h($deuda['Deuda']['contraoferta']); ?>
							&nbsp;
						</dd>
						<dt><?php echo __('Status Aprobacion'); ?></dt>
						<dd>
							<?php echo h($deuda['Deuda']['status_aprobacion']); ?>
							&nbsp;
						</dd>
						<dt><?php echo __('Origen'); ?></dt>
						<dd>
							<?php echo h($deuda['Deuda']['origen']); ?>
							&nbsp;
						</dd>
						<dt><?php echo __('Created'); ?></dt>
						<dd>
							<?php echo h($deuda['Deuda']['created']); ?>
							&nbsp;
						</dd>
						<dt><?php echo __('Modified'); ?></dt>
						<dd>
							<?php echo h($deuda['Deuda']['modified']); ?>
							&nbsp;
						</dd>
						<dt><?php echo __('Updated At'); ?></dt>
						<dd>
							<?php echo h($deuda['Deuda']['updated_at']); ?>
							&nbsp;
						</dd>
					</dl>
					<p>
						<?php //echo $this->Html->link(__('Editar'), array('action' => 'edit', $deuda['Deuda']['id'])); ?>
						|
						<?php echo $this->Html->link(__('Volver al listado'), array('action' => 'index')); ?>
					</p>
				</div>
			</div><!-- /.box -->
		</div><!-- /.col -->
	</div><!-- /.row -->
</section><!-- /.content -->