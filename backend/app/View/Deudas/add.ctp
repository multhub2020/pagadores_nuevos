<section class="content-header">
  <h1>
    <?= Configure::read('namesysS'); ?>    <small><?php echo __('Deudas'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= $this->Html->url('/Dashboard/')?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
    <li class="active"><?php echo __('Deudas'); ?></li>
  </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><?php echo __('Add Deuda'); ?></h3>
                    <hr>
                </div><!-- /.box-header -->
                <div class="box-body">
					<?php echo $this->Form->create('Deuda', array('class'=>'form-horizontal')); ?>
					<div class='row'>
							<div class='col-md-12'>
								<?php
	
echo'<div class="form-group">';	
echo'<label class="control-label col-md-2" for="Deudacedula">cedula</label>';		
echo'<div class="col-md-9">';			
echo $this->Form->input('cedula', array('id'=>'Deudacedula', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
echo '</div>';	
echo '</div>';
	
echo'<div class="form-group">';	
echo'<label class="control-label col-md-2" for="Deudaaccount_number">account_number</label>';		
echo'<div class="col-md-9">';			
echo $this->Form->input('account_number', array('id'=>'Deudaaccount_number', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
echo '</div>';	
echo '</div>';
	
echo'<div class="form-group">';	
echo'<label class="control-label col-md-2" for="Deudaproduct_type_id">product_type_id</label>';		
echo'<div class="col-md-9">';			
echo $this->Form->input('product_type_id', array('id'=>'Deudaproduct_type_id', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
echo '</div>';	
echo '</div>';
	
echo'<div class="form-group">';	
echo'<label class="control-label col-md-2" for="Deudaproduct_resolution_id">product_resolution_id</label>';		
echo'<div class="col-md-9">';			
echo $this->Form->input('product_resolution_id', array('id'=>'Deudaproduct_resolution_id', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
echo '</div>';	
echo '</div>';
	
echo'<div class="form-group">';	
echo'<label class="control-label col-md-2" for="Deudacreditor_id">creditor_id</label>';		
echo'<div class="col-md-9">';			
echo $this->Form->input('creditor_id', array('id'=>'Deudacreditor_id', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
echo '</div>';	
echo '</div>';
	
echo'<div class="form-group">';	
echo'<label class="control-label col-md-2" for="Deudamonto_deuda">monto_deuda</label>';		
echo'<div class="col-md-9">';			
echo $this->Form->input('monto_deuda', array('id'=>'Deudamonto_deuda', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
echo '</div>';	
echo '</div>';
	
echo'<div class="form-group">';	
echo'<label class="control-label col-md-2" for="Deudatipo_deuda">tipo_deuda</label>';		
echo'<div class="col-md-9">';			
echo $this->Form->input('tipo_deuda', array('id'=>'Deudatipo_deuda', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
echo '</div>';	
echo '</div>';
	
echo'<div class="form-group">';	
echo'<label class="control-label col-md-2" for="Deudamanual_debt_add">manual_debt_add</label>';		
echo'<div class="col-md-9">';			
echo $this->Form->input('manual_debt_add', array('id'=>'Deudamanual_debt_add', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
echo '</div>';	
echo '</div>';
	
echo'<div class="form-group">';	
echo'<label class="control-label col-md-2" for="Deudabanco_deuda">banco_deuda</label>';		
echo'<div class="col-md-9">';			
echo $this->Form->input('banco_deuda', array('id'=>'Deudabanco_deuda', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
echo '</div>';	
echo '</div>';
	
echo'<div class="form-group">';	
echo'<label class="control-label col-md-2" for="Deudaidentifica_banco">identifica_banco</label>';		
echo'<div class="col-md-9">';			
echo $this->Form->input('identifica_banco', array('id'=>'Deudaidentifica_banco', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
echo '</div>';	
echo '</div>';
	
echo'<div class="form-group">';	
echo'<label class="control-label col-md-2" for="Deudafecha">fecha</label>';		
echo'<div class="col-md-9">';			
echo $this->Form->input('fecha', array('id'=>'Deudafecha', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
echo '</div>';	
echo '</div>';
	
echo'<div class="form-group">';	
echo'<label class="control-label col-md-2" for="Deudaactivo">activo</label>';		
echo'<div class="col-md-9">';			
echo $this->Form->input('activo', array('id'=>'Deudaactivo', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
echo '</div>';	
echo '</div>';
	
echo'<div class="form-group">';	
echo'<label class="control-label col-md-2" for="Deudaestatus">estatus</label>';		
echo'<div class="col-md-9">';			
echo $this->Form->input('estatus', array('id'=>'Deudaestatus', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
echo '</div>';	
echo '</div>';
	
echo'<div class="form-group">';	
echo'<label class="control-label col-md-2" for="Deudapago_inicial">pago_inicial</label>';		
echo'<div class="col-md-9">';			
echo $this->Form->input('pago_inicial', array('id'=>'Deudapago_inicial', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
echo '</div>';	
echo '</div>';
	
echo'<div class="form-group">';	
echo'<label class="control-label col-md-2" for="Deudaofrecer_saldo">ofrecer_saldo</label>';		
echo'<div class="col-md-9">';			
echo $this->Form->input('ofrecer_saldo', array('id'=>'Deudaofrecer_saldo', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
echo '</div>';	
echo '</div>';
	
echo'<div class="form-group">';	
echo'<label class="control-label col-md-2" for="Deudan_cuotas">n_cuotas</label>';		
echo'<div class="col-md-9">';			
echo $this->Form->input('n_cuotas', array('id'=>'Deudan_cuotas', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
echo '</div>';	
echo '</div>';
	
echo'<div class="form-group">';	
echo'<label class="control-label col-md-2" for="Deudam_cuota">m_cuota</label>';		
echo'<div class="col-md-9">';			
echo $this->Form->input('m_cuota', array('id'=>'Deudam_cuota', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
echo '</div>';	
echo '</div>';
	
echo'<div class="form-group">';	
echo'<label class="control-label col-md-2" for="Deudadia_cuota">dia_cuota</label>';		
echo'<div class="col-md-9">';			
echo $this->Form->input('dia_cuota', array('id'=>'Deudadia_cuota', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
echo '</div>';	
echo '</div>';
	
echo'<div class="form-group">';	
echo'<label class="control-label col-md-2" for="Deudacuentaid">cuentaid</label>';		
echo'<div class="col-md-9">';			
echo $this->Form->input('cuentaid', array('id'=>'Deudacuentaid', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
echo '</div>';	
echo '</div>';
	
echo'<div class="form-group">';	
echo'<label class="control-label col-md-2" for="Deudamoneda">moneda</label>';		
echo'<div class="col-md-9">';			
echo $this->Form->input('moneda', array('id'=>'Deudamoneda', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
echo '</div>';	
echo '</div>';
	
echo'<div class="form-group">';	
echo'<label class="control-label col-md-2" for="Deudaaplicacionid">aplicacionid</label>';		
echo'<div class="col-md-9">';			
echo $this->Form->input('aplicacionid', array('id'=>'Deudaaplicacionid', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
echo '</div>';	
echo '</div>';
	
echo'<div class="form-group">';	
echo'<label class="control-label col-md-2" for="Deudastatus_tkl">status_tkl</label>';		
echo'<div class="col-md-9">';			
echo $this->Form->input('status_tkl', array('id'=>'Deudastatus_tkl', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
echo '</div>';	
echo '</div>';
	
echo'<div class="form-group">';	
echo'<label class="control-label col-md-2" for="Deudafecha_apertura">fecha_apertura</label>';		
echo'<div class="col-md-9">';			
echo $this->Form->input('fecha_apertura', array('id'=>'Deudafecha_apertura', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
echo '</div>';	
echo '</div>';
	
echo'<div class="form-group">';	
echo'<label class="control-label col-md-2" for="Deudacontraoferta">contraoferta</label>';		
echo'<div class="col-md-9">';			
echo $this->Form->input('contraoferta', array('id'=>'Deudacontraoferta', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
echo '</div>';	
echo '</div>';
	
echo'<div class="form-group">';	
echo'<label class="control-label col-md-2" for="Deudastatus_aprobacion">status_aprobacion</label>';		
echo'<div class="col-md-9">';			
echo $this->Form->input('status_aprobacion', array('id'=>'Deudastatus_aprobacion', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
echo '</div>';	
echo '</div>';
	
echo'<div class="form-group">';	
echo'<label class="control-label col-md-2" for="Deudaorigen">origen</label>';		
echo'<div class="col-md-9">';			
echo $this->Form->input('origen', array('id'=>'Deudaorigen', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
echo '</div>';	
echo '</div>';
	
echo'<div class="form-group">';	
echo'<label class="control-label col-md-2" for="Deudaupdated_at">updated_at</label>';		
echo'<div class="col-md-9">';			
echo $this->Form->input('updated_at', array('id'=>'Deudaupdated_at', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
echo '</div>';	
echo '</div>';
	?>
							</div>
							<div class="col-md-12">
								<div class="form-group">
	                                <div class="col-md-12">
	                                    <?php echo $this->Html->link(__('Volver al listado'), array('action' => 'index')); ?>	                                    <input value="Guardar" class="btn btn-primary pull-right" type="submit">
	                                </div>
	                            </div>
                            </div>
					</div></form>                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section><!-- /.content -->
