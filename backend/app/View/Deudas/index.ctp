<section class="content-header">
	<h1>
		<?= Configure::read('namesysS'); ?> <small><?php echo __('Deudas'); ?></small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="<?= $this->Html->url('/Dashboard/') ?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
		<li class="active"><?php echo __('Deudas'); ?></li>
	</ol>
</section>
<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title"><?php echo __('Deudas'); ?> Registrados</h3>
					<hr>
				</div><!-- /.box-header -->
				<div class="box-body">
					<p><?php //echo $this->Html->link(__('Crear Nuevo'), array('action' => 'add'),  array('class' => 'btn btn-primary')); ?></p>

					<table id="data" class="table table-striped table-bordered responsive nowrap" width="100%" cellspacing="0">
						<thead>
							<tr>
								<th><?php echo __('cedula'); ?></th>
								<!--<th><?php echo __('account_number'); ?></th>
								<th><?php echo __('product_type_id'); ?></th>
								<th><?php echo __('product_resolution_id'); ?></th>
								<th><?php echo __('creditor_id'); ?></th>-->
								<th><?php echo __('monto_deuda'); ?></th>
								<!--<th><?php echo __('tipo_deuda'); ?></th>
								<th><?php echo __('manual_debt_add'); ?></th>
								<th><?php echo __('banco_deuda'); ?></th>
								<th><?php echo __('identifica_banco'); ?></th>-->
								<th><?php echo __('fecha'); ?></th>
								<!--<th><?php echo __('activo'); ?></th>
								<th><?php echo __('estatus'); ?></th>
								<th><?php echo __('pago_inicial'); ?></th>
								<th><?php echo __('ofrecer_saldo'); ?></th>
								<th><?php echo __('n_cuotas'); ?></th>
								<th><?php echo __('m_cuota'); ?></th>
								<th><?php echo __('dia_cuota'); ?></th>
								<th><?php echo __('cuentaid'); ?></th>-->
								<th><?php echo __('moneda'); ?></th>
								<th><?php echo __('aplicacionid'); ?></th>
								<th><?php echo __('status_tkl'); ?></th>
								<!--<th><?php echo __('fecha_apertura'); ?></th>
								<th><?php echo __('contraoferta'); ?></th>
								<th><?php echo __('status_aprobacion'); ?></th>
								<th><?php echo __('origen'); ?></th>
								<th><?php echo __('created'); ?></th>
								<th><?php echo __('modified'); ?></th>
								<th><?php echo __('updated_at'); ?></th>-->
								<th class="actions"><?php echo __('Acción'); ?></th>
							</tr>
						</thead>
						<tbody>
							<?php $id_tr = 0; ?><?php foreach ($deudas as $deuda) : ?>
							<?php $id_tr++; ?>
							<tr id="tr_<?php echo $id_tr; ?>">
								<td><?php echo h($deuda['Deuda']['cedula']); ?>&nbsp;</td>
								<!--<td><?php echo h($deuda['Deuda']['account_number']); ?>&nbsp;</td>
								<td><?php echo h($deuda['Deuda']['product_type_id']); ?>&nbsp;</td>
								<td><?php echo h($deuda['Deuda']['product_resolution_id']); ?>&nbsp;</td>
								<td><?php echo h($deuda['Deuda']['creditor_id']); ?>&nbsp;</td>-->
								<td><?php echo h($deuda['Deuda']['monto_deuda']); ?>&nbsp;</td>
								<!--<td><?php echo h($deuda['Deuda']['tipo_deuda']); ?>&nbsp;</td>
								<td><?php echo h($deuda['Deuda']['manual_debt_add']); ?>&nbsp;</td>
								<td><?php echo h($deuda['Deuda']['banco_deuda']); ?>&nbsp;</td>
								<td><?php echo h($deuda['Deuda']['identifica_banco']); ?>&nbsp;</td>-->
								<td><?php echo h($deuda['Deuda']['fecha']); ?>&nbsp;</td>
								<!--<td><?php echo h($deuda['Deuda']['activo']); ?>&nbsp;</td>
								<td><?php echo h($deuda['Deuda']['estatus']); ?>&nbsp;</td>
								<td><?php echo h($deuda['Deuda']['pago_inicial']); ?>&nbsp;</td>
								<td><?php echo h($deuda['Deuda']['ofrecer_saldo']); ?>&nbsp;</td>
								<td><?php echo h($deuda['Deuda']['n_cuotas']); ?>&nbsp;</td>
								<td><?php echo h($deuda['Deuda']['m_cuota']); ?>&nbsp;</td>
								<td><?php echo h($deuda['Deuda']['dia_cuota']); ?>&nbsp;</td>
								<td><?php echo h($deuda['Deuda']['cuentaid']); ?>&nbsp;</td>-->
								<td><?php echo h($deuda['Deuda']['moneda']); ?>&nbsp;</td>
								<td><?php echo h($deuda['Deuda']['aplicacionid']); ?>&nbsp;</td>
								<td><?php echo h($deuda['Deuda']['status_tkl']); ?>&nbsp;</td>
								<!--<td><?php echo h($deuda['Deuda']['fecha_apertura']); ?>&nbsp;</td>
								<td><?php echo h($deuda['Deuda']['contraoferta']); ?>&nbsp;</td>
								<td><?php echo h($deuda['Deuda']['status_aprobacion']); ?>&nbsp;</td>
								<td><?php echo h($deuda['Deuda']['origen']); ?>&nbsp;</td>
								<td><?php echo h($deuda['Deuda']['created']); ?>&nbsp;</td>
								<td><?php echo h($deuda['Deuda']['modified']); ?>&nbsp;</td>
								<td><?php echo h($deuda['Deuda']['updated_at']); ?>&nbsp;</td>-->
								<td class="actions">
									<?php $usuario_rol              = $this->Session->read('usuario_rol');
													$usuario_id             = $this->Session->read('usuario_id');
									?> <?php //echo $this->Html->link(__('Editar'),   array('action' => 'edit',   $deuda['Deuda']['id']), array('class' => 'btn btn-primary')); ?>
									<?php echo $this->Html->link(__('Ver'),      array('action' => 'view',   $deuda['Deuda']['id']), array('class' => 'btn btn-success')); ?>
									<?php //echo $this->Html->link(__('Eliminar'), '#', array('onclick' => "eliminar('tr_" . $id_tr . "', '" . $deuda['Deuda']['id'] . "'  )", 'class' => 'btn btn-danger', 'confirm' => __('Esta seguro que desea eliminar el registro # %s?', $deuda['Deuda']['id']))); ?>
								</td>
							</tr>
						<?php endforeach; ?>
						</tbody>
					</table>
				</div>
			</div><!-- /.box -->
		</div><!-- /.col -->
	</div><!-- /.row -->
</section><!-- /.content -->
<script type="text/javascript">
	//$(document).ready(function() {
	$('#data').DataTable({
		dom: 'Bfrtlip',
		responsive: true,
		buttons: [{
				extend: 'copy',
				title: '<?php echo __('Deudas'); ?>'
			},
			{
				extend: 'csv',
				title: '<?php echo __('Deudas'); ?>'
			},
			{
				extend: 'excel',
				title: '<?php echo __('Deudas'); ?>'
			},
			{
				extend: 'pdf',
				title: '<?php echo __('Deudas'); ?>'
			}, ,
			{
				extend: 'print',
				title: '<?php echo __('Deudas'); ?>'
			}
		],
		"language": {
			"sProcessing": "Procesando...",
			"sLengthMenu": "Mostrar _MENU_ registros",
			"sZeroRecords": "No se encontraron resultados",
			"sEmptyTable": "Ningún dato disponible en esta tabla",
			"sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
			"sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
			"sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
			"sInfoPostFix": "",
			"sSearch": "Buscar:",
			"sUrl": "",
			"sInfoThousands": ",",
			"sLoadingRecords": "Cargando...",
			"oPaginate": {
				"sFirst": "Primero",
				"sLast": "Último",
				"sNext": "Siguiente",
				"sPrevious": "Anterior"
			},
			"oAria": {
				"sSortAscending": ": Activar para ordenar la columna de manera ascendente",
				"sSortDescending": ": Activar para ordenar la columna de manera descendente"
			}
		}
	});

	function eliminar(id, valor) {
		table = $('#data').DataTable();
		table.row('#' + id).remove().draw(false);
		//$.ajax({url:'/Deudas/delete/'+valor,type:"post",data:{},success:function(n){} });
		$.ajax({
			url: '<?= $this->Html->url("/Deudas/delete/") ?>' + valor,
			type: "post",
			data: {},
			success: function(n) {}
		});
	}
	//} );
</script>