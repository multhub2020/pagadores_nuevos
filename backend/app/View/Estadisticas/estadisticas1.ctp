<section class="content-header">
    <h1>
        <?= Configure::read('namesysS'); ?>    <small><?php echo __('Estadistica'); ?></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= $this->Html->url('/Principal/') ?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
        <li class="active"><?php echo __('Estadistica'); ?></li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><?= $title = "Reporte por rango de fechas "?></h3>
                    <hr>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <?php echo $this->Form->create('Reporte', array('class' => 'form-horizontal')); ?>
                    <div class="row">
                        <div class="col-sm-2 text-center">
                            <div style="margin-top: 7px;"><label>Fecha de Desde</label></div>
                        </div>
                        <div class="col-sm-2">
                            <?= $this->Form->input('fecha_desde', array('id' => 'fecha_desde', 'div' => false, 'label' => false, 'class' => 'form-control fecha-mask', 'type' => 'text', 'title' => 'Fecha  desde', 'placeholder' => 'Fecha desde', 'value' => (isset($this->request->data['Reporte']['fecha_desde']) && !empty($this->request->data['Reporte']['fecha_desde']) ? $this->request->data['Reporte']['fecha_desde'] : ''))); ?>
                        </div>
                        <div class="col-sm-2 text-center">
                            <div style="margin-top: 7px;"><label>Fecha de Hasta</label></div>
                        </div>
                        <div class="col-sm-2">
                            <?= $this->Form->input('fecha_hasta', array('id' => 'fecha_hasta', 'div' => false, 'label' => false, 'class' => 'form-control fecha-mask', 'type' => 'text', 'title' => 'Fecha  hasta', 'placeholder' => 'Fecha hasta', 'value' => (isset($this->request->data['Reporte']['fecha_hasta']) && !empty($this->request->data['Reporte']['fecha_hasta']) ? $this->request->data['Reporte']['fecha_hasta'] : ''))); ?>
                        </div>
                    </div>

                    <br />

                    <div class="row">
                        <div class="col-sm-2 pull-right">
                            <input value="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Filtrar &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" class="btn bg-purple btn-flat margin" type="submit">
                        </div>
                    </div>
                    </form>

                    <hr />

                    <table id="data" class="table table-striped table-bordered responsive nowrap" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th><?php echo __('Usuarios Nuevos'); ?></th>
                                <th><?php echo __('Usuarios Invitados'); ?></th>
                                <th><?php echo __('Invitados Registrados'); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $id_tr = 0; ?>
                            <?php if(isset($datos)){ ?>
                            <?php foreach ($datos as $dato): ?>
                                <?php $id_tr++; ?>
                                <tr id="<?= 'tr_' . $id_tr; ?>">
                                    <td><?php echo h($dato['cantidad1']); ?>&nbsp;</td>
                                    <td><?php echo h($dato['cantidad2']); ?>&nbsp;</td>
                                    <td><?php echo h($dato['cantidad3']);?>&nbsp;</td>
                                </tr>
                            <?php endforeach; ?>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section><!-- /.content -->

<?php
$title = 'Estadistica rango fechas del ' . date("d/m/Y", strtotime($fecha_desde)) . ' al ' . date("d/m/Y", strtotime($fecha_hasta));
?>

<script type="text/javascript">

    $(document).ready(function () {
        $(".fecha-mask").mask("00/00/0000", {placeholder: "___/___/______"});
    });

    //$(document).ready(function() {
    $('#data').DataTable({
        dom: 'Brtlip',
        responsive: true,
        buttons: [
            {
                extend: 'copy',
                title: '<?php echo $title; ?>'
            },
            {
                extend: 'csv',
                title: '<?php echo $title; ?>',
                exportOptions: {
                    columns: [1, 2, 3]
                }
            },
            {
                extend: 'excel',
                title: '<?php echo $title; ?>'
            },
            {
                extend: 'pdf',
                title: '<?php echo $title; ?>',
                orientation: 'landscape',
                pageSize: 'LEGAL',
                exportOptions: {
                    columns: [1, 2, 3]
                }
            },
            ,
                    {
                        extend: 'print',
                        title: '<?php echo $title; ?>'
                    }
        ],
        "language":
                {
                    "sProcessing": "Procesando...",
                    "sLengthMenu": "Mostrar _MENU_ registros",
                    "sZeroRecords": "No se encontraron resultados",
                    "sEmptyTable": "Ningún dato disponible en esta tabla",
                    "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix": "",
                    "sSearch": "Buscar:",
                    "sUrl": "",
                    "sInfoThousands": ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst": "Primero",
                        "sLast": "Último",
                        "sNext": "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                }
    });


</script>
