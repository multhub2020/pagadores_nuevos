<section class="content-header">
  <h1>
    <?= Configure::read('namesysS'); ?>    <small><?php echo __('Homologación bancos'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= $this->Html->url('/Dashboard/')?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
    <li class="active"><?php echo __('Homologación bancos'); ?></li>
  </ol>
</section>
<section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"><?php echo __('Homologación bancos'); ?> Detalles</h3>
                        <hr>
                    </div><!-- /.box-header -->
                    <div class="box-body">
						<dl class="dl-horizontal">
								<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($homologacionbanco['Homologacionbanco']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Buro Afiliado'); ?></dt>
		<dd>
			<?php echo h($homologacionbanco['Homologacionbanco']['buro_afiliado']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Tkl Creditor Id'); ?></dt>
		<dd>
			<?php echo h($homologacionbanco['Homologacionbanco']['tkl_creditor_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created At'); ?></dt>
		<dd>
			<?php echo h($homologacionbanco['Homologacionbanco']['created_at']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Update At'); ?></dt>
		<dd>
			<?php echo h($homologacionbanco['Homologacionbanco']['update_at']); ?>
			&nbsp;
		</dd>
						</dl>
						<p>
						    		<?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $homologacionbanco['Homologacionbanco']['id'])); ?>
                            |
                            		<?php echo $this->Html->link(__('Volver al listado'), array('action' => 'index')); ?>
                        </p>
					</div>
			    </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
</section><!-- /.content -->
