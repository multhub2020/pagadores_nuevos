<section class="content-header">
  <h1>
    <?= Configure::read('namesysS'); ?>    <small><?php echo __('Instruccion Acreedors'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= $this->Html->url('/Dashboard/')?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
    <li class="active"><?php echo __('Instruccion Acreedors'); ?></li>
  </ol>
</section>
<section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"><?php echo __('Instruccion Acreedors'); ?> Detalles</h3>
                        <hr>
                    </div><!-- /.box-header -->
                    <div class="box-body">
						<dl class="dl-horizontal">
								<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($instruccionAcreedor['InstruccionAcreedor']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Idiomas'); ?></dt>
		<dd>
			<?php echo h($instruccionAcreedor['InstruccionAcreedor']['idiomas']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Acreedor'); ?></dt>
		<dd>
			<?php echo h($instruccionAcreedor['InstruccionAcreedor']['acreedor']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Instruccion'); ?></dt>
		<dd>
			<?php echo h($instruccionAcreedor['InstruccionAcreedor']['instruccion']); ?>
			&nbsp;
		</dd>
						</dl>
						<p>
						    		<?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $instruccionAcreedor['InstruccionAcreedor']['id'])); ?>
                            |
                            		<?php echo $this->Html->link(__('Volver al listado'), array('action' => 'index')); ?>
                        </p>
					</div>
			    </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
</section><!-- /.content -->
