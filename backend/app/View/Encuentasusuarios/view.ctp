<section class="content-header">
  <h1>
    <?= Configure::read('namesysS'); ?>    <small><?php echo __('Encuentasusuarios'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= $this->Html->url('/Dashboard/')?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
    <li class="active"><?php echo __('Encuentasusuarios'); ?></li>
  </ol>
</section>
<section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"><?php echo __('Encuentasusuarios'); ?> Detalles</h3>
                        <hr>
                    </div><!-- /.box-header -->
                    <div class="box-body">
						<dl class="dl-horizontal">
								<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($encuentasusuario['Encuentasusuario']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Encuenta'); ?></dt>
		<dd>
			<?php echo $this->Html->link($encuentasusuario['Encuenta']['id'], array('controller' => 'encuentas', 'action' => 'view', $encuentasusuario['Encuenta']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Usuario'); ?></dt>
		<dd>
			<?php echo $this->Html->link($encuentasusuario['Usuario']['email'], array('controller' => 'usuarios', 'action' => 'view', $encuentasusuario['Usuario']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Encuentasrespuesta'); ?></dt>
		<dd>
			<?php echo $this->Html->link($encuentasusuario['Encuentasrespuesta']['id'], array('controller' => 'encuentasrespuestas', 'action' => 'view', $encuentasusuario['Encuentasrespuesta']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Activo'); ?></dt>
		<dd>
			<?php echo h($encuentasusuario['Encuentasusuario']['activo']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($encuentasusuario['Encuentasusuario']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($encuentasusuario['Encuentasusuario']['modified']); ?>
			&nbsp;
		</dd>
						</dl>
						<p>
						    		<?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $encuentasusuario['Encuentasusuario']['id'])); ?>
                            |
                            		<?php echo $this->Html->link(__('Volver al listado'), array('action' => 'index')); ?>
                        </p>
					</div>
			    </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
</section><!-- /.content -->
