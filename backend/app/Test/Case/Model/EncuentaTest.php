<?php
App::uses('Encuenta', 'Model');

/**
 * Encuenta Test Case
 *
 */
class EncuentaTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.encuenta',
		'app.encuentasrespuesta',
		'app.encuentasusuario'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Encuenta = ClassRegistry::init('Encuenta');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Encuenta);

		parent::tearDown();
	}

}
