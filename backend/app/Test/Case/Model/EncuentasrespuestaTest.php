<?php
App::uses('Encuentasrespuesta', 'Model');

/**
 * Encuentasrespuesta Test Case
 *
 */
class EncuentasrespuestaTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.encuentasrespuesta',
		'app.encuenta',
		'app.encuentasusuario'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Encuentasrespuesta = ClassRegistry::init('Encuentasrespuesta');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Encuentasrespuesta);

		parent::tearDown();
	}

}
