<?php
App::uses('Ayuda', 'Model');

/**
 * Ayuda Test Case
 *
 */
class AyudaTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.ayuda',
		'app.usuario',
		'app.role',
		'app.historialpushusuario',
		'app.rolesmodulo',
		'app.modulo'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Ayuda = ClassRegistry::init('Ayuda');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Ayuda);

		parent::tearDown();
	}

}
