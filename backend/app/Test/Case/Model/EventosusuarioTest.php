<?php
App::uses('Eventosusuario', 'Model');

/**
 * Eventosusuario Test Case
 *
 */
class EventosusuarioTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.eventosusuario',
		'app.evento',
		'app.eventosrespuesta',
		'app.usuario',
		'app.role',
		'app.historialpushusuario'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Eventosusuario = ClassRegistry::init('Eventosusuario');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Eventosusuario);

		parent::tearDown();
	}

}
