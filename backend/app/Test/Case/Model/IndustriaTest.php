<?php
App::uses('Industria', 'Model');

/**
 * Industria Test Case
 *
 */
class IndustriaTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.industria'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Industria = ClassRegistry::init('Industria');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Industria);

		parent::tearDown();
	}

}
