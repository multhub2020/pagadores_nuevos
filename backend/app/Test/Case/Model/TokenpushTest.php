<?php
App::uses('Tokenpush', 'Model');

/**
 * Tokenpush Test Case
 *
 */
class TokenpushTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.tokenpush'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Tokenpush = ClassRegistry::init('Tokenpush');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Tokenpush);

		parent::tearDown();
	}

}
