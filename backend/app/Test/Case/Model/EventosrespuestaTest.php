<?php
App::uses('Eventosrespuesta', 'Model');

/**
 * Eventosrespuesta Test Case
 *
 */
class EventosrespuestaTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.eventosrespuesta',
		'app.evento',
		'app.eventosusuario'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Eventosrespuesta = ClassRegistry::init('Eventosrespuesta');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Eventosrespuesta);

		parent::tearDown();
	}

}
