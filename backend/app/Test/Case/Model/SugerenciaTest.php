<?php
App::uses('Sugerencia', 'Model');

/**
 * Sugerencia Test Case
 *
 */
class SugerenciaTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.sugerencia',
		'app.usuario',
		'app.role',
		'app.historialpushusuario'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Sugerencia = ClassRegistry::init('Sugerencia');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Sugerencia);

		parent::tearDown();
	}

}
