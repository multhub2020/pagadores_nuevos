<?php
App::uses('Encuentasusuario', 'Model');

/**
 * Encuentasusuario Test Case
 *
 */
class EncuentasusuarioTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.encuentasusuario',
		'app.encuenta',
		'app.encuentasrespuesta',
		'app.usuario',
		'app.role',
		'app.historialpushusuario'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Encuentasusuario = ClassRegistry::init('Encuentasusuario');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Encuentasusuario);

		parent::tearDown();
	}

}
