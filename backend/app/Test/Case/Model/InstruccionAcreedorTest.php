<?php
App::uses('InstruccionAcreedor', 'Model');

/**
 * InstruccionAcreedor Test Case
 *
 */
class InstruccionAcreedorTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.instruccion_acreedor'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->InstruccionAcreedor = ClassRegistry::init('InstruccionAcreedor');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->InstruccionAcreedor);

		parent::tearDown();
	}

}
