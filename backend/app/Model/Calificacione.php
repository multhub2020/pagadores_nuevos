<?php
App::uses('AppModel', 'Model');
/**
 * Calificacione Model
 *
 * @property Usuario $Usuario
 */
class Calificacione extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'usuario_id';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Usuario' => array(
			'className' => 'Usuario',
			'foreignKey' => 'usuario_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
