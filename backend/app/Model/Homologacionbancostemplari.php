<?php
App::uses('AppModel', 'Model');
/**
 * Homologacionbancostemplari Model
 *
 */
class Homologacionbancostemplari extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'buro_afiliado';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'buro_afiliado' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'tkl_creditor_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);
}
