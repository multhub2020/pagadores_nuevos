<?php
App::uses('AppModel', 'Model');
/**
 * Ayuda Model
 *
 * @property Usuario $Usuario
 */
class Ayuda extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'denominacion';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Usuario' => array(
			'className' => 'Usuario',
			'foreignKey' => 'usuario_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
