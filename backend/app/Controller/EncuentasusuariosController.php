<?php
App::uses('AppController', 'Controller');
/**
 * Encuentasusuarios Controller
 *
 * @property Encuentasusuario $Encuentasusuario
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class EncuentasusuariosController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Flash', 'Session');

/*
** var de layout
*
*/
	public $layout = "dashbord";

/*
*  *  beforeFilter check de session
*
*/	
	public function beforeFilter() {
		$this->checkSession();
	}

/**
 * index method
 *
 * @return void
 */
	public function index() {
     	  $usuario_rol      = $this->Session->read('usuario_rol');
     	  $usuario_id       = $this->Session->read('usuario_id');
          $this->set('encuentasusuarios', $this->Encuentasusuario->find('all', array('conditions'=>array('Encuentasusuario.activo'=>1))));
     	
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
	    $usuario_rol      = $this->Session->read('usuario_rol');
     	$usuario_id       = $this->Session->read('usuario_id');
		if (!$this->Encuentasusuario->exists($id)) {
			throw new NotFoundException(__('Invalid encuentasusuario'));
		}
		$options = array('conditions' => array('Encuentasusuario.' . $this->Encuentasusuario->primaryKey => $id));
		$this->set('encuentasusuario', $this->Encuentasusuario->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
	    $usuario_rol      = $this->Session->read('usuario_rol');
     	$usuario_id       = $this->Session->read('usuario_id');
		if ($this->request->is('post')) {
			$this->Encuentasusuario->create();
			if ($this->Encuentasusuario->save($this->request->data)) {
				$this->Flash->success(__('Registro Guardado.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
			}
		}
		$encuentas = $this->Encuentasusuario->Encuentum->find('list');
		$usuarios = $this->Encuentasusuario->Usuario->find('list');
		$encuentasrespuestas = $this->Encuentasusuario->Encuentasrespuestum->find('list');
		$this->set(compact('encuentas', 'usuarios', 'encuentasrespuestas'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
	    $usuario_rol      = $this->Session->read('usuario_rol');
     	$usuario_id       = $this->Session->read('usuario_id');
		if (!$this->Encuentasusuario->exists($id)) {
			throw new NotFoundException(__('Invalid encuentasusuario'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Encuentasusuario->save($this->request->data)) {
				$this->Flash->success(__('Registro Guardado.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
			}
		} else {
			$options = array('conditions' => array('Encuentasusuario.' . $this->Encuentasusuario->primaryKey => $id));
			$this->request->data = $this->Encuentasusuario->find('first', $options);
		}
		$encuentas = $this->Encuentasusuario->Encuentum->find('list');
		$usuarios = $this->Encuentasusuario->Usuario->find('list');
		$encuentasrespuestas = $this->Encuentasusuario->Encuentasrespuestum->find('list');
		$this->set(compact('encuentas', 'usuarios', 'encuentasrespuestas'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void 
 */
	public function delete($id = null) {
	    $this->layout     = "ajax";
	    $usuario_rol      = $this->Session->read('usuario_rol');
     	$usuario_id       = $this->Session->read('usuario_id');
        $this->request->data['Encuentasusuario']['id']     = $id;
	    $this->request->data['Encuentasusuario']['activo'] = 2;
	}
}
