<?php
App::uses('AppController', 'Controller');
/**
 * Historialpushes Controller
 *
 * @property Historialpush $Historialpush
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class EstadisticasController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Flash', 'Session');

	var $uses = array('Usuario');

/*
** var de layout
*
*/
	public $layout = "principal";

/*
*  *  beforeFilter check de session
*
*/	
	public function beforeFilter() {
		$this->checkSession(3);
	}

/**
 * index method
 * ALTER TABLE `usuarios` CHANGE `fechaingreso` `fechaingreso` DATE NULL;
 * ALTER TABLE `usuarios` ADD `fechaaceptacion` DATE NULL AFTER `fechaingreso`;
 * @return void
 */
	public function estadisticas1() {
     	$usuario_rol      = $this->Session->read('usuario_rol');
     	$usuario_id       = $this->Session->read('usuario_id');
     	if ($this->request->is('post')) {
			$fecha_desde = $this->request->data['Reporte']['fecha_desde'];
			$fecha_hasta = $this->request->data['Reporte']['fecha_hasta'];
			$this->set('fecha_desde', $fecha_desde);
			$this->set('fecha_hasta', $fecha_hasta);
			$contar = 0;
			$datos  = array();

			$conditions2['Usuario.fechaaceptacion BETWEEN ? and ?'] = array(cambiar_formato_fecha($fecha_desde), cambiar_formato_fecha($fecha_hasta) );
			$d2 = $this->Usuario->find('all', array('conditions'=> $conditions2 ));

			foreach($d2 as $key2) {
				if(!isset($datos[$contar]['cantidad1'])){$datos[$contar]['cantidad1']=0;}
				if(!isset($datos[$contar]['cantidad2'])){$datos[$contar]['cantidad2']=0;}
				if(!isset($datos[$contar]['cantidad3'])){$datos[$contar]['cantidad3']=0;}
				$datos[$contar]['cantidad1'] = $key2['Usuario']['invitado']==2?($datos[$contar]['cantidad1']+1):$datos[$contar]['cantidad1'];
				//$contar++;
			}

			$conditions['Usuario.fechaingreso BETWEEN ? and ?'] = array(cambiar_formato_fecha($fecha_desde), cambiar_formato_fecha($fecha_hasta) );
			$d = $this->Usuario->find('all', array('conditions'=> $conditions ));

			foreach($d as $key1) {
				if(!isset($datos[$contar]['cantidad1'])){$datos[$contar]['cantidad1']=0;}
				if(!isset($datos[$contar]['cantidad2'])){$datos[$contar]['cantidad2']=0;}
				if(!isset($datos[$contar]['cantidad3'])){$datos[$contar]['cantidad3']=0;}
				//$datos[$contar]['cantidad2'] = $key1['Usuario']['invitado']==1?($datos[$contar]['cantidad2']+1):$datos[$contar]['cantidad2'];
				$datos[$contar]['cantidad2'] = $datos[$contar]['cantidad2']+1;
				//$contar++;
			}

			$conditions3['Usuario.fechaaceptacion BETWEEN ? and ?'] = array(cambiar_formato_fecha("01/01/2020"), cambiar_formato_fecha($fecha_hasta) );
			$d3 = $this->Usuario->find('all', array('conditions'=> $conditions3 ));
			
			foreach($d3 as $key3) {
				if(!isset($datos[$contar]['cantidad1'])){$datos[$contar]['cantidad1']=0;}
				if(!isset($datos[$contar]['cantidad2'])){$datos[$contar]['cantidad2']=0;}
				if(!isset($datos[$contar]['cantidad3'])){$datos[$contar]['cantidad3']=0;}
				$datos[$contar]['cantidad3'] = $key3['Usuario']['invitado']==2?($datos[$contar]['cantidad3']+1):$datos[$contar]['cantidad3'];
				//$contar++;
			}

			
			//pr($d);
			$this->set('datos', $datos);
		}else{

			$this->set('fecha_desde', "");
			$this->set('fecha_hasta', "");
		}
	}

}

?>