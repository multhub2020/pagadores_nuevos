<?php
App::uses('AppController', 'Controller');
/**
 * Homologacionbancostemplaris Controller
 *
 * @property Homologacionbancostemplari $Homologacionbancostemplari
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class HomologacionbancostemplarisController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Flash', 'Session');

/*
** var de layout
*
*/
	public $layout = "principal";

/*
*  *  beforeFilter check de session
*
*/	
	public function beforeFilter() {
		$this->checkSession(4);
	}

/**
 * index method
 *
 * @return void
 */
	public function index() {
     	  $usuario_rol      = $this->Session->read('usuario_rol');
     	  $usuario_id       = $this->Session->read('usuario_id');
          $this->set('homologacionbancostemplaris', $this->Homologacionbancostemplari->find('all', array('order'=>array('Homologacionbancostemplari.buro_afiliado'=>'ASC'))));
     	
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
	    $usuario_rol      = $this->Session->read('usuario_rol');
     	$usuario_id       = $this->Session->read('usuario_id');
		if (!$this->Homologacionbancostemplari->exists($id)) {
			throw new NotFoundException(__('Invalid homologacionbancostemplari'));
		}
		$options = array('conditions' => array('Homologacionbancostemplari.' . $this->Homologacionbancostemplari->primaryKey => $id));
		$this->set('homologacionbancostemplari', $this->Homologacionbancostemplari->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
	    $usuario_rol      = $this->Session->read('usuario_rol');
     	$usuario_id       = $this->Session->read('usuario_id');
		if ($this->request->is('post')) {
			$this->Homologacionbancostemplari->create();
			if ($this->Homologacionbancostemplari->save($this->request->data)) {
				$this->Flash->success(__('Registro Guardado.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
	    $usuario_rol      = $this->Session->read('usuario_rol');
     	$usuario_id       = $this->Session->read('usuario_id');
		if (!$this->Homologacionbancostemplari->exists($id)) {
			throw new NotFoundException(__('Invalid homologacionbancostemplari'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Homologacionbancostemplari->save($this->request->data)) {
				$this->Flash->success(__('Registro Guardado.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
			}
		} else {
			$options = array('conditions' => array('Homologacionbancostemplari.' . $this->Homologacionbancostemplari->primaryKey => $id));
			$this->request->data = $this->Homologacionbancostemplari->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void 
 */
	public function delete($id = null) {
	    $this->layout     = "ajax";
	    $usuario_rol      = $this->Session->read('usuario_rol');
     	$usuario_id       = $this->Session->read('usuario_id');
        $this->request->data['Homologacionbancostemplari']['id']     = $id;
	    $this->request->data['Homologacionbancostemplari']['activo'] = 2;
		$this->Homologacionbancostemplari->deleteAll(array('Homologacionbancostemplari.id'=>$id));
	}
}
