<?php
App::uses('AppController', 'Controller');
/**
 * Encuentasrespuestas Controller
 *
 * @property Encuentasrespuesta $Encuentasrespuesta
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class EncuentasrespuestasController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Flash', 'Session');

/*
** var de layout
*
*/
	public $layout = "dashbord";

/*
*  *  beforeFilter check de session
*
*/	
	public function beforeFilter() {
		$this->checkSession();
	}

/**
 * index method
 *
 * @return void
 */
	public function index() {
     	  $usuario_rol      = $this->Session->read('usuario_rol');
     	  $usuario_id       = $this->Session->read('usuario_id');
          $this->set('encuentasrespuestas', $this->Encuentasrespuesta->find('all', array('conditions'=>array('Encuentasrespuesta.activo'=>1))));
     	
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
	    $usuario_rol      = $this->Session->read('usuario_rol');
     	$usuario_id       = $this->Session->read('usuario_id');
		if (!$this->Encuentasrespuesta->exists($id)) {
			throw new NotFoundException(__('Invalid encuentasrespuesta'));
		}
		$options = array('conditions' => array('Encuentasrespuesta.' . $this->Encuentasrespuesta->primaryKey => $id));
		$this->set('encuentasrespuesta', $this->Encuentasrespuesta->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
	    $usuario_rol      = $this->Session->read('usuario_rol');
     	$usuario_id       = $this->Session->read('usuario_id');
		if ($this->request->is('post')) {
			$this->Encuentasrespuesta->create();
			if ($this->Encuentasrespuesta->save($this->request->data)) {
				$this->Flash->success(__('Registro Guardado.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
			}
		}
		$encuentas = $this->Encuentasrespuesta->Encuentum->find('list');
		$this->set(compact('encuentas'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
	    $usuario_rol      = $this->Session->read('usuario_rol');
     	$usuario_id       = $this->Session->read('usuario_id');
		if (!$this->Encuentasrespuesta->exists($id)) {
			throw new NotFoundException(__('Invalid encuentasrespuesta'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Encuentasrespuesta->save($this->request->data)) {
				$this->Flash->success(__('Registro Guardado.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
			}
		} else {
			$options = array('conditions' => array('Encuentasrespuesta.' . $this->Encuentasrespuesta->primaryKey => $id));
			$this->request->data = $this->Encuentasrespuesta->find('first', $options);
		}
		$encuentas = $this->Encuentasrespuesta->Encuentum->find('list');
		$this->set(compact('encuentas'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void 
 */
	public function delete($id = null) {
	    $this->layout     = "ajax";
	    $usuario_rol      = $this->Session->read('usuario_rol');
     	$usuario_id       = $this->Session->read('usuario_id');
        $this->request->data['Encuentasrespuesta']['id']     = $id;
	    $this->request->data['Encuentasrespuesta']['activo'] = 2;
	}
}
