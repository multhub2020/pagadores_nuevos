<?php
App::uses('AppController', 'Controller');
/**
 * InstruccionAcreedors Controller
 *
 * @property InstruccionAcreedor $InstruccionAcreedor
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class InstruccionAcreedorsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Flash', 'Session');

/*
** var de layout
*
*/
	public $layout = "principal";

/*
*  *  beforeFilter check de session
*
*/	
	public function beforeFilter() {
		$this->checkSession(5);
	}

/**
 * index method
 *
 * @return void
 */
	public function index() {
     	  $usuario_rol      = $this->Session->read('usuario_rol');
     	  $usuario_id       = $this->Session->read('usuario_id');
          $this->set('instruccionAcreedors', $this->InstruccionAcreedor->find('all', array('conditions'=>array('InstruccionAcreedor.activo'=>1))));
     	
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
	    $usuario_rol      = $this->Session->read('usuario_rol');
     	$usuario_id       = $this->Session->read('usuario_id');
		if (!$this->InstruccionAcreedor->exists($id)) {
			throw new NotFoundException(__('Invalid instruccion acreedor'));
		}
		$options = array('conditions' => array('InstruccionAcreedor.' . $this->InstruccionAcreedor->primaryKey => $id));
		$this->set('instruccionAcreedor', $this->InstruccionAcreedor->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
	    $usuario_rol      = $this->Session->read('usuario_rol');
     	$usuario_id       = $this->Session->read('usuario_id');
		if ($this->request->is('post')) {
			$this->InstruccionAcreedor->create();
			if ($this->InstruccionAcreedor->save($this->request->data)) {
				$this->Flash->success(__('Registro Guardado.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
	    $usuario_rol      = $this->Session->read('usuario_rol');
     	$usuario_id       = $this->Session->read('usuario_id');
		if (!$this->InstruccionAcreedor->exists($id)) {
			throw new NotFoundException(__('Invalid instruccion acreedor'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->InstruccionAcreedor->save($this->request->data)) {
				$this->Flash->success(__('Registro Guardado.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
			}
		} else {
			$options = array('conditions' => array('InstruccionAcreedor.' . $this->InstruccionAcreedor->primaryKey => $id));
			$this->request->data = $this->InstruccionAcreedor->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void 
 */
	public function delete($id = null) {
	    $this->layout     = "ajax";
	    $usuario_rol      = $this->Session->read('usuario_rol');
     	$usuario_id       = $this->Session->read('usuario_id');
        $this->request->data['InstruccionAcreedor']['id']     = $id;
	    $this->request->data['InstruccionAcreedor']['activo'] = 2;
	}
}
