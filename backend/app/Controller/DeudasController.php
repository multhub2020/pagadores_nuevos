<?php
App::uses('AppController', 'Controller');
/**
 * Deudas Controller
 *
 * @property Deuda $Deuda
 * @property PaginatorComponent $Paginator
 */
class DeudasController extends AppController
{

	/**
	 * Components
	 *
	 * @var array
	 */
	public $components = array('Paginator');

	/*
** var de layout
*
*/
	public $layout = "principal";

	/*
*  *  beforeFilter check de session
*
*/
	public function beforeFilter()
	{
		$this->checkSession(7);
	}

	/**
	 * index method
	 *
	 * @return void
	 */
	public function index()
	{
		$usuario_rol      = $this->Session->read('usuario_rol');
		$usuario_id       = $this->Session->read('usuario_id');
		$this->set('deudas', $this->Deuda->find('all'));
	}

	/**
	 * view method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function view($id = null)
	{
		$usuario_rol      = $this->Session->read('usuario_rol');
		$usuario_id       = $this->Session->read('usuario_id');
		if (!$this->Deuda->exists($id)) {
			throw new NotFoundException(__('Invalid deuda'));
		}
		$options = array('conditions' => array('Deuda.' . $this->Deuda->primaryKey => $id));
		$this->set('deuda', $this->Deuda->find('first', $options));
	}

	/**
	 * add method
	 *
	 * @return void
	 */
	/* public function add()
	{
		$usuario_rol      = $this->Session->read('usuario_rol');
		$usuario_id       = $this->Session->read('usuario_id');
		if ($this->request->is('post')) {
			$this->Deuda->create();
			if ($this->Deuda->save($this->request->data)) {
				$this->Flash->success(__('Registro Guardado.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
			}
		}
	} */

	/**
	 * edit method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	/* public function edit($id = null)
	{
		$usuario_rol      = $this->Session->read('usuario_rol');
		$usuario_id       = $this->Session->read('usuario_id');
		if (!$this->Deuda->exists($id)) {
			throw new NotFoundException(__('Invalid deuda'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Deuda->save($this->request->data)) {
				$this->Flash->success(__('Registro Guardado.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
			}
		} else {
			$options = array('conditions' => array('Deuda.' . $this->Deuda->primaryKey => $id));
			$this->request->data = $this->Deuda->find('first', $options);
		}
	} */

	/**
	 * delete method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void 
	 */
	/* public function delete($id = null)
	{
		$this->layout     = "ajax";
		$usuario_rol      = $this->Session->read('usuario_rol');
		$usuario_id       = $this->Session->read('usuario_id');
		$this->request->data['Deuda']['id']     = $id;
		$this->request->data['Deuda']['activo'] = 2;
	} */
}
