<?php
App::uses('AppController', 'Controller');
/**
 * Eventos Controller
 *
 * @property Evento $Evento
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class EventosController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Flash', 'Session');

	var $uses = array('Evento', 'Eventosrespuesta', 'Historialpush', 'Usuario', 'Tokenpush', 'Eventosusuario');

/*
** var de layout
*
*/
	public $layout = "principal";

/*
*  *  beforeFilter check de session
*
*/	
	public function beforeFilter() {
		$this->checkSession(6);
	}


/**
 * index method
 *
 * @return void
 */

	public function reporte() {
     	$usuario_rol      = $this->Session->read('usuario_rol');
     	$usuario_id       = $this->Session->read('usuario_id');
     	if ($this->request->is('post')) {
			$fecha_desde = $this->request->data['Reporte']['fecha_desde'];
			$fecha_hasta = $this->request->data['Reporte']['fecha_hasta'];
			$this->set('fecha_desde', $fecha_desde);
			$this->set('fecha_hasta', $fecha_hasta);
			$contar = 0;
			$datos  = array();

			$conditions2['Eventosusuario.created BETWEEN ? and ?'] = array(cambiar_formato_fecha($fecha_desde), cambiar_formato_fecha($fecha_hasta) );
			$d2 = $this->Eventosusuario->find('all', array('conditions'=> $conditions2 ));
			
			//pr($d);
			$this->set('datos', $d2);
		}else{

			$this->set('fecha_desde', "");
			$this->set('fecha_hasta', "");
		}
	}
		

/**
 * index method
 *
 * @return void
 */
	public function index() {
     	  $usuario_rol      = $this->Session->read('usuario_rol');
     	  $usuario_id       = $this->Session->read('usuario_id');
          $this->set('eventos', $this->Evento->find('all', array('conditions'=>array('Evento.activo'=>1))));
     	
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
	    $usuario_rol      = $this->Session->read('usuario_rol');
     	$usuario_id       = $this->Session->read('usuario_id');
		if (!$this->Evento->exists($id)) {
			throw new NotFoundException(__('Invalid evento'));
		}
		$options = array('conditions' => array('Evento.' . $this->Evento->primaryKey => $id));
		$this->set('evento', $this->Evento->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
	    $usuario_rol      = $this->Session->read('usuario_rol');
     	$usuario_id       = $this->Session->read('usuario_id');
		if ($this->request->is('post')) {
			$this->Evento->create();
			if ($this->Evento->save($this->request->data)) {
				$id_  =  $this->Evento->id;
				if(!empty($this->request->data["Afiliado"]["Modulos2"])){
					foreach($this->request->data["Afiliado"]["Modulos2"] as $modulo){
						$this->request->data["Eventosrespuesta"]["evento_id"]  = $id_;
						$this->request->data["Eventosrespuesta"]["denominacion"] = $modulo['denominacion'];
						$this->Eventosrespuesta->create();
						if ($this->Eventosrespuesta->save($this->request->data)) {

						}else{
							$stop = 1;
						}
					}
				}
				$Usuarios = $this->Tokenpush->find('all', array('conditions'=>array()));
                foreach ($Usuarios as $key) {
							$token = Configure::read('firebase');
							$not   = "";//"DATABASE OBJECT NOTIFICATION";
							//Datos/
							$to_android = $key['Tokenpush']['token_push'];//$usuario[0]['User']['push_token'];//Datos del usuario
							$to_ios     = $key['Tokenpush']['token_push'];//$usuario[0]['User']['push_token'];//Datos del usuario
							$platform   = $key['Tokenpush']['platf_push'];//Datos del usuario
							$titulo     = "MultHub!!";
							$mensaje    = Configure::read('mensaje_evento');
							$data       = null;
							$headers = [
							    "Authorization:key=".$token." ",
							    'Content-Type: application/json'
							];
							if($platform === 'ios') {
							    $data = [
							        'to' => $to_ios,
							        'notification' => [
							            'body'   => $mensaje,
							            'title'  => $titulo,
							        ],
							        "data" => [// aditional data for iOS
							            "extra-key" => "extra-value",
							        ],
							        'notId' =>$not,//unique id for each notification
							    ];
							} elseif ($platform === 'android') {
							    $data = [
							        'to' => $to_android,
							        'data' => [
							            'body'   => $mensaje,
							            'title'  => $titulo,
							        ]
							    ];
							}
							$ch = curl_init();
							//curl_setopt( $ch,CURLOPT_URL, 'https://gcm-http.googleapis.com/gcm/send' );
							curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
							curl_setopt( $ch,CURLOPT_POST, true );
							curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
							curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
							curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
							curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $data ) );
							curl_setopt($ch, CURLOPT_FAILONERROR, TRUE);
							$result = curl_exec($ch);
							curl_close( $ch );
				}//fin function
				$this->Flash->success(__('Registro Guardado.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
	    $usuario_rol      = $this->Session->read('usuario_rol');
     	$usuario_id       = $this->Session->read('usuario_id');
		if (!$this->Evento->exists($id)) {
			throw new NotFoundException(__('Invalid evento'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Evento->save($this->request->data)) {
								$this->Eventosrespuesta->deleteAll(array('Eventosrespuesta.evento_id'=>$id));
				//pr($this->request->data["Afiliado"]);
				if(!empty($this->request->data["Afiliado"]["Modulos2"])){
					foreach($this->request->data["Afiliado"]["Modulos2"] as $modulo){
						$this->request->data["Eventosrespuesta"]["evento_id"]  = $id;
						$this->request->data["Eventosrespuesta"]["denominacion"] = $modulo['denominacion'];
						$this->Eventosrespuesta->create();
						if ($this->Eventosrespuesta->save($this->request->data)) {

						}else{
							$stop = 1;
						}
					}
				}
				$Usuarios = $this->Tokenpush->find('all', array('conditions'=>array()));
                foreach ($Usuarios as $key) {
							$token = Configure::read('firebase');
							$not   = "";//"DATABASE OBJECT NOTIFICATION";
							//Datos/
							$to_android = $key['Tokenpush']['token_push'];//$usuario[0]['User']['push_token'];//Datos del usuario
							$to_ios     = $key['Tokenpush']['token_push'];//$usuario[0]['User']['push_token'];//Datos del usuario
							$platform   = $key['Tokenpush']['platf_push'];//Datos del usuario
							$titulo     = "MultHub!!";
							$mensaje    = Configure::read('mensaje_evento');
							$data       = null;
							$headers = [
							    "Authorization:key=".$token." ",
							    'Content-Type: application/json'
							];
							if($platform === 'ios') {
							    $data = [
							        'to' => $to_ios,
							        'notification' => [
							            'body'   => $mensaje,
							            'title'  => $titulo,
							        ],
							        "data" => [// aditional data for iOS
							            "extra-key" => "extra-value",
							        ],
							        'notId' =>$not,//unique id for each notification
							    ];
							} elseif ($platform === 'android') {
							    $data = [
							        'to' => $to_android,
							        'data' => [
							            'body'   => $mensaje,
							            'title'  => $titulo,
							        ]
							    ];
							}
							$ch = curl_init();
							//curl_setopt( $ch,CURLOPT_URL, 'https://gcm-http.googleapis.com/gcm/send' );
							curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
							curl_setopt( $ch,CURLOPT_POST, true );
							curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
							curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
							curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
							curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $data ) );
							curl_setopt($ch, CURLOPT_FAILONERROR, TRUE);
							$result = curl_exec($ch);
							curl_close( $ch );
				}//fin function
				$this->Flash->success(__('Registro Guardado.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
			}
		} else {
			$options = array('conditions' => array('Evento.' . $this->Evento->primaryKey => $id));
			$this->request->data = $this->Evento->find('first', $options);
		}
		$eventosrespuestas = $this->Eventosrespuesta->find('all', array('conditions'=>array('evento_id'=>$id)));
		$this->set(compact('eventosrespuestas'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void 
 */
	public function delete($id = null) {
	    $this->layout     = "ajax";
	    $usuario_rol      = $this->Session->read('usuario_rol');
     	$usuario_id       = $this->Session->read('usuario_id');
        $this->request->data['Evento']['id']     = $id;
	    $this->request->data['Evento']['activo'] = 2;
	    $this->Evento->save($this->request->data);
	}
}
