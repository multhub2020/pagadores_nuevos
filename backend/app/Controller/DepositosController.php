<?php
App::uses('AppController', 'Controller');
/**
 * Depositos Controller
 *
 * @property Deposito $Deposito
 * @property PaginatorComponent $Paginator
 */
class DepositosController extends AppController
{

	/**
	 * Components
	 *
	 * @var array
	 */
	public $components = array('Paginator');

	/*
** var de layout
*
*/
	public $layout = "principal";

	/*
*  *  beforeFilter check de session
*
*/
	public function beforeFilter()
	{
		$this->checkSession(6);
	}

	/**
	 * index method
	 *
	 * @return void
	 */
	public function index()
	{
		$usuario_rol      = $this->Session->read('usuario_rol');
		$usuario_id       = $this->Session->read('usuario_id');
		$this->set('depositos', $this->Deposito->find('all', array('order' => array('Deposito.id' => 'DESC'))));
	}

	/**
	 * view method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function view($id = null)
	{
		$usuario_rol      = $this->Session->read('usuario_rol');
		$usuario_id       = $this->Session->read('usuario_id');
		if (!$this->Deposito->exists($id)) {
			throw new NotFoundException(__('Invalid deposito'));
		}
		$options = array('conditions' => array('Deposito.' . $this->Deposito->primaryKey => $id));
		$this->set('deposito', $this->Deposito->find('first', $options));
	}

	/**
	 * add method
	 *
	 * @return void
	 */
	public function add()
	{
		$usuario_rol      = $this->Session->read('usuario_rol');
		$usuario_id       = $this->Session->read('usuario_id');
		if ($this->request->is('post')) {
			$this->Deposito->create();
			if ($this->Deposito->save($this->request->data)) {
				$this->Flash->success(__('Registro Guardado.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
			}
		}
	}

	/**
	 * edit method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function edit($id = null)
	{
		$usuario_rol      = $this->Session->read('usuario_rol');
		$usuario_id       = $this->Session->read('usuario_id');
		if (!$this->Deposito->exists($id)) {
			throw new NotFoundException(__('Invalid deposito'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Deposito->save($this->request->data)) {
				$this->Flash->success(__('Registro Guardado.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
			}
		} else {
			$options = array('conditions' => array('Deposito.' . $this->Deposito->primaryKey => $id));
			$this->request->data = $this->Deposito->find('first', $options);
		}
	}

	/**
	 * delete method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void 
	 */
	public function delete($id = null)
	{
		$this->layout     = "ajax";
		$usuario_rol      = $this->Session->read('usuario_rol');
		$usuario_id       = $this->Session->read('usuario_id');
		$this->request->data['Deposito']['id']     = $id;
		$this->request->data['Deposito']['activo'] = 2;
		$this->Deposito->deleteAll(array('Deposito.id'=>$id));
	}
}
