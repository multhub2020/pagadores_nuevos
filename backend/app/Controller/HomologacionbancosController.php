<?php
App::uses('AppController', 'Controller');
/**
 * Homologacionbancos Controller
 *
 * @property Homologacionbanco $Homologacionbanco
 * @property PaginatorComponent $Paginator
 */
class HomologacionbancosController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/*
** var de layout
*
*/
	public $layout = "principal";

/*
*  *  beforeFilter check de session
*
*/	
	public function beforeFilter() {
		$this->checkSession(3);
	}

/**
 * index method
 *
 * @return void
 */
	public function index() {
     	  $usuario_rol      = $this->Session->read('usuario_rol');
     	  $usuario_id       = $this->Session->read('usuario_id');
		  //$this->set('homologacionbancos', $this->Homologacionbanco->find('all', array('conditions'=>array('Homologacionbanco.activo'=>1))));
		  $this->set('homologacionbancos', $this->Homologacionbanco->find('all', array('order'=>array('Homologacionbanco.buro_afiliado'=>'ASC'))));
     	
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
	    $usuario_rol      = $this->Session->read('usuario_rol');
     	$usuario_id       = $this->Session->read('usuario_id');
		if (!$this->Homologacionbanco->exists($id)) {
			throw new NotFoundException(__('Invalid homologacionbanco'));
		}
		$options = array('conditions' => array('Homologacionbanco.' . $this->Homologacionbanco->primaryKey => $id));
		$this->set('homologacionbanco', $this->Homologacionbanco->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
	    $usuario_rol      = $this->Session->read('usuario_rol');
     	$usuario_id       = $this->Session->read('usuario_id');
		if ($this->request->is('post')) {
			$this->Homologacionbanco->create();
			if ($this->Homologacionbanco->save($this->request->data)) {
				$this->Flash->success(__('Registro Guardado.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
	    $usuario_rol      = $this->Session->read('usuario_rol');
     	$usuario_id       = $this->Session->read('usuario_id');
		if (!$this->Homologacionbanco->exists($id)) {
			throw new NotFoundException(__('Invalid homologacionbanco'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Homologacionbanco->save($this->request->data)) {
				$this->Flash->success(__('Registro Guardado.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
			}
		} else {
			$options = array('conditions' => array('Homologacionbanco.' . $this->Homologacionbanco->primaryKey => $id));
			$this->request->data = $this->Homologacionbanco->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void 
 */
	public function delete($id = null) {
	    $this->layout     = "ajax";
	    $usuario_rol      = $this->Session->read('usuario_rol');
     	$usuario_id       = $this->Session->read('usuario_id');
        $this->request->data['Homologacionbanco']['id']     = $id;
		$this->request->data['Homologacionbanco']['activo'] = 2;
		$this->Homologacionbanco->deleteAll(array('Homologacionbanco.id'=>$id));
	}
}
