import Foundation
import credoappsdk
 
@objc(CredoAppSdk)
public class CredoAppSdk : CDVPlugin {
 
  @objc
  func collect(_ command: CDVInvokedUrlCommand) {
    let request = command.argument(at: 0) as? Dictionary<String, String>
    DispatchQueue.global().async {
            
            let pluginResult:CDVPluginResult
            
            do{
                let url: String = request?["url"] ?? ""
                let authKey: String = request?["authKey"] ?? ""
                let recordNumber: String = request?["recordNumber"] ?? ""
                
                let credoAppService = try CredoAppService(url: url, authKey: authKey)
                let rn = try credoAppService.collectData(recordNumber:recordNumber)
                
                pluginResult = CDVPluginResult.init(status: CDVCommandStatus_OK, messageAs: rn)
                
            } catch let error as CredoAppError{
                let errorType: String = self.convertErrorTypeToString(errorType: error.getErrorType())
                
                let errorMessage: String? = error.getErrorMessage() != nil ? error.getErrorMessage()! : nil
 
                let errorResultMsg = errorMessage == nil ? errorType : "\(errorType) - \(errorMessage!)"
                
                pluginResult = CDVPluginResult.init(status: CDVCommandStatus_ERROR, messageAs: errorResultMsg)
 
            } catch {
                pluginResult = CDVPluginResult.init(status: CDVCommandStatus_ERROR)
            }
        
            self.commandDelegate.send(pluginResult, callbackId: command.callbackId)
       }
    }
    
    private func convertErrorTypeToString(errorType: credoappsdk.ErrorType) -> String {
        switch errorType {
        case ErrorType.InvalidArgument:
            return "InvalidArgument"
        case ErrorType.NetworkConnectionIssue:
            return "NetworkConnectionIssues"
        case ErrorType.ForbiddenAction:
            return "ForbiddenAction"
        case ErrorType.ServerError:
            return "ServerError"
        case ErrorType.UnknownIssue:
            return "UnknownIssue"
        default:
            return "UnknownIssue"
        }
    }
}
 
 

