package com.credoappsdk;

import android.os.Build;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.PermissionHelper;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Collection;

import credoapp.CredoAppException;
import credoapp.CredoAppService;

public class CredoAppSdk extends CordovaPlugin {

    private static final int PERMISSION_REQUEST_CODE = 0x0ba2c;
    private static final String URL = "url";
    private static final String AUTH_KEY = "authKey";
    private static final String RECORD_NUMBER = "recordNumber";

    private CredoAppService _credoAppService;
    private CallbackContext _callbackContext;
    private String _recordNumber;

    public CredoAppSdk() {
    }

    @Override
    public boolean execute(String action, JSONArray args, final CallbackContext callbackContext) {

        if (action.equals("collect")) {
            try {
                this._callbackContext = callbackContext;
                JSONObject obj = args.optJSONObject(0);
                if (obj != null) {
                    final String url = obj.optString(URL);
                    final String authKey = obj.optString(AUTH_KEY);
                    _recordNumber = obj.optString(RECORD_NUMBER);

                    cordova.getThreadPool().execute(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                _credoAppService = new CredoAppService(cordova.getActivity().getBaseContext().getApplicationContext(), url, authKey);
                                _credoAppService.setIgnorePermission(true);
                                final Collection<String> ungrantedPermissions = _credoAppService.getUngrantedPermissions();
                                ungrantedPermissions.remove("android.permission.QUERY_ALL_PACKAGES");
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !ungrantedPermissions.isEmpty()) {
                                        PermissionHelper.requestPermissions(CredoAppSdk.this,
                                                PERMISSION_REQUEST_CODE,
                                                ungrantedPermissions.toArray(new String[ungrantedPermissions.size()]));
                                } else {
                                    CredoAppSdk.this.collect(_credoAppService, _recordNumber, _callbackContext);
                                }
                            } catch (CredoAppException e) {
                                callbackContext.error(e.getType() + " - " + e.getMessage());
                            }
                        }
                    });
                }
            } catch (Exception e) {
                callbackContext.error(e.getMessage());
            }

            return true;
        }

        return false;
    }

    public void onRequestPermissionResult(int requestCode, String[] permissions, int[] grantResults) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && PERMISSION_REQUEST_CODE == requestCode) {
            cordova.getThreadPool().execute(new Runnable() {
                @Override
                public void run() {
                    CredoAppSdk.this.collect(_credoAppService, _recordNumber, _callbackContext);
                }
            });
        }
    }

    private void collect(CredoAppService credoAppService, String recordNumber, CallbackContext callbackContext) {
        try {
            callbackContext.success(credoAppService.collectData(recordNumber));
        } catch (CredoAppException e) {
            callbackContext.error(e.getType() + " - " + e.getMessage());
        }
    }
}
