import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl} from '@angular/forms';

import { Router } from '@angular/router';
import { Platform, NavController, LoadingController, AlertController } from '@ionic/angular';

import { Menus } from '../../providers/menus';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { Variablesglobales } from '../../providers/variablesglobal';

@Component({
  selector: 'app-listarinvitados',
  templateUrl: 'listarinvitados.html',
  styleUrls: ['listarinvitados.scss'],
  providers:[Menus, SocialSharing, Variablesglobales]
})

export class Listarinvitados {
  public myForm: FormGroup;
  public loading: any;
  public usuarioid: string;
  public imgurl   = new Variablesglobales();
  public imgurl2: any;
  public nombres: string;
  public apellidos: string;
  public avatar:string="";
  public invitados = "";
  public invitados2 = "";
  public ver = "";
  public seguidores = "";
  public color     = "";
  public usuario = "";
  public selected = "";
  public selectedIndex=0;
   public appPages = [
      {
        title: 'Sugerencias',
        url: 'perfilsugerencias',
        icon: 'mail'
      }
    ];
  constructor(public navController: NavController,
  			  public formBuilder: FormBuilder,
  			  private router: Router, 
  			  private provider: Menus, 
          private platform: Platform,
  			  public alertCtrl: AlertController,
          private socialSharing: SocialSharing,
  			  public loadingCtrl: LoadingController,
          private provider_menu: Menus
  			  ) {
      this.platform.backButton.subscribe(() => {
        // code that is executed when the user pressed the back button
        this.navController.navigateRoot('menu');
      });
      this.usuario   = localStorage.getItem('NOMBRESAPELLIDOS');
      this.usuarioid = localStorage.getItem('IDUSER');
      //this.usuarioid = 74;
      this.selected  = '2'; 
      this.provider.listamenu(this.usuarioid, false).subscribe((response) => {  
                              if(response['code']==200){
                                  this.imgurl2 = this.imgurl.getServar();
                                  this.color         = response['color'];
                                  this.avatar        = response['avatar'];
                              }else if (response['code']==201){
                                          const alert = this.alertCtrl.create({
                                            subHeader: "Aviso",
                                              message: response['msg'],
                                              buttons: [
                                                {
                                                  text: "Ok", 
                                                  role: 'cancel'
                                                }
                                              ]
                                          }).then(alert => alert.present());
                              }//Fin else
      },error => {
                          const alert = this.alertCtrl.create({
                              subHeader: "Aviso",
                              message: "No pudo conectarse al servidor",
                              buttons: [
                                {
                                    text: "Reintentar",
                                    role: 'cancel',
                                    cssClass:'ion-aceptar',
                                    handler: data => {
                                        this.provider_menu.logerroresadd("listar invitados ", JSON.stringify(error)).subscribe((response) => { });//FIN LOADING DISS
                                        this.ionViewDidEnter();
                                    }
                                }
                              ]
                          }).then(alert => alert.present());
      });//FIN POST
  }
  salir(){
      this.navController.navigateRoot('menu');
  }
  filtro(){
      if(this.selected == '1'){
              this.ver = this.invitados;
      }else{
              this.ver = this.invitados2;
      }
  }
  linkwhatsapp(num){
      this.socialSharing.shareViaWhatsAppToReceiver(num, '', null, null);
  }
  linkmenu(var1, i){
      if(var1=="notificacion"){
            this.navController.navigateForward('perfilnotificaciones');
      }else if(var1=="top"){
             this.navController.navigateForward('perfiltop');
       }else if(var1=="logout"){
              localStorage.removeItem('IDUSER');
              localStorage.removeItem('USUARIO');
              localStorage.removeItem('NOMBRESAPELLIDOS');
              localStorage.removeItem('TOKEN');
              localStorage.removeItem('SESSIONACTIVA');
              localStorage.removeItem('consultaFechaBuro');
              localStorage.setItem('SESSIONACTIVA','false');
              this.navController.navigateRoot('login');
      }
  }//fin function

  linkreenviarall(){
      

      const loader = this.loadingCtrl.create({
        message: "Un momento por favor..."
      }).then(load => {
              load.present();
              this.provider.reenviarinvitacionall(this.usuarioid).subscribe((response) => {  
                          this.loadingCtrl.dismiss().then( () => { 
                                  if(response['code']==200){
                                              const alert = this.alertCtrl.create({
                                                subHeader: "Aviso",
                                                  message: response['msj'],
                                                  buttons: [
                                                    {
                                                      text: "Ok", 
                                                      role: 'cancel'
                                                    }
                                                  ]
                                              }).then(alert => alert.present());
                                  }//Fin else
                          });//FIN LOADING DISS
              },error => {
                    this.loadingCtrl.dismiss().then( () => {
                              const alert = this.alertCtrl.create({
                                  subHeader: "Aviso",
                                  message: "No pudo conectarse al servidor",
                                  buttons: [
                                    {
                                        text: "Reintentar",
                                        role: 'cancel',
                                        cssClass:'ion-aceptar',
                                        handler: data => {
                                            this.provider_menu.logerroresadd("listar invitados ", JSON.stringify(error)).subscribe((response) => { });//FIN LOADING DISS
                                            this.ionViewDidEnter();
                                        }
                                    }
                                  ]
                              }).then(alert => alert.present());
                  });//FIN LOADING DISS
              });//FIN POST
      });//FIn LOADING
  }
  linkreenviar(vid){
      
      const loader = this.loadingCtrl.create({
        message: "Un momento por favor..."
      }).then(load => {
              load.present();
              this.provider.reenviarinvitacion(this.usuarioid, vid).subscribe((response) => {  
                          this.loadingCtrl.dismiss().then( () => { 
                                  if(response['code']==200){
                                              const alert = this.alertCtrl.create({
                                                subHeader: "Aviso",
                                                  message: response['msj'],
                                                  buttons: [
                                                    {
                                                      text: "Ok", 
                                                      role: 'cancel'
                                                    }
                                                  ]
                                              }).then(alert => alert.present());
                                  }//Fin else
                          });//FIN LOADING DISS
              },error => {
                    this.loadingCtrl.dismiss().then( () => {
                              const alert = this.alertCtrl.create({
                                  subHeader: "Aviso",
                                  message: "No pudo conectarse al servidor",
                                  buttons: [
                                    {
                                        text: "Reintentar",
                                        role: 'cancel',
                                        cssClass:'ion-aceptar',
                                        handler: data => {
                                            this.provider_menu.logerroresadd("listar invitados ", JSON.stringify(error)).subscribe((response) => { });//FIN LOADING DISS
                                            this.ionViewDidEnter();
                                        }
                                    }
                                  ]
                              }).then(alert => alert.present());
                  });//FIN LOADING DISS
              });//FIN POST
      });//FIn LOADING
  }
  ionViewDidEnter() {
  			
            const loader = this.loadingCtrl.create({
            	message: "Un momento por favor..."
            }).then(load => {
                    load.present();
                    this.provider.listarinvitados(this.usuarioid).subscribe((response) => {  
                                this.loadingCtrl.dismiss().then( () => { 
                                        if(response['code']==200){
                                            this.invitados    = response['datos'];
                                            this.invitados2    = response['datos2'];
                                            this.ver = this.invitados2;
                                        }else if (response['code']==201){
                                                    const alert = this.alertCtrl.create({
                                                      subHeader: "Aviso",
                                                        message: response['msg'],
                                                        buttons: [
                                                          {
                                                            text: "Ok", 
                                                            role: 'cancel'
                                                          }
                                                        ]
                                                    }).then(alert => alert.present());
                                        }//Fin else
                                });//FIN LOADING DISS
                    },error => {
                          this.loadingCtrl.dismiss().then( () => {
                                    const alert = this.alertCtrl.create({
                                        subHeader: "Aviso",
                                        message: "No pudo conectarse al servidor",
                                        buttons: [
                                          {
                                              text: "Reintentar",
                                              role: 'cancel',
                                              cssClass:'ion-aceptar',
                                              handler: data => {
                                                  this.provider_menu.logerroresadd("listar invitados ", JSON.stringify(error)).subscribe((response) => { });//FIN LOADING DISS
                                                  this.ionViewDidEnter();
                                              }
                                          }
                                        ]
                                    }).then(alert => alert.present());
                        });//FIN LOADING DISS
                    });//FIN POST
            });//FIn LOADING
         
  }
}//FIN CLASS
