import { Component, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl} from '@angular/forms';

import { Router } from '@angular/router';
import { NavController, LoadingController, AlertController } from '@ionic/angular';
import { Menus } from '../../providers/menus';
import { Usuario } from '../../providers/usuario';

@Component({
  selector: 'app-perfilclaveeditar',
  templateUrl: 'perfilclaveeditar.html',
  styleUrls: ['perfilclaveeditar.scss'],
  providers:[Usuario, Menus]
})

export class Perfilclaveeditar {
  public myForm: FormGroup;
  public loading: any;
  public email: string;

  public t_push: string;
  public p_push: string;
  public u_push: string;

  public usuarioregistro: string;
  public usuariocedula: string;
  public usuarionombres: string;

  public registroemail: string;
  public registrotelefono: string;
  public registrotelefono2: string;
  public claves: string;

  public usuarioid: string;
  public usuario = "";
  public clave = "";
  public textcheckbox = false;


  public nombres: string;
  public apellidos: string;
  public user: string;


   @ViewChild('passwordEyeRegister1', { static: false }) passwordEye1;
  //@ViewChild(SuperTabs, { static: false }) superTabs: SuperTabs;
  // Seleccionamos el elemento con el nombre que le pusimos con el #
  passwordTypeInput1  =  'password';
  // Variable para cambiar dinamicamente el tipo de Input que por defecto sera 'password'
  iconpassword1  =  'eye-off';


   @ViewChild('passwordEyeRegister2', { static: false }) passwordEye2;
  //@ViewChild(SuperTabs, { static: false }) superTabs: SuperTabs;
  // Seleccionamos el elemento con el nombre que le pusimos con el #
  passwordTypeInput2  =  'password';
  // Variable para cambiar dinamicamente el tipo de Input que por defecto sera 'password'
  iconpassword2  =  'eye-off';


   @ViewChild('passwordEyeRegister3', { static: false }) passwordEye3;
  //@ViewChild(SuperTabs, { static: false }) superTabs: SuperTabs;
  // Seleccionamos el elemento con el nombre que le pusimos con el #
  passwordTypeInput3  =  'password';
  // Variable para cambiar dinamicamente el tipo de Input que por defecto sera 'password'
  iconpassword3  =  'eye-off';

  constructor(public navController: NavController,
  			  public formBuilder: FormBuilder,
  			  private router: Router, 
  			  private provider_usuario: Usuario, 
  			  public alertCtrl: AlertController,
  			  public loadingCtrl: LoadingController,
  			  private provider_menu: Menus
  			  ) {
  		this.usuario   = localStorage.getItem('NOMBRESAPELLIDOS');
      	this.usuarioid = localStorage.getItem('IDUSER');
	  	this.myForm = this.formBuilder.group({
	  											clave: new FormControl('', Validators.compose([
															Validators.required,
															Validators.minLength(4)
														  ])
											   ),
										        clave1: new FormControl('', Validators.compose([
															Validators.required,
															Validators.minLength(4)
														  ])
											   ),
										       clave2: new FormControl('', Validators.compose([
																		Validators.required,
																		Validators.minLength(4)
																	  ])
											   )
	    });
  }

  togglePasswordMode1() {
      this.passwordTypeInput1  =  this.passwordTypeInput1  ===  'text'  ?  'password'  :  'text';
      this.iconpassword1  =  this.iconpassword1  ===  'eye-off'  ?  'eye'  :  'eye-off';
      this.passwordEye1.el.setFocus();
  }

  togglePasswordMode2() {
      this.passwordTypeInput2  =  this.passwordTypeInput2  ===  'text'  ?  'password'  :  'text';
      this.iconpassword2  =  this.iconpassword2  ===  'eye-off'  ?  'eye'  :  'eye-off';
      this.passwordEye2.el.setFocus();
  }

  togglePasswordMode3() {
      this.passwordTypeInput3  =  this.passwordTypeInput3  ===  'text'  ?  'password'  :  'text';
      this.iconpassword3  =  this.iconpassword3  ===  'eye-off'  ?  'eye'  :  'eye-off';
      this.passwordEye3.el.setFocus();
  }
  salir(){
      this.navController.navigateRoot('login');
  }
  ionViewDidEnter() {
  	
  }
  enviarformulario(){
  	if(this.myForm.value.clave1!=this.myForm.value.clave2){
  					const alert = this.alertCtrl.create({
                    subHeader: "Aviso",
                    message: "La contraseña nueva no son iguales",
                    buttons: [
                      {
                          text: "Aceptar",
                          role: 'cancel',
                          cssClass:'ion-aceptar',
                          handler: data => {
                          }
                      }
                    ]
                }).then(alert => alert.present());

  	}else{
				  	const loader = this.loadingCtrl.create({
				  		message: "Un momento por favor..."
				    }).then(load => {
					    				load.present();
									  	this.provider_usuario.perfilclaveeditar(this.usuarioid, this.myForm.value).subscribe((response) => {  
									                this.loadingCtrl.dismiss().then( () => { 
									                        if(response['code']==200){
									                        			const alert = this.alertCtrl.create({
														                    subHeader: "Aviso",
														                    message: "Los datos fueron actualizados",
														                    buttons: [
														                      {
														                          text: "Aceptar",
														                          role: 'cancel',
														                          cssClass:'ion-aceptar',
														                          handler: data => {
														                            	this.navController.navigateRoot('principal/perfil');
														                          }
														                      }
														                    ]
														                }).then(alert => alert.present());
									                        }else if (response['code']==201){
									                                    const alert = this.alertCtrl.create({
									                                    	subHeader: "Aviso",
									                                        message: response['msg'],
									                                        buttons: [
									                                          {
									                                            text: "Ok", 
									                                            role: 'cancel'
									                                          }
									                                        ]
									                                    }).then(alert => alert.present());
									                        }//Fin else
									                });//FIN LOADING DISS
									    },error => {
									    			this.loadingCtrl.dismiss().then( () => {
											                const alert = this.alertCtrl.create({
											                    subHeader: "Aviso",
											                    message: "No pudo conectarse al servidor",
											                    buttons: [
											                      {
											                          text: "Reintentar",
											                          role: 'cancel',
											                          cssClass:'ion-aceptar',
											                          handler: data => {
											                          	    this.provider_menu.logerroresadd("Perfil clave", JSON.stringify(error)).subscribe((response) => { });//FIN LOADING DISS
											                            	this.ionViewDidEnter();
											                          }
											                      }
											                    ]
											                }).then(alert => alert.present());
													});//FIN LOADING DISS
								    	});//FIN POST
								});//FIn LOADING
	}//fin else
  }//FIN FUNCTION

  regresar(){
        this.navController.back();
    }

}//FIN CLASS
