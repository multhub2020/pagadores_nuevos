import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl} from '@angular/forms';

import { Router } from '@angular/router';
import { NavController, LoadingController, AlertController } from '@ionic/angular';

import { Usuario } from '../../providers/usuario';

@Component({
  selector: 'app-loginrecuperar4',
  templateUrl: 'loginrecuperar4.html',
  styleUrls: ['loginrecuperar4.scss'],
  providers:[Usuario]
})

export class Loginrecuperar4 {
  public myForm: FormGroup;
  public loading: any;
  public usuarioregistro: string;

  public t_push: string;
  public p_push: string;
  public u_push: string;
  constructor(public navController: NavController,
  			  public formBuilder: FormBuilder,
  			  private router: Router,
  			  private provider_usuario: Usuario,
  			  public alertCtrl: AlertController,
  			  public loadingCtrl: LoadingController
  			  ) {
  }
  ionViewDidEnter() {

  }
  link(){
      this.navController.navigateRoot('login');
  }
 
}//FIN CLASS
