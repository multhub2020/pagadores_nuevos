import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Menucuentas } from './menucuentas';
//import { MenuRoutingModule } from './menu-routing.module';
import { SuperTabsModule } from '@ionic-super-tabs/angular';
//import { MapaModule } from '../mapa/mapa.module';
@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
  //  MenuRoutingModule,
    SuperTabsModule,
    RouterModule.forChild([{ path: '', component: Menucuentas }])
  //  MapaModule
  ],
  declarations: [Menucuentas]
})
export class MenucuentasModule {}
