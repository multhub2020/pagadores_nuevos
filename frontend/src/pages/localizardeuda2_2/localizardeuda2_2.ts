import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NavController, LoadingController, AlertController, Platform } from '@ionic/angular';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Menus } from '../../providers/menus';
import { Usuario } from '../../providers/usuario';
import { SuperTabs } from "@ionic-super-tabs/angular";
import { Variablesglobales } from '../../providers/variablesglobal';

import { IonicSelectableComponent } from 'ionic-selectable';

class Productos {
  public id: string;
  public name: string;
}

@Component({
  selector: 'app-localizardeuda2_2',
  templateUrl: 'localizardeuda2_2.html',
  styleUrls: ['localizardeuda2_2.scss'],
  providers: [Menus, Usuario, Variablesglobales]
})

export class Localizardeuda2_2 {
  //myPage = Mapa;
  @ViewChild(SuperTabs, { static: false }) superTabs: SuperTabs;

  public customerid = "";

  public versionapp = "1.0.5";
  public datosdeudas: any = [''];
  public datos_publi: any;
  public selectedIndex = 0;
  public usuario: string;
  public usuarioid: string;
  public bodys: string;
  public myForm: FormGroup;
  public imgurl = new Variablesglobales();
  public imgurl2: any;
  public listaContactos: any = [];
  public listaContactos2: any = [];
  //Variables para calcularr monto
  public checkbox_re: any = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
  public checkbox_mon: any = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
  public checkbox_cuo: any = [0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1];
  public checkbox_pro: any = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1];
  public porcentaje = 0;
  ///////////////////////////////
  public datosMostar: any = [];
  public datosMostar2: any = [];
  public checkbox: any;
  public avatar: string = "";
  public invitados = 0;
  public contador_c = 0;
  public seguidores = 0;
  public color = "";
  public i = 0;
  public pago_inicial = 0;
  public value = 0;
  public finalizar = 1;
  public slideOpts = {
    effect: 'flip',
    autoplay: {
      delay: 5000
    }
  };

  productos: Productos[];
  producto: Productos;

  public invitado = "";
  public appPages = [
    {
      title: 'Sugerencias',
      url: 'perfilsugerencias',
      icon: 'mail'
    }
  ];
  public cargandoBtn = false;
  public pending_loader = null;
  public inicio = 1;

  public montodeuda = 0;
  public accountnumber = "";
  public producttypeid = 0;
  public productresolutionid = 0;
  public creditorid = 0;

  public entidadbancaria = "";

  public mensaje1 = "";
  public mensaje2 = "";
  public mensaje6 = "";

  public manual_debt_add = false;
  public currency = 0;


  public politicas = false;


  constructor(private router: Router,
    public formBuilder: FormBuilder,
    private navController: NavController,
    private provider_menu: Menus,
    private provider_usuario: Usuario,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    private platform: Platform,
    //public supertabs: SuperTabs
  ) {
    this.usuario = localStorage.getItem('NOMBRESAPELLIDOS');
    this.usuarioid = localStorage.getItem('IDUSER');
    this.customerid = localStorage.getItem('CUSTOMERID');
    this.myForm = this.formBuilder.group({
      
    });

    this.productos = [
      { id: '1', name: '1' },
      { id: '2', name: '2' },
      { id: '3', name: '3' },
      { id: '4', name: '4' },
      { id: '5', name: '5' },
      { id: '6', name: '6' },
      { id: '7', name: '7' },
      { id: '8', name: '8' },
      { id: '9', name: '9' },
      { id: '10', name: '10' },
      { id: '11', name: '11' },
      { id: '12', name: '12' },
    ];
  }//FIN FUncTION
  vaciar(i) {
    if (this.checkbox_mon[i] == 0) { this.checkbox_mon[i] = ""; }
  }
  vaciar2(i) {
    console.log('i '+i);
    if (this.checkbox_mon[i] == '') { this.checkbox_mon[i] = 0; }
  }
  inf(v) {
    this.finalizar = v;
  }
  seleccionradio(a, b) {
    //alert(a+' - '+b);
    this.checkbox_re[a] = b;
  }
  resolver(a, b) {
    if (this.finalizar == 1) {
      this.checkbox_pro[a] = b;
    }
  }
  quitarresena() {
    this.inicio = 1;
  }

  productosChange(event: {
    component: IonicSelectableComponent,
    value: any
  }) {
    this.checkbox_cuo[0] =  event.value.id;
    console.log('port:', event.value.id);
  }
  primeracarga() {
    //const browser = this.iab.create(url);
    //const browser = this.iab.create(url, '_system', this.options2);
    this.inicio = 2;
  }
  getText(e) {
    var elementValue = e.srcElement.value;
    if (elementValue) {
      var regex = /^[0-9., ]+$/;
      var tempValue = elementValue.substring(0, elementValue.length - 1);
      if (!regex.test(elementValue)) {
        console.log("Entered char is not alphabet");
        e.srcElement.value = tempValue;
      }
    }
  }



  ionViewDidEnter() {

    //this.slider.startAutoplay();
    this.checkbox = localStorage.getItem('localizardeuda2_2_id1');
    this.pago_inicial = JSON.parse(localStorage.getItem('localizardeuda2_2_monto'));
    const loader = this.loadingCtrl.create({
      message: "Un momento por favor..."
    }).then(load2 => {
      load2.present();
      this.provider_menu.localizardeudaid(this.usuarioid, this.checkbox).subscribe((response) => {
        load2.dismiss().then(() => {
          if (response['code'] == 200) {

            console.log('localizardeudaid: ' + JSON.stringify(response));
            this.imgurl2 = this.imgurl.getServar();
            this.datosdeudas = response['datosdeudas'];
            if (response['datosdeudas'][0]['tipo_deuda'] == 1) {
              this.mensaje1 = 'Tarjeta de Crédito ' + response['datosdeudas'][0]['identifica_banco'];
            } else {
              this.mensaje1 = 'Préstamo ' + response['datosdeudas'][0]['identifica_banco'];
            }
            if (response['datosdeudas'][0]['manual_debt_add'] == 2) {
              this.manual_debt_add = true;
            } else {
              this.manual_debt_add = false;
            }
            this.mensaje6 = response['datosdeudas'][0]['monto_deuda'];
            this.montodeuda = response['datosdeudas'][0]['monto_deuda2'];
            this.accountnumber   = response['datosdeudas'][0]['account_number'];
            

            this.entidadbancaria = response['datosdeudas'][0]['banco_deuda'];



            console.log('deuda ' + this.montodeuda);

            //this.producttypeid = response['datosdeudas'][0]['product_type_id'];
            this.producttypeid = 2;
            this.productresolutionid = response['datosdeudas'][0]['product_resolution_id'];
            this.creditorid = response['datosdeudas'][0]['creditor_id'];
            this.currency = response['datosdeudas'][0]['moneda'];

          } else if (response['code'] == 201) {
            const alert = this.alertCtrl.create({
              subHeader: "Aviso",
              message: response['msg'],
              buttons: [
                {
                  text: "Ok",
                  role: 'cancel'
                }
              ]
            }).then(alert => alert.present());
          }//Fin else
        });//FIN LOADING DISS
      }, error => {
        load2.dismiss().then(() => {
          const alert = this.alertCtrl.create({
            subHeader: "Aviso",
            message: "No pudo conectarse al servidor",
            buttons: [
              {
                text: "Reintentar",
                role: 'cancel',
                cssClass: 'ion-aceptar',
                handler: data => {
                  this.provider_menu.logerroresadd("localizardeuda2 2", JSON.stringify(error)).subscribe((response) => { });//FIN LOADING DISS
                  this.ionViewDidEnter();
                }
              }
            ]
          }).then(alert => alert.present());
        });//FIN LOADING DISS
      });//FIN POST
    });//FIn LOADING

  }//FIN FcuntiN
  respuestapro(a) {
    return this.checkbox_re[a];
  }
  finalizar_() {
    let contar = 0;
    let contarsin = 0;
    var that = this;
    this.datosdeudas.forEach(function (value) {
      console.log('value: ' + JSON.stringify(value));
      console.log(contar + ' - ');
      if (that.respuestapro(contar) == 0) {
        contarsin++;
      }
      contar++;
    });
    if (contarsin != 0) {
      const alert = this.alertCtrl.create({
        subHeader: "Antes de finalizar",
        message: "Debe ingresar forma de cuota.",
        buttons: [
          {
            text: "Continuar",
            role: 'cancel',
            cssClass: 'ion-aceptar',
            handler: data => {

            }
          },
        ]
      }).then(alert => alert.present());
    } else {
      const loader = this.loadingCtrl.create({
        message: "Un momento por favor..."
      }).then(load2 => {
        load2.present();
        this.provider_menu.deudassolicitudenviarcuotas(this.usuarioid,
          this.checkbox,
          this.checkbox_re,
          this.checkbox_mon,
          this.checkbox_cuo,
          this.pago_inicial
        ).subscribe((response) => {
          load2.dismiss().then(() => {
            if (response['code'] == 200) {
              if (this.checkbox_re[0] == 1) {
                //this.checkbox_mon[0];
                this.checkbox_cuo[0] = 0;
              } else {
                //this.checkbox_cuo[0];
              }
              //this.finalizar = 2;
              this.finalizar_2();
            } else if (response['code'] == 201) {
              const alert = this.alertCtrl.create({
                subHeader: "Aviso",
                message: response['msg'],
                buttons: [
                  {
                    text: "Ok",
                    role: 'cancel'
                  }
                ]
              }).then(alert => alert.present());
            }//Fin else
          });//FIN LOADING DISS
        }, error => {
          load2.dismiss().then(() => {
            const alert = this.alertCtrl.create({
              subHeader: "Aviso",
              message: "No pudo conectarse al servidor",
              buttons: [
                {
                  text: "Reintentar",
                  role: 'cancel',
                  cssClass: 'ion-aceptar',
                  handler: data => {
                    this.provider_menu.logerroresadd("localizardeuda2 2", JSON.stringify(error)).subscribe((response) => { });//FIN LOADING DISS
                    this.ionViewDidEnter();
                  }
                }
              ]
            }).then(alert => alert.present());
          });//FIN LOADING DISS
        });//FIN POST
      });//FIn LOADING         
    }//FIN ELSE
  }
  finalizar_2() {
    const loader = this.loadingCtrl.create({
      message: "Un momento por favor..."
    }).then(load2 => {
      load2.present();
      //let offer_payoff_amount = ((this.montodeuda - this.pago_inicial) / this.checkbox_cuo[0]);
                if(this.checkbox_re[0]==2){ 
                  this.checkbox_mon[0] = ((this.montodeuda-this.pago_inicial) / this.checkbox_cuo[0]).toFixed(2);
                }
                let offer_payoff_amount = 0;
                if (!this.checkbox_cuo[0]) {
                  offer_payoff_amount = this.checkbox_mon[0];
                }
                console.log('monto '+this.checkbox_mon[0]);

                var credolabscores = localStorage.getItem('credolabscores');

                
                this.provider_usuario.api_pagadores_applications(
                  
                  this.customerid,
                  this.accountnumber,
                  this.pago_inicial, 
                  offer_payoff_amount,
                  this.checkbox_cuo[0],
                  this.checkbox_mon[0],
                  Math.trunc(this.montodeuda),
                  this.producttypeid,
                  this.productresolutionid,
                  this.creditorid,
                  this.manual_debt_add,
                  this.currency,
                  0,
                  2,
                  this.entidadbancaria,
                  credolabscores,
                  this.datosdeudas[0]['id_supervisor']
                ).subscribe((responsepagadores) => {
                  load2.dismiss().then(() => {
                    //console.log(responsepagadores.data.application_id);
                    localStorage.setItem('localizardeuda2_2_mensaje1', 'Número de aplicación ' + responsepagadores['data'].application_id);
                    localStorage.setItem('localizardeuda2_2_mensaje2', JSON.stringify(this.pago_inicial));
                    localStorage.setItem('localizardeuda2_2_mensaje3', JSON.stringify(this.checkbox_re[0]));
                    localStorage.setItem('localizardeuda2_2_mensaje4', this.checkbox_mon[0]);
                    localStorage.setItem('localizardeuda2_2_mensaje5', this.checkbox_cuo[0]);
                    localStorage.setItem('localizardeuda2_2_mensaje6', JSON.stringify(this.mensaje6));
                    //this.navController.navigateForward('principal/localizardeuda2_3');
                    this.provider_usuario.deudasaplication(this.checkbox, responsepagadores['data'].application_id, credolabscores).subscribe((response) => { });
                    this.navController.navigateForward('principal/localizardeuda2_3_1');
                  });//FIN LOADING DISS
                }, error => {
                  load2.dismiss().then(() => {
                    const alert = this.alertCtrl.create({
                      subHeader: "Aviso",
                      message: "No pudo conectarse al servidor",
                      buttons: [
                        {
                          text: "Reintentar",
                          role: 'cancel',
                          cssClass: 'ion-aceptar',
                          handler: data => {
                            this.provider_menu.logerroresadd("localizardeuda2 2", JSON.stringify(error)).subscribe((response) => { });//FIN LOADING DISS
                            this.ionViewDidEnter();
                          }
                        }
                      ]
                    }).then(alert => alert.present());
                  });//FIN LOADING DISS
                });//FIN POST);//FIN LOADING DISS
    });//FIn LOADING

  }


  finalizar_consulta(){
          if(this.checkbox_cuo[0]==0){

                    const alert = this.alertCtrl.create({
                      subHeader: "",
                      message: "Debe agregar la cuota",
                      buttons: [
                        {
                            
                            text: "Reintentar",
                            role: 'cancel',
                            cssClass:'ion-pagar2',
                            handler: data => {
                            }
                        }
                        
                      ]
                  }).then(alert => alert.present());



          }else{

                  const alert = this.alertCtrl.create({
                      subHeader: "¿Estás seguro de enviar",
                      message: "esta oferta? ",
                      buttons: [
                        {
                            text: "Quiero modificar mi oferta",
                            cssClass:'ion-pagar3',
                            handler: data => {
                                //this.navController.back();
                            }
                        },
                        {
                            
                            text: "Si, estoy seguro",
                            role: 'cancel',
                            cssClass:'ion-pagar2',
                            handler: data => {
                                this.checkbox_re[0] = 2;
                                this.finalizar_();
                            }
                        }
                        
                      ]
                  }).then(alert => alert.present());
          }
  }//fin function


  regresar() {
    this.navController.back();
  }

  informacion(var1) {
    if (var1 == 1) {
      this.navController.navigateForward('inforcondiciones');
    } else if (var1 == 2) {
      this.navController.navigateForward('inforpoliticas');
    } else if (var1 == 3) {
      this.navController.navigateForward('loginrecuperar1');
    } else if (var1 == 4) {
      this.navController.navigateForward('loginclave');
    }
  }
}//FIN CLASS