import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Localizardeuda2_2 } from './localizardeuda2_2';
import { NgxMaskIonicModule } from 'ngx-mask-ionic';
import { NgxCurrencyModule } from "ngx-currency";
import { IonicSelectableModule } from 'ionic-selectable';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgxMaskIonicModule,
    NgxCurrencyModule,
    IonicSelectableModule,
    RouterModule.forChild([{ path: '', component: Localizardeuda2_2 }])
  ],
  declarations: [Localizardeuda2_2]
})
export class Localizardeuda2_2Module {}
