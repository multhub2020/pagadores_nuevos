import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl} from '@angular/forms';

import { Router } from '@angular/router';
import { NavController, LoadingController, AlertController } from '@ionic/angular';
import { Menus } from '../../providers/menus';
import { Usuario } from '../../providers/usuario';
import { GooglePlus } from '@ionic-native/google-plus/ngx';

@Component({
  selector: 'app-login',
  templateUrl: 'login.html',
  styleUrls: ['login.scss'],
  providers:[Usuario, Menus]
})

export class Login {
  public myForm: FormGroup;
  public loading: any;
  public t_push: string;
  public p_push: string;
  public userData: any = {};
  public inicio = 1;
  constructor(public navController: NavController,
  			  public formBuilder: FormBuilder,
  			  private router: Router, 
  			  private provider_usuario: Usuario, 
  			  public alertCtrl: AlertController,
  			  public loadingCtrl: LoadingController,
          private googlePlus: GooglePlus,
          private provider_menu: Menus

  			  ) {
	  	/*this.myForm = this.formBuilder.group({
	        email: new FormControl('', Validators.compose([ 
															Validators.required, 
															Validators.email 
														  ])
								   )
	    });*/
  }
  ionViewDidEnter() {
        this.t_push = localStorage.getItem('T_PUSH');
        this.p_push = localStorage.getItem('P_PUSH');
        //alert(this.t_push+' - '+this.p_push);


        this.provider_usuario.api_pagadores_loginclave().subscribe((responseapipagadores) => {  

                  localStorage.setItem('TOKEAPIPAGADORES',       responseapipagadores["access_token"]);
                  this.provider_usuario.actualizarpush_pull(this.t_push, this.p_push).subscribe((response2) => {});
                  if(localStorage.getItem("banderainiciosplash1")!='1'){
                    localStorage.setItem('banderainiciosplash1', '1');
                    this.primeracarga();  
                  }

        },error => {
                  const alert = this.alertCtrl.create({
                      subHeader: "Aviso",
                      message: "No pudo conectarse al servidor",
                      buttons: [
                        {
                            text: "Reintentar",
                            role: 'cancel',
                            cssClass:'ion-aceptar',
                            handler: data => {
                                this.provider_menu.logerroresadd("login", JSON.stringify(error)).subscribe((response) => { });//FIN LOADING DISS
                                this.ionViewDidEnter();
                            }
                        }
                      ]
                  }).then(alert => alert.present());
       });//FIN POST

  }
  primeracarga(){
      this.inicio = 2;
  }
  cargarsiguiente(){
      this.inicio = 21; 
  }
  cargarsiguiente21(){
      //this.inicio = 3; 
      this.inicio = 1; 
  }
  cargarfinal(){
      this.inicio = 1; 
  }
  googleSignIn() {
    const loader = this.loadingCtrl.create({
      message: "Un momento por favor..."
    }).then(load => {
                    load.present();
                    this.googlePlus.login({})
                      .then(result => {
                          this.userData = result;
                          //name: result.displayName,
                          //email: result.email,
                          console.log(result);          
                          this.provider_usuario.loginregistroinvitador_gmail(result.email).subscribe((response) => {  
                                      this.loadingCtrl.dismiss().then( () => { 
                                                    if(response['code']==200){
                                                              localStorage.setItem('USUARIOREGISTRO',  result.email);
                                                              localStorage.setItem('REGISTROEMAIL',    response['email']);
                                                              localStorage.setItem('REGISTROTELEFONO', response['telefono']);
                                                              localStorage.setItem('REGISTROGMAIL', '1');
                                                              this.navController.navigateForward('loginregistro2');
                                              }else if (response['code']==202){
                                                              localStorage.setItem('IDUSER',           response['datos']['id']);
                                                              localStorage.setItem('NOMBRESAPELLIDOS', response['datos']['nombre_apellido']);
                                                              localStorage.setItem('TIPOUSUARIO',      response['datos']['role_id']);
                                                              localStorage.setItem('credolabcollect',  response['datos']['credolabcollect']);
                                                              localStorage.setItem('credolabscores',   response['datos']['credolabscores']);
                                                              localStorage.setItem('SESSIONACTIVA','true');
                                                              this.t_push = localStorage.getItem('T_PUSH');
                                                              this.p_push = localStorage.getItem('P_PUSH');
                                                              this.provider_usuario.actualizarpush(this.t_push, this.p_push, response['datos']['id'] ).subscribe((response2) => {});
                                                              this.navController.navigateRoot('menu');
                                              }else if (response['code']==201){
                                                          const alert = this.alertCtrl.create({
                                                            subHeader: "Aviso",
                                                              message: response['msg'],
                                                              buttons: [
                                                                {
                                                                  text: "Ok", 
                                                                  role: 'cancel'
                                                                }
                                                              ]
                                                          }).then(alert => alert.present());
                                              }//Fin else
                                      });//FIN LOADING DISS
                          },error => {
                                      this.loadingCtrl.dismiss().then( () => {
                                                  const alert = this.alertCtrl.create({
                                                      subHeader: "Aviso",
                                                      message: "No pudo conectarse al servidor",
                                                      buttons: [
                                                        {
                                                            text: "Reintentar",
                                                            role: 'cancel',
                                                            cssClass:'ion-aceptar',
                                                            handler: data => {
                                                                this.provider_menu.logerroresadd("login", JSON.stringify(error)).subscribe((response) => { });//FIN LOADING DISS
                                                                this.ionViewDidEnter();
                                                            }
                                                        }
                                                      ]
                                                  }).then(alert => alert.present());
                                      });//FIN LOADING DISS
                          });//FIN POST

                      }).catch(err => {
                          this.userData = JSON.stringify(err);
                          this.loadingCtrl.dismiss().then( () => {
                                      const alert = this.alertCtrl.create({
                                          subHeader: "Aviso",
                                          message: "Disculpe, no se pudo conectar a gmail "+this.userData ,
                                          buttons: [
                                            {
                                                text: "Reintentar",
                                                role: 'cancel',
                                                cssClass:'ion-aceptar',
                                                handler: data => {
                                                    this.ionViewDidEnter();
                                                }
                                            }
                                          ]
                                      }).then(alert => alert.present());
                          });//FIN LOADING DISS
                      } );
    });//FIn LOADING
  }//fin function
  enviarformulario(var1){
          localStorage.setItem('REGISTROGMAIL', '2');
  				this.navController.navigateForward('loginregistro3');
  }//FIN FUNCTION
  link(var1){
	  		if(var1==1){
	  		this.navController.navigateForward('loginclave');
	  	}else if(var1==2){
	  		this.navController.navigateForward('loginregistro3');
	  	}
  }
}//FIN CLASS
