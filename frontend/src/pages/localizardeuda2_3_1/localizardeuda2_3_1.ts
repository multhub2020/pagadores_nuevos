import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl} from '@angular/forms';

import { Router } from '@angular/router';
import { NavController, LoadingController, AlertController } from '@ionic/angular';
import { GooglePlus } from '@ionic-native/google-plus/ngx';
import { FacebookLoginResponse, Facebook} from "@ionic-native/facebook/ngx";
import { Usuario } from '../../providers/usuario';

import { IonicSelectableComponent } from 'ionic-selectable';

class Empleos {
  public id: string;
  public name: string;
}


@Component({
  selector: 'app-localizardeuda2_3_1',
  templateUrl: 'localizardeuda2_3_1.html',
  styleUrls: ['localizardeuda2_3_1.scss'],
  providers:[Usuario, GooglePlus, Facebook]
})

export class Localizardeuda2_3_1 {
  public myForm: FormGroup;
  public loading: any;
  public email: string;

  public t_push: string;
  public p_push: string;
  public u_push: string;

  public usuarioregistro: string;
  public usuariocedula: string;
  public usuarionombres: string;

  public registroemail: string;
  public registrotelefono: string;
  public registrogmail = "";
  public clavecool = '12345';

  empleos: Empleos[];
  empleo: Empleos;

  public vehiculo = false;

  constructor(public navController: NavController,
  			  public formBuilder: FormBuilder,
  			  private router: Router,
          private googlePlus: GooglePlus, 
          private fb: Facebook,
  			  private provider_usuario: Usuario, 
  			  public alertCtrl: AlertController,
  			  public loadingCtrl: LoadingController
  			  ) {
	  	this.myForm = this.formBuilder.group({
         salario: new FormControl('', Validators.compose([ 
                              Validators.required,
                               Validators.maxLength(50)
                              ])
                   ),
         empleo: new FormControl('', Validators.compose([ 
                              Validators.required,
                              ])
                   ),
          nacionalidad: new FormControl('Dominicana', Validators.compose([ 
                              Validators.required,
                              Validators.minLength(5)
                              ])
                   ),
          valida: new FormControl('', Validators.compose([ 
                              ])
                   )
          /*ocupacion: new FormControl('', Validators.compose([ 
                              Validators.required,
                              Validators.maxLength(50)
                              ])
                   ),*/
      });
      this.empleos = [
        { id: '1', name: 'Empleado privado' },
        { id: '2', name: 'Empleado público' },
        { id: '3', name: 'Independiente' },
      ];
  }
  getText(e){
     var elementValue = e.srcElement.value;
     if(elementValue){
       var regex = /^[A-Za-z ]+$/;   
        var tempValue = elementValue.substring(0, elementValue.length - 1);
        if (!regex.test(elementValue)) {
          console.log("Entered char is not alphabet");
          e.srcElement.value = tempValue;
        }
     }
   }

  getNum(e){
     var elementValue = e.srcElement.value;
    if(elementValue){
       var regex = /^[0-9.,]+$/;   
        var tempValue = elementValue.substring(0, elementValue.length - 1);
        if (!regex.test(elementValue)) {
          console.log("Entered char is not alphabet");
          e.srcElement.value = tempValue;
        }
    }
  }
  salir(){
      this.navController.navigateRoot('login');
  }
  ionViewDidEnter() {
       

  }

  empleosChange(event: {
    component: IonicSelectableComponent,
    value: any
  }) {
    console.log('port:', event.value);
  }


  enviarformulario(v){
    const loader = this.loadingCtrl.create({
      message: "Un momento por favor..."
    }).then(load => {

                      load.present();
                                  this.loadingCtrl.dismiss().then( () => {
                                                localStorage.setItem('SALARIO',      this.myForm.value.salario);
                                                localStorage.setItem('EMPLEO',       this.myForm.value.empleo.name);
                                                localStorage.setItem('NACIONALIDAD', this.myForm.value.nacionalidad);
                                                localStorage.setItem('OCUPACION',    this.myForm.value.ocupacion);
                                                if(this.vehiculo==true){
                                                     var var15 =  "true"; 
                                                }else{
                                                      var var15 =  "false";  
                                                }
                                                localStorage.setItem('VEHICULO',     var15);
                                                this.navController.navigateForward('principal/localizardeuda2_3_2');
                                  });//FIN LOADING DISS
                     
        });//FIn LOADING
  }//FIN FUNCTION

}//FIN CLASS
