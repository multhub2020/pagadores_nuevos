import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Localizardeuda2_3_1 } from './localizardeuda2_3_1';

import { IonicSelectableModule } from 'ionic-selectable';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicSelectableModule,
    RouterModule.forChild([{ path: '', component: Localizardeuda2_3_1 }])
  ],
  declarations: [Localizardeuda2_3_1]
})
export class Localizardeuda2_3_1Module {}
