import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Loginregistro3 } from './loginregistro3';
import {NgxMaskIonicModule} from 'ngx-mask-ionic';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgxMaskIonicModule,
    RouterModule.forChild([{ path: '', component: Loginregistro3 }])
  ],
  declarations: [Loginregistro3]
})
export class Loginregistro3Module {}
