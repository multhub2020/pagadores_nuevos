import { Component, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl} from '@angular/forms';

import { Router } from '@angular/router';
import { NavController, LoadingController, AlertController } from '@ionic/angular';
import { GooglePlus } from '@ionic-native/google-plus/ngx';
import { FacebookLoginResponse, Facebook} from "@ionic-native/facebook/ngx";
import { Usuario } from '../../providers/usuario';
import { Menus } from '../../providers/menus';

@Component({
  selector: 'app-loginregistro3',
  templateUrl: 'loginregistro3.html',
  styleUrls: ['loginregistro3.scss'],
  providers:[Usuario, GooglePlus, Facebook, Menus]
})

export class Loginregistro3 {
  public myForm: FormGroup;
  public loading: any;
  public email: string;

  @ViewChild('passwordEyeRegister', { static: false }) passwordEye;
  //@ViewChild(SuperTabs, { static: false }) superTabs: SuperTabs;
  // Seleccionamos el elemento con el nombre que le pusimos con el #
  passwordTypeInput  =  'password';
  // Variable para cambiar dinamicamente el tipo de Input que por defecto sera 'password'
  iconpassword  =  'eye-off';

  public t_push: string;
  public p_push: string;
  public u_push: string;

  public usuarioregistro: string;
  public usuariocedula: string;
  public usuarionombres: string;

  public registroemail: string;
  public registrotelefono: string;
  public registrogmail = "";
  public clavecool = '12345';

  constructor(public navController: NavController,
  			  public formBuilder: FormBuilder,
  			  private router: Router,
          private googlePlus: GooglePlus, 
          private fb: Facebook,
  			  private provider_usuario: Usuario, 
  			  public alertCtrl: AlertController,
  			  public loadingCtrl: LoadingController,
          private provider_menu: Menus
  			  ) {
	  	this.myForm = this.formBuilder.group({
         nombres: new FormControl('', Validators.compose([ 
                              Validators.required,
                               Validators.maxLength(30)
                              ])
                   ),
         apellidos: new FormControl('', Validators.compose([ 
                              Validators.required,
                              Validators.maxLength(30)
                              ])
                   ),
         user: new FormControl('', Validators.compose([ 
                              Validators.required
                              ])
                   ),
          clave: new FormControl('', Validators.compose([ 
                              Validators.required,
                              Validators.minLength(5)
                              ])
                   ),
          celular: new FormControl('', Validators.compose([ 
                                Validators.required,
                                Validators.pattern('^[0-9 ]+$'),
                                Validators.minLength(10)
                                ])
                     )
      });
  }
  togglePasswordMode() {
      this.passwordTypeInput  =  this.passwordTypeInput  ===  'text'  ?  'password'  :  'text';
      this.iconpassword  =  this.iconpassword  ===  'eye-off'  ?  'eye'  :  'eye-off';
      this.passwordEye.el.setFocus();
  }
  getText(e){
     var elementValue = e.srcElement.value;
     if(elementValue){
       var regex = /^[A-Za-z ]+$/;   
        var tempValue = elementValue.substring(0, elementValue.length - 1);
        if (!regex.test(elementValue)) {
          console.log("Entered char is not alphabet");
          e.srcElement.value = tempValue;
        }
     }
   }
  salir(){
      this.navController.navigateRoot('login');
  }
  ionViewDidEnter() {
         //this.registroemail    = localStorage.getItem('REGISTROEMAIL');
         //this.registrotelefono = localStorage.getItem('REGISTROTELEFONO');
         //this.registrogmail    = localStorage.getItem('REGISTROGMAIL');
  }
  enviarformulario(v){
    const loader = this.loadingCtrl.create({
      message: "Un momento por favor..."
    }).then(load => {
                      load.present();

                      let cadena1 = this.myForm.value.nombres;
                      let cadena2 = this.myForm.value.apellidos;
                      let cadena3 = this.myForm.value.user;
                      let cadena4 = this.myForm.value.clave;
                      let cadena5 = this.myForm.value.celular;
                      // esta es la palabra a buscar
                      //“script”,” onerror”,” javascript:alert” , “iframe” , ”onclick”
                      let termino1 = "SCRIPT";
                      let termino2 = "ONERROR";
                      let termino3 = "IFRAME";
                      let termino4 = "ONCLICK";
                      let termino5 = "JAVASCRIPT:ALERT";
                      // para buscar la palabra hacemos
                      let posicion1 = cadena1.toLowerCase().indexOf(termino1.toLowerCase());
                      let posicion2 = cadena2.toLowerCase().indexOf(termino2.toLowerCase());
                      let posicion3 = cadena3.toLowerCase().indexOf(termino3.toLowerCase());
                      let posicion4 = cadena4.toLowerCase().indexOf(termino4.toLowerCase());
                      let posicion5 = cadena5.toLowerCase().indexOf(termino5.toLowerCase());
                      ///////////
                      let vererror = 0;
                      if (posicion1 !== -1 || posicion2 !== -1 || posicion3 !== -1 || posicion4 !== -1 || posicion5 !== -1){
                        let vererror = 1;
                              this.loadingCtrl.dismiss().then( () => {
                                    const alert = this.alertCtrl.create({
                                        subHeader: "Aviso",
                                        message: "Datos incorrectos",
                                        buttons: [
                                          {
                                              text: "Reintentar",
                                              role: 'cancel',
                                              cssClass:'ion-aceptar',
                                              handler: data => {

                                              }
                                          }
                                        ]
                                    }).then(alert => alert.present());
                            });//FIN LOADING DISS
                      }else{

                                    this.provider_usuario.verificar(this.myForm.value, v).subscribe((response) => {
                                                this.loadingCtrl.dismiss().then( () => {
                                                        if(response['code']==200){
                                                              localStorage.setItem('USUARIOEMAIL',      this.myForm.value.user);
                                                              localStorage.setItem('USUARIOCLAVE',      this.myForm.value.clave);
                                                              localStorage.setItem('USUARIONOMBRES',    this.myForm.value.nombres);
                                                              localStorage.setItem('USUARIOAPELLIDOS',  this.myForm.value.apellidos);
                                                              localStorage.setItem('USUARIOCELULAR',    this.myForm.value.celular);
                                                              localStorage.setItem('USUARIOREGISTROID', response['usuarioid']);
                                                              this.navController.navigateRoot('loginregistro6');
                                                        }else{
                                                              const alert = this.alertCtrl.create({
                                                                  subHeader: "Aviso",
                                                                  message: response['msg'],
                                                                  buttons: [
                                                                    {
                                                                        text: "Reintentar",
                                                                        role: 'cancel',
                                                                        cssClass:'ion-aceptar',
                                                                        handler: data => {

                                                                        }
                                                                    }
                                                                  ]
                                                              }).then(alert => alert.present());
                                                        }
                                                });//FIN LOADING DISS
                                    },error => {
                                          this.loadingCtrl.dismiss().then( () => {
                                                    const alert = this.alertCtrl.create({
                                                        subHeader: "Aviso",
                                                        message: "No pudo conectarse al servidor",
                                                        buttons: [
                                                          {
                                                              text: "Reintentar",
                                                              role: 'cancel',
                                                              cssClass:'ion-aceptar',
                                                              handler: data => {
                                                                this.provider_menu.logerroresadd("login registro 3", JSON.stringify(error)).subscribe((response) => { });//FIN LOADING DISS
                                                              }
                                                          }
                                                        ]
                                                    }).then(alert => alert.present());
                                        });//FIN LOADING DISS
                                    });//FIN POST
                  }//fin else
        });//FIn LOADING
  }//FIN FUNCTION
  googleSignIn(){
        if(this.myForm.value.nombres!="" && this.myForm.value.apellidos!=""){
                  const loader = this.loadingCtrl.create({
                    ////duration: 10000
                    //message: "Un momento por favor..."
                  }).then(load => {
                                  load.present();
                                  this.googlePlus.login({})
                                    .then(result => {
                                            //this.userData = result;
                                            //name: result.displayName,
                                            //email: result.email,
                                            //console.log(result); 
                                            this.loadingCtrl.dismiss().then( () => { 
                                              this.myForm.value['clave'] = "re_p_*my_kil*1865Cm98xmngfZaa1"; 
                                              this.myForm.value['user']  = result.email;  
                                              this.enviarformulario('2');
                                            });//FIN LOADING DISS
                                  }).catch(e => {
                                        this.loadingCtrl.dismiss().then( () => { 
                                            console.info('Error logging into Gmail', e);
                                            //alert('error'+JSON.stringify(e));
                                        });//FIN LOADING DISS
                                  });
                  });//FIn LOADING 

       }else{

               const alert = this.alertCtrl.create({
                    subHeader: "Aviso",
                    message: "Disculpe, debe ingresar nombres y apellidos",
                    buttons: [
                      {
                          text: "Reintentar",
                          role: 'cancel',
                          cssClass:'ion-aceptar',
                          handler: data => {
                          }
                      }
                    ]
                }).then(alert => alert.present());
        }
  }//fin function
  facebookSignIn(){
    if(this.myForm.value.nombres!="" && this.myForm.value.apellidos!="" && this.myForm.value.celular!=""){
          const loader = this.loadingCtrl.create({
            ////duration: 10000
            //message: "Un momento por favor..."
          }).then(load => {
                          load.present();
                                this.fb.login(['public_profile', 'email'])
                                .then((res: FacebookLoginResponse) => {
                                      this.fb.api('me?fields=' + ['name', 'email', 'first_name', 'last_name', 'picture.type(large)'].join(), null)
                                      .then((res2: any) => {
                                                 //this.userData = res2;
                                                 //this.userData = result;
                                                  //name: result.displayName,
                                                  //email: result.email,
                                                  //console.log(result); 
                                                  this.loadingCtrl.dismiss().then( () => { 
                                                    this.myForm.value['clave'] = "re_p_*my_kil*1865Cm98xmngfZaa1"; 
                                                    this.myForm.value['user']  = res2.email;  
                                                    this.enviarformulario(2);
                                                  });//FIN LOADING DISS
                                      }).catch(e => {
                                            this.loadingCtrl.dismiss().then( () => { 
                                                console.info('Error logging into Facebook', e);
                                                //alert('error'+JSON.stringify(e));
                                            });//FIN LOADING DISS
                                      });
                          }).catch(e => {
                                this.loadingCtrl.dismiss().then( () => { 
                                    console.info('Error logging into Facebook', e);
                                    //alert('error'+JSON.stringify(e));
                                });//FIN LOADING DISS
                          });
          });//FIn LOADING 
    }else{

           const alert = this.alertCtrl.create({
                subHeader: "Aviso",
                message: "Disculpe, debe ingresar nombres, apellidos y celular",
                buttons: [
                  {
                      text: "Reintentar",
                      role: 'cancel',
                      cssClass:'ion-aceptar',
                      handler: data => {
                      }
                  }
                ]
            }).then(alert => alert.present());
    }


  }
}//FIN CLASS
