import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl} from '@angular/forms';

import { Router } from '@angular/router';
import { NavController, LoadingController, AlertController } from '@ionic/angular';

import { Perfils } from '../../providers/perfils';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { Crop, CropOptions } from '@ionic-native/crop/ngx';
import { File } from '@ionic-native/file/ngx';
import { Menus } from '../../providers/menus';
import { Variablesglobales } from '../../providers/variablesglobal';

@Component({
  selector: 'app-notificaciones',
  templateUrl: 'notificaciones.html',
  styleUrls: ['notificaciones.scss'],
  providers:[Perfils, Camera, File, Crop, Variablesglobales, Menus]
})

export class Notificaciones {
  public myForm: FormGroup;
  public loading: any;
  public usuarioid: string;

  public url = new Variablesglobales();

  public nombres: string;
  public apellidos: string;
  public avatar:string="";
  public invitados = 0;
  public seguidores = 0;
  public color     = "";
  public usuario = "";
  public porcentaje = "";

  public croppedImagepath = "";
  public isLoading = false;
  public cropOptions: CropOptions = {
    quality: 100
  }

  public image: string[] = ['', '', '', ''];
  public image1: string = '';
  public image2: string = '';
  public image3: string = '';
  public image4: string = ''; 

  public fotoperfil = "";
  
  constructor(public navController: NavController,
  			  public formBuilder: FormBuilder,
  			  private router: Router, 
  			  private provider_perfil: Perfils, 
  			  public alertCtrl: AlertController,
  			  public loadingCtrl: LoadingController,
           private camera: Camera,
          private crop: Crop,
          private file: File,
          private provider_menu: Menus
  			  ) {

      this.usuario   = localStorage.getItem('NOMBRESAPELLIDOS');
      this.usuarioid = localStorage.getItem('IDUSER');

      
  }
  salir(){
      this.navController.navigateRoot('menu');
  }
  perfilclaveeditar(){
      this.navController.navigateForward('perfilclaveeditar');
  }
  perfileditar(){
      this.navController.navigateForward('perfileditar');
  }
  perfilsobrenosotros(){
      this.navController.navigateForward('perfilsobrenosotros');
  }
  perfilmejorando(){
      this.navController.navigateForward('perfilmejorando'); 
  }
  perfilayuda(){
      this.navController.navigateForward('perfilayuda'); 
  }
  perfilinvitado(){
      this.navController.navigateForward('perfilinvitado'); 
  }
  perfilconfiguracion(){
      this.navController.navigateForward('perfilconfiguracion'); 
  }
  ionViewDidEnter() {
  			this.usuarioid  = localStorage.getItem('IDUSER');
        this.fotoperfil = localStorage.getItem('FOTOPERFIL');
            /*const loader = this.loadingCtrl.create({
            	message: "Un momento por favor..."
            }).then(load => {
                    load.present();
                    this.provider_perfil.listarperfil(this.usuarioid).subscribe((response) => {  
                                this.loadingCtrl.dismiss().then( () => { 
                                        if(response['code']==200){
                                            this.seguidores    = response['seguidores'];
                                            this.invitados     = response['invitados'];
                                            this.color         = response['color'];
                                            this.avatar        = response['avatar'];
                                            this.porcentaje    = response['porcentaje'];
                                        }else if (response['code']==201){
                                                    const alert = this.alertCtrl.create({
                                                      subHeader: "Aviso",
                                                        message: response['msg'],
                                                        buttons: [
                                                          {
                                                            text: "Ok", 
                                                            role: 'cancel'
                                                          }
                                                        ]
                                                    }).then(alert => alert.present());
                                        }//Fin else
                                });//FIN LOADING DISS
                    },error => {
                          this.loadingCtrl.dismiss().then( () => {
                                    const alert = this.alertCtrl.create({
                                        subHeader: "Aviso",
                                        message: "No pudo conectarse al servidor",
                                        buttons: [
                                          {
                                              text: "Reintentar",
                                              role: 'cancel',
                                              cssClass:'ion-aceptar',
                                              handler: data => {
                                                  this.provider_menu.logerroresadd("Perfil", JSON.stringify(error)).subscribe((response) => { });//FIN LOADING DISS
                                                  this.ionViewDidEnter();
                                              }
                                          }
                                        ]
                                    }).then(alert => alert.present());
                        });//FIN LOADING DISS
                    });//FIN POST
            });//FIn LOADING*/
         
  }

  cerrar(){
        localStorage.removeItem('IDUSER');
        localStorage.removeItem('USUARIO');
        localStorage.removeItem('NOMBRESAPELLIDOS');
        localStorage.removeItem('TOKEN');
        localStorage.removeItem('SESSIONACTIVA');
        localStorage.removeItem('consultaFechaBuro');
        localStorage.setItem('SESSIONACTIVA','false');
        this.navController.navigateRoot('login');
  }

  cerrarsession(){
      const alert = this.alertCtrl.create({
          subHeader: "Aviso",
          message: "Está seguro de cerrar sesión? ",
          buttons: [
            {
                text: "No",
                role: 'cancel',
                cssClass:'ion-cancelar-small',
                handler: data => {
                }
            },
            {
                text: "Sí",
                cssClass:'ion-aceptar-small',
                handler: data => {
                    this.cerrar();
                }
            }
          ]
      }).then(alert => alert.present());

  }


   cambiarimagen(){
    const alert = this.alertCtrl.create({
                subHeader: "Cambiar imagen",
                message:"Seleccione una opción",
                buttons: [
                  {
                      text: "Galería",
                      cssClass:'ion-cancelar-small',
                      handler: data => {
                          this.getPicture(this.camera.PictureSourceType.PHOTOLIBRARY);
                      }
                  },
                  {
                      text: "Cámara",
                      cssClass:'ion-aceptar-small',
                      handler: data => {
                          this.getPicture(this.camera.PictureSourceType.CAMERA);
                      }
                  }
                ]
         }).then(alert => alert.present());
  }
  getPicture(sourceType){ 
      let options: CameraOptions = {
          destinationType: this.camera.DestinationType.FILE_URI,
          //targetWidth: 1080,
          //targetHeight: 1080,
          quality: 100,
          correctOrientation: true,
          cameraDirection: this.camera.Direction.BACK,
          sourceType: sourceType
        }
        this.camera.getPicture( options )
        .then(imageData => {
            //this.image1 = 'data:image/jpeg;base64,' + imageData;
            //this.image[0] = this.image1;
            //this.p_push
            this.cropImage(imageData);
        })
        .catch(error =>{
          console.error( error );
        });
  }//FIN FUNCTION
  cropImage(fileUrl) {
    this.crop.crop(fileUrl, this.cropOptions)
      .then(
        newPath => {
          this.showCroppedImage(newPath.split('?')[0])
        },
        error => {
          //alert('Error cropping image' + error);
        }
      );
  }
  showCroppedImage(ImagePath) {
    this.isLoading = true;
    var copyPath = ImagePath;
    var splitPath = copyPath.split('/');
    var imageName = splitPath[splitPath.length - 1];
    var filePath = ImagePath.split(imageName)[0];
    this.file.readAsDataURL(filePath, imageName).then(base64 => {
      this.croppedImagepath = base64;
      this.isLoading        = false;
      this.image1   = base64;
      this.image[0] = this.image1;
      this.updatefoto();
    }, error => {
      //alert('Error in showing image' + error);
      this.isLoading = false;
    });
  }
  updatefoto() {
    const loader = this.loadingCtrl.create({
      ////duration: 10000
      //message: "Un momento por favor..."
    }).then(load => {
              load.present();
              this.provider_perfil.miperfilfotoupdate(this.usuarioid, this.image).subscribe((response) => {  
                          this.loadingCtrl.dismiss().then( () => { 
                                  if(response['code']==200){
                                            localStorage.setItem('FOTOPERFIL',  this.url.getApivarcoreHomeI()+'api/storage/imagenes'+response['datos']['foto2']);
                                            this.fotoperfil = this.url.getApivarcoreHomeI()+'api/storage/imagenes'+response['datos']['foto2'];
                                  }else if (response['code']==201){
                                              const alert = this.alertCtrl.create({
                                                subHeader: "Aviso",
                                                  message: response['msg'],
                                                  buttons: [
                                                    {
                                                      text: "Ok", 
                                                      role: 'cancel'
                                                    }
                                                  ]
                                              }).then(alert => alert.present());
                                  }//Fin else
                          });//FIN LOADING DISS
              },error => {
                    this.loadingCtrl.dismiss().then( () => {
                              const alert = this.alertCtrl.create({
                                  subHeader: "Aviso",
                                  message: "Disculpe, no se conecto al servidor",
                                  buttons: [
                                    {
                                        text: "Reintentar",
                                        role: 'cancel',
                                        cssClass:'ion-aceptar',
                                        handler: data => {
                                            //this.ionViewDidEnter();
                                        }
                                    }
                                  ]
                              }).then(alert => alert.present());
                  });//FIN LOADING DISS
              });//FIN POST
        });//FIn LOADING
  }//FIN FUNCTION
}//FIN CLASS
