import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Localizardeuda1_6 } from './localizardeuda1_6';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild([{ path: '', component: Localizardeuda1_6 }])
  ],
  declarations: [Localizardeuda1_6]
})
export class Localizardeuda1_6Module {}
