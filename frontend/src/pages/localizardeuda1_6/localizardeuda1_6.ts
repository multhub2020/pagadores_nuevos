import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl} from '@angular/forms';

import { Router } from '@angular/router';
import { NavController, LoadingController, AlertController } from '@ionic/angular';

import { Menus } from '../../providers/menus';

@Component({
  selector: 'app-localizardeuda1_6',
  templateUrl: 'localizardeuda1_6.html',
  styleUrls: ['localizardeuda1_6.scss'],
  providers:[Menus]
})

export class Localizardeuda1_6 {
  public myForm: FormGroup;
  public loading: any;
  public usuarioregistro: string;
  public usuarioid: string;
  public t_push: string;
  public p_push: string;
  public u_push: string;
  public mensaje1 = "";
  public mensaje2 = "";
  public inicio=0;  
  public timeLeft =1;
  public timeLeft2='1';
  public interval=0;
  public checkbox: any;
  constructor(public navController: NavController,
  			  public formBuilder: FormBuilder,
  			  private router: Router,
  			  private provider: Menus,
  			  public alertCtrl: AlertController,
  			  public loadingCtrl: LoadingController
  			  ) {

      this.usuarioid = localStorage.getItem('IDUSER');
  }
  ionViewDidEnter() {
        this.startTimer();
  }
  startTimer() {
    this.interval = setInterval(() => {
      if(this.timeLeft > 0) {
        this.timeLeft--;
      } else {
        this.timeLeft  = 1;
        this.checkbox = localStorage.getItem('checkboxdeuda');
        this.provider.deudaslista(this.usuarioid, this.checkbox).subscribe((response) => {
              if(response['code']==200){
                  if(response['contar']!=0){
                          clearInterval(this.interval);
                          this.navController.navigateForward('principal/localizardeuda1');
                  }else{
                          ///AQUI COLOCAR LO DE LA CONSULTA
                          clearInterval(this.interval);
                          let contarinicio   = '';
                          let contarinicio2  = 0;
                          contarinicio  = localStorage.getItem('contarinicio_1_1');
                          contarinicio2 = parseInt(localStorage.getItem('contarinicio_1_1'));
                          contarinicio2++;  contarinicio2++;
                          localStorage.setItem('contarinicio_1_1', contarinicio2+'')
                          this.navController.navigateForward('principal/inicio/'+contarinicio2);
                  }
              }
        });

            
      }
    },1000)
  }
}//FIN CLASS
