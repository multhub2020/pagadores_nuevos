import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl} from '@angular/forms';

import { Router } from '@angular/router';
import { NavController, LoadingController, AlertController } from '@ionic/angular';

import { Perfils } from '../../providers/perfils';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';

@Component({
  selector: 'app-perfilinvitado',
  templateUrl: 'perfilinvitado.html',
  styleUrls: ['perfilinvitado.scss'],
  providers:[Perfils, SocialSharing]
})

export class Perfilinvitado {
  public myForm: FormGroup;
  public loading: any;
  public email: string;

  public t_push: string;
  public p_push: string;
  public u_push: string;

  public usuarioregistro: string;
  public usuariocedula: string;
  public usuarionombres: string;

  public registroemail: string;
  public registrotelefono: string;
  public registrotelefono2: string;
  public claves: string;

  public usuarioid: string;
  public usuario = "";
  public clave = "";
  public textcheckbox = false;


  public nombres: string;
  public apellidos: string;
  public user: string;

  public link = "shorturl.at/jrHV7";

  public texto: string;

  constructor(public navController: NavController,
  			  public formBuilder: FormBuilder,
  			  private router: Router, 
          private socialSharing: SocialSharing,
  			  private provider: Perfils, 
  			  public alertCtrl: AlertController,
  			  public loadingCtrl: LoadingController
  			  ) {
  		this.usuario   = localStorage.getItem('NOMBRESAPELLIDOS');
      	this.usuarioid = localStorage.getItem('IDUSER');
	  	
  }
  salir(){
      this.navController.navigateRoot('login');
  }
  ionViewDidEnter() {
				  	
  }
  regresar(){
        this.navController.back();
    }

   compartir(){
        this.socialSharing.share(null, null, null, this.link);
    }
}//FIN CLASS
