import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Verdetallecontraofertapagar } from './verdetallecontraofertapagar';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild([{ path: '', component: Verdetallecontraofertapagar }])
  ],
  declarations: [Verdetallecontraofertapagar]
})
export class VerdetallecontraofertapagarModule {}
