import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NavController, LoadingController, AlertController, Platform } from '@ionic/angular';
import { FormBuilder, FormGroup, Validators, FormControl} from '@angular/forms';
import { Menus } from '../../providers/menus';
import { SuperTabs } from "@ionic-super-tabs/angular";
import { Variablesglobales } from '../../providers/variablesglobal';

import { ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-verdetallecontraofertapagar',
  templateUrl: 'verdetallecontraofertapagar.html',
  styleUrls: ['verdetallecontraofertapagar.scss'],
  providers:[Menus, Variablesglobales]
})

export class Verdetallecontraofertapagar {
    //myPage = Mapa;
    @ViewChild(SuperTabs, { static: false }) superTabs: SuperTabs;
    public versionapp = "1.0.5";
    public datosdeudas: any=[''];
    public datos_publi: any;
    public selectedIndex = 0;
    public usuario: string;
    public usuarioid: string;
    public bodys: string;
    public myForm: FormGroup;
    public imgurl   = new Variablesglobales();
    public imgurl2: any;
    public listaContactos: any = [];
    public listaContactos2: any = [];
    //Variables para calcularr monto
    public checkbox_re: any  = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
    public checkbox_mon: any = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
    public checkbox_cuo: any = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
    public checkbox_pro: any = [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1];
    public porcentaje = 0;
    ///////////////////////////////
    public datosMostar: any = [];
    public datosMostar2: any = [];
    public checkbox: any;
    public avatar:string="";
    public invitados = 0;
    public contador_c = 0;
    public seguidores = 0;
    public color     = "";
    public i=0;
    public value=0;
    public finalizar = 1;
    public slideOpts = {
          effect: 'flip',
          autoplay: {
            delay: 5000
          }
    };
    public invitado = "";
    public appPages = [
      {
        title: 'Sugerencias',
        url: 'perfilsugerencias',
        icon: 'mail'
      }
    ];
    public cargandoBtn = false;
    public pending_loader = null;
    public inicio = 1;
    public deudaid = "";

    public mensaje1 = "";
    public mensaje2 = "";
    constructor(private router: Router,
                public formBuilder: FormBuilder,
                private navController: NavController,
                private provider_menu: Menus,
                public alertCtrl: AlertController, 
                public loadingCtrl: LoadingController,
                private platform: Platform,
                private rutaActiva: ActivatedRoute
                //public supertabs: SuperTabs
                ){
        this.usuario   = localStorage.getItem('NOMBRESAPELLIDOS');
        this.usuarioid = localStorage.getItem('IDUSER');
        this.myForm = this.formBuilder.group({
            usuario: new FormControl(0, Validators.compose([
                                Validators.required,
                                Validators.pattern('^[0-9 ]+$'),
                                ])
                     )

        });
    }//FIN FUncTION
    inf(v){
        this.finalizar = v;
    }
    seleccionradio(a, b){
      //alert(a+' - '+b);
      this.checkbox_re[a]=b;
    }
    resolver(a, b){
      if(this.finalizar==1){
          this.checkbox_pro[a]=b;  
      }
    }
    quitarresena(){
       this.inicio = 1; 
    }
    primeracarga(){
      //const browser = this.iab.create(url);
      //const browser = this.iab.create(url, '_system', this.options2);
      this.inicio = 2;
    }
    ionViewDidEnter() {

      //this.slider.startAutoplay();
             if(localStorage.getItem("banderainiciodeudaadd")!='1'){
                localStorage.setItem('banderainiciodeudaadd', '1');
                this.primeracarga();  
              }
             this.checkbox = this.rutaActiva.snapshot.paramMap.get('id');
              const loader = this.loadingCtrl.create({
                message: "Un momento por favor..."
              }).then(load2 => {
                        load2.present();
                        this.provider_menu.localizardeudaid(this.usuarioid, this.checkbox).subscribe((response) => {
                                    load2.dismiss().then( () => {
                                            if(response['code']==200){
                                                this.imgurl2 = this.imgurl.getServar();
                                                this.datosdeudas   = response['datosdeudas'];
                                            }else if (response['code']==201){
                                                        const alert = this.alertCtrl.create({
                                                          subHeader: "Aviso",
                                                            message: response['msg'],
                                                            buttons: [
                                                              {
                                                                text: "Ok",
                                                                role: 'cancel'
                                                              }
                                                            ]
                                                        }).then(alert => alert.present());
                                            }//Fin else
                                    });//FIN LOADING DISS
                        },error => {
                              load2.dismiss().then( () => {
                                        const alert = this.alertCtrl.create({
                                            subHeader: "Aviso",
                                            message: "No pudo conectarse al servidor",
                                            buttons: [
                                              {
                                                  text: "Reintentar",
                                                  role: 'cancel',
                                                  cssClass:'ion-aceptar',
                                                  handler: data => {
                                                    this.provider_menu.logerroresadd("Ver detalle contraoferta pagar", JSON.stringify(error)).subscribe((response) => { });//FIN LOADING DISS
                                                      this.ionViewDidEnter();
                                                  }
                                              }
                                            ]
                                        }).then(alert => alert.present());
                            });//FIN LOADING DISS
                        });//FIN POST
              });//FIn LOADING

    }//FIN FcuntiN
   
    regresar(){
        this.navController.back();
    }

     informacion(var1){
            if(var1==1){
            this.navController.navigateForward('inforcondiciones');
          }else if(var1==2){
            this.navController.navigateForward('inforpoliticas');
          }else if(var1==3){
            this.navController.navigateForward('loginrecuperar1');
          }else if(var1==4){
            this.navController.navigateForward('loginclave');
          }
      }
      continuar() {
      //this.slider.startAutoplay();
      this.deudaid = this.rutaActiva.snapshot.paramMap.get('id');
              const loader = this.loadingCtrl.create({
                message: "Un momento por favor..."
              }).then(load2 => {
                        load2.present();
                        this.provider_menu.pagardeuda(this.usuarioid, this.deudaid ).subscribe((response) => {
                                    load2.dismiss().then( () => {
                                            if(response['code']==200){
                                                this.navController.navigateForward('verdetallecontraofertapagarmsj'); 
                                                console.log(this.datosdeudas);
                                            }else if (response['code']==201){
                                                        const alert = this.alertCtrl.create({
                                                          subHeader: "Aviso",
                                                            message: response['msg'],
                                                            buttons: [
                                                              {
                                                                text: "Ok",
                                                                role: 'cancel'
                                                              }
                                                            ]
                                                        }).then(alert => alert.present());
                                            }//Fin else
                                    });//FIN LOADING DISS
                        },error => {
                              load2.dismiss().then( () => {
                                        const alert = this.alertCtrl.create({
                                            subHeader: "Aviso",
                                            message: "No pudo conectarse al servidor",
                                            buttons: [
                                              {
                                                  text: "Reintentar",
                                                  role: 'cancel',
                                                  cssClass:'ion-aceptar',
                                                  handler: data => {
                                                      this.provider_menu.logerroresadd("Ver detalle contraoferta pagar", JSON.stringify(error)).subscribe((response) => { });//FIN LOADING DISS
                                                      this.ionViewDidEnter();
                                                  }
                                              }
                                            ]
                                        }).then(alert => alert.present());
                            });//FIN LOADING DISS
                        });//FIN POST
              });//FIn LOADING

    }//FIN FcuntiN
}//FIN CLASS