import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Localizardeuda2_1 } from './localizardeuda2_1';
import { SuperTabsModule } from '@ionic-super-tabs/angular';
import { NgxMaskIonicModule } from 'ngx-mask-ionic';
import { NgxCurrencyModule } from "ngx-currency";

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SuperTabsModule,
    NgxMaskIonicModule,
    NgxCurrencyModule,
    RouterModule.forChild([{ path: '', component: Localizardeuda2_1 }])
  ],
  declarations: [Localizardeuda2_1]
})
export class Localizardeuda2_1Module {}
