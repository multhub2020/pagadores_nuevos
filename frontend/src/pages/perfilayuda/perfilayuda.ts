import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl} from '@angular/forms';

import { Router } from '@angular/router';
import { NavController, LoadingController, AlertController } from '@ionic/angular';

import { Perfils } from '../../providers/perfils';

@Component({
  selector: 'app-perfilayuda',
  templateUrl: 'perfilayuda.html',
  styleUrls: ['perfilayuda.scss'],
  providers:[Perfils]
})

export class Perfilayuda {
  public myForm: FormGroup;
  public loading: any;
  public email: string;

  public t_push: string;
  public p_push: string;
  public u_push: string;

  public usuarioregistro: string;
  public usuariocedula: string;
  public usuarionombres: string;

  public registroemail: string;
  public registrotelefono: string;
  public registrotelefono2: string;
  public claves: string;

  public usuarioid: string;
  public usuario = "";
  public clave = "";
  public textcheckbox = false;


  public nombres: string;
  public apellidos: string;
  public user: string;


  public texto: string;

  constructor(public navController: NavController,
  			  public formBuilder: FormBuilder,
  			  private router: Router, 
  			  private provider: Perfils, 
  			  public alertCtrl: AlertController,
  			  public loadingCtrl: LoadingController
  			  ) {
  		this.usuario   = localStorage.getItem('NOMBRESAPELLIDOS');
      	this.usuarioid = localStorage.getItem('IDUSER');
	  	
  }
  salir(){
      this.navController.navigateRoot('login');
  }
  ionViewDidEnter() {
				  	
  }
  regresar(){
        this.navController.back();
    }

}//FIN CLASS
