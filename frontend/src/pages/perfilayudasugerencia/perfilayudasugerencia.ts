import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl} from '@angular/forms';

import { Router } from '@angular/router';
import { NavController, LoadingController, AlertController } from '@ionic/angular';

import { Perfils } from '../../providers/perfils';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { Crop, CropOptions } from '@ionic-native/crop/ngx';
import { File } from '@ionic-native/file/ngx';
import { Menus } from '../../providers/menus';
import { Variablesglobales } from '../../providers/variablesglobal';

@Component({
  selector: 'app-perfilayudasugerencia',
  templateUrl: 'perfilayudasugerencia.html',
  styleUrls: ['perfilayudasugerencia.scss'],
  providers:[Perfils, Camera, File, Crop, Variablesglobales, Menus]
})

export class Perfilayudasugerencia {
  public myForm: FormGroup;
  public loading: any;
  public usuarioid: string;

  public url = new Variablesglobales();
  public contador = 0 //Agrego esta linea
  public nombres: string;
  public apellidos: string;
  public avatar:string="";
  public text:string="";
  public invitados = 0;
  public seguidores = 0;
  public color     = "";
  public usuario = "";
  public porcentaje = "";

  public croppedImagepath = "";
  public isLoading = false;
  public cropOptions: CropOptions = {
    quality: 100
  }

  public image: string[] = ['', '', '', ''];
  public image1: string = '';
  public image2: string = '';
  public image3: string = '';
  public image4: string = ''; 

  public fotoperfil = "";
  
  constructor(public navController: NavController,
  			  public formBuilder: FormBuilder,
  			  private router: Router, 
  			  private provider_perfil: Perfils, 
  			  public alertCtrl: AlertController,
  			  public loadingCtrl: LoadingController,
           private camera: Camera,
          private crop: Crop,
          private file: File,
          private provider_menu: Menus
  			  ) {
      this.myForm = this.formBuilder.group({
         nombres: new FormControl('', Validators.compose([ 
                              Validators.required,
                               Validators.maxLength(540)
                              ])
                   )      
      });
      this.usuario   = localStorage.getItem('NOMBRESAPELLIDOS');
      this.usuarioid = localStorage.getItem('IDUSER');

      
  }
  salir(){
      this.navController.navigateRoot('menu');
  }
  perfilclaveeditar(){
      this.navController.navigateForward('perfilclaveeditar');
  }
  perfileditar(){
      this.navController.navigateForward('perfileditar');
  }
  perfilsobrenosotros(){
      this.navController.navigateForward('perfilsobrenosotros');
  }
  perfilmejorando(){
      this.navController.navigateForward('perfilmejorando'); 
  }
  perfilayuda(){
      this.navController.navigateForward('perfilayuda'); 
  }
  perfilinvitado(){
      this.navController.navigateForward('perfilinvitado'); 
  }
  perfilconfiguracion(){
      this.navController.navigateForward('perfilconfiguracion'); 
  }
  regresar(){
        this.navController.back();
  }
  ionViewDidEnter() {
  			this.usuarioid  = localStorage.getItem('IDUSER');
         
  }


  onKey(event){
    this.contador = event.target.value.length
  }

  textareaMaxLengthValidation() {
    if (this.text.length > 540) {
      this.text= this.text.slice(0, 540);
    }
  }


enviarformulario(v){
    const loader = this.loadingCtrl.create({
      message: "Un momento por favor..."
    }).then(load => {
                      load.present();

                      let cadena1 = this.myForm.value.nombres;
                      let termino1 = "SCRIPT";
                      let termino2 = "ONERROR";
                      let termino3 = "IFRAME";
                      let termino4 = "ONCLICK";
                      let termino5 = "JAVASCRIPT:ALERT";
                      // para buscar la palabra hacemos
                      let posicion1 = cadena1.toLowerCase().indexOf(termino1.toLowerCase());
                      let posicion2 = cadena1.toLowerCase().indexOf(termino2.toLowerCase());
                      let posicion3 = cadena1.toLowerCase().indexOf(termino3.toLowerCase());
                      let posicion4 = cadena1.toLowerCase().indexOf(termino4.toLowerCase());
                      let posicion5 = cadena1.toLowerCase().indexOf(termino5.toLowerCase());
                      ///////////
                      let vererror = 0;
                      if (posicion1 !== -1 || posicion2 !== -1 || posicion3 !== -1 || posicion4 !== -1 || posicion5 !== -1){
                        let vererror = 1;
                              this.loadingCtrl.dismiss().then( () => {
                                    const alert = this.alertCtrl.create({
                                        subHeader: "Aviso",
                                        message: "Datos incorrectos",
                                        buttons: [
                                          {
                                              text: "Reintentar",
                                              role: 'cancel',
                                              cssClass:'ion-aceptar',
                                              handler: data => {

                                              }
                                          }
                                        ]
                                    }).then(alert => alert.present());
                            });//FIN LOADING DISS

                      }else{

                                    this.provider_perfil.addayudas(this.usuarioid, this.myForm.value.nombres).subscribe((response) => {
                                                this.loadingCtrl.dismiss().then( () => {
                                                        if(response['code']==200){
                                                               const alert = this.alertCtrl.create({
                                                                      subHeader: "Gracias por ayudarnos ",
                                                                      message: "a mejorar ",
                                                                      buttons: [
                                                                        {
                                                                            text: "Regresar a mi perfil",
                                                                            cssClass:'ion-pagar3',
                                                                            handler: data => {
                                                                                this.navController.back();
                                                                            }
                                                                        },
                                                                        {
                                                                            text: "Ir al inicio",
                                                                            role: 'cancel',
                                                                            cssClass:'ion-pagar2',
                                                                            handler: data => {
                                                                                let contarinicio   = '';
                                                                                let contarinicio2  = 0;
                                                                                contarinicio  = localStorage.getItem('contarinicio_1_1');
                                                                                contarinicio2 = parseInt(localStorage.getItem('contarinicio_1_1'));
                                                                                contarinicio2++;  contarinicio2++;
                                                                                localStorage.setItem('contarinicio_1_1', contarinicio2+'')
                                                                                this.navController.navigateForward('principal/inicio/'+contarinicio2);
                                                                            }
                                                                        }
                                                                        
                                                                      ]
                                                                  }).then(alert => alert.present());
                                                        }else{
                                                              const alert = this.alertCtrl.create({
                                                                  subHeader: "Aviso",
                                                                  message: response['msg'],
                                                                  buttons: [
                                                                    {
                                                                        text: "Reintentar",
                                                                        role: 'cancel',
                                                                        cssClass:'ion-aceptar',
                                                                        handler: data => {

                                                                        }
                                                                    }
                                                                  ]
                                                              }).then(alert => alert.present());
                                                        }
                                                });//FIN LOADING DISS
                                    },error => {
                                          this.loadingCtrl.dismiss().then( () => {
                                                    const alert = this.alertCtrl.create({
                                                        subHeader: "Aviso",
                                                        message: "No pudo conectarse al servidor",
                                                        buttons: [
                                                          {
                                                              text: "Reintentar",
                                                              role: 'cancel',
                                                              cssClass:'ion-aceptar',
                                                              handler: data => {
                                                                this.provider_menu.logerroresadd("login registro 3", JSON.stringify(error)).subscribe((response) => { });//FIN LOADING DISS
                                                              }
                                                          }
                                                        ]
                                                    }).then(alert => alert.present());
                                          });//FIN LOADING DISS
                                    });//FIN POST
                  }//fin else
        });//FIn LOADING
  }//FIN FUNCTION


}//fin class
