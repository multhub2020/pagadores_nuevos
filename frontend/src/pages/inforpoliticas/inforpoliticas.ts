import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl} from '@angular/forms';

import { Router } from '@angular/router';
import { NavController, LoadingController, AlertController } from '@ionic/angular';
import { Menus } from '../../providers/menus';
import { Informacion } from '../../providers/informacion';

@Component({
  selector: 'app-inforpoliticas',
  templateUrl: 'inforpoliticas.html',
  styleUrls: ['inforpoliticas.scss'],
  providers:[Informacion, Menus]
})

export class Inforpoliticas {
    public texto: string;
    constructor(public navController: NavController,
  			  private router: Router, 
  			  private provider_informacion: Informacion, 
  			  public alertCtrl: AlertController,
  			  public loadingCtrl: LoadingController,
          private provider_menu: Menus
  			  ){
    }
    ionViewDidEnter() {
          const loader = this.loadingCtrl.create({
            message: "Un momento por favor..."
          }).then(load => {
              load.present();
              this.provider_informacion.inforpoliticas(2).subscribe((response) => {  
                    this.loadingCtrl.dismiss().then( () => { 
                          this.texto=response['texto'];
                    });
              },error => {
                    this.loadingCtrl.dismiss().then( () => {
                              const alert = this.alertCtrl.create({
                                  subHeader: "Aviso",
                                  message: "No pudo conectarse al servidor",
                                  buttons: [
                                    {
                                        text: "Reintentar",
                                        role: 'cancel',
                                        cssClass:'ion-aceptar',
                                        handler: data => {
                                            this.provider_menu.logerroresadd("nformacion politica", JSON.stringify(error)).subscribe((response) => { });//FIN LOADING DISS
                                            this.ionViewDidEnter();
                                        }
                                    }
                                  ]
                              }).then(alert => alert.present());
                    });//FIN LOADING DISS
              });//FIN POST
          });
    }
}//FIN CLASS
