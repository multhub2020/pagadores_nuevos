import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { Principal } from './principal';
import { PrincipalRoutingModule } from './principal-routing.module';

import { SuperTabsModule } from '@ionic-super-tabs/angular';

//import { MapaModule } from '../mapa/mapa.module';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    PrincipalRoutingModule,
    SuperTabsModule,
  //  MapaModule
  ],
  declarations: [Principal]
})
export class PrincipalModule {}
