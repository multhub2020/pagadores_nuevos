import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AclGuard } from '../../guards/acl.guard';
import { Principal } from './principal';

const routes: Routes = [
    {
        path: '',
        component: Principal,
        children: [
                {
                  path: 'inicio/:id',
                  loadChildren: () => import('../inicio/inicio.module').then(m => m.InicioModule),
                  canActivate:[AclGuard]
                },
                {
                  path: 'perfil',
                  loadChildren: () => import('../perfil/perfil.module').then(m => m.PerfilModule),
                  canActivate:[AclGuard]
                },
                {
                  path: 'notificaciones',
                  loadChildren: () => import('../notificaciones/notificaciones.module').then(m => m.NotificacionesModule),
                  canActivate:[AclGuard]
                },
                {
                  path: 'perfilnotificaciones',
                  loadChildren: () => import('../perfilnotificaciones/perfilnotificaciones.module').then(m => m.PerfilnotificacionesModule),
                  canActivate:[AclGuard]
                },
                
        ]				
    },    
    {
      path: 'menu',
      loadChildren: () => import('../menu/menu.module').then(m => m.MenuModule),
      canActivate:[AclGuard]
    },
    {
      path: 'menusolucionadas',
      loadChildren: () => import('../menusolucionadas/menusolucionadas.module').then(m => m.MenusolucionadasModule),
      canActivate:[AclGuard]
    },
    {
      path: 'menucuentas',
      loadChildren: () => import('../menucuentas/menucuentas.module').then(m => m.MenucuentasModule),
      canActivate:[AclGuard]
    },
    {
      path: 'menuenproceso',
      loadChildren: () => import('../menuenproceso/menuenproceso.module').then(m => m.MenuenprocesoModule),
      canActivate:[AclGuard]
    },
    {
        path: 'menudeudas1',
        loadChildren: () => import('../menudeudas1/menudeudas1.module').then(m => m.Menudeudas1Module),
        canActivate:[AclGuard]
    },
    {
      path: 'localizardeuda1',
      loadChildren: () => import('../localizardeuda1/localizardeuda1.module').then(m => m.Localizardeuda1Module),
      canActivate:[AclGuard]
    },   
    {
      path: 'localizardeuda1_1',
      loadChildren: () => import('../localizardeuda1_1/localizardeuda1_1.module').then(m => m.Localizardeuda1_1Module),
      canActivate:[AclGuard]
    },  
    {
      path: 'localizardeuda1_2',
      loadChildren: () => import('../localizardeuda1_2/localizardeuda1_2.module').then(m => m.Localizardeuda1_2Module),
      canActivate:[AclGuard]
    },   
    {
      path: 'localizardeuda1_3',
      loadChildren: () => import('../localizardeuda1_3/localizardeuda1_3.module').then(m => m.Localizardeuda1_3Module),
      canActivate:[AclGuard]
    },   
    {
      path: 'localizardeuda1_4',
      loadChildren: () => import('../localizardeuda1_4/localizardeuda1_4.module').then(m => m.Localizardeuda1_4Module),
      canActivate:[AclGuard]
    },   
    {
      path: 'localizardeuda1_5',
      loadChildren: () => import('../localizardeuda1_5/localizardeuda1_5.module').then(m => m.Localizardeuda1_5Module),
      canActivate:[AclGuard]
    },   
    {
      path: 'localizardeuda1_6',
      loadChildren: () => import('../localizardeuda1_6/localizardeuda1_6.module').then(m => m.Localizardeuda1_6Module),
      canActivate:[AclGuard]
    },

    {
      path: 'localizardeuda2_1',
      loadChildren: () => import('../localizardeuda2_1/localizardeuda2_1.module').then(m => m.Localizardeuda2_1Module),
      canActivate:[AclGuard]
    },  
    {
      path: 'localizardeuda2_2',
      loadChildren: () => import('../localizardeuda2_2/localizardeuda2_2.module').then(m => m.Localizardeuda2_2Module),
      canActivate:[AclGuard]
    },

    

    {
      path: 'localizardeuda2_3',
      loadChildren: () => import('../localizardeuda2_3/localizardeuda2_3.module').then(m => m.Localizardeuda2_3Module),
      canActivate:[AclGuard]
    },  

    {
      path: 'localizardeuda2_3_1',
      loadChildren: () => import('../localizardeuda2_3_1/localizardeuda2_3_1.module').then(m => m.Localizardeuda2_3_1Module),
      canActivate:[AclGuard]
    }, 
    {
      path: 'localizardeuda2_3_2',
      loadChildren: () => import('../localizardeuda2_3_2/localizardeuda2_3_2.module').then(m => m.Localizardeuda2_3_2Module),
      canActivate:[AclGuard]
    }, 


    {
        path: 'mejorar1',
        loadChildren: () => import('../mejorar1/mejorar1.module').then(m => m.Mejorar1Module),
        canActivate:[AclGuard]
    },
    {
      path: '',
      redirectTo: 'inicio', // redirección a este componente en el primer acceso
      pathMatch: 'full'
    }
]
@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class PrincipalRoutingModule {}
