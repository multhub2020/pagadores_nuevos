import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NavController, LoadingController, AlertController, Platform } from '@ionic/angular';
import { FormBuilder, FormGroup, Validators, FormControl} from '@angular/forms';
import { GooglePlus } from '@ionic-native/google-plus/ngx';
import { FacebookLoginResponse, Facebook} from "@ionic-native/facebook/ngx";
//import { Mapa } from '../mapa/mapa';
import { Menus } from '../../providers/menus';
import { Listacontactos } from '../../providers/listacontactos';
import { SuperTabs } from "@ionic-super-tabs/angular";

import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { Variablesglobales } from '../../providers/variablesglobal';

import { Contacts, Contact, ContactField, ContactName } from '@ionic-native/contacts';
import { InAppBrowser, InAppBrowserObject, InAppBrowserOptions } from '@ionic-native/in-app-browser/ngx';


@Component({
  selector: 'app-principal',
  templateUrl: 'principal.html',
  styleUrls: ['principal.scss'],
  providers:[Menus, SocialSharing,  GooglePlus, Facebook, Variablesglobales, Listacontactos, InAppBrowser]
})

export class Principal {
    public usuario: string;
    public usuarioid: string;
    public inicio = 1;

     public timeLeft =5;
    public timeLeft2='5';
    public interval=0;
    constructor(private router: Router,
                public formBuilder: FormBuilder,
                private navController: NavController,
                private provider_menu: Menus,
                private iab: InAppBrowser,
                private provider: Listacontactos,
                public alertCtrl: AlertController,
                public loadingCtrl: LoadingController,
                private socialSharing: SocialSharing,
                private contacts:Contacts,
                private platform: Platform,
                private fb: Facebook,
                private googlePlus: GooglePlus,
                //public supertabs: SuperTabs
                ){
        this.usuario   = localStorage.getItem('NOMBRESAPELLIDOS');
        this.usuarioid = localStorage.getItem('IDUSER');
    }//FIN FUncTION
    ionViewDidEnter(){
      if(localStorage.getItem("banderainicioimagenadd")!='1'){
        localStorage.setItem('banderainicioimagenadd', '1');
        this.primeracarga();
      }
    }//FIN FcuntiN
     primeracarga(){
      this.inicio = 2;
      this.startTimer();
      //const browser = this.iab.create(url);
      //const browser = this.iab.create(url, '_system', this.options2);
    }
    startTimer() {
        this.interval = setInterval(() => {
          if(this.timeLeft > 0) {
            this.timeLeft--;
          } else {
               clearInterval(this.interval);
                this.navController.navigateRoot('principal/menu');
          }
        },1000)
      }
    linkmenu(var1){
       if(var1=="logout"){
              localStorage.removeItem('IDUSER');
              localStorage.removeItem('USUARIO');
              localStorage.removeItem('NOMBRESAPELLIDOS');
              localStorage.removeItem('TOKEN');
              localStorage.removeItem('SESSIONACTIVA');
              localStorage.removeItem('consultaFechaBuro');
              localStorage.setItem('SESSIONACTIVA','false');
              this.navController.navigateRoot('login');
              this.googlePlus.logout();
              this.fb.logout();
       }else{
              this.inicio = 1;
              this.navController.navigateForward(var1);
       }
    }//fin function
  
}//FIN CLASS