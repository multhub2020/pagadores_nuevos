import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NavController, LoadingController, AlertController } from '@ionic/angular';

import { Menus } from '../../providers/menus';
import { Variablesglobales } from '../../providers/variablesglobal';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';

import { IonicSelectableComponent } from 'ionic-selectable';

class Port {
  public id: string;
  public name: string;
  public identification_number: string;
}

class Monedas {
  public id: string;
  public name: string;
}

class Productos {
  public id: string;
  public name: string;
}


@Component({
  selector: 'app-agregadeuda1',
  templateUrl: 'agregadeuda1.html',
  styleUrls: ['agregadeuda1.scss'],
  providers: [Menus, Variablesglobales]
})
export class Agregadeuda1 {
  public myForm: FormGroup;
  public datos_menus: any;
  public datos_publi: any;
  public imgurl = new Variablesglobales();
  public imgurl2: any;
  public selectedIndex = 0;
  public usuario: string;
  public usuarioid: string;
  public slideOpts = {
    effect: 'flip',
    autoplay: {
      delay: 5000
    }
  };

  ports: Port[];
  port: Port;

  public value = '';

  monedas: Monedas[];
  moneda: Monedas;

  productos: Productos[];
  producto: Productos;

  constructor(private router: Router,
    private navController: NavController,
    private provider_menu: Menus,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public formBuilder: FormBuilder
  ) {
    this.usuario = localStorage.getItem('NOMBRESAPELLIDOS');
    this.usuarioid = localStorage.getItem('IDUSER');
    this.myForm = this.formBuilder.group({
      /*banco: new FormControl('', Validators.compose([
        Validators.required,
      ])
      ),*/
      banco2: new FormControl('', Validators.compose([
        Validators.required,
      ])
      ),
      moneda: new FormControl('', Validators.compose([
        Validators.required,
      ])
      ),
      producto: new FormControl('', Validators.compose([
        Validators.required,
      ])
      ),
      deuda: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[0-9., ]+$'),
        Validators.maxLength(10)
      ])
      )
    });

    this.ports = [
      { id: 'Trebol', name: 'Trebol', identification_number: '' },
      { id: 'Scotiabank', name: 'Scotiabank', identification_number: '' },
      { id: 'Banco BHD León', name: 'Banco BHD León', identification_number: '' },
      { id: 'Banco Popular', name: 'Banco Popular', identification_number: '' }
    ];

    this.monedas = [
      { id: 'USD', name: 'Dólares' },
      { id: 'DOP', name: 'Pesos dominicanos' },
    ];

    this.productos = [
      { id: '1', name: 'Tarjeta de crédito' },
      { id: '2', name: 'Préstamo' },
    ];

    this.moneda = this.monedas[1];
  }//FIN FUncTION    

  ionViewDidEnter() {
    this.loadingCtrl.create({
      message: "Un momento por favor..."
    }).then(load2 => {
      load2.present();
      this.provider_menu.get_all_creditors().subscribe((response) => {
        //console.log(response);
        load2.dismiss().then(() => {
          if (!response['error']) {
            let allCreditors = response['results'];
            allCreditors.forEach((element, index) => {
              this.ports[index] = {
                id: element.id,
                name: element.name,
                identification_number: element.identification_number
              };
            });
          } else {
            const alert = this.alertCtrl.create({
              subHeader: "Aviso",
              message: response['message'],
              buttons: [
                {
                  text: "Ok",
                  role: 'cancel'
                }
              ]
            }).then(alert => alert.present());
          }//Fin else
        });//FIN LOADING DISS
      }, error => {
        load2.dismiss().then(() => {
          const alert = this.alertCtrl.create({
            subHeader: "Aviso",
            message: "No pudo conectarse al servidor",
            buttons: [
              {
                text: "Reintentar",
                role: 'cancel',
                cssClass: 'ion-aceptar',
                handler: data => {
                  this.provider_menu.logerroresadd("Agregar deuda", JSON.stringify(error)).subscribe((response) => { });//FIN LOADING DISS
                  this.ionViewDidEnter();
                }
              }
            ]
          }).then(alert => alert.present());
        });//FIN LOADING DISS
      });//FIN POST
    });
  }//FIN FcuntiN

  scrollTo() {

    this.value = ""

  }

  portChange(event: {
    component: IonicSelectableComponent,
    value: any
  }) {
    console.log('port:', event.value);
  }


  monedasChange(event: {
    component: IonicSelectableComponent,
    value: any
  }) {
    console.log('port:', event.value);
  }

  productosChange(event: {
    component: IonicSelectableComponent,
    value: any
  }) {
    console.log('port:', event.value);
  }

  enviarformulario(v) {
    if(this.myForm.value.deuda=="" || this.myForm.value.deuda=="0"){
                const alert = this.alertCtrl.create({
                  subHeader: "Aviso",
                  message: "Debe ingresar un monto",
                  buttons: [
                    {
                      text: "Aceptar",
                      role: 'cancel',
                      cssClass: 'ion-aceptar',
                      handler: data => {

                      }
                    }
                  ]
                }).then(alert => alert.present());
    }else{

              const alert = this.alertCtrl.create({
                            subHeader: "Aviso",
                            message: "¿Estás seguro que desea continuar?",
                            buttons: [
                              {
                                text: "No",
                                role: 'cancel',
                                cssClass: 'ion-aceptar',
                                handler: data => {
                                }
                              },
                               {
                                text: "Sí",                                
                                cssClass: 'ion-pagar',
                                handler: data => {
                                   this.enviarformulario_(v);
                                }
                              }
                            ]
                          }).then(alert => alert.present());
    }
  }
  enviarformulario_(v) {
        
                const loader = this.loadingCtrl.create({
                  message: "Un momento por favor..."
                }).then(load2 => {
                  load2.present();
                              this.myForm.value.banco = "1";
                              this.provider_menu.agregadeuda1(this.usuarioid, this.myForm.value).subscribe((response) => {
                                load2.dismiss().then(() => {
                                  if (response['code'] == 200) {

                                    const alert = this.alertCtrl.create({
                                      subHeader: "Aviso",
                                      message: "La deuda fue agregada",
                                      buttons: [
                                        {
                                          text: "Aceptar",
                                          role: 'cancel',
                                          cssClass: 'ion-aceptar',
                                          handler: data => {
                                            this.navController.back();
                                          }
                                        }
                                      ]
                                    }).then(alert => alert.present());

                                  }//Fin else
                                });//FIN LOADING DISS
                              }, error => {
                                load2.dismiss().then(() => {
                                  const alert = this.alertCtrl.create({
                                    subHeader: "Aviso",
                                    message: "No pudo conectarse al servidor",
                                    buttons: [
                                      {
                                        text: "Reintentar",
                                        role: 'cancel',
                                        cssClass: 'ion-aceptar',
                                        handler: data => {
                                          this.provider_menu.logerroresadd("Agregar deuda", JSON.stringify(error)).subscribe((response) => { });//FIN LOADING DISS
                                          this.ionViewDidEnter();
                                        }
                                      }
                                    ]
                                  }).then(alert => alert.present());
                                });//FIN LOADING DISS
                              });//FIN POST
                });//FIn LOADING
  }//fin functin

  regresar() {
    this.navController.back();
  }

  slidesDidLoad(slides) {
    slides.startAutoplay();

  }
}//FIN CLASS