import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Agregadeuda1 } from './agregadeuda1';
import { NgxMaskIonicModule } from 'ngx-mask-ionic';
import { NgxCurrencyModule } from "ngx-currency";

import { IonicSelectableModule } from 'ionic-selectable';


@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgxMaskIonicModule,
    NgxCurrencyModule,
    //IonicPageModule.forChild(Agregadeuda1),
    IonicSelectableModule,
    RouterModule.forChild([{ path: '', component: Agregadeuda1 }])
  ],
  declarations: [Agregadeuda1]
})
export class Agregadeuda1Module {}
