import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NavController, LoadingController, AlertController, Platform } from '@ionic/angular';
import { FormBuilder, FormGroup, Validators, FormControl} from '@angular/forms';
import { GooglePlus } from '@ionic-native/google-plus/ngx';
import { FacebookLoginResponse, Facebook} from "@ionic-native/facebook/ngx";
//import { Mapa } from '../mapa/mapa';
import { Menus } from '../../providers/menus';
import { Listacontactos } from '../../providers/listacontactos';
import { SuperTabs } from "@ionic-super-tabs/angular";

import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { Variablesglobales } from '../../providers/variablesglobal';

import { Contacts, Contact, ContactField, ContactName } from '@ionic-native/contacts';
import { InAppBrowser, InAppBrowserObject, InAppBrowserOptions } from '@ionic-native/in-app-browser/ngx';

import { ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-verdetallecontinua',
  templateUrl: 'verdetallecontinua.html',
  styleUrls: ['verdetallecontinua.scss'],
  providers:[Menus, SocialSharing,  GooglePlus, Facebook, Variablesglobales, Listacontactos, InAppBrowser]
})

export class Verdetallecontinua {
    //myPage = Mapa;
    @ViewChild(SuperTabs, { static: false }) superTabs: SuperTabs;
    public versionapp = "1.0.5";
    public datosdeudas: any=[''];
    public datos_publi: any;
    public selectedIndex = 0;
    public usuario: string;
    public usuarioid: string;
    public bodys: string;
    public myForm: FormGroup;
    public imgurl   = new Variablesglobales();
    public imgurl2: any;
    public listaContactos: any = [];
    public listaContactos2: any = [];

    public datosMostar: any = [];
    public datosMostar2: any = [];
    public checkbox: any = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
    public avatar:string="";
    public invitados = 0;
    public contador_c = 0;
    public seguidores = 0;
    public color     = "";
    public i=0;
    public deudaid = "";
    public slideOpts = {
          effect: 'flip',
          autoplay: {
            delay: 5000
          }
    };
    public invitado = "";
    public appPages = [
      {
        title: 'Sugerencias',
        url: 'perfilsugerencias',
        icon: 'mail'
      }
    ];
    public options2: InAppBrowserOptions = {
       //location : 'no',
       //fullscreen : 'yes',
    };
    public cargandoBtn = false;
    public pending_loader = null;

    constructor(private router: Router,
                public formBuilder: FormBuilder,
                private navController: NavController,
                private provider_menu: Menus,
                private iab: InAppBrowser,
                private provider: Listacontactos,
                public alertCtrl: AlertController,
                public loadingCtrl: LoadingController,
                private socialSharing: SocialSharing,
                private contacts:Contacts,
                private platform: Platform,
                private fb: Facebook,
                private googlePlus: GooglePlus,
                private rutaActiva: ActivatedRoute
                //public supertabs: SuperTabs
                ){
        this.usuario   = localStorage.getItem('NOMBRESAPELLIDOS');
        this.usuarioid = localStorage.getItem('IDUSER');
        this.myForm = this.formBuilder.group({
            usuario: new FormControl('', Validators.compose([
                                Validators.required
                                ])
                     )

        });
    }//FIN FUncTION
    primeracarga(url){
      //const browser = this.iab.create(url);
      //const browser = this.iab.create(url, '_system', this.options2);
    }
    ionViewDidEnter() {
      //this.slider.startAutoplay();
      this.deudaid = this.rutaActiva.snapshot.paramMap.get('id');
      this.checkbox = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
              const loader = this.loadingCtrl.create({
                message: "Un momento por favor..."
              }).then(load2 => {
                        load2.present();
                        this.provider_menu.statusaprobacion(this.usuarioid, this.deudaid ).subscribe((response) => {
                                    load2.dismiss().then( () => {
                                            if(response['code']==200){
                                                this.datosdeudas   = response['datosdeudas'];
                                                console.log(this.datosdeudas);
                                            }else if (response['code']==201){
                                                        const alert = this.alertCtrl.create({
                                                          subHeader: "Aviso",
                                                            message: response['msg'],
                                                            buttons: [
                                                              {
                                                                text: "Ok",
                                                                role: 'cancel'
                                                              }
                                                            ]
                                                        }).then(alert => alert.present());
                                            }//Fin else
                                    });//FIN LOADING DISS
                        },error => {
                              load2.dismiss().then( () => {
                                        const alert = this.alertCtrl.create({
                                            subHeader: "Aviso",
                                            message: "No pudo conectarse al servidor",
                                            buttons: [
                                              {
                                                  text: "Reintentar",
                                                  role: 'cancel',
                                                  cssClass:'ion-aceptar',
                                                  handler: data => {
                                                    this.provider_menu.logerroresadd("Ver detalle continuar", JSON.stringify(error)).subscribe((response) => { });//FIN LOADING DISS
                                                      this.ionViewDidEnter();
                                                  }
                                              }
                                            ]
                                        }).then(alert => alert.present());
                            });//FIN LOADING DISS
                        });//FIN POST
              });//FIn LOADING

    }//FIN FcuntiN
    regresar(){
        this.navController.back();
    }
    continuar(){        
    

    }

    finalizar(){        
      let contarinicio   = '';
                          let contarinicio2  = 0;
                          contarinicio  = localStorage.getItem('contarinicio_1_1');
                          contarinicio2 = parseInt(localStorage.getItem('contarinicio_1_1'));
                          contarinicio2++;  contarinicio2++;
                          localStorage.setItem('contarinicio_1_1', contarinicio2+'')
                          this.navController.navigateRoot('principal/inicio/'+contarinicio2);

    }

    pagar(a){
        this.deudaid = this.rutaActiva.snapshot.paramMap.get('id');
        this.navController.navigateForward('verdetallecontraofertapagar/'+this.deudaid );
    }


    firmar(a) {
      //this.slider.startAutoplay();
      this.deudaid = this.rutaActiva.snapshot.paramMap.get('id');
              const loader = this.loadingCtrl.create({
                message: "Un momento por favor..."
              }).then(load2 => {
                        load2.present();
                        this.provider_menu.firmardeuda(this.usuarioid, this.deudaid ).subscribe((response) => {
                                    load2.dismiss().then( () => {
                                            if(response['code']==200){
                                                this.datosdeudas   = response['datosdeudas'];
                                                console.log(this.datosdeudas);
                                            }else if (response['code']==201){
                                                        const alert = this.alertCtrl.create({
                                                          subHeader: "Aviso",
                                                            message: response['msg'],
                                                            buttons: [
                                                              {
                                                                text: "Ok",
                                                                role: 'cancel'
                                                              }
                                                            ]
                                                        }).then(alert => alert.present());
                                            }//Fin else
                                    });//FIN LOADING DISS
                        },error => {
                              load2.dismiss().then( () => {
                                        const alert = this.alertCtrl.create({
                                            subHeader: "Aviso",
                                            message: "No pudo conectarse al servidor",
                                            buttons: [
                                              {
                                                  text: "Reintentar",
                                                  role: 'cancel',
                                                  cssClass:'ion-aceptar',
                                                  handler: data => {
                                                      this.provider_menu.logerroresadd("Ver detalle continuar", JSON.stringify(error)).subscribe((response) => { });//FIN LOADING DISS
                                                      this.ionViewDidEnter();
                                                  }
                                              }
                                            ]
                                        }).then(alert => alert.present());
                            });//FIN LOADING DISS
                        });//FIN POST
              });//FIn LOADING

    }//FIN FcuntiN


}//FIN CLASS