import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Verdetallecontinua } from './verdetallecontinua';
import { SuperTabsModule } from '@ionic-super-tabs/angular';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
  //  MenuRoutingModule,
    SuperTabsModule,
    RouterModule.forChild([{ path: '', component: Verdetallecontinua }])
  //  MapaModule
  ],
  declarations: [Verdetallecontinua]
})
export class VerdetallecontinuaModule {}
