import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Listarseguidoressub } from './listarseguidoressub';
import { IonicRatingModule } from 'ionic4-rating';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicRatingModule,
    RouterModule.forChild([{ path: '', component: Listarseguidoressub }])
  ],
  declarations: [Listarseguidoressub]
})
export class ListarseguidoressubModule {}
