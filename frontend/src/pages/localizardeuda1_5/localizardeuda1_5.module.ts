import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Localizardeuda1_5 } from './localizardeuda1_5';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild([{ path: '', component: Localizardeuda1_5 }])
  ],
  declarations: [Localizardeuda1_5]
})
export class Localizardeuda1_5Module {}
