import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl} from '@angular/forms';

import { Router, ActivatedRoute } from '@angular/router';
import { NavController, LoadingController, AlertController } from '@ionic/angular';

import { Menus } from '../../providers/menus';

@Component({
  selector: 'app-localizardeuda1_5',
  templateUrl: 'localizardeuda1_5.html',
  styleUrls: ['localizardeuda1_5.scss'],
  providers:[Menus]
})

export class Localizardeuda1_5 {
  public datosdeudas: any=[''];
  public AccountNumber: string;
  public ExpirationMonth: string;
  public ExpirationYear: string;
  public CVV: string;
  public ZipCode: string;
  public CustomerID: string;
  public CustomerName: string;
  public CustomerEmail: string;
  public Amount: string;
  public Currency: any;
  public DATA: string;

  public Customeraddress: string;
  public CustomerCity: string;
  public CustomerState: string;

  public myForm: FormGroup;
  public loading: any;
  public usuarioregistro: string;
  public usuarioid: string;
  public t_push: string;
  public p_push: string;
  public u_push: string;
  public mensaje1 = "";
  public mensaje2 = "";
  public inicio=0;  
  public timeLeft =1;
  public timeLeft2='1';
  public interval=0;
  public checkbox: any;
  constructor(public navController: NavController,
  			  public formBuilder: FormBuilder,
  			  private router: Router,
  			  private provider: Menus,
  			  public alertCtrl: AlertController,
          public loadingCtrl: LoadingController,
          private route: ActivatedRoute,
          private provider_menu: Menus,
  			  ) {
      this.myForm = this.formBuilder.group({
        AccountNumber: new FormControl('', Validators.compose([ 
          Validators.required,
          Validators.minLength(16),
          Validators.maxLength(16)
        ])),
        ExpirationMonth: new FormControl('', Validators.compose([ 
          Validators.required
        ])),
        ExpirationYear: new FormControl('', Validators.compose([ 
          Validators.required
        ])),
        CVV: new FormControl('', Validators.compose([ 
          Validators.required,
          Validators.minLength(3),
          Validators.maxLength(3),
        ])),
        ZipCode: new FormControl('', Validators.compose([ 
          Validators.required
        ])),
        CustomerName: new FormControl('', Validators.compose([ 
          Validators.required,
          Validators.minLength(3)
        ])),
        CustomerID: new FormControl('', Validators.compose([ 
          Validators.required
        ])),
        CustomerEmail: new FormControl('', Validators.compose([ 
          Validators.required,
          Validators.email
        ])),
        Customeraddress: new FormControl('', Validators.compose([ 
          Validators.required
        ])),
        CustomerCity: new FormControl('', Validators.compose([ 
          Validators.required
        ])),
        CustomerState: new FormControl('', Validators.compose([ 
          Validators.required
        ]))
      });

      this.usuarioid = localStorage.getItem('IDUSER');
      this.checkbox = localStorage.getItem('localizardeuda1_1_id1');
      this.Currency = 214;
  }
  ionViewDidEnter() {
    const loader = this.loadingCtrl.create({
      message: "Un momento por favor..."
    }).then(load2 => {
      load2.present();
      this.provider.localizardeudaid(this.usuarioid, this.checkbox).subscribe((response) => {
        load2.dismiss().then( () => {
          if(response['code']==200){
            this.datosdeudas = response['datosdeudas'];
            console.log(JSON.stringify(this.datosdeudas[0]));
          }else if (response['code']==201){
            const alert = this.alertCtrl.create({
              subHeader: "Aviso",
                message: response['msg'],
                buttons: [
                  {
                    text: "Ok",
                    role: 'cancel'
                  }
                ]
            }).then(alert => alert.present());
          }//Fin else
        });//FIN LOADING DISS
      },error => {
        load2.dismiss().then( () => {
          const alert = this.alertCtrl.create({
              subHeader: "Aviso",
              message: "No pudo conectarse al servidor",
              buttons: [
                {
                  text: "Reintentar",
                  role: 'cancel',
                  cssClass:'ion-aceptar',
                  handler: data => {
                    this.provider_menu.logerroresadd("localizardeuda1 5", JSON.stringify(error)).subscribe((response) => { });//FIN LOADING DISS
                      this.ionViewDidEnter();
                  }
                }
              ]
          }).then(alert => alert.present());
        });//FIN LOADING DISS
      });//FIN POST
    });//FIn LOADING
  }
  ngOnInit() {
    //this.datosdeudas = this.route.snapshot.paramMap.get('datosdeudas');
    //this.datosdeudas = Array.of(this.datosdeudas);
  }
  quitarresena(){

  }
  enviarformulario(){
    
  }
  regresar(){
    this.navController.back();
  }

  informacion(var1){
    if(var1==1){
      this.navController.navigateForward('inforcondiciones');
    }else if(var1==2){
      this.navController.navigateForward('inforpoliticas');
    }else if(var1==3){
      this.navController.navigateForward('loginrecuperar1');
    }else if(var1==4){
      this.navController.navigateForward('loginclave');
    }
  }

  isset(ref) { return typeof ref !== 'undefined' }

  continuar() {
    /*let datos = JSON.stringify({
      'AccountNumber':this.AccountNumber,
      'ExpirationMonth':this.ExpirationMonth,
      'ExpirationYear':this.ExpirationYear,
      'CVV':this.CVV,
      'AccountType':1,
      'ZipCode':this.ZipCode,
      'CustomerName':this.CustomerName,
      'CustomerEmail':this.CustomerEmail,
      'Amount':this.Amount
    });*/
    //alert(datos);

    const loader = this.loadingCtrl.create({
      message: "Un momento por favor..."
    }).then(load2 => {
      load2.present();

      this.Amount = this.datosdeudas[0]['pago_inicial'];
      this.Currency = 214;
      
      let ExpirationMonth = ("0" + (new Date(this.ExpirationMonth).getMonth() + 1)).slice(-2);
      let ExpirationYear = new Date(this.ExpirationYear).getFullYear();

      this.provider.authorizetoken(this.AccountNumber, ExpirationMonth, ExpirationYear, this.CVV, 1, this.ZipCode, this.CustomerID, this.CustomerName, this.CustomerEmail, this.Amount, this.Currency, this.Customeraddress, this.CustomerCity, this.CustomerState).subscribe((response) => {
        load2.dismiss().then( () => {
          console.log(JSON.stringify(response));
          if(response['code']==200){
            this.DATA = response['data'];
            if(!response['error'] && response['data']['ResponseCode'] == '00'){
              this.navController.navigateForward('principal/localizardeuda1_6'); 
            } else {
              const alert = this.alertCtrl.create({
                subHeader: "Aviso",
                message: response['error'] ? response['message'] : response['data']['Message'],
                buttons: [
                  {
                    text: "Ok",
                    role: 'cancel'
                  }
                ]
               }).then(alert => alert.present());
            }
          }else if (response['code']==201){
            const alert = this.alertCtrl.create({
              subHeader: "Aviso",
              message: response['msg'],
              buttons: [
                {
                  text: "Ok",
                  role: 'cancel'
                }
              ]
             }).then(alert => alert.present());
          }//Fin else
        });//FIN LOADING DISS
      },error => {
        load2.dismiss().then( () => {
          const alert = this.alertCtrl.create({
            subHeader: "Aviso",
            message: "No pudo conectarse al servidor",
            buttons: [
              {
                text: "Reintentar",
                role: 'cancel',
                cssClass:'ion-aceptar',
                handler: data => {
                   this.provider_menu.logerroresadd("localizardeuda1 5", JSON.stringify(error)).subscribe((response) => { });//FIN LOADING DISS
                  this.continuar(); 
                }
              }
            ]
          }).then(alert => alert.present());
        });//FIN LOADING DISS
      });//FIN POST
    });//FIn LOADING
  }

}//FIN CLASS
