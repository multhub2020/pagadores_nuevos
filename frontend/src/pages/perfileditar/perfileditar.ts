import { Component, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl} from '@angular/forms';

import { Router } from '@angular/router';
import { NavController, LoadingController, AlertController } from '@ionic/angular';
import { Menus } from '../../providers/menus';
import { Usuario } from '../../providers/usuario';

@Component({
  selector: 'app-perfileditar',
  templateUrl: 'perfileditar.html',
  styleUrls: ['perfileditar.scss'],
  providers:[Usuario, Menus]
})

export class Perfileditar {

	@ViewChild('passwordEyeRegister', { static: false }) passwordEye;
  //@ViewChild(SuperTabs, { static: false }) superTabs: SuperTabs;
  // Seleccionamos el elemento con el nombre que le pusimos con el #
  passwordTypeInput  =  'password';
  // Variable para cambiar dinamicamente el tipo de Input que por defecto sera 'password'
  iconpassword  =  'eye-off';


  public myForm: FormGroup;
  public loading: any;
  public email: string;

  public t_push: string;
  public p_push: string;
  public u_push: string;

  public usuarioregistro: string;
  public usuariocedula: string;
  public usuarionombres: string;

  public registroemail: string;
  public registrotelefono: string;
  public registrotelefono2: string;
  public claves: string;

  public customerid = "";
  public usuarioid: string;
  public usuario = "";
  public clave = "";
  public textcheckbox = false;


  public nombres: string;
  public apellidos: string;
  public user: string;
  public celular: string;

  public national_id: string;
  public cellphone: string;

  constructor(public navController: NavController,
  			  public formBuilder: FormBuilder,
  			  private router: Router, 
  			  private provider_usuario: Usuario, 
  			  public alertCtrl: AlertController,
  			  public loadingCtrl: LoadingController,
  			  private provider_menu: Menus
  			  ) {
  		this.usuario    = localStorage.getItem('NOMBRESAPELLIDOS');
      	this.usuarioid  = localStorage.getItem('IDUSER');
      	this.customerid = localStorage.getItem('CUSTOMERID');
	    				

	  	this.myForm = this.formBuilder.group({
										        nombres: new FormControl('', Validators.compose([ 
										                              Validators.required,
										                               Validators.maxLength(30)
										                              ])
										                   ),
										         apellidos: new FormControl('', Validators.compose([ 
										                              Validators.required,
										                              Validators.maxLength(30)
										                              ])
										                   ),
										         user: new FormControl('', Validators.compose([ 
							                              Validators.required
							                              ])
							                   ),
										         clave: new FormControl('', Validators.compose([ 
                              Validators.minLength(5)
                              ])
                   					),
									          celular: new FormControl('', Validators.compose([ 
									                                Validators.required,
									                                Validators.pattern('^[0-9 ]+$'),
									                                Validators.minLength(10)
									                                ])
									                     )
	    });
  }
  togglePasswordMode() {
      this.passwordTypeInput  =  this.passwordTypeInput  ===  'text'  ?  'password'  :  'text';
      this.iconpassword  =  this.iconpassword  ===  'eye-off'  ?  'eye'  :  'eye-off';
      this.passwordEye.el.setFocus();
  }
  salir(){
      this.navController.navigateRoot('login');
  }
  getText(e){
     var elementValue = e.srcElement.value;
     if(elementValue){
       var regex = /^[A-Za-z ]+$/;   
        var tempValue = elementValue.substring(0, elementValue.length - 1);
        if (!regex.test(elementValue)) {
          console.log("Entered char is not alphabet");
          e.srcElement.value = tempValue;
        }
     }
   }
  ionViewDidEnter() {
  	this.customerid = localStorage.getItem('CUSTOMERID');
  	const loader = this.loadingCtrl.create({
  		message: "Un momento por favor..."
    }).then(load => {
	    				load.present();
					  	this.provider_usuario.perfileditar(this.usuarioid).subscribe((response) => {  
					                this.loadingCtrl.dismiss().then( () => { 
					                        if(response['code']==200){
					                        	console.log(response);
					                        		this.nombres   = response['datos_usuarios'][0]['nombres'];
					                        		this.apellidos = response['datos_usuarios'][0]['apellidos'];
					                        		this.user      = response['datos_usuarios'][0]['email'];

					                        		this.cellphone   = response['datos_usuarios'][0]['telefono'];
					                        		this.national_id = response['datos_usuarios'][0]['ci'];
					                        		
					                        }else if (response['code']==201){
					                                    const alert = this.alertCtrl.create({
					                                    	subHeader: "Aviso",
					                                        message: response['msg'],
					                                        buttons: [
					                                          {
					                                            text: "Ok", 
					                                            role: 'cancel'
					                                          }
					                                        ]
					                                    }).then(alert => alert.present());
					                        }//Fin else
					                });//FIN LOADING DISS
					    },error => {
					    			this.loadingCtrl.dismiss().then( () => {
							                const alert = this.alertCtrl.create({
							                    subHeader: "Aviso",
							                    message: "No pudo conectarse al servidor",
							                    buttons: [
							                      {
							                          text: "Reintentar",
							                          role: 'cancel',
							                          cssClass:'ion-aceptar',
							                          handler: data => {
							                          	    this.provider_menu.logerroresadd("Perfil editar", JSON.stringify(error)).subscribe((response) => { });//FIN LOADING DISS
							                            	this.ionViewDidEnter();
							                          }
							                      }
							                    ]
							                }).then(alert => alert.present());
									});//FIN LOADING DISS
				    	});//FIN POST
				});//FIn LOADING
  }
  enviarformulario(){
  	const loader = this.loadingCtrl.create({
  		message: "Un momento por favor..."
    }).then(load => {
	    				load.present();
	    				//this.customerid = localStorage.getItem('CUSTOMERID');
	    				//alert(this.customerid);
	    				//alert(this.customerid);
	    				let cadena1 = this.myForm.value.nombres;
              let cadena2 = this.myForm.value.apellidos;
              let cadena3 = this.myForm.value.user;
              let cadena4 = this.myForm.value.clave;
              let cadena5 = this.myForm.value.celular;

              //let cadena4 = "";
              //let cadena5 = "";
              // esta es la palabra a buscar
              //“script”,” onerror”,” javascript:alert” , “iframe” , ”onclick”
              let termino1 = "SCRIPT";
              let termino2 = "ONERROR";
              let termino3 = "IFRAME";
              let termino4 = "ONCLICK";
              let termino5 = "JAVASCRIPT:ALERT";
              // para buscar la palabra hacemos
              let posicion1 = cadena1.toLowerCase().indexOf(termino1.toLowerCase());
              let posicion2 = cadena2.toLowerCase().indexOf(termino2.toLowerCase());
              let posicion3 = cadena3.toLowerCase().indexOf(termino3.toLowerCase());
              let posicion4 = cadena4.toLowerCase().indexOf(termino4.toLowerCase());
              let posicion5 = cadena5.toLowerCase().indexOf(termino5.toLowerCase());
              ///////////
              let vererror = 0;
              if (posicion1 !== -1 || posicion2 !== -1 || posicion3 !== -1 || posicion4 !== -1 || posicion5 !== -1){
                let vererror = 1;
                      this.loadingCtrl.dismiss().then( () => {
                            const alert = this.alertCtrl.create({
                                subHeader: "Aviso",
                                message: "Datos incorrectos",
                                buttons: [
                                  {
                                      text: "Reintentar",
                                      role: 'cancel',
                                      cssClass:'ion-aceptar',
                                      handler: data => {

                                      }
                                  }
                                ]
                            }).then(alert => alert.present());
                    });//FIN LOADING DISS
              }else{
							    				this.provider_usuario.api_pagadores_customers_id(this.customerid, this.myForm.value, this.national_id, this.cellphone).subscribe((responseapipagadores) => {  

																				  	this.provider_usuario.perfileditarupdate(this.usuarioid, this.myForm.value).subscribe((response) => {  
																				                this.loadingCtrl.dismiss().then( () => { 
																				                        if(response['code']==200){
																				                        			localStorage.setItem('NOMBRESAPELLIDOS', response['datos_usuarios'][0]['nombre_apellido']);
																				                        			const alert = this.alertCtrl.create({
																									                    subHeader: "Aviso",
																									                    message: "Los datos fueron actualizados",
																									                    buttons: [
																									                      {
																									                          text: "Aceptar",
																									                          role: 'cancel',
																									                          cssClass:'ion-aceptar',
																									                          handler: data => {
																									                            	this.navController.navigateRoot('principal/perfil');
																									                          }
																									                      }
																									                    ]
																									                }).then(alert => alert.present());
																				                        }else if (response['code']==201){
																				                                    const alert = this.alertCtrl.create({
																				                                    	subHeader: "Aviso",
																				                                        message: response['msg'],
																				                                        buttons: [
																				                                          {
																				                                            text: "Ok", 
																				                                            role: 'cancel'
																				                                          }
																				                                        ]
																				                                    }).then(alert => alert.present());
																				                        }//Fin else
																				                });//FIN LOADING DISS
																				    },error => {
																				    			this.loadingCtrl.dismiss().then( () => {
																						                const alert = this.alertCtrl.create({
																						                    subHeader: "Aviso",
																						                    message: "No pudo conectarse al servidor",
																						                    buttons: [
																						                      {
																						                          text: "Reintentar",
																						                          role: 'cancel',
																						                          cssClass:'ion-aceptar',
																						                          handler: data => {
																						                          		this.provider_menu.logerroresadd("Perfil editar", JSON.stringify(error)).subscribe((response) => { });//FIN LOADING DISS
																						                            	this.ionViewDidEnter();
																						                          }
																						                      }
																						                    ]
																						                }).then(alert => alert.present());
																								});//FIN LOADING DISS
																                    });//FIN POST
								                },error => {

								              
						                                                            this.loadingCtrl.dismiss().then( () => {
						                                                                      const alert = this.alertCtrl.create({
						                                                                          subHeader: "Aviso",
						                                                                          message: "No pudo conectarse al servidor",
						                                                                          buttons: [
						                                                                            {
						                                                                                text: "Reintentar",
						                                                                                role: 'cancel',
						                                                                                cssClass:'ion-aceptar',
						                                                                                handler: data => {
						                                                                                	this.provider_menu.logerroresadd("Perfil editar", JSON.stringify(error)).subscribe((response) => { });//FIN LOADING DISS
						                                                                                    this.ionViewDidEnter();
						                                                                                }
						                                                                            }
						                                                                          ]
						                                                                      }).then(alert => alert.present());
						                                                          });//FIN LOADING DISS
								                });//FIN POST api_pagadores_customers_id
						}//fin else
				});//FIn LOADING
  }//FIN FUNCTION

  regresar(){
        this.navController.back();
    }

}//FIN CLASS
