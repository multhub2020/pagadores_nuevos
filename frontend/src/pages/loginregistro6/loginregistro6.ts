import { Component, ViewChild, ElementRef, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl} from '@angular/forms';

import { Router } from '@angular/router';
import { NavController, LoadingController, AlertController } from '@ionic/angular';
import { Menus } from '../../providers/menus';
import { Usuario } from '../../providers/usuario';

@Component({
  selector: 'app-loginregistro6',
  templateUrl: 'loginregistro6.html',
  styleUrls: ['loginregistro6.scss'],
  providers:[Usuario, Menus]
})

export class Loginregistro6 {
	//@ViewChild('input1') myInput1: ElementRef;
	//@ViewChild('input2') myInput2: ElementRef;
	//@ViewChild('input3') myInput3: ElementRef;
	//@ViewChild('input4') myInput4: ElementRef;

  @ViewChild('campo1', {static: false})  inputElement1;
  @ViewChild('campo2', {static: false})  inputElement2;
  @ViewChild('campo3', {static: false})  inputElement3;
  @ViewChild('campo4', {static: false})  inputElement4;

  public myForm: FormGroup;
  public loading: any;
  public usuariore: any =['usuario'];
  public inicio=0;	
  public timeLeft =30;
  public timeLeft2='30';
  public interval=0;
  public usuarioemail;
  public usuarioclave;
  public usuarioregistroid;
  public usuariocedula;
  public usuariocelular;
  public usuariocelular2;
  public campo1="";
  public campo2="";
  public campo3="";
  public campo4="";
  public usuarionombres="";
  public usuarioapellidos="";
  constructor(public navController: NavController,
  			  public formBuilder: FormBuilder,
  			  private router: Router, 
  			  private provider_usuario: Usuario, 
  			  public alertCtrl: AlertController,
  			  public loadingCtrl: LoadingController,
  			  private provider_menu: Menus
  			  ) {
	  	this.myForm = this.formBuilder.group({
	  		campo1: new FormControl('', Validators.compose([ 
															Validators.required
														  ])
								   ),
	  		campo2: new FormControl('', Validators.compose([ 
															Validators.required
														  ])
								   ),
	  		campo3: new FormControl('', Validators.compose([ 
															Validators.required
														  ])
								   ),
	  		campo4: new FormControl('', Validators.compose([ 
															Validators.required
														  ])
								   )
	    });
  }
  pasar1(){
  	//this.myInput2.setFocus();
  }
  pasar2(){
  	//this.myInput3.setFocus();
  }
  pasar3(){
  	//this.myInput4.setFocus();
  }
  ionViewDidEnter() {
  	this.usuarioemail      = localStorage.getItem('USUARIOEMAIL');
	this.usuarioclave      = localStorage.getItem('USUARIOCLAVE');
    this.usuarioregistroid = localStorage.getItem('USUARIOREGISTROID');
    //this.usuariocedula     = localStorage.getItem('USUARIOCEDULA');
    this.usuariocelular    = localStorage.getItem('USUARIOCELULAR')
    //this.usuariocelular2   = this.usuariocelular.substr(0, 6);
    this.usuariocelular2   = this.usuariocelular;
    this.usuariocedula     = '';
    this.usuarionombres    = localStorage.getItem('USUARIONOMBRES');
    this.usuarioapellidos  = localStorage.getItem('USUARIOAPELLIDOS');

    const loader = this.loadingCtrl.create({
  		message: "Un momento por favor..."
    }).then(load => {
	    				load.present();
	    				const tiporegistro = localStorage.getItem('P_PUSH');
					  	this.provider_usuario.loginregistro(this.usuariocedula,
					  										this.usuariocelular,
					  										this.usuarioemail,
					  										this.usuarioclave,
					  										this.usuarioregistroid,
					  										this.usuarionombres,
					  										this.usuarioapellidos,
					  										tiporegistro																							  										
					  										).subscribe((response) => {  
					                this.loadingCtrl.dismiss().then( () => { 
					                        if(response['code']==200){
					                        	        //this.startTimer();
					                        	        //this.navController.navigateForward('loginregistro4');
					                        }else if (response['code']==201){
					                                    const alert = this.alertCtrl.create({
					                                    	subHeader: "Aviso",
					                                        message: response['msg'],
					                                        buttons: [
					                                          {
					                                            text: "Ok", 
					                                            role: 'cancel'
					                                          }
					                                        ]
					                                    }).then(alert => alert.present());
					                        }//Fin else
					                });//FIN LOADING DISS
					    },error => {
					    			this.loadingCtrl.dismiss().then( () => {
							             /*   const alert = this.alertCtrl.create({
							                    subHeader: "Aviso",
							                    message: "Disculpe, error con tu número celular",
							                    buttons: [
							                      {
							                          text: "Reintentar",
							                          role: 'cancel',
							                          cssClass:'ion-aceptar',
							                          handler: data => {
							                            	this.ionViewDidEnter();
							                          }
							                      }
							                    ]
							                }).then(alert => alert.present());*/
									});//FIN LOADING DISS
				    	});//FIN POST
				});//FIn LOADING 
  }
  cambiar1(){
  	this.inputElement2.setFocus();
  }
  cambiar2(){
  	this.inputElement3.setFocus();
  }
  cambiar3(){
  	this.inputElement4.setFocus();
  }
  cambiar4(){
  	//this.inputElement2.setFocus();
  }
  getText(e){
     var elementValue = e.srcElement.value;
     if(elementValue){
       var regex = /^[0-9]+$/;   
        var tempValue = elementValue.substring(0, elementValue.length - 1);
        if (!regex.test(elementValue)) {
          console.log("Entered char is not alphabet");
          e.srcElement.value = tempValue;
        }else{
        	
        }
     }
  }
  salir(){
      this.navController.navigateRoot('login');
  }
  enviarformulario(){
  	const loader = this.loadingCtrl.create({
  		message: "Un momento por favor..."
    }).then(load => {
	    				load.present();
	    				this.myForm.value.codigo = this.myForm.value.campo1+this.myForm.value.campo2+this.myForm.value.campo3+this.myForm.value.campo4;
					  	this.provider_usuario.loginregistrocodigo(this.usuarioregistroid, this.myForm.value).subscribe((response) => {  
					                this.loadingCtrl.dismiss().then( () => { 
					                        if(response['code']==200){
					                        	        this. pauseTimer();
					                              		this.navController.navigateForward('loginregistro4');
					                        }else if (response['code']==201){
					                                    const alert = this.alertCtrl.create({
					                                    	subHeader: "Aviso",
					                                        message: response['msg'],
					                                        buttons: [
					                                          {
					                                            text: "Ok", 
					                                            role: 'cancel'
					                                          }
					                                        ]
					                                    }).then(alert => alert.present());
					                        
					                        }//Fin else
					                });//FIN LOADING DISS
					    },error => {
					    			this.loadingCtrl.dismiss().then( () => {
							                const alert = this.alertCtrl.create({
							                    subHeader: "Aviso",
							                    message: "Disculpe, error con tu número celular",
							                    buttons: [
							                      {
							                          text: "Reintentar",
							                          role: 'cancel',
							                          cssClass:'ion-aceptar',
							                          handler: data => {
							                            	this.ionViewDidEnter();
							                          }
							                      }
							                    ]
							                }).then(alert => alert.present());
									});//FIN LOADING DISS
				    	});//FIN POST
				});//FIn LOADING
  }//FIN FUNCTION

  reenviar(){
  	const loader = this.loadingCtrl.create({
  		message: "Un momento por favor..."
    }).then(load => {
						load.present();
						  	this.provider_usuario.loginregistrocodigore(this.usuarioregistroid, this.usuariocelular).subscribe((response) => {  
						                this.loadingCtrl.dismiss().then( () => { 
						                              if(response['code']==200){
						                              		this.pauseTimer();
						                              		this.startTimer();
						                        }else if (response['code']==201){
						                                    const alert = this.alertCtrl.create({
						                                    	subHeader: "Aviso",
						                                        message: response['msg'],
						                                        buttons: [
						                                          {
						                                            text: "Ok", 
						                                            role: 'cancel'
						                                          }
						                                        ]
						                                    }).then(alert => alert.present());
						                        }//Fin else
						                });//FIN LOADING DISS
						    },error => {
						    			this.loadingCtrl.dismiss().then( () => {
								                const alert = this.alertCtrl.create({
								                    subHeader: "Aviso",
								                    message: "No pudo conectarse al servidor",
								                    buttons: [
								                      {
								                          text: "Reintentar",
								                          role: 'cancel',
								                          cssClass:'ion-aceptar',
								                          handler: data => {
								                          		this.provider_menu.logerroresadd("login registro 6", JSON.stringify(error)).subscribe((response) => { });//FIN LOADING DISS
								                            	this.ionViewDidEnter();
								                          }
								                      }
								                    ]
								                }).then(alert => alert.present());
										});//FIN LOADING DISS
					    	});//FIN POST
						});//FIn LOADING
  }//FIN FUNCTION
  startTimer() {
    this.interval = setInterval(() => {
      if(this.timeLeft > 0) {
        this.timeLeft--;
        this.timeLeft2=this.timeLeft+'';
        if(this.timeLeft<10){this.timeLeft2='0'+this.timeLeft;}
      } else {
        this.timeLeft  = 30;
        this.timeLeft2 = '30';
        clearInterval(this.interval);
        const alert = this.alertCtrl.create({
            subHeader: "Aviso",
            message: "Disculpe, el código a expirado",
            buttons: [
              {
                  text: "Aceptar",
                  role: 'cancel',
                  cssClass:'ion-aceptar',
                  handler: data => {
                    	//this.navController.navigateForward('login');
                  }
              }
            ]
        }).then(alert => alert.present());
      }
    },1000)
  }

  pauseTimer() {
    clearInterval(this.interval);
    this.timeLeft = 30;
    this.timeLeft2 = '30';
  }

}//FIN CLASS
