import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Loginregistro4 } from './loginregistro4';
import {NgxMaskIonicModule} from 'ngx-mask-ionic';


@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgxMaskIonicModule,
    RouterModule.forChild([{ path: '', component: Loginregistro4 }])
  ],
  declarations: [Loginregistro4]
})
export class Loginregistro4Module {}
