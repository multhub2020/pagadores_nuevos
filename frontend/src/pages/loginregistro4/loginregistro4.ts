import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl} from '@angular/forms';

import { Router } from '@angular/router';
import { NavController, LoadingController, AlertController } from '@ionic/angular';

import { Usuario } from '../../providers/usuario';

@Component({
  selector: 'app-loginregistro4',
  templateUrl: 'loginregistro4.html',
  styleUrls: ['loginregistro4.scss'],
  providers:[Usuario]
})

export class Loginregistro4 {
  public myForm: FormGroup;
  public loading: any;
  public email: string;

  public t_push: string;
  public p_push: string;
  public u_push: string;

  public usuarioid = "";

  public registroemail: string;
  public registrotelefono: string;
  public registronomresapellidos = "";
  public veri = "";

  public politicas = false;
  constructor(public navController: NavController,
  			  public formBuilder: FormBuilder,
  			  private router: Router, 
  			  private provider_usuario: Usuario, 
  			  public alertCtrl: AlertController,
  			  public loadingCtrl: LoadingController
  			  ) {
	  	this.myForm = this.formBuilder.group({
	        cedula: new FormControl('', Validators.compose([ 
																Validators.required,
                                Validators.minLength(11)
															  ])
									   ),
          /*valida: new FormControl('', Validators.compose([ 
                              Validators.required,
                              ])
                   )*/
	    });
  }
  salir(){
      this.navController.navigateRoot('login');
  }
  ionViewDidEnter() {
  }
  enviarformulario(){
  	 localStorage.setItem('USUARIOCEDULA', this.myForm.value.cedula);
     this.navController.navigateForward('loginregistro7');
  }//FIN FUNCTION
  informacion(var1){
	  		if(var1==1){
	  		this.navController.navigateForward('inforcondiciones');
	  	}else if(var1==2){
	  		this.navController.navigateForward('inforpoliticas');
	    }else if(var1==3){
	  		this.navController.navigateForward('loginrecuperar1');
	  	}else if(var1==4){
	  		this.navController.navigateForward('loginclave');
	  	}
  }
}//FIN CLASS
