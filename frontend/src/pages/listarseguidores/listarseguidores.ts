import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl} from '@angular/forms';

import { Router } from '@angular/router';
import { Platform, NavController, LoadingController, AlertController } from '@ionic/angular';

import { Menus } from '../../providers/menus';
import { MenuController } from '@ionic/angular';

import { ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-listarseguidores',
  templateUrl: 'listarseguidores.html',
  styleUrls: ['listarseguidores.scss'],
  providers:[Menus]
})

export class Listarseguidores {
  public myForm: FormGroup;
  public loading: any;
  public usuarioid: string;
  public puntaje = "5";
  public selected = "1";
  public selected2 = "1";
  public nombres: string;
  public apellidos: string;
  public avatar:string="";
  public invitados = "";
  public seguidores: any;
  public seguidores1: any;
  public seguidores2: any;
  public ordena      = "";
  public color     = "";
  public usuario = "";
  public searchTerm: any;
  public allItems: any;
  public items: any;
  constructor(public navController: NavController,
  			  public formBuilder: FormBuilder,
  			  private router: Router, 
  			  private provider: Menus, 
          private platform: Platform,
          private menu: MenuController,
  			  public alertCtrl: AlertController,
  			  public loadingCtrl: LoadingController,
          private rutaActiva: ActivatedRoute,
          private provider_menu: Menus
  			  ) {
      this.platform.backButton.subscribe(() => {
        // code that is executed when the user pressed the back button
        this.navController.navigateRoot('menu');
      });
      this.usuario   = localStorage.getItem('NOMBRESAPELLIDOS');
  }
  sub(id){
    this.navController.navigateForward('listarseguidoressub/'+id);
  }
  salir(){
      this.navController.navigateRoot('menu');
  }
  ordenar(var1){
      if(var1=='1'){
        this.allItems   = this.seguidores1;
        this.seguidores = this.seguidores1;
        this.ordena     = '2';
      }else{
        this.allItems   = this.seguidores2;
        this.seguidores = this.seguidores2;
        this.ordena     = '1';
      }
      this.searchTerm = "";
  }
  filtrar(){
       this.menu.open();
  }
  filtro(){
      //this.puntaje
      //this.selected
      this.menu.close();
      const loader = this.loadingCtrl.create({
              message: "Un momento por favor..."
            }).then(load => {
                    load.present();
                    this.provider.listarseguidoresfiltro(this.usuarioid, this.puntaje, this.selected, this.selected2).subscribe((response) => {  
                                this.loadingCtrl.dismiss().then( () => { 
                                        if(response['code']==200){
                                            this.seguidores1 = response['datos1'];
                                            this.allItems    = this.seguidores1;
                                            this.seguidores  = this.seguidores1;
                                            this.ordena      = '1';
                                        }else if (response['code']==201){
                                                    const alert = this.alertCtrl.create({
                                                      subHeader: "Aviso",
                                                        message: response['msg'],
                                                        buttons: [
                                                          {
                                                            text: "Ok", 
                                                            role: 'cancel'
                                                          }
                                                        ]
                                                    }).then(alert => alert.present());
                                        }//Fin else
                                });//FIN LOADING DISS
                    },error => {
                          this.loadingCtrl.dismiss().then( () => {
                                    const alert = this.alertCtrl.create({
                                        subHeader: "Aviso",
                                        message: "No pudo conectarse al servidor",
                                        buttons: [
                                          {
                                              text: "Reintentar",
                                              role: 'cancel',
                                              cssClass:'ion-aceptar',
                                              handler: data => {
                                                  this.provider_menu.logerroresadd("listar seguidores ", JSON.stringify(error)).subscribe((response) => { });//FIN LOADING DISS
                                                  this.ionViewDidEnter();
                                              }
                                          }
                                        ]
                                    }).then(alert => alert.present());
                        });//FIN LOADING DISS
                    });//FIN POST
      });//FIn LOADING

  }
  linkmenu(var1){
      if(var1=="notificacion"){

      }
  }
  onSearchTerm(ev: CustomEvent) {
        this.seguidores = this.allItems;
          this.seguidores = this.seguidores.filter((term) => {
              return term.nombre_apellido.toLowerCase().indexOf(this.searchTerm.toLowerCase()) > -1;
          });
  }
  ionViewDidEnter() {
  			this.usuarioid = localStorage.getItem('IDUSER');
            const loader = this.loadingCtrl.create({
            	message: "Un momento por favor..."
            }).then(load => {
                    load.present();
                    this.provider.listarseguidores(this.usuarioid).subscribe((response) => {  
                                this.loadingCtrl.dismiss().then( () => { 
                                        if(response['code']==200){
                                            this.seguidores1 = response['datos1'];
                                            this.allItems    = this.seguidores1;
                                            this.seguidores  = this.seguidores1;
                                            this.ordena      = '1';
                                        }else if (response['code']==201){
                                                    const alert = this.alertCtrl.create({
                                                      subHeader: "Aviso",
                                                        message: response['msg'],
                                                        buttons: [
                                                          {
                                                            text: "Ok", 
                                                            role: 'cancel'
                                                          }
                                                        ]
                                                    }).then(alert => alert.present());
                                        }//Fin else
                                });//FIN LOADING DISS
                    },error => {
                          this.loadingCtrl.dismiss().then( () => {
                                    const alert = this.alertCtrl.create({
                                        subHeader: "Aviso",
                                        message: "No pudo conectarse al servidor",
                                        buttons: [
                                          {
                                              text: "Reintentar",
                                              role: 'cancel',
                                              cssClass:'ion-aceptar',
                                              handler: data => {
                                                  this.provider_menu.logerroresadd("listar seguidores ", JSON.stringify(error)).subscribe((response) => { });//FIN LOADING DISS
                                                  this.ionViewDidEnter();
                                              }
                                          }
                                        ]
                                    }).then(alert => alert.present());
                        });//FIN LOADING DISS
                    });//FIN POST
            });//FIn LOADING
         
  }
}//FIN CLASS
