import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl} from '@angular/forms';

import { Router } from '@angular/router';
import { NavController, LoadingController, AlertController } from '@ionic/angular';

import { Perfils } from '../../providers/perfils';
import { Menus } from '../../providers/menus';

@Component({
  selector: 'app-perfilsobrenosotros',
  templateUrl: 'perfilsobrenosotros.html',
  styleUrls: ['perfilsobrenosotros.scss'],
  providers:[Perfils, Menus]
})

export class Perfilsobrenosotros {
  public myForm: FormGroup;
  public loading: any;
  public email: string;

  public t_push: string;
  public p_push: string;
  public u_push: string;

  public usuarioregistro: string;
  public usuariocedula: string;
  public usuarionombres: string;

  public registroemail: string;
  public registrotelefono: string;
  public registrotelefono2: string;
  public claves: string;

  public usuarioid: string;
  public usuario = "";
  public clave = "";
  public textcheckbox = false;


  public nombres: string;
  public apellidos: string;
  public user: string;


  public texto: string;

  constructor(public navController: NavController,
  			  public formBuilder: FormBuilder,
  			  private router: Router, 
  			  private provider: Perfils, 
  			  public alertCtrl: AlertController,
  			  public loadingCtrl: LoadingController,
          private provider_menu: Menus,
  			  ) {
  		this.usuario   = localStorage.getItem('NOMBRESAPELLIDOS');
      	this.usuarioid = localStorage.getItem('IDUSER');
	  	
  }
  salir(){
      this.navController.navigateRoot('login');
  }
  ionViewDidEnter() {
				  	const loader = this.loadingCtrl.create({
				  		message: "Un momento por favor..."
				    }).then(load => {
					    				load.present();
									  	this.provider.sobrenosotros().subscribe((response) => {  
									                this.loadingCtrl.dismiss().then( () => { 
									                        if(response['code']==200){
									                        		this.texto   = response['msg'];
									                        }else if (response['code']==201){
									                                    const alert = this.alertCtrl.create({
									                                    	subHeader: "Aviso",
									                                        message: response['msg'],
									                                        buttons: [
									                                          {
									                                            text: "Ok", 
									                                            role: 'cancel'
									                                          }
									                                        ]
									                                    }).then(alert => alert.present());
									                        }//Fin else
									                });//FIN LOADING DISS
									    },error => {
									    			this.loadingCtrl.dismiss().then( () => {
											                const alert = this.alertCtrl.create({
											                    subHeader: "Aviso",
											                    message: "No pudo conectarse al servidor",
											                    buttons: [
											                      {
											                          text: "Reintentar",
											                          role: 'cancel',
											                          cssClass:'ion-aceptar',
											                          handler: data => {
                                                    this.provider_menu.logerroresadd("Perfil sobre nosotros", JSON.stringify(error)).subscribe((response) => { });//FIN LOADING DISS
											                            	this.ionViewDidEnter();
											                          }
											                      }
											                    ]
											                }).then(alert => alert.present());
													});//FIN LOADING DISS
								    	});//FIN POST
								});//FIn LOADING	
  }
  regresar(){
        this.navController.back();
    }

}//FIN CLASS
