import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Perfilsobrenosotros } from './perfilsobrenosotros';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild([{ path: '', component: Perfilsobrenosotros }])
  ],
  declarations: [Perfilsobrenosotros]
})
export class PerfilsobrenosotrosModule {}
