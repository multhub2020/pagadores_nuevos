import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Localizardeuda1_3 } from './localizardeuda1_3';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild([{ path: '', component: Localizardeuda1_3 }])
  ],
  declarations: [Localizardeuda1_3]
})
export class Localizardeuda1_3Module {}
