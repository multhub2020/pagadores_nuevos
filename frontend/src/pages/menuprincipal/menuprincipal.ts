import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NavController, LoadingController, AlertController } from '@ionic/angular';

import { Menus } from '../../providers/menus';
import { Variablesglobales } from '../../providers/variablesglobal';


@Component({
  selector: 'app-menuprincipal',
  templateUrl: 'menuprincipal.html',
  styleUrls: ['menuprincipal.scss'],
  providers:[Menus, Variablesglobales]
})
export class Menuprincipal {
    
    public datos_menus: any;
    public datos_publi: any;
    public imgurl   = new Variablesglobales();
    public imgurl2: any;
    public selectedIndex = 0;
    public usuario: string;
 
    public slideOpts = {
          effect: 'flip',
          autoplay: {
            delay: 5000
          }
    };

    constructor(private router: Router, 
                private navController: NavController, 
                private provider_menu: Menus, 
                public alertCtrl: AlertController,
                public loadingCtrl: LoadingController
                ){
        this.usuario = localStorage.getItem('NOMBRESAPELLIDOS');
    }//FIN FUncTION    

    ionViewDidEnter() {
              
    }//FIN FcuntiN

    slidesDidLoad(slides) {
      slides.startAutoplay();
    }

}//FIN CLASS