import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NavController, LoadingController, AlertController, Platform } from '@ionic/angular';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Menus } from '../../providers/menus';
import { SuperTabs } from "@ionic-super-tabs/angular";
import { Variablesglobales } from '../../providers/variablesglobal';
import { ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-vercuenta',
  templateUrl: 'vercuenta.html',
  styleUrls: ['vercuenta.scss'],
  providers: [Menus, Variablesglobales]
})

export class Vercuenta {
  //myPage = Mapa;
  @ViewChild(SuperTabs, { static: false }) superTabs: SuperTabs;
  public versionapp = "1.0.5";
  public datosdeudas: any = [''];
  public data: any;
  public dataTransacciones: any;
  public datos_publi: any;
  public selectedIndex = 0;
  public usuario: string;
  public usuarioid: string;
  public customerid: string;
  public bodys: string;
  public myForm: FormGroup;
  public imgurl = new Variablesglobales();
  public imgurl2: any;
  public listaContactos: any = [];
  public listaContactos2: any = [];
  //Variables para calcularr monto
  public checkbox_re: any = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
  public checkbox_mon: any = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
  public checkbox_cuo: any = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1];
  public checkbox_pro: any = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1];
  ///////////////////////////////
  public datosMostar: any = [];
  public datosMostar2: any = [];
  public checkbox: any;
  public avatar: string = "";
  public invitados = 0;
  public contador_c = 0;
  public seguidores = 0;
  public color = "";
  public i = 0;
  public value = 0;
  public finalizar = 1;
  public slideOpts = {
    effect: 'flip',
    autoplay: {
      delay: 5000
    }
  };
  public invitado = "";
  public appPages = [
    {
      title: 'Sugerencias',
      url: 'perfilsugerencias',
      icon: 'mail'
    }
  ];
  public cargandoBtn = false;
  public pending_loader = null;
  public inicio = 1;
  constructor(private router: Router,
    public formBuilder: FormBuilder,
    private navController: NavController,
    private provider_menu: Menus,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    private platform: Platform,
    private rutaActiva: ActivatedRoute
    //public supertabs: SuperTabs
  ) {
    this.usuario = localStorage.getItem('NOMBRESAPELLIDOS');
    this.usuarioid = localStorage.getItem('IDUSER');
    this.customerid = localStorage.getItem('CUSTOMERID');
    this.myForm = this.formBuilder.group({
      usuario: new FormControl(0, Validators.compose([
        Validators.required,
        Validators.pattern('^[0-9 ]+$'),
      ])
      )

    });
  }//FIN FUncTION
  inf(v) {
    this.finalizar = v;
  }
  seleccionradio(a, b) {
    //alert(a+' - '+b);
    this.checkbox_re[a] = b;
  }
  resolver(a, b) {
    if (this.finalizar == 1) {
      this.checkbox_pro[a] = b;
    }
  }
  quitarresena() {
    this.inicio = 1;
  }
  pagalink(id1, id2){
    const alert = this.alertCtrl.create({
                subHeader: "Tipo de Pago",
                message:"Seleccione una forma de pago",
                buttons: [
                  {
                      text: "Pagar con TC",
                      cssClass:'ion-cancelar-small',
                      handler: data => {
                          this.pagarcuenta(id1, id2);
                      }
                  },
                  {
                      text: "Pagar con Deposito",
                      cssClass:'ion-aceptar-small',
                      handler: data => {
                          this.pagarcuentadeposito(id2);
                      }
                  }
                ]
         }).then(alert => alert.present());
  }
  ionViewDidEnter() {
    this.checkbox = this.rutaActiva.snapshot.paramMap.get('id');
    const loader = this.loadingCtrl.create({
      message: "Un momento por favor..."
    }).then(load2 => {
      load2.present();
      this.provider_menu.get_customer_accounts(this.checkbox).subscribe((response) => {
        console.log(response);
        load2.dismiss().then(() => {
          if (!response['error']) {
            this.imgurl2 = this.imgurl.getServar();
            console.log(JSON.stringify(response['data']));
            this.data = [response['data']];

            this.provider_menu.get_creditors(response['data']['creditor_id']).subscribe((responseCreditor) => {
              //console.log(responseCreditor);
              this.data[0]['identifica_banco'] = responseCreditor['data']['identification_number'];
              this.data[0]['banco_deuda'] = responseCreditor['data']['name'];
            });//FIN POST

            console.log(this.data);

            this.provider_menu.get_payments_contract(this.checkbox).subscribe((responseTransacciones) => {
              console.log('a: ');
              console.log(responseTransacciones);
              if (!responseTransacciones['error']) {
                this.dataTransacciones = responseTransacciones['results'];
              } else {
                const alert = this.alertCtrl.create({
                  subHeader: "Aviso",
                  message: responseTransacciones['message'],
                  buttons: [
                    {
                      text: "Ok",
                      role: 'cancel'
                    }
                  ]
                }).then(alert => alert.present());
              }
            });//FIN POST*/

          } else {
            const alert = this.alertCtrl.create({
              subHeader: "Aviso",
              message: response['message'],
              buttons: [
                {
                  text: "Ok",
                  role: 'cancel'
                }
              ]
            }).then(alert => alert.present());
          }//Fin else
        });//FIN LOADING DISS
      }, error => {
        load2.dismiss().then(() => {
          const alert = this.alertCtrl.create({
            subHeader: "Aviso",
            message: "No pudo conectarse al servidor",
            buttons: [
              {
                text: "Reintentar",
                role: 'cancel',
                cssClass: 'ion-aceptar',
                handler: data => {
                  this.provider_menu.logerroresadd("Ver cuenta", JSON.stringify(error)).subscribe((response) => { });//FIN LOADING DISS
                  this.ionViewDidEnter();
                }
              }
            ]
          }).then(alert => alert.present());
        });//FIN LOADING DISS
      });//FIN POST
    });//FIn LOADING

  }//FIN FcuntiN
  regresar() {
    this.navController.back();
  }
  pagarcuenta(contract_id, application_id) {
    this.navController.navigateForward('pagarcuenta/' + contract_id + '/' + application_id);
  }
  pagarcuentadeposito(a) {
    this.navController.navigateForward('pagarcuentadeposito/' + a);
  }
}//FIN CLASS