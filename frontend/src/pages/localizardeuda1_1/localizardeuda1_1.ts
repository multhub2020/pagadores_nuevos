import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NavController, LoadingController, AlertController, Platform } from '@ionic/angular';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Menus } from '../../providers/menus';
import { Usuario } from '../../providers/usuario';
import { SuperTabs } from "@ionic-super-tabs/angular";
import { Variablesglobales } from '../../providers/variablesglobal';


@Component({
  selector: 'app-localizardeuda1_1',
  templateUrl: 'localizardeuda1_1.html',
  styleUrls: ['localizardeuda1_1.scss'],
  providers: [Menus, Usuario, Variablesglobales]
})

export class Localizardeuda1_1 {
  //myPage = Mapa;
  @ViewChild(SuperTabs, { static: false }) superTabs: SuperTabs;
  public versionapp = "1.0.5";
  public datosdeudas: any = [''];
  public datos_publi: any;
  public selectedIndex = 0;
  public usuario: string;
  public usuarioid: string;
  public bodys: string;
  public myForm: FormGroup;
  public imgurl = new Variablesglobales();
  public imgurl2: any;
  public listaContactos: any = [];
  public listaContactos2: any = [];
  //Variables para calcularr monto
  public checkbox_re: any = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
  public checkbox_mon: any = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
  public checkbox_cuo: any = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
  public checkbox_pro: any = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1];
  public porcentaje = 0;
  ///////////////////////////////
  public datosMostar: any = [];
  public datosMostar2: any = [];
  public checkbox: any;
  public avatar: string = "";
  public invitados = 0;
  public contador_c = 0;
  public seguidores = 0;
  public color = "";
  public i = 0;
  public value = 0;
  public finalizar = 1;
  public slideOpts = {
    effect: 'flip',
    autoplay: {
      delay: 5000
    }
  };
  public invitado = "";
  public appPages = [
    {
      title: 'Sugerencias',
      url: 'perfilsugerencias',
      icon: 'mail'
    }
  ];
  public cargandoBtn = false;
  public pending_loader = null;
  public inicio = 1;

  public mensaje1 = "";
  public mensaje2 = "";

  public customerid = "";
  public montodeuda = 0;
  public accountnumber = "";
  public producttypeid = 0;
  public productresolutionid = 0;
  public creditorid = 0;
  public currency = 0;
  public entidadbancaria = "";

  public manual_debt_add = false;


  constructor(private router: Router,
    public formBuilder: FormBuilder,
    private navController: NavController,
    private provider_menu: Menus,
    private provider_usuario: Usuario,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    private platform: Platform,
    //public supertabs: SuperTabs
  ) {
    this.usuario = localStorage.getItem('NOMBRESAPELLIDOS');
    this.usuarioid = localStorage.getItem('IDUSER');
    this.customerid = localStorage.getItem('CUSTOMERID');
    this.myForm = this.formBuilder.group({
      usuario: new FormControl(0, Validators.compose([
        Validators.required,
        Validators.pattern('^[0-9 ]+$'),
      ])
      )

    });
  }//FIN FUncTION
  inf(v) {
    this.finalizar = v;
  }
  seleccionradio(a, b) {
    //alert(a+' - '+b);
    this.checkbox_re[a] = b;
  }
  resolver(a, b) {
    if (this.finalizar == 1) {
      this.checkbox_pro[a] = b;
    }
  }
  quitarresena() {
    this.inicio = 1;
  }
  primeracarga() {
    //const browser = this.iab.create(url);
    //const browser = this.iab.create(url, '_system', this.options2);
    this.inicio = 2;
  }
  getText(e, i) {
    var elementValue = e.srcElement.value;
    if (elementValue) {
      var regex = /^[0-9., ]+$/;
      var tempValue = elementValue.substring(0, elementValue.length - 1);
      console.log(elementValue+' . '+this.datosdeudas[0]['monto_deuda2']);
      if(parseFloat(elementValue)>this.datosdeudas[0]['monto_deuda2']){
              const alert = this.alertCtrl.create({
                      subHeader: "Aviso",
                      message: "Disculpe, el monto es mayor a el monto deuda",
                      buttons: [
                        {
                          text: "Reintentar",
                          role: 'cancel',
                          cssClass: 'ion-aceptar',
                          handler: data => {

                            this.checkbox_cuo[i] = this.datosdeudas[0]['monto_deuda2'];

                          }
                        }
                      ]
                    }).then(alert => alert.present());
      }//fin
      if (!regex.test(elementValue)) {
        console.log("Entered char is not alphabet");
        e.srcElement.value = tempValue;
      }
    }
  }
  vaciar(i) {
    if (this.checkbox_cuo[i] == 0) { this.checkbox_cuo[i] = ""; }
  }
  vaciar2(i) {
    console.log('i '+i);
    if (this.checkbox_cuo[i] == '') { this.checkbox_cuo[i] = 0; }
  }
  ionViewDidEnter() {

    //this.slider.startAutoplay();
    if (localStorage.getItem("banderainiciodeudaadd") != '1') {
      localStorage.setItem('banderainiciodeudaadd', '1');
      //this.primeracarga();
    }
    this.checkbox = localStorage.getItem('checkboxdeuda');
    const loader = this.loadingCtrl.create({
      message: "Un momento por favor..."
    }).then(load2 => {
      load2.present();
      this.provider_menu.listarmenudeudas(this.usuarioid, this.checkbox).subscribe((response) => {
        load2.dismiss().then(() => {
          if (response['code'] == 200) {
            console.log(response);
            this.imgurl2 = this.imgurl.getServar();
            this.datosdeudas = response['datosdeudas'];
            this.datos_publi = response['datos_publi'];
            this.seguidores = response['seguidores'];
            this.invitados = response['invitados'];
            this.color = response['color'];
            this.avatar = response['avatar'];
            this.invitado = "";
            if (response['datosdeudas'][0]['tipo_deuda'] == 1) {
              this.mensaje1 = 'Tarjeta de Crédito ' + response['datosdeudas'][0]['identifica_banco'];
            } else {
              this.mensaje1 = 'Préstamo ' + response['datosdeudas'][0]['identifica_banco'];
            }
            if (response['datosdeudas'][0]['manual_debt_add'] == 2) {
              this.manual_debt_add = true;
            } else {
              this.manual_debt_add = false;
            }
            this.accountnumber = response['datosdeudas'][0]['account_number'];
            this.montodeuda = response['datosdeudas'][0]['monto_deuda2'];
            //this.producttypeid = response['datosdeudas'][0]['product_type_id'];
            this.producttypeid = 1;
            this.productresolutionid = response['datosdeudas'][0]['product_resolution_id'];
            this.creditorid = response['datosdeudas'][0]['creditor_id'];
            this.currency = response['datosdeudas'][0]['moneda'];

            this.entidadbancaria = response['datosdeudas'][0]['banco_deuda'];

            if (localStorage.getItem("banderainicio") != '1') {
              localStorage.setItem('banderainicio', '1');
              //this.primeracarga(response['parametrovideo']);
            }
            if (this.versionapp != response['versionapp']) {
              const alert = this.alertCtrl.create({
                subHeader: "Aviso",
                message: "Disculpe, Debe actualizar su versión de app para poder continuar",
                buttons: [
                  {
                    text: "Reintentar",
                    role: 'cancel',
                    cssClass: 'ion-aceptar',
                    handler: data => {
                      localStorage.removeItem('IDUSER');
                      localStorage.removeItem('USUARIO');
                      localStorage.removeItem('NOMBRESAPELLIDOS');
                      localStorage.removeItem('TOKEN');
                      localStorage.removeItem('SESSIONACTIVA');
                      localStorage.removeItem('consultaFechaBuro');
                      localStorage.setItem('SESSIONACTIVA', 'false');
                      this.navController.navigateRoot('login');
                    }
                  }
                ]
              }).then(alert => alert.present());
            }
          } else if (response['code'] == 201) {
            const alert = this.alertCtrl.create({
              subHeader: "Aviso",
              message: response['msg'],
              buttons: [
                {
                  text: "Ok",
                  role: 'cancel'
                }
              ]
            }).then(alert => alert.present());
          }//Fin else
        });//FIN LOADING DISS
      }, error => {
        load2.dismiss().then(() => {
          const alert = this.alertCtrl.create({
            subHeader: "Aviso",
            message: "No pudo conectarse al servidor",
            buttons: [
              {
                text: "Reintentar",
                role: 'cancel',
                cssClass: 'ion-aceptar',
                handler: data => {
                  this.provider_menu.logerroresadd("localizardeuda1_2", JSON.stringify(error)).subscribe((response) => { });//FIN LOADING DISS
                  this.ionViewDidEnter();
                }
              }
            ]
          }).then(alert => alert.present());
        });//FIN LOADING DISS
      });//FIN POST
    });//FIn LOADING

  }//FIN FcuntiN


  enviartodo(){
          const alert = this.alertCtrl.create({
              subHeader: "¿Estás seguro de enviar",
              message: "esta oferta? ",
              buttons: [
                {
                    text: "Quiero modificar mi oferta",
                    cssClass:'ion-pagar3',
                    handler: data => {
                        
                        //this.navController.back();
                    }
                },
                {
                    
                    text: "Si, estoy seguro",
                    role: 'cancel',
                    cssClass:'ion-pagar2',
                    handler: data => {
                        this.continnuar();
                    }
                }
                
              ]
          }).then(alert => alert.present());
  }//fin function


  continnuar() {
            var monto = 1;
            this.checkbox_cuo.forEach(function (value) {
              if (value != 0) {
                monto = value;
              }
            });
            if (monto != 0) {
              const loader = this.loadingCtrl.create({
                message: "Un momento por favor..."
              }).then(load2 => {
                      load2.present();
                      this.provider_menu.deudassolicitudenviar(this.usuarioid, this.datosdeudas[0]['id'], monto).subscribe((response) => {
                            load2.dismiss().then(() => {
                              if (response['code'] == 200) {
                                this.continuar2();
                              } else if (response['code'] == 201) {
                                const alert = this.alertCtrl.create({
                                  subHeader: "Aviso",
                                  message: response['msg'],
                                  buttons: [
                                    {
                                      text: "Ok",
                                      role: 'cancel'
                                    }
                                  ]
                                }).then(alert => alert.present());
                              }//Fin else
                            });//FIN LOADING DISS
                      }, error => {
                              load2.dismiss().then(() => {
                                const alert = this.alertCtrl.create({
                                  subHeader: "Aviso",
                                  message: "No pudo conectarse al servidor",
                                  buttons: [
                                    {
                                      text: "Reintentar",
                                      role: 'cancel',
                                      cssClass: 'ion-aceptar',
                                      handler: data => {
                                        this.provider_menu.logerroresadd("localizardeuda1", JSON.stringify(error)).subscribe((response) => { });//FIN LOADING DISS
                                        this.continnuar();
                                      }
                                    }
                                  ]
                                }).then(alert => alert.present());
                              });//FIN LOADING DISS
                      });//FIN POST
              });//FIn LOADING

            } else {
              const alert = this.alertCtrl.create({
                subHeader: "Aviso",
                message: "Disculpe, no se tiene opción seleccionada",
                buttons: [
                  {
                    text: "Continuar",
                    role: 'cancel',
                    cssClass: 'ion-aceptar',
                    handler: data => {
                    }
                  }
                ]
              }).then(alert => alert.present());
            }
  }
  continuar2() {
    var monto = 1;
    var credolabscores = localStorage.getItem('credolabscores');
    this.checkbox_cuo.forEach(function (value) {
      if (value != 0) {
        monto = value;
      }
    });
    if (monto != 0) {
      this.loadingCtrl.create({
        message: "Un momento por favor..."
      }).then(load2 => {
        load2.present();
        this.provider_usuario.api_pagadores_applications(
          this.customerid,
          this.accountnumber,
          0,
          monto,
          0,
          0,
          Math.trunc(this.montodeuda),
          this.producttypeid,
          this.productresolutionid,
          this.creditorid,
          this.manual_debt_add,
          this.currency,
          0,
          1,
          this.entidadbancaria,
          credolabscores,
          this.datosdeudas[0]['id_supervisor']
        ).subscribe((responsepagadores) => {
                        
                        console.log('responsepagadores: ' + JSON.stringify(responsepagadores));
                        localStorage.setItem('localizardeuda1_1_mensaje1', 'Número de aplicación ' + responsepagadores['data'].application_id);
                        localStorage.setItem('localizardeuda1_1_mensaje2', JSON.stringify(monto));
                        localStorage.setItem('localizardeuda1_1_mensaje3', JSON.stringify(this.currency));
                        localStorage.setItem('localizardeuda1_1_id1', this.datosdeudas[0]['id']);
                        this.provider_usuario.deudasaplication(this.datosdeudas[0]['id'], responsepagadores['data'].application_id, credolabscores).subscribe((response) => { });
                        //this.navController.navigateForward('principal/localizardeuda1_2');
                        this.checkbox = localStorage.getItem('checkboxdeuda');
                          this.provider_menu.deudaslista(this.usuarioid, this.checkbox).subscribe((response) => {
                            if (response['code'] == 200) {
                              if (response['contar'] != 0) {
                                      load2.dismiss().then(() => {
                                        this.navController.navigateForward('principal/menu');
                                      });
                              } else {
                                      ///AQUI COLOCAR LO DE LA CONSULTA
                                      let contarinicio   = '';
                                      let contarinicio2  = 0;
                                      contarinicio  = localStorage.getItem('contarinicio_1_1');
                                      contarinicio2 = parseInt(localStorage.getItem('contarinicio_1_1'));
                                      contarinicio2++;  contarinicio2++;
                                      localStorage.setItem('contarinicio_1_1', contarinicio2+'');
                                      load2.dismiss().then(() => {
                                        this.navController.navigateForward('principal/inicio/'+contarinicio2);
                                      });
                              }
                            }
                        
                });//FIN LOADING DISS
        }, error => {
                load2.dismiss().then(() => {
                        console.log('error al crear aplicacion');
                        const alert = this.alertCtrl.create({
                          subHeader: "Aviso",
                          message: "No pudo conectarse al servidor",
                          buttons: [
                            {
                              text: "Reintentar",
                              role: 'cancel',
                              cssClass: 'ion-aceptar',
                              handler: data => {
                                this.provider_menu.logerroresadd("localizardeuda1 1", JSON.stringify(error)).subscribe((response) => { });//FIN LOADING DISS
                                this.continuar2();
                              }
                            }
                          ]
                        }).then(alert => alert.present());
                });//FIN LOADING DISS
        });
      });//FIn LOADING
    }
  }
  cerrarmensajefinalizar() {
    this.finalizar = 4;
  }
  respuestapro(a) {
    return this.checkbox_pro[a];
  }
  regresar() {
    this.navController.back();
  }
  verificar() {
   
  }//fin function
}//FIN CLASS