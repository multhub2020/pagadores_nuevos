import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Localizardeuda1_1 } from './localizardeuda1_1';
import { SuperTabsModule } from '@ionic-super-tabs/angular';
import { NgxMaskIonicModule } from 'ngx-mask-ionic';
import { NgxCurrencyModule } from "ngx-currency";

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SuperTabsModule,
    NgxMaskIonicModule,
    NgxCurrencyModule,
    RouterModule.forChild([{ path: '', component: Localizardeuda1_1 }])
  ],
  declarations: [Localizardeuda1_1]
})
export class Localizardeuda1_1Module {}
