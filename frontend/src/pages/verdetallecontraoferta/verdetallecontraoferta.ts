import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NavController, LoadingController, AlertController, Platform } from '@ionic/angular';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Menus } from '../../providers/menus';
import { SuperTabs } from "@ionic-super-tabs/angular";
import { Variablesglobales } from '../../providers/variablesglobal';
import { ActivatedRoute, Params } from '@angular/router';
import { Inicio } from '../inicio/inicio';
import {formatDate } from '@angular/common';
import { EmailComposer } from '@ionic-native/email-composer/ngx';

@Component({
  selector: 'app-verdetallecontraoferta',
  templateUrl: 'verdetallecontraoferta.html',
  styleUrls: ['verdetallecontraoferta.scss'],
  providers: [Menus, Variablesglobales, Inicio]
})

export class Verdetallecontraoferta {
  //myPage = Mapa;
  @ViewChild(SuperTabs, { static: false }) superTabs: SuperTabs;
  public versionapp = "1.0.5";
  public datosdeudas: any = [''];
  public datos_publi: any;
  public selectedIndex = 0;
  public usuario: string;
  public usuarioid: string;
  public bodys: string;
  public myForm: FormGroup;
  public imgurl = new Variablesglobales();
  public imgurl2: any;
  public listaContactos: any = [];
  public listaContactos2: any = [];
  public datey = "";
  public datem = "";
  public dated = "";
  public meses_ES: any = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];
  public mes  = 0;
  public mes2 = 0;
  //Variables para calcularr monto

  public solicitudespendientes1;
  public solicitudespendientes2;
  public solicitudespendientes3;
  public solicitudespendientes4;
  public solicitudespendientes5;

  public checkbox_re: any = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
  public checkbox_mon: any = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
  public checkbox_cuo: any = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
  public checkbox_pro: any = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1];
  public porcentaje = 0;
  ///////////////////////////////
  public datosMostar: any = [];
  public datosMostar2: any = [];
  public checkbox: any;
  public avatar: string = "";
  public invitados = 0;
  public contador_c = 0;
  public seguidores = 0;
  public color = "";
  public i = 0;
  public deudaid = "";
  public value = 0;
  public finalizar = 1;
  public slideOpts = {
    effect: 'flip',
    autoplay: {
      delay: 5000
    }
  };
  public invitado = "";
  public appPages = [
    {
      title: 'Sugerencias',
      url: 'perfilsugerencias',
      icon: 'mail'
    }
  ];
  public cargandoBtn = false;
  public pending_loader = null;
  public inicio = 1;

  public mensaje1 = "";
  public mensaje2 = "";
  constructor(private router: Router,
    public formBuilder: FormBuilder,
    private navController: NavController,
    private provider_menu: Menus,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    private platform: Platform,
    private rutaActiva: ActivatedRoute,
    //public supertabs: SuperTabs,
    public inicioPage: Inicio,
    private emailComposer: EmailComposer
  ) {
    this.usuario = localStorage.getItem('NOMBRESAPELLIDOS');
    this.usuarioid = localStorage.getItem('IDUSER');
    this.myForm = this.formBuilder.group({
      usuario: new FormControl(0, Validators.compose([
        Validators.required,
        Validators.pattern('^[0-9 ]+$'),
      ])
      )

    });
  }//FIN FUncTION
  inf(v) {
    this.finalizar = v;
  }
  seleccionradio(a, b) {
    //alert(a+' - '+b);
    this.checkbox_re[a] = b;
  }
  resolver(a, b) {
    if (this.finalizar == 1) {
      this.checkbox_pro[a] = b;
    }
  }
  quitarresena() {
    this.inicio = 1;
  }
  primeracarga() {
    //const browser = this.iab.create(url);
    //const browser = this.iab.create(url, '_system', this.options2);
    this.inicio = 2;
  }
  ionViewDidEnter() {
    this.deudaid = this.rutaActiva.snapshot.paramMap.get('id');
    //this.slider.startAutoplay();
    if (localStorage.getItem("banderainiciodeudaadd") != '1') {
      localStorage.setItem('banderainiciodeudaadd', '1');
      this.primeracarga();
    }
    const loader = this.loadingCtrl.create({
      message: "Un momento por favor..."
    }).then(load2 => {
      load2.present();
      this.provider_menu.get_applications(this.deudaid).subscribe((response) => {
        load2.dismiss().then(() => {
          if (!response['error']) {
            /*if(response['status'] != 'CO' && response['status'] != 'COL'){
              this.inicioPage.pendientes();
              const alert = this.alertCtrl.create({
                subHeader: "Aviso",
                message: 'La contra oferta ya ha sido procesada',
                buttons: [
                  {
                    text: "Ok",
                    role: 'cancel'
                  }
                ]
              }).then(alert => alert.present());
              let contarinicio   = '';
                          let contarinicio2  = 0;
                          contarinicio  = localStorage.getItem('contarinicio_1_1');
                          contarinicio2 = parseInt(localStorage.getItem('contarinicio_1_1'));
                          contarinicio2++;  contarinicio2++;
                          localStorage.setItem('contarinicio_1_1', contarinicio2+'')
                          this.navController.navigateForward('principal/inicio/'+contarinicio2);
            }*/
            this.datosdeudas = [response['data']];
            this.datey = formatDate(response['data']['aplication_date'], 'yyyy', 'en-US');

            this.mes = parseInt(formatDate(response['data']['aplication_date'], 'M', 'en-US'));
            this.mes2 = this.mes-1;
            this.datem = this.meses_ES[this.mes2];
            this.dated = formatDate(response['data']['aplication_date'], 'dd', 'en-US');
            //console.log(this.meses_ES[formatDate(response['data']['aplication_date'], 'M', 'en-US')]);
            //console.log(this.datosdeudas);
            //this.solicitudespendientes =  response['data'];
                      //this.solicitudespendientes[0]['monto_deuda'] = this.datosdeudas.debt_Amount;
                      //this.solicitudespendientes[0]['n_cuotas'] = this.datosdeudas.installment_pay_num_offer;
                      //this.solicitudespendientes[0]['application_id'] = this.datosdeudas.application_id;
                      this.provider_menu.get_creditors(response['data']['creditor_id']).subscribe((responseCreditor) => {
                        //console.log(responseCreditor);
                        this.solicitudespendientes1 = responseCreditor['data']['identification_number'];
                        this.solicitudespendientes2 = responseCreditor['data']['name'];
                      });//FIN POST
                      //this.provider_menu.get_product_types(this.datosdeudas.product_type_id).subscribe((responseProductType) => {
                        //console.log(responseProductType);
                        //this.solicitudespendientes[0]['tipo_deuda'] = responseProductType['data']['id'];
                      //});//FIN POST

            console.log(response['data']);
            console.log("----");
          } else {
            const alert = this.alertCtrl.create({
              subHeader: "Aviso",
              message: response['msg'],
              buttons: [
                {
                  text: "Ok",
                  role: 'cancel'
                }
              ]
            }).then(alert => alert.present());
          }//Fin else
        });//FIN LOADING DISS
      }, error => {
        load2.dismiss().then(() => {
          const alert = this.alertCtrl.create({
            subHeader: "Aviso",
            message: "No pudo conectarse al servidor",
            buttons: [
              {
                text: "Reintentar",
                role: 'cancel',
                cssClass: 'ion-aceptar',
                handler: data => {
                  this.provider_menu.logerroresadd("Ver detalle contraoferta", JSON.stringify(error)).subscribe((response) => { });//FIN LOADING DISS
                  this.ionViewDidEnter();
                }
              }
            ]
          }).then(alert => alert.present());
        });//FIN LOADING DISS
      });//FIN POST
    });//FIn LOADING

  }//FIN FcuntiN

  regresar() {
    //this.navController.back();
    //this.inicioPage.pendientes();
                          let contarinicio   = '';
                          let contarinicio2  = 0;
                          contarinicio  = localStorage.getItem('contarinicio_1_1');
                          contarinicio2 = parseInt(localStorage.getItem('contarinicio_1_1'));
                          contarinicio2++;  contarinicio2++;
                          localStorage.setItem('contarinicio_1_1', contarinicio2+'')
                          this.navController.navigateRoot('principal/inicio/'+contarinicio2);
  }

  informacion(var1) {
    if (var1 == 1) {
      this.navController.navigateForward('inforcondiciones');
    } else if (var1 == 2) {
      this.navController.navigateForward('inforpoliticas');
    } else if (var1 == 3) {
      this.navController.navigateForward('loginrecuperar1');
    } else if (var1 == 4) {
      this.navController.navigateForward('loginclave');

    }
  }
  /*continuar() {
    const loader = this.loadingCtrl.create({
      message: "Un momento por favor..."
    }).then(load2 => {
      load2.present();
      this.provider_menu.aprobarcontraoferta(this.usuarioid, this.deudaid).subscribe((response) => {
        load2.dismiss().then(() => {
          if (response['code'] == 200) {
            this.navController.navigateForward('verdetallecontraofertaaprobar/' + response['nombre']);
          }//Fin else
        });//FIN LOADING DISS
      }, error => {
        load2.dismiss().then(() => {
          const alert = this.alertCtrl.create({
            subHeader: "Aviso",
            message: "No pudo conectarse al servidor",
            buttons: [
              {
                text: "Reintentar",
                role: 'cancel',
                cssClass: 'ion-aceptar',
                handler: data => {
                  this.provider_menu.logerroresadd("Ver detalle contraoferta", JSON.stringify(error)).subscribe((response) => { });//FIN LOADING DISS
                  this.ionViewDidEnter();
                }
              }
            ]
          }).then(alert => alert.present());
        });//FIN LOADING DISS
      });//FIN POST
    });//FIn LOADING
  }*/

  continuar() {
    const loader = this.loadingCtrl.create({
      message: "Un momento por favor..."
    }).then(load2 => {
      load2.present();
      this.provider_menu.post_customer_decision(this.deudaid).subscribe((response) => {
        load2.dismiss().then(() => {
          if (!response['false']) {
            /*setTimeout(() => {
              this.inicioPage.pendientes();
            }, 6000);*/
            //this.navController.navigateForward('verdetallecontraofertaaprobar/' + response['data']['final_decision']);
            this.navController.navigateForward('verdetallecontraofertaaprobar/Aprobado');
          }//Fin else
        });//FIN LOADING DISS
      }, error => {
        load2.dismiss().then(() => {
          const alert = this.alertCtrl.create({
            subHeader: "Aviso",
            message: "No pudo conectarse al servidor",
            buttons: [
              {
                text: "Reintentar",
                role: 'cancel',
                cssClass: 'ion-aceptar',
                handler: data => {
                  this.provider_menu.logerroresadd("Ver detalle contraoferta", JSON.stringify(error)).subscribe((response) => { });//FIN LOADING DISS
                  this.ionViewDidEnter();
                }
              }
            ]
          }).then(alert => alert.present());
        });//FIN LOADING DISS
      });//FIN POST
    });//FIn LOADING
  }

  asesor() {
      let application_id = this.rutaActiva.snapshot.paramMap.get('id');
      let customerid = localStorage.getItem('CUSTOMERID');
      let usuario = localStorage.getItem('NOMBRESAPELLIDOS');
      let product_type = this.datosdeudas[0]['product_type_id'] == 1 ? 'Saldo con descuente' : 'Refinanciamiento';
      let moneda = this.datosdeudas[0]['currency'];
      let deuda = this.datosdeudas[0]['debt_Amount'];
      let email = {
        to: 'soporte@pagadores.com',
        subject: 'Solicitud de soporte app #' + application_id,
        body: 'CustomerId: ' + customerid + ' <br/> Nombre: ' + usuario + ' <br/> ApplicationId: ' + application_id + ' <br/> Tipo de Producto: ' + product_type + ' <br/> Deuda: ' + moneda + ' ' + deuda + ' <br/> ',
        isHtml: true
      }
      //console.log(email);
      // Send a text message using default options
      this.emailComposer.open(email);
  }
  propuestainicial(a) {

  }
}//FIN CLASS