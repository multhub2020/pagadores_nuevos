import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Verdetallecontraofertapagarmsj } from './verdetallecontraofertapagarmsj';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild([{ path: '', component: Verdetallecontraofertapagarmsj }])
  ],
  declarations: [Verdetallecontraofertapagarmsj]
})
export class VerdetallecontraofertapagarmsjModule {}
