import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl} from '@angular/forms';

import { Router } from '@angular/router';
import { Platform, NavController, LoadingController, AlertController } from '@ionic/angular';

import { Menus } from '../../providers/menus';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { Variablesglobales } from '../../providers/variablesglobal';
import { Listacontactos } from '../../providers/listacontactos';

@Component({
  selector: 'app-listacontactoslistar',
  templateUrl: 'listacontactoslistar.html',
  styleUrls: ['listacontactoslistar.scss'],
  providers:[Menus, SocialSharing, Variablesglobales, Listacontactos]
})

export class Listacontactoslistar {
  public myForm: FormGroup;
  public loading: any;
  public usuarioid: string;
  public imgurl   = new Variablesglobales();
  public imgurl2: any;
  public check = [];
  public nombres: string;
  public apellidos: string;
  public avatar:string="";
  public invitados: any;
  public invitados2 = "";
  public ver: any;
  public seguidores = "";
  public color     = "";
  public usuario = "";
  public selected = "";
  public selectedIndex=0;
  public searchTerm: any;
  public allItems: any;
  public items: any;
  public page_number=1;
   public appPages = [
      {
        title: 'Sugerencias',
        url: 'perfilsugerencias',
        icon: 'mail'
      }
    ];
  constructor(public navController: NavController,
  			  public formBuilder: FormBuilder,
  			  private router: Router,
  			  private provider: Menus,
          private provider2: Listacontactos,
          private platform: Platform,
  			  public alertCtrl: AlertController,
          private socialSharing: SocialSharing,
  			  public loadingCtrl: LoadingController,
          private provider_menu: Menus
  			  ) {
      this.platform.backButton.subscribe(() => {
        // code that is executed when the user pressed the back button
        this.navController.navigateRoot('menu');
      });
      this.usuario   = localStorage.getItem('NOMBRESAPELLIDOS');
      this.usuarioid = localStorage.getItem('IDUSER');
      this.selected  = '2';
  }
  loadData(event) {
        this.provider2.listacontactoslistar(this.usuarioid, this.page_number).subscribe((response) => {
                            if(response['code']==200){
                                for (let i = 0; i < response['datos']['data'].length; i++) {
                                  this.invitados.push(response['datos']['data'][i]);
                                }
                                this.allItems   = this.invitados;
                                this.ver        = this.invitados;
                                this.page_number++;
                                event.target.complete();
                            }else if (response['code']==201){
                                        const alert = this.alertCtrl.create({
                                          subHeader: "Aviso",
                                            message: response['msg'],
                                            buttons: [
                                              {
                                                text: "Ok",
                                                role: 'cancel'
                                              }
                                            ]
                                        }).then(alert => alert.present());
                            }//Fin else
        },error => {
              this.loadingCtrl.dismiss().then( () => {
                        const alert = this.alertCtrl.create({
                            subHeader: "Aviso",
                            message: "No pudo conectarse al servidor",
                            buttons: [
                              {
                                  text: "Reintentar",
                                  role: 'cancel',
                                  cssClass:'ion-aceptar',
                                  handler: data => {
                                      this.provider_menu.logerroresadd("listar contactos ", JSON.stringify(error)).subscribe((response) => { });//FIN LOADING DISS
                                      this.ionViewDidEnter();
                                  }
                              }
                            ]
                        }).then(alert => alert.present());
            });//FIN LOADING DISS
        });//FIN POST
  }
  onSearchTerm(ev: CustomEvent) {
        this.ver = this.allItems;
        /*this.ver = this.ver.filter((term) => {
                return term.nombres.toLowerCase().indexOf(this.searchTerm.toLowerCase()) > -1;
        }); */
        this.ver = this.ver.filter(currentFood => {
         // if ((currentFood.nombres || currentFood.telefono) && this.searchTerm  ) {
            return (currentFood.nombres.toLowerCase().indexOf(this.searchTerm.toLowerCase()) > -1 || currentFood.telefono.toLowerCase().indexOf(this.searchTerm.toLowerCase()) > -1);
          //}
        });
  }
  salir(){
      this.navController.navigateRoot('menu');
  }
  linkwhatsapp(num){
      this.socialSharing.shareViaWhatsAppToReceiver(num, '', null, null);
  }
  linkmenu(var1, i){
      if(var1=="notificacion"){
            this.navController.navigateForward('perfilnotificaciones');
      }else if(var1=="top"){
             this.navController.navigateForward('perfiltop');
       }else if(var1=="logout"){
              localStorage.removeItem('IDUSER');
              localStorage.removeItem('USUARIO');
              localStorage.removeItem('NOMBRESAPELLIDOS');
              localStorage.removeItem('TOKEN');
              localStorage.removeItem('SESSIONACTIVA');
              localStorage.removeItem('consultaFechaBuro');
              localStorage.setItem('SESSIONACTIVA','false');
              this.navController.navigateRoot('login');
      }
  }//fin function

  onChangeMarca(a, b, c){
    //this.check[c]['id']    = a;
    this.check[c] = b;
    console.log(this.check[c]);
  }
  linkreenviarall(){
      this.usuarioid = localStorage.getItem('IDUSER');
      const loader = this.loadingCtrl.create({
        message: "Un momento por favor..."
      }).then(load => {
              load.present();
              console.log(this.check);
              this.provider2.listacontactossend(this.usuarioid, this.check).subscribe((response) => {
                          this.loadingCtrl.dismiss().then( () => {
                                  if(response['code']==200){
                                              const alert = this.alertCtrl.create({
                                                subHeader: "Aviso",
                                                  message: response['msg'],
                                                  buttons: [
                                                    {
                                                      text: "Continuar",
                                                      role: 'cancel',
                                                      cssClass:'ion-aceptar',
                                                      handler: data => {
                                                          this.navController.navigateRoot('menu');
                                                      }
                                                    }
                                                  ]
                                              }).then(alert => alert.present());
                                  }//Fin else
                          });//FIN LOADING DISS
              },error => {
                    this.loadingCtrl.dismiss().then( () => {
                              const alert = this.alertCtrl.create({
                                  subHeader: "Aviso",
                                  message: "No pudo conectarse al servidor",
                                  buttons: [
                                    {
                                        text: "Reintentar",
                                        role: 'cancel',
                                        cssClass:'ion-aceptar',
                                        handler: data => {
                                            this.provider_menu.logerroresadd("listar contactos ", JSON.stringify(error)).subscribe((response) => { });//FIN LOADING DISS
                                            //this.ionViewDidEnter();
                                        }
                                    }
                                  ]
                              }).then(alert => alert.present());
                  });//FIN LOADING DISS
              });//FIN POST
      });//FIn LOADING
  }
  ionViewDidEnter() {
  			this.usuarioid = localStorage.getItem('IDUSER');
        //this.usuarioid = 74;
            const loader = this.loadingCtrl.create({
            	message: "Un momento, cargando..."
            }).then(load => {
                    load.present();
                    this.page_number = 1;
                    this.provider2.listacontactoslistar(this.usuarioid, this.page_number).subscribe((response) => {
                                this.loadingCtrl.dismiss().then( () => {
                                        if(response['code']==200){
                                            this.invitados  = response['datos']['data'];
                                            console.log('this.invitados', this.invitados);
                                            this.page_number++;
                                            this.allItems   = this.invitados;
                                            this.ver        = this.invitados;
                                        }else if (response['code']==201){
                                                    const alert = this.alertCtrl.create({
                                                      subHeader: "Aviso",
                                                        message: response['msg'],
                                                        buttons: [
                                                          {
                                                            text: "Ok",
                                                            role: 'cancel'
                                                          }
                                                        ]
                                                    }).then(alert => alert.present());
                                        }//Fin else
                                });//FIN LOADING DISS
                    },error => {
                          this.loadingCtrl.dismiss().then( () => {
                                    const alert = this.alertCtrl.create({
                                        subHeader: "Aviso",
                                        message: "No pudo conectarse al servidor",
                                        buttons: [
                                          {
                                              text: "Reintentar",
                                              role: 'cancel',
                                              cssClass:'ion-aceptar',
                                              handler: data => {
                                                  this.provider_menu.logerroresadd("listar contactos ", JSON.stringify(error)).subscribe((response) => { });//FIN LOADING DISS
                                                  this.ionViewDidEnter();
                                              }
                                          }
                                        ]
                                    }).then(alert => alert.present());
                        });//FIN LOADING DISS
                    });//FIN POST
            });//FIn LOADING

  }
}//FIN CLASS
