import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl} from '@angular/forms';

import { Router } from '@angular/router';
import { NavController, LoadingController, AlertController } from '@ionic/angular';
import { Menus } from '../../providers/menus';
import { Usuario } from '../../providers/usuario';

@Component({
  selector: 'app-loginrecuperar1',
  templateUrl: 'loginrecuperar1.html',
  styleUrls: ['loginrecuperar1.scss'],
  providers:[Usuario, Menus]
})

export class Loginrecuperar1 {
  public myForm: FormGroup;
  public loading: any;
  constructor(public navController: NavController,
  			  public formBuilder: FormBuilder,
  			  private router: Router, 
  			  private provider_usuario: Usuario, 
  			  public alertCtrl: AlertController,
  			  public loadingCtrl: LoadingController,
  			  private provider_menu: Menus
  			  ) {
	  	this.myForm = this.formBuilder.group({
	        usuario: new FormControl('', Validators.compose([ 
															Validators.required
														  ])
								   )
	    });
  }
  ionViewDidEnter() {
       
  }
  salir(){
  		this.navController.navigateRoot('login');
  }
  enviarformulario(){
  	const loader = this.loadingCtrl.create({
  		message: "Un momento por favor..."
    }).then(load => {
	    				load.present();
	    				if(this.myForm.value.usuario==""){
	    							this.loadingCtrl.dismiss().then( () => { 
	    									const alert = this.alertCtrl.create({
		                                    	subHeader: "Aviso",
		                                        message: "Debe completarse campo teléfono / email",
		                                        buttons: [
		                                          {
		                                            text: "Ok", 
		                                            role: 'cancel'
		                                          }
		                                        ]
		                                    }).then(alert => alert.present());
	    							});//FIN LOADING DISS
              			}else{
								  	this.provider_usuario.loginrecuperar1(this.myForm.value).subscribe((response) => {  
								                this.loadingCtrl.dismiss().then( () => { 
								                              if(response['code']==200){
								                              		localStorage.setItem('USUARIORECUPERAR', response['usuario']);  
								                              		this.navController.navigateForward('loginrecuperar2');
								                        }else if (response['code']==201){
								                                    const alert = this.alertCtrl.create({
								                                    	subHeader: "Aviso",
								                                        message: response['msg'],
								                                        buttons: [
								                                          {
								                                            text: "Ok", 
								                                            role: 'cancel'
								                                          }
								                                        ]
								                                    }).then(alert => alert.present());
								                        }//Fin else
								                });//FIN LOADING DISS
								    },error => {
								    			this.loadingCtrl.dismiss().then( () => {
										                const alert = this.alertCtrl.create({
										                    subHeader: "Aviso",
										                    message: "No pudo conectarse al servidor",
										                    buttons: [
										                      {
										                          text: "Reintentar",
										                          role: 'cancel',
										                          cssClass:'ion-aceptar',
										                          handler: data => {
										                          	    this.provider_menu.logerroresadd("login recuperar 1", JSON.stringify(error)).subscribe((response) => { });//FIN LOADING DISS
										                            	this.ionViewDidEnter();
										                          }
										                      }
										                    ]
										                }).then(alert => alert.present());
												});//FIN LOADING DISS
							    	});//FIN POST
						}//fin else
				});//FIn LOADING
  }//FIN FUNCTION
}//FIN CLASS
