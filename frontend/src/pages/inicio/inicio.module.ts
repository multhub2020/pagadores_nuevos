import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Inicio } from './inicio';
import { SharedComponentsModule } from '../../app/components/shared-components.module';


@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild([{ path: '', component: Inicio }]),
    SharedComponentsModule
  ],
  declarations: [Inicio]
})
export class InicioModule {}
