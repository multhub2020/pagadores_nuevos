import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NavController, LoadingController, AlertController, Platform } from '@ionic/angular';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Menus } from '../../providers/menus';
import { Usuario } from '../../providers/usuario';
import { Informacion } from '../../providers/informacion';
import { SuperTabs } from "@ionic-super-tabs/angular";
import { Variablesglobales } from '../../providers/variablesglobal';
import { HttpClient, HttpHeaders } from '@angular/common/http';


@Component({
  selector: 'app-inicio',
  templateUrl: 'inicio.html',
  styleUrls: ['inicio.scss'],
  providers: [Menus, Usuario, Variablesglobales, Informacion]
})

export class Inicio {
  public usuario: string;
  public usuarioid: string;
  public customerid: string;
  public information: any = [true, false, false, false];
  public automaticClose = false;
  public checkbox: any = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

  public cuentadeudassinresolver = 0;
  public solicitudespendientes: any = [];
  public cuentaspendientes: any;
  public ccuentaspendientes=0;
  public datosdeudassinresolver: any;


  public slideOpts2 = {
          effect: 'coverflow',
          autoplay: false,
          slidesPerView: 1,
          paginationType:"arrows"
  };


  public totaldeudaactual = 0;
  public totaldeudamensual = 0;
  public deudaslocalizadas = 0;

  public allCreditors: any;
  public allProductResolutions: any;

  constructor(private router: Router,
    public formBuilder: FormBuilder,
    private navController: NavController,
    private provider_menu: Menus,
    private provider_informacion: Informacion,
    private provider_usuario: Usuario,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    private platform: Platform,
    private http: HttpClient
  ) {
    this.usuario = localStorage.getItem('NOMBRESAPELLIDOS');
    this.usuarioid = localStorage.getItem('IDUSER');
    this.customerid = localStorage.getItem('CUSTOMERID');
  }//FIN FUncTION

  toggleSection(index) {
    this.information[index] = !this.information[index];
    if (this.automaticClose && this.information[index]) {
      this.information.filter((item, itemIndex) => itemIndex != index).map(item => item = false);
    }
  }
  ayuda() {
    this.navController.navigateForward('ayuda');
  }
  verdetallecontraofertacontinua(a) {
    this.navController.navigateForward('verdetallecontraofertacontinua/' + a);
  }
  verdetallecontraoferta(a) {
    this.navController.navigateForward('verdetallecontraoferta/' + a);
  }
  verdetallecontinua(a) {
    this.navController.navigateForward('verdetallecontinua/' + a);
  }
  resolverdeuda(a, b) {
    if (this.checkbox[a] == 0) {
      this.checkbox[a] = b;
    } else {
      this.checkbox[a] = 0;
    }
    console.log(a + ' - ' + JSON.stringify(this.checkbox));
    localStorage.setItem('checkboxdeuda', this.checkbox);
    this.navController.navigateForward('principal/localizardeuda1');
  }
  verDetalleCuenta(a) {
    this.navController.navigateForward('verdetallecuenta/' + a);
  }
  pagarcuenta(a) {
    this.navController.navigateForward('pagarcuenta/' + a);
  }
  vercuenta(a) {
    this.navController.navigateForward('vercuenta/' + a);
  }
  pagarcuentadeposito(a) {
    this.navController.navigateForward('pagarcuentadeposito/' + a);
  }
  verdetalleaceptar(a) {
    this.navController.navigateForward('verdetalleaceptar/' + a);
  }
  enviarpago(application_id, account_number) {
    this.navController.navigateForward('enviarpago/' + application_id + '/' + account_number);
  }



  irdatos(a){
              if(a=='1'){
                              this.navController.navigateRoot('principal/menucuentas');

        }else if(a=='2'){

                              this.navController.navigateRoot('principal/menu');
        }else if(a=='3'){

                              this.navController.navigateRoot('principal/menusolucionadas');

        }else if(a=='4'){

                            this.navController.navigateRoot('principal/menuenproceso');
        }
  }

  doRefresh(event) {
    //this.inicio(1, event);
    event.target.complete();
    let contarinicio = '';
    let contarinicio2 = 0;
    contarinicio = localStorage.getItem('contarinicio_1_1');
    contarinicio2 = parseInt(localStorage.getItem('contarinicio_1_1'));
    contarinicio2++; contarinicio2++;
    localStorage.setItem('contarinicio_1_1', contarinicio2 + '')
    this.navController.navigateRoot('principal/inicio/' + contarinicio2);
  }

  ionViewDidEnter() {
    this.provider_usuario.prueba_encriptacion();
    this.provider_usuario.prueba_desencriptacion();
    this.provider_usuario.prueba_encriptacionresa();
    this.inicio(2, null);
  }

  inicio(e, event) {
    const loader = this.loadingCtrl.create({
      message: "Un momento por favor..."
    }).then(load2 => {
      load2.present();
      this.provider_menu.inicio(this.usuarioid, this.customerid).subscribe((response) => {
        
          if (response['code'] == 200) {
            this.datosdeudassinresolver = response['datosdeudassinresolver'];
            this.cuentadeudassinresolver = response['cuentadeudassinresolver'];
            this.totaldeudaactual = response['totaldeudaactual'];
            this.totaldeudamensual = response['totaldeudamensual'];
            this.solicitudespendientes = response['solicitudespendientes'];
            this.cuentaspendientes = response['cuentaspendientes'];
            if(this.cuentaspendientes.length>0){
                this.ccuentaspendientes = this.cuentaspendientes.length;
            }
            console.log(response);
            if (e == 1) {
              event.target.complete();
            }
          }//Fin else
          load2.dismiss().then(() => {});//FIN LOADING DISS
      }, error => {
        load2.dismiss().then(() => {
          // this.ionViewDidEnter();
          const alert = this.alertCtrl.create({
            subHeader: "Aviso",
            message: "No pudo conectarse al servidor",
            buttons: [
              {
                text: "Reintentar",
                role: 'cancel',
                cssClass: 'ion-aceptar',
                handler: data => {
                  this.provider_menu.logerroresadd("Inicio", JSON.stringify(error)).subscribe((response) => { });//FIN LOADING DISS
                  this.ionViewDidEnter();
                }
              }
            ]
          }).then(alert => alert.present());
        });//FIN LOADING DISS
      });//FIN POST
    });//FIn LOADING
  }//fin inicio

}//FIN CLASS