import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-enviarpago',
  templateUrl: 'enviarpago.html',
  styleUrls: ['enviarpago.scss'],
})

export class Enviarpago {
  public application_id: any;
  public account_number: any;
  
  constructor(public navController: NavController, private router: Router, private route: ActivatedRoute) {
    this.application_id = this.route.snapshot.paramMap.get('application_id');
    this.account_number = this.route.snapshot.paramMap.get('account_number');
  }

  ionViewDidEnter() {
    
  }
  regresar() {
    this.navController.back();
  }
  pagarcuenta(contract_id, application_id) {
    this.navController.navigateForward('pagarcuenta/' + contract_id + '/' + application_id);
  }
  pagarcuentadeposito(a) {
    this.navController.navigateForward('pagarcuentadeposito/' + a);
  }

}//FIN CLASS
