import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl} from '@angular/forms';

import { Router } from '@angular/router';
import { NavController, LoadingController, AlertController } from '@ionic/angular';
import { GooglePlus } from '@ionic-native/google-plus/ngx';
import { FacebookLoginResponse, Facebook} from "@ionic-native/facebook/ngx";
import { Usuario } from '../../providers/usuario';
import { Menus } from '../../providers/menus';
import { Variablesglobales } from '../../providers/variablesglobal';

@Component({
  selector: 'app-loginclave',
  templateUrl: 'loginclave.html',
  styleUrls: ['loginclave.scss'],
  providers:[Usuario, GooglePlus, Facebook, Variablesglobales, Menus]
})

export class Loginclave {
  public myForm: FormGroup;
  public loading: any;
  public email: string;
  public url = new Variablesglobales();
  public t_push: string;
  public p_push: string;
  public u_push: string;
  constructor(public navController: NavController,
  			  public formBuilder: FormBuilder,
  			  private router: Router, 
  			  private googlePlus: GooglePlus,
  			  private fb: Facebook,
  			  private provider_usuario: Usuario, 
  			  public alertCtrl: AlertController,
  			  public loadingCtrl: LoadingController,
  			  private provider_menu: Menus
  			  ) {
	  	this.myForm = this.formBuilder.group({
	  		 user: new FormControl('', Validators.compose([ 
															Validators.required
														  ])
								   ),
	        clave: new FormControl('', Validators.compose([ 
															Validators.required
														  ])
								   )
	    });
  }
  ionViewDidEnter() {
     
  }
  salir(){
  		this.navController.navigateRoot('login');
  }
  enviarformulario(v){
  	const loader = this.loadingCtrl.create({
  		message: "Un momento por favor..."
    }).then(load => {
	    				load.present();
	    				if(this.myForm.value.user==""){
	    							this.loadingCtrl.dismiss().then( () => { 
	    									const alert = this.alertCtrl.create({
		                                    	subHeader: "Aviso",
		                                        message: "Debe completarse campo teléfono / email",
		                                        buttons: [
		                                          {
		                                            text: "Ok", 
		                                            role: 'cancel'
		                                          }
		                                        ]
		                                    }).then(alert => alert.present());
	    							});//FIN LOADING DISS
	    				}else if(this.myForm.value.clave==""){
	    							this.loadingCtrl.dismiss().then( () => { 
	    									const alert = this.alertCtrl.create({
		                                    	subHeader: "Aviso",
		                                        message: "Debe ingresar su clave",
		                                        buttons: [
		                                          {
		                                            text: "Ok", 
		                                            role: 'cancel'
		                                          }
		                                        ]
		                                    }).then(alert => alert.present());
	    							});//FIN LOADING DISS
              			}else{
								  	this.provider_usuario.loginclave(this.myForm.value, v).subscribe((response) => {  
								                this.loadingCtrl.dismiss().then( () => { 
								                        if(response['code']==200){

								                        		this.provider_usuario.api_pagadores_loginclave().subscribe((responseapipagadores) => {  

								                        			           console.log('datos: '+JSON.stringify(response) );
								                        			           console.log('datos2: '+JSON.stringify(responseapipagadores) );

																	            localStorage.setItem('IDUSER',           response['datos']['id']);
														                        localStorage.setItem('NOMBRESAPELLIDOS', response['datos']['nombre_apellido']);
														                        localStorage.setItem('TIPOUSUARIO',      response['datos']['role_id']);
														                        localStorage.setItem('FOTOPERFIL',       this.url.getApivarcoreHomeI()+'api/storage/imagenes'+response['datos']['foto2']);
														                        localStorage.setItem('CUSTOMERID',       response['datos']['customer_id']);
																										localStorage.setItem('credolabcollect',  response['datos']['credolabcollect']);
																										localStorage.setItem('credolabscores',   response['datos']['credolabscores']);

														                        localStorage.setItem('TOKEAPIPAGADORES',       responseapipagadores["access_token"]);

														                          localStorage.setItem('SESSIONACTIVA','true');
														                          this.t_push = localStorage.getItem('T_PUSH');
						             														  this.p_push = localStorage.getItem('P_PUSH');
						             														  this.provider_usuario.actualizarpush(this.t_push, this.p_push, response['datos']['id'] ).subscribe((response2) => {});
														                          let contarinicio   = '';
														                          let contarinicio2  = 0;
														                          contarinicio  = localStorage.getItem('contarinicio_1_1');
														                          contarinicio2 = parseInt(localStorage.getItem('contarinicio_1_1'));
														                          contarinicio2++;  contarinicio2++;
														                          localStorage.setItem('contarinicio_1_1', contarinicio2+'')
														                          this.navController.navigateRoot('principal/inicio/'+contarinicio2);

											                    },error => {
														    			this.loadingCtrl.dismiss().then( () => {
																                const alert = this.alertCtrl.create({
																                    subHeader: "Aviso",
																                    message: "No pudo conectarse al servidor",
																                    buttons: [
																                      {
																                          text: "Reintentar",
																                          role: 'cancel',
																                          cssClass:'ion-aceptar',
																                          handler: data => {
																                          	    this.provider_menu.logerroresadd("login clave", JSON.stringify(error)).subscribe((response) => { });//FIN LOADING DISS
																                            	this.ionViewDidEnter();
																                          }
																                      }
																                    ]
																                }).then(alert => alert.present());
																		});//FIN LOADING DISS
										                    	});//FIN POST




								                        }else if (response['code']==201){
								                                    const alert = this.alertCtrl.create({
								                                    	subHeader: "Aviso",
								                                        message: response['msg'],
								                                        buttons: [
								                                          {
								                                            text: "Ok", 
								                                            role: 'cancel'
								                                          }
								                                        ]
								                                    }).then(alert => alert.present());
								                        }//Fin else
								                });//FIN LOADING DISS
								    },error => {
								    			this.loadingCtrl.dismiss().then( () => {
										                const alert = this.alertCtrl.create({
										                    subHeader: "Aviso",
										                    message: "No pudo conectarse al servidor",
										                    buttons: [
										                      {
										                          text: "Reintentar",
										                          role: 'cancel',
										                          cssClass:'ion-aceptar',
										                          handler: data => {
										                          		this.provider_menu.logerroresadd("login clave", JSON.stringify(error)).subscribe((response) => { });//FIN LOADING DISS
										                            	this.ionViewDidEnter();
										                          }
										                      }
										                    ]
										                }).then(alert => alert.present());
												});//FIN LOADING DISS
							    	});//FIN POST
						}//fin else
				});//FIn LOADING
  }//FIN FUNCTION
  informacion(var1){
	  		if(var1==1){
	  		this.navController.navigateForward('inforcondiciones');
	  	}else if(var1==2){
	  		this.navController.navigateForward('inforpoliticas');
	    }else if(var1==3){
	  		this.navController.navigateForward('loginrecuperar1');
	  	}
  }
  googleSignIn(){
      const loader = this.loadingCtrl.create({
        ////duration: 10000
        //message: "Un momento por favor..."
      }).then(load => {
                      load.present();
                      this.googlePlus.login({})
                        .then(result => {
                                //this.userData = result;
                                //name: result.displayName,
                                //email: result.email,
                                //console.log(result); 
                                this.loadingCtrl.dismiss().then( () => { 
		                                this.myForm.value['clave'] = "re_p_*my_kil*1865Cm98xmngfZaa1"; 
		                                this.myForm.value['user']  = result.email;  
		                                this.enviarformulario(2);
                                });//FIN LOADING DISS
                      }).catch(e => {
                            this.loadingCtrl.dismiss().then( () => { 
                                console.info('Error logging into Gmail', e);
                                //alert('error'+JSON.stringify(e));
                            });//FIN LOADING DISS
                      });
      });//FIn LOADING 
  }//fin function
  facebookSignIn(){
      const loader = this.loadingCtrl.create({
        ////duration: 10000
        //message: "Un momento por favor..."
      }).then(load => {
                      load.present();
                            this.fb.login(['public_profile', 'email'])
                            .then((res: FacebookLoginResponse) => {
                                  this.fb.api('me?fields=' + ['name', 'email', 'first_name', 'last_name', 'picture.type(large)'].join(), null)
                                  .then((res2: any) => {
                                            //this.userData = res2;
                                             //this.userData = result;
			                                //name: result.displayName,
			                                //email: result.email,
			                                //console.log(result);
			                                this.loadingCtrl.dismiss().then( () => { 
					                                this.myForm.value['clave'] = "re_p_*my_kil*1865Cm98xmngfZaa1"; 
					                                this.myForm.value['user']  = res2.email;  
					                                this.enviarformulario(2);
			                                });//FIN LOADING DISS
                                  }).catch(e => {
                                        this.loadingCtrl.dismiss().then( () => { 
                                            console.info('Error logging into Facebook', e);
                                            //alert('error'+JSON.stringify(e));
                                        });//FIN LOADING DISS
                                  });
                      }).catch(e => {
                            this.loadingCtrl.dismiss().then( () => { 
                                console.info('Error logging into Facebook', e);
                                //alert('error'+JSON.stringify(e));
                            });//FIN LOADING DISS
                      });
      });//FIn LOADING 
  }
}//FIN CLASS
