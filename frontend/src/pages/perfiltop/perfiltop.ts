import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl} from '@angular/forms';

import { Router } from '@angular/router';
import { Platform, NavController, LoadingController, AlertController } from '@ionic/angular';

import { Menus } from '../../providers/menus';
import { MenuController } from '@ionic/angular';

import { ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-perfiltop',
  templateUrl: 'perfiltop.html',
  styleUrls: ['perfiltop.scss'],
  providers:[Menus]
})

export class Perfiltop {
  public myForm: FormGroup;
  public loading: any;
  public usuarioid: string;
  public puntaje = "5";
  public selected = "1";
  public selected2 = "1";
  public nombres: string;
  public datos = "";
  public apellidos: string;
  public avatar:string="";
  public invitados = "";
  public seguidores: any;
  public seguidores1: any;
  public seguidores2: any;
  public ordena      = "";
  public color     = "";
  public usuario = "";
  public searchTerm: any;
  public allItems: any;
  public items: any;
  public seguidoresid = "";
  public nombressub = "";
  constructor(public navController: NavController,
  			  public formBuilder: FormBuilder,
  			  private router: Router, 
  			  private provider: Menus, 
          private platform: Platform,
          private menu: MenuController,
  			  public alertCtrl: AlertController,
  			  public loadingCtrl: LoadingController,
          private rutaActiva: ActivatedRoute,
          private provider_menu: Menus
  			  ) {
      this.platform.backButton.subscribe(() => {
        // code that is executed when the user pressed the back button
        //this.navController.navigateRoot('menu');
      });
      this.usuario      = localStorage.getItem('NOMBRESAPELLIDOS');
      this.seguidoresid = this.rutaActiva.snapshot.paramMap.get('id');
  }
  sub(id){
    this.navController.navigateForward('listarseguidoressub/'+id);
  }
  salir(){
      this.navController.navigateRoot('menu');
  }
  linkmenu(var1, i){
      if(var1=="notificacion"){
            this.navController.navigateForward('perfilnotificaciones');
      }else if(var1=="top"){
             this.navController.navigateForward('perfiltop');
      }
  }//fin function
  ionViewDidEnter() {
  		this.usuarioid = localStorage.getItem('IDUSER');
      const loader = this.loadingCtrl.create({
              message: "Un momento por favor..."
            }).then(load => {  
                    load.present();
                    this.provider.listartop().subscribe((response) => {  
                                this.loadingCtrl.dismiss().then( () => { 
                                        if(response['code']==200){
                                            this.datos = response['datos'];
                                            console.log(this.datos);
                                        }else if (response['code']==201){
                                                    const alert = this.alertCtrl.create({
                                                      subHeader: "Aviso",
                                                        message: response['msg'],
                                                        buttons: [
                                                          {
                                                            text: "Ok", 
                                                            role: 'cancel'
                                                          }
                                                        ]
                                                    }).then(alert => alert.present());
                                        }//Fin else
                                });//FIN LOADING DISS
                    },error => {
                          this.loadingCtrl.dismiss().then( () => {
                                    const alert = this.alertCtrl.create({
                                        subHeader: "Aviso",
                                        message: "No pudo conectarse al servidor",
                                        buttons: [
                                          {
                                              text: "Reintentar",
                                              role: 'cancel',
                                              cssClass:'ion-aceptar',
                                              handler: data => {
                                                  this.provider_menu.logerroresadd("Perfil top", JSON.stringify(error)).subscribe((response) => { });//FIN LOADING DISS
                                                  this.ionViewDidEnter();
                                              }
                                          }
                                        ]
                                    }).then(alert => alert.present());
                        });//FIN LOADING DISS
                    });//FIN POST
      });//FIn LOADING
  }//fin function
}//FIN CLASS
