import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AclGuard } from '../../guards/acl.guard';
import { Menu } from './menu';

const routes: Routes = [
    {
      path: '',
      component: Menu,
    },
    {
      path: 'menuprincipal',
      loadChildren: () => import('../menuprincipal/menuprincipal.module').then(m => m.MenuprincipalModule),
      canActivate:[AclGuard]
    },    
    {
      path: 'perfil',
      loadChildren: () => import('../perfil/perfil.module').then(m => m.PerfilModule),
      canActivate:[AclGuard]
    },
    {
      path: 'perfileditar',
      loadChildren: () => import('../perfileditar/perfileditar.module').then(m => m.PerfileditarModule),
      canActivate:[AclGuard]
    },    
    {
      path: 'perfilnotificaciones',
      loadChildren: () => import('../perfilnotificaciones/perfilnotificaciones.module').then(m => m.PerfilnotificacionesModule),
      canActivate:[AclGuard]
    },
    {
      path: 'notificacionesconfirma/:id',
      loadChildren: () => import('../notificacionesconfirma/notificacionesconfirma.module').then(m => m.NotificacionesconfirmaModule),
      canActivate:[AclGuard]
    },
    {
      path: 'perfiltop',
      loadChildren: () => import('../perfiltop/perfiltop.module').then(m => m.PerfiltopModule),
      canActivate:[AclGuard]
    },
    {
      path: 'listacontactoslistar/:id',
      loadChildren: () => import('../listacontactoslistar/listacontactoslistar.module').then(m => m.ListacontactoslistarModule),
      canActivate:[AclGuard]
    },
    {
      path: 'notificacionesrespuestas/:id',
      loadChildren: () => import('../notificacionesrespuestas/notificacionesrespuestas.module').then(m => m.NotificacionesrespuestasModule),
      canActivate:[AclGuard]
    }

    
    /*,
    {
      path: '',
      redirectTo: '/menu/menuprincipal',
      pathMatch: 'full'
    }*/
]
@NgModule({
  imports: [
    //RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
    //RouterModule.forRoot(routes)
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class MenuRoutingModule {}
