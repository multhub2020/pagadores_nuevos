import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NavController, LoadingController, AlertController, Platform } from '@ionic/angular';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { GooglePlus } from '@ionic-native/google-plus/ngx';
import { FacebookLoginResponse, Facebook } from "@ionic-native/facebook/ngx";
//import { Mapa } from '../mapa/mapa';
import { Menus } from '../../providers/menus';
import { Listacontactos } from '../../providers/listacontactos';
import { SuperTabs } from "@ionic-super-tabs/angular";

import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { Variablesglobales } from '../../providers/variablesglobal';

import { Contacts, Contact, ContactField, ContactName } from '@ionic-native/contacts';
import { InAppBrowser, InAppBrowserObject, InAppBrowserOptions } from '@ionic-native/in-app-browser/ngx';



import { Usuario } from '../../providers/usuario';

import { Helpers } from '../../providers/helpers';

declare var cordova: any;
var exec;// = cordova.require("cordova/exec");

@Component({
  selector: 'app-menu',
  templateUrl: 'menu.html',
  styleUrls: ['menu.scss'],
  providers: [Menus, Usuario, SocialSharing, GooglePlus, Facebook, Variablesglobales, Listacontactos, InAppBrowser]
})

export class Menu {
  //myPage = Mapa;
  @ViewChild(SuperTabs, { static: false }) superTabs: SuperTabs;
  public versionapp = "1.0.5";
  public datosdeudas: any = [''];
  public datos_publi: any;
  public selectedIndex = 0;
  public usuario: string;
  public usuarioid: string;
  public customerid: string;
  public bodys: string;
  public myForm: FormGroup;
  public imgurl = new Variablesglobales();
  public imgurl2: any;
  public listaContactos: any = [];
  public listaContactos2: any = [];

  public datosMostar: any = [];
  public datosMostar2: any = [];
  public checkbox: any = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
  public avatar: string = "";
  public invitados = 0;
  public contador_c = 0;
  public seguidores = 0;
  public color = "";
  public i = 0;
  public slideOpts = {
    effect: 'flip',
    autoplay: {
      delay: 5000
    }
  };
  public invitado = "";
  public appPages = [
    {
      title: 'Sugerencias',
      url: 'perfilsugerencias',
      icon: 'mail'
    }
  ];
  public options2: InAppBrowserOptions = {
    //location : 'no',
    //fullscreen : 'yes',
  };
  public cargandoBtn = false;
  public pending_loader = null;
  public datos_usuarios: any;

  public helpers = new Helpers();


  public inicio = 0;

  constructor(private router: Router,
    public formBuilder: FormBuilder,
    private navController: NavController,
    private provider_menu: Menus,
    private provider_usuario: Usuario,
    private iab: InAppBrowser,
    private provider: Listacontactos,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    private socialSharing: SocialSharing,
    private contacts: Contacts,
    private platform: Platform,
    private fb: Facebook,
    private googlePlus: GooglePlus,
    //public supertabs: SuperTabs
  ) {
    this.usuario = localStorage.getItem('NOMBRESAPELLIDOS');
    this.usuarioid = localStorage.getItem('IDUSER');
    this.customerid = localStorage.getItem('CUSTOMERID');
    this.myForm = this.formBuilder.group({
      usuario: new FormControl('', Validators.compose([
        Validators.required
      ])
      )

    });
    if (!this.platform.is('mobileweb')) {
      exec = cordova.require("cordova/exec");
    }

  }//FIN FUncTION


  cargarsiguiente(){
      this.inicio = 1; 
  }
  cargarsiguiente2(){
      this.inicio = 2; 
  }
  cargarfinal(){
      this.inicio = 0; 
  }



  primeracarga(url) {
    //const browser = this.iab.create(url);
    //const browser = this.iab.create(url, '_system', this.options2);
  }
  ionViewDidEnter() {

    //this.datos_usuarios['sexo']="";
    //this.slider.startAutoplay();

    /*let consultaFechaBuro = localStorage.getItem('consultaFechaBuro');
    let consultarBuro = false;
    let fechaBuro = new Date().getTime();
    if (!consultaFechaBuro) {
      consultarBuro = true;
      localStorage.setItem('consultaFechaBuro', String(fechaBuro));
    } else {
      let fechaActual = new Date().getTime();
      let consultaFechaBuroFutura = new Date(Number(consultaFechaBuro));
      let consultaFechaFutura = consultaFechaBuroFutura.setMinutes(consultaFechaBuroFutura.getMinutes() + 129600);
      let fechaFutura = new Date(consultaFechaFutura).getTime();
      if (Number(fechaActual) >= Number(fechaFutura)) {
        consultarBuro = true;
        localStorage.setItem('consultaFechaBuro', String(fechaBuro));
      }
    }*/

    if (!this.platform.is('mobileweb')) {
      this.guardarCredoLabReferenceNumber();
    }

    let consultarBuro = true;
    this.checkbox = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    const loader = this.loadingCtrl.create({
      message: "Un momento por favor..."
    }).then(load2 => {
      load2.present();
      console.log('usuarioid: ' + this.usuarioid);
      this.provider_menu.listamenu(this.usuarioid, consultarBuro).subscribe((response) => {
        load2.dismiss().then(() => {
          if (response['code'] == 200) {
            console.log('listamenu: ' + JSON.stringify(response));
            this.imgurl2 = this.imgurl.getServar();
            this.datosdeudas = response['datosdeudas'];
            this.datos_publi = response['datos_publi'];
            this.seguidores = response['seguidores'];
            this.invitados = response['invitados'];
            this.color = response['color'];
            this.avatar = response['avatar'];
            this.datos_usuarios = response['datos_usuarios'];
            this.invitado = "";
            console.log(this.datosdeudas);
            if (localStorage.getItem("banderainicio") != '1') {
              localStorage.setItem('banderainicio', '1');
              this.primeracarga(response['parametrovideo']);
            }
            if (this.versionapp != response['versionapp']) {
              const alert = this.alertCtrl.create({
                subHeader: "Aviso",
                message: "Disculpe, Debe actualizar su versión de app para poder continuar",
                buttons: [
                  {
                    text: "Reintentar",
                    role: 'cancel',
                    cssClass: 'ion-aceptar',
                    handler: data => {
                      localStorage.removeItem('IDUSER');
                      localStorage.removeItem('USUARIO');
                      localStorage.removeItem('NOMBRESAPELLIDOS');
                      localStorage.removeItem('TOKEN');
                      localStorage.removeItem('SESSIONACTIVA');
                      localStorage.removeItem('consultaFechaBuro');
                      localStorage.setItem('SESSIONACTIVA', 'false');
                      this.navController.navigateRoot('login');
                    }
                  }
                ]
              }).then(alert => alert.present());
            }

            if (consultarBuro) {
              this.datosusuarios();
            }

          } else if (response['code'] == 201) {
            const alert = this.alertCtrl.create({
              subHeader: "Aviso",
              message: response['msg'],
              buttons: [
                {
                  text: "Ok",
                  role: 'cancel'
                }
              ]
            }).then(alert => alert.present());
          }//Fin else
        });//FIN LOADING DISS
      }, error => {
        load2.dismiss().then(() => {
          const alert = this.alertCtrl.create({
            subHeader: "Aviso",
            message: "No pudo conectarse al servidor",
            buttons: [
              {
                text: "Reintentar",
                role: 'cancel',
                cssClass: 'ion-aceptar',
                handler: data => {
                  this.provider_menu.logerroresadd("Menu", JSON.stringify(error)).subscribe((response) => { });//FIN LOADING DISS
                  this.ionViewDidEnter();
                }
              }
            ]
          }).then(alert => alert.present());
        });//FIN LOADING DISS
      });//FIN POST
    });//FIn LOADING

  }//FIN FcuntiN
  regresar() {
    this.navController.back();
  }
  continuar() {
    //alert(JSON.stringify(this.checkbox));
    var contar = 0;
    this.checkbox.forEach(function (value) {
      if (value != 0) {
        contar++;
      }
    });
    if (contar != 0) {
      this.guardarCredoLabReferenceNumber();
      localStorage.setItem('checkboxdeuda', this.checkbox);
      this.navController.navigateForward('principal/localizardeuda1');
    } else {
      const alert = this.alertCtrl.create({
        subHeader: "Aviso",
        message: "Seleccione la deuda",
        buttons: [
          {
            text: "Continuar",
            role: 'cancel',
            cssClass: 'ion-aceptar',
            handler: data => {
            }
          }
        ]
      }).then(alert => alert.present());
    }
    //this.router.navigate(['menudeudas1', this.checkbox]);
  }
  actualiza(a, b) {
    if (this.checkbox[a] == 0) {
      this.checkbox[a] = b;
    } else {
      this.checkbox[a] = 0;
    }
    this.continuar();
    console.log(a + ' - ' + JSON.stringify(this.checkbox));

  }
  linkmenu(var1, i) {
    this.selectedIndex = i;
    if (var1 == "logout") {
      localStorage.removeItem('IDUSER');
      localStorage.removeItem('USUARIO');
      localStorage.removeItem('NOMBRESAPELLIDOS');
      localStorage.removeItem('TOKEN');
      localStorage.removeItem('SESSIONACTIVA');
      localStorage.removeItem('consultaFechaBuro');
      localStorage.setItem('SESSIONACTIVA', 'false');
      this.navController.navigateRoot('login');
      this.googlePlus.logout();
      this.fb.logout();
    } else if (var1 == "servicio") {

      this.socialSharing.shareViaWhatsAppToReceiver('+1 (809) 408-0064', '', null, null);

    } else if (var1 == "notificacion") {

      this.navController.navigateForward('perfilnotificaciones');

    } else if (var1 == "top") {

      this.navController.navigateForward('perfiltop');

    } else if (var1 == "compartir") {

      this.bodys = "¡Hola! Descárgate nuestra app de pagadores de descuento en nuestros servicios.";
      this.socialSharing.share(this.bodys, null, null, null);

    } else if (var1 == "perfil") {

      this.navController.navigateForward('perfil');

    } else if (var1 == "agregar") {

      this.navController.navigateForward('agregadeuda1');

    } else {

      this.navController.navigateForward(var1);

    }
  }//fin function

  datosusuarios() {
    //console.log(this.datos_usuarios);
    if (this.datos_usuarios['sexo']) {
      //console.log(this.datos_usuarios['sexo']);

      const loader = this.loadingCtrl.create({
        message: "Un momento por favor..."
      }).then(load2 => {
        load2.present();
        let customerid = localStorage.getItem('CUSTOMERID');
        let dataUser = this.datos_usuarios;
        this.provider_menu.put_customers_id(customerid, dataUser['nombres'], dataUser['apellidos'], dataUser['email'], dataUser['ci'], dataUser['telefono'], dataUser['fechanacimiento'], dataUser['nacionalidad'], dataUser['sexo'], dataUser['estadocivil']).subscribe((response) => {
          load2.dismiss().then(() => {
            if (!response['error']) {

            } else {
              const alert = this.alertCtrl.create({
                subHeader: "Aviso",
                message: response['msg'],
                buttons: [
                  {
                    text: "Ok",
                    role: 'cancel'
                  }
                ]
              }).then(alert => alert.present());
            }//Fin else
          });//FIN LOADING DISS
        }, error => {
          load2.dismiss().then(() => {
            const alert = this.alertCtrl.create({
              subHeader: "Aviso",
              message: "No pudo conectarse al servidor",
              buttons: [
                {
                  text: "Reintentar",
                  role: 'cancel',
                  cssClass: 'ion-aceptar',
                  handler: data => {
                    this.provider_menu.logerroresadd("Menu", JSON.stringify(error)).subscribe((response) => { });//FIN LOADING DISS
                    this.datosusuarios();
                  }
                }
              ]
            }).then(alert => alert.present());
          });//FIN LOADING DISS
        });//FIN POST
      });//FIn LOADING
    }
  }

  CredolabDatasets() {
    let customer_id = localStorage.getItem('CUSTOMERID');
    if(customer_id){
      this.provider_menu.credolabDatasets(customer_id).subscribe((response) => {
        if(!response['error']){
          localStorage.setItem('credolabscores', response['scores']);
          console.log('Credolab Datasets: ' + JSON.stringify(response));
        } else {
          console.log('ERROR Credolab Datasets: ' + JSON.stringify(response));
        }
      });//FIN POST
    }
  }

  guardarCredoLabReferenceNumber(){
    let credolabcollect = localStorage.getItem('credolabcollect');
    let customer_id = localStorage.getItem('CUSTOMERID');
    console.log('credolabcollect: ' + credolabcollect);
    console.log('customer_id: ' + customer_id);
    if(credolabcollect && credolabcollect !== 'undefined' && credolabcollect !== 'null' && credolabcollect !== null){
      this.provider_menu.credolabcollect(customer_id, credolabcollect).subscribe((response) => {
        console.log('credolabcollect: ' + JSON.stringify(response));
        
        this.CredolabDatasets();

      });//FIN POST
    } else {
      if (!this.platform.is('mobileweb')) {
        this.credolabcollect(customer_id);
      }
    }
  }

  credolabcollect(recordNumber) {
    console.log('collect');
    let url = this.imgurl.getCredolabUrl();
    let auth = this.imgurl.getCredolabAuth();

    exec(function (succes) {
      console.log('succes ' + succes);
      localStorage.setItem('credolabcollect', succes);
      this.guardarCredoLabReferenceNumber();
      //alert("Data has been successfully uploaded:" + succes);
    },
      function (error) {
        console.log('error ' + error);
        //alert("Error: " + error)
      },
      "CredoAppSdk",
      "collect",
      [{
        url: url,
        authKey: auth,
        recordNumber: recordNumber
      }]
    );
  }

}//FIN CLASS