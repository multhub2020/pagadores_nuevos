import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl} from '@angular/forms';
import { Menus } from '../../providers/menus';
import { Router } from '@angular/router';
import { NavController, LoadingController, AlertController } from '@ionic/angular';
import { Variablesglobales } from '../../providers/variablesglobal';
import { Usuario } from '../../providers/usuario';

@Component({
  selector: 'app-loginregistro7',
  templateUrl: 'loginregistro7.html',
  styleUrls: ['loginregistro7.scss'],
  providers:[Usuario, Variablesglobales, Menus]
})

export class Loginregistro7 {
  public myForm: FormGroup;
  public loading: any;
  public usuarioregistro: string;

  public t_push: string;
  public p_push: string;
  public u_push: string;
  public url = new Variablesglobales();
  public usuarioemail;
  public usuarioclave;
  public usuarioregistroid;
  public usuariocedula;
  public usuariocelular;
  public usuarionombres="";
  public usuarioapellidos="";

  public inicio=0;  
  public timeLeft =30;
  public timeLeft2='30';
  public interval=0;
  constructor(public navController: NavController,
  			  public formBuilder: FormBuilder,
  			  private router: Router,
  			  private provider_usuario: Usuario,
  			  public alertCtrl: AlertController,
  			  public loadingCtrl: LoadingController,
          private provider_menu: Menus
  			  ) {
  }
  ionViewDidEnter() {
      this.usuarioemail      = localStorage.getItem('USUARIOEMAIL');
      this.usuarioclave      = localStorage.getItem('USUARIOCLAVE');
      this.usuarioregistroid = localStorage.getItem('USUARIOREGISTROID');
      this.usuariocedula     = localStorage.getItem('USUARIOCEDULA');
      this.usuariocelular    = localStorage.getItem('USUARIOCELULAR')
      this.usuarionombres    = localStorage.getItem('USUARIONOMBRES');
      this.usuarioapellidos  = localStorage.getItem('USUARIOAPELLIDOS');
      /*const loader = this.loadingCtrl.create({
        message: "Un momento por favor..."
      }).then(load => {
                load.present();*/
                this.provider_usuario.api_pagadores_customers(this.usuariocedula,
                                this.usuariocelular,
                                this.usuarioemail,
                                this.usuarioclave,
                                this.usuarioregistroid,
                                this.usuarionombres,
                                this.usuarioapellidos
                                ).subscribe((response_ai) => {  
                                  //alert(response_ai['data']['customer_id']);
                                  //console.log(response_ai);
                                                                    this.provider_usuario.loginregistrocedula(this.usuarioregistroid, this.usuariocedula, response_ai['data']['customer_id']).subscribe((response) => {  
                                                                                                  //this.loadingCtrl.dismiss().then( () => { 
                                                                                                          if(response['code']==200){
                                                                                                                    this.provider_usuario.api_pagadores_loginclave().subscribe((responseapipagadores) => {  
                                                                                                                            localStorage.setItem('IDUSER',           response['datos']['id']);
                                                                                                                            localStorage.setItem('NOMBRESAPELLIDOS', response['datos']['nombre_apellido']);
                                                                                                                            localStorage.setItem('TIPOUSUARIO',      response['datos']['role_id']);
                                                                                                                            localStorage.setItem('FOTOPERFIL',       this.url.getApivarcoreHomeI()+'api/storage/imagenes'+response['datos']['foto2']);
                                                                                                                            localStorage.setItem('CUSTOMERID',       response['datos']['customer_id']);
                                                                                                                            localStorage.setItem('credolabcollect',  response['datos']['credolabcollect']);
                                                                                                                            localStorage.setItem('credolabscores',   response['datos']['credolabscores']);
                                                                                                                            localStorage.setItem('TOKEAPIPAGADORES',       responseapipagadores["access_token"]);
                                                                                                                            localStorage.setItem('SESSIONACTIVA','true');
                                                                                                                            this.t_push = localStorage.getItem('T_PUSH');
                                                                                                                            this.p_push = localStorage.getItem('P_PUSH');
                                                                                                                            this.provider_usuario.actualizarpush(this.t_push, this.p_push, response['datos']['id'] ).subscribe((response2) => {});
                                                                                                                            //this.startTimer();
                                                                                                                             /*const alert = this.alertCtrl.create({
                                                                                                                                subHeader: '',
                                                                                                                                message: "Tu cuenta se créo correctaente",
                                                                                                                                buttons: [
                                                                                                                                  {
                                                                                                                                      text: "Continuar",
                                                                                                                                      role: 'cancel',
                                                                                                                                      cssClass:'ion-pagar2',
                                                                                                                                      handler: data => {
                                                                                                                                          this.link();
                                                                                                                                      }

                                                                                                                                  }
                                                                                                                                ]
                                                                                                                            }).then(alert => alert.present());*/
                                                                                                                    },error => {
                                                                                                                            const alert = this.alertCtrl.create({
                                                                                                                                subHeader: "Aviso",
                                                                                                                                message: "No pudo conectarse al servidor",
                                                                                                                                buttons: [
                                                                                                                                  {
                                                                                                                                      text: "Reintentar",
                                                                                                                                      role: 'cancel',
                                                                                                                                      cssClass:'ion-aceptar',
                                                                                                                                      handler: data => {
                                                                                                                                          this.provider_menu.logerroresadd("login registro 7", JSON.stringify(error)).subscribe((response) => { });//FIN LOADING DISS
                                                                                                                                          this.ionViewDidEnter();
                                                                                                                                      }
                                                                                                                                  }
                                                                                                                                ]
                                                                                                                            }).then(alert => alert.present());
                                                                                                                  });//FIN POST
                                                                                                          }else if (response['code']==201){
                                                                                                                      const alert = this.alertCtrl.create({
                                                                                                                        subHeader: "Aviso",
                                                                                                                          message: response['msg'],
                                                                                                                          buttons: [
                                                                                                                            {
                                                                                                                              text: "Ok", 
                                                                                                                              role: 'cancel'
                                                                                                                            }
                                                                                                                          ]
                                                                                                                      }).then(alert => alert.present());
                                                                                                          }//Fin else
                                                                                                 // });//FIN LOADING DISS
                                                                    },error => {
                                                                          //this.loadingCtrl.dismiss().then( () => {
                                                                                    const alert = this.alertCtrl.create({
                                                                                        subHeader: "Aviso",
                                                                                        message: "No pudo conectarse al servidor",
                                                                                        buttons: [
                                                                                          {
                                                                                              text: "Reintentar",
                                                                                              role: 'cancel',
                                                                                              cssClass:'ion-aceptar',
                                                                                              handler: data => {
                                                                                                this.provider_menu.logerroresadd("login registro 7", JSON.stringify(error)).subscribe((response) => { });//FIN LOADING DISS
                                                                                                  this.ionViewDidEnter();
                                                                                              }
                                                                                          }
                                                                                        ]
                                                                                    }).then(alert => alert.present());
                                                                        //});//FIN LOADING DISS
                                                                    });//FIN POST
                },error => {

              
                                                                    //this.loadingCtrl.dismiss().then( () => {
                                                                              const alert = this.alertCtrl.create({
                                                                                  subHeader: "Aviso",
                                                                                  message: "No pudo conectarse al servidor",
                                                                                  buttons: [
                                                                                    {
                                                                                        text: "Reintentar",
                                                                                        role: 'cancel',
                                                                                        cssClass:'ion-aceptar',
                                                                                        handler: data => {
                                                                                            this.provider_menu.logerroresadd("login registro 7", JSON.stringify(error)).subscribe((response) => { });//FIN LOADING DISS
                                                                                            this.ionViewDidEnter();
                                                                                        }
                                                                                    }
                                                                                  ]
                                                                              }).then(alert => alert.present());
                                                                  //});//FIN LOADING DISS
                });//FIN POST api_pagadores_customers
          //});//FIn LOADING

  }
  link(){
                          let contarinicio   = '';
                          let contarinicio2  = 0;
                          contarinicio  = localStorage.getItem('contarinicio_1_1');
                          contarinicio2 = parseInt(localStorage.getItem('contarinicio_1_1'));
                          contarinicio2++;  contarinicio2++;
                          localStorage.setItem('contarinicio_1_1', contarinicio2+'')
                          this.navController.navigateRoot('principal/inicio/'+contarinicio2);
  }
  startTimer() {
    this.interval = setInterval(() => {
      if(this.timeLeft > 0) {
        this.timeLeft--;
      } else {
        clearInterval(this.interval);
        let contarinicio   = '';
                          let contarinicio2  = 0;
                          contarinicio  = localStorage.getItem('contarinicio_1_1');
                          contarinicio2 = parseInt(localStorage.getItem('contarinicio_1_1'));
                          contarinicio2++;  contarinicio2++;
                          localStorage.setItem('contarinicio_1_1', contarinicio2+'')
                          this.navController.navigateRoot('principal/inicio/'+contarinicio2);
      }
    },1000)
  }
}//FIN CLASS
