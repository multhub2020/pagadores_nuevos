import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';

import { Router } from '@angular/router';
import { NavController, LoadingController, AlertController } from '@ionic/angular';

import { Menus } from '../../providers/menus';

@Component({
  selector: 'app-localizardeuda1_2',
  templateUrl: 'localizardeuda1_2.html',
  styleUrls: ['localizardeuda1_2.scss'],
  providers: [Menus]
})

export class Localizardeuda1_2 {
  public myForm: FormGroup;
  public loading: any;
  public usuarioregistro: string;
  public usuarioid: string;
  public t_push: string;
  public p_push: string;
  public u_push: string;
  public mensaje1 = "";
  public mensaje2 = "";
  public inicio = 0;
  public timeLeft = 1;
  public timeLeft2 = '1';
  public interval = 0;
  public checkbox: any;
  public currency: string;
  constructor(public navController: NavController,
    public formBuilder: FormBuilder,
    private router: Router,
    private provider: Menus,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController
  ) {

    this.usuarioid = localStorage.getItem('IDUSER');
  }
  ionViewDidEnter() {
    this.mensaje1 = localStorage.getItem('localizardeuda1_1_mensaje1');
    this.mensaje2 = localStorage.getItem('localizardeuda1_1_mensaje2');
    this.currency = localStorage.getItem('localizardeuda1_1_mensaje3');
    this.mensaje2 = this.mensaje2.replace('"', '');
    this.mensaje2 = this.mensaje2.replace('"', '');

    this.currency = this.currency.replace('"', '');
    this.currency = this.currency.replace('"', '');
   // this.startTimer();
      this.iniciar();
  }

  iniciar(){

          const alert = this.alertCtrl.create({
              subHeader: "¿Estás seguro de enviar",
              message: "esta oferta? ",
              buttons: [
                {
                    text: "Quiero modificar mi oferta",
                    cssClass:'ion-pagar3',
                    handler: data => {
                        
                        this.navController.back();
                    }
                },
                {
                    
                    text: "Si, estoy seguro",
                    role: 'cancel',
                    cssClass:'ion-pagar2',
                    handler: data => {
                        this.verificar();
                    }
                }
                
              ]
          }).then(alert => alert.present());
  }
  startTimer() {
    this.interval = setInterval(() => {
      if (this.timeLeft > 0) {
        this.timeLeft--;
      } else {
        this.timeLeft = 1;
        clearInterval(this.interval);
        console.log("saldo: "+this.mensaje2);
        this.verificar();
        /*
        const alert = this.alertCtrl.create({
            subHeader: "¡Felicitaciones!",
            message: "Hemos encontrado que puedes pagar esta",
            buttons: [
              {
                  text: "En otro momento",
                  role: 'cancel',
                  cssClass:'ion-aceptar',
                  handler: data => {
                      //this.navController.navigateForward('login');
                      this.verificar();
                  }
              },
              {
                  text: "Pagar",
                  cssClass:'ion-pagar',
                  handler: data => {
                      this.navController.navigateForward('principal/localizardeuda1_3');
                  }
              }
            ]
        }).then(alert => alert.present());*/
      }
    }, 3000)
  }
  verificar() {
    this.checkbox = localStorage.getItem('checkboxdeuda');
    this.provider.deudaslista(this.usuarioid, this.checkbox).subscribe((response) => {
      if (response['code'] == 200) {
        if (response['contar'] != 0) {
          this.navController.navigateForward('principal/localizardeuda1');
        } else {
          ///AQUI COLOCAR LO DE LA CONSULTA
          let contarinicio   = '';
          let contarinicio2  = 0;
          contarinicio  = localStorage.getItem('contarinicio_1_1');
          contarinicio2 = parseInt(localStorage.getItem('contarinicio_1_1'));
          contarinicio2++;  contarinicio2++;
          localStorage.setItem('contarinicio_1_1', contarinicio2+'')
          this.navController.navigateForward('principal/inicio/'+contarinicio2);
        }
      }
    });
  }//fin function
}//FIN CLASS
