import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Localizardeuda1_2 } from './localizardeuda1_2';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild([{ path: '', component: Localizardeuda1_2 }])
  ],
  declarations: [Localizardeuda1_2]
})
export class Localizardeuda1_2Module {}
