import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Pagarcuentadeposito } from './pagarcuentadeposito';

import { IonicSelectableModule } from 'ionic-selectable';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild([{ path: '', component: Pagarcuentadeposito }]),
    IonicSelectableModule
  ],
  declarations: [Pagarcuentadeposito]
})
export class PagarcuentadepositoModule { }
