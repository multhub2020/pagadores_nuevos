import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';

import { Router, ActivatedRoute } from '@angular/router';
import { NavController, LoadingController, AlertController } from '@ionic/angular';

import { Menus } from '../../providers/menus';

import { Perfils } from '../../providers/perfils';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { Crop, CropOptions } from '@ionic-native/crop/ngx';
import { File } from '@ionic-native/file/ngx';
import { EmailComposer } from '@ionic-native/email-composer/ngx';
import { IonicSelectableComponent } from 'ionic-selectable';
import { Platform } from '@ionic/angular'; 

class Port {
  public id: string;
  public name: string;
  public identification_number: string;
}

@Component({
  selector: 'app-pagarcuentadeposito',
  templateUrl: 'pagarcuentadeposito.html',
  styleUrls: ['pagarcuentadeposito.scss'],
  providers: [Menus, Camera, File, Crop]
})

export class Pagarcuentadeposito {
  public datosdeudas: any = [''];
  public numeroTransaccion: any;
  public banco: string;
  public montoDepositado = "0";
  public Amount: string;
  public Currency: any;
  public DATA: string;

  public activa = '1';

  public myForm: FormGroup;
  public loading: any;
  public usuarioregistro: string;
  public usuario: string;
  public usuarioid: string;
  public customerid: string;
  public t_push: string;
  public p_push: string;
  public u_push: string;
  public mensaje1 = "";
  public mensaje2 = "";
  public inicio = 0;
  public timeLeft = 1;
  public timeLeft2 = '1';
  public interval = 0;
  public checkbox: any;

  public image: string[] = ['', '', '', ''];
  public image1: string = '';
  public croppedImagepath = "";
  public isLoading = false;
  public cropOptions: CropOptions = {
    quality: 100
  }
  public fotoperfil = "";
  public currentImagen = null;
  public currentImagenEmail = null;

  ports: Port[];
  port: Port;

  constructor(public navController: NavController,
    public formBuilder: FormBuilder,
    private router: Router,
    private provider: Menus,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    private route: ActivatedRoute,
    private camera: Camera,
    private crop: Crop,
    private file: File,
    private emailComposer: EmailComposer,
    private provider_menu: Menus,
    public platform: Platform
  ) {
    this.myForm = this.formBuilder.group({
      /*numeroTransaccion: new FormControl('', Validators.compose([
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(30)
      ])),
      banco: new FormControl('', Validators.compose([
        Validators.required,
      ])),*/
      montoDepositado: new FormControl('', Validators.compose([
        Validators.minLength(1),
        Validators.maxLength(30)
      ]))
    });

    this.usuario = localStorage.getItem('NOMBRESAPELLIDOS');
    this.usuarioid = localStorage.getItem('IDUSER');
    this.customerid = localStorage.getItem('CUSTOMERID');
    this.checkbox = localStorage.getItem('localizardeuda1_1_id1');
    this.Currency = 214;

    this.ports = [
      { id: 'Trebol', name: 'Trebol', identification_number: '' },
      { id: 'Scotiabank', name: 'Scotiabank', identification_number: '' },
      { id: 'Banco BHD León', name: 'Banco BHD León', identification_number: '' },
      { id: 'Banco Popular', name: 'Banco Popular', identification_number: '' }
    ];
  }

  vaciar() {
    if (this.montoDepositado == '0') { this.montoDepositado = ""; }
  }
  vaciar2() {

    if (this.montoDepositado == '') { this.montoDepositado = '0'; }
  }

  getText(e) {
    var elementValue = e.srcElement.value;
    if (elementValue) {
      var regex = /^[0-9., ]+$/;
      var tempValue = elementValue.substring(0, elementValue.length - 1);
      if (!regex.test(elementValue)) {
        console.log("Entered char is not alphabet");
        e.srcElement.value = tempValue;
      }
    }
  }

  portChange(event: {
    component: IonicSelectableComponent,
    value: any
  }) {
    console.log('port:', event.value);
    this.banco = event.value.id;
  }

  /*ionViewDidEnter() {
    const loader = this.loadingCtrl.create({
      message: "Un momento por favor..."
    }).then(load2 => {
      load2.present();
      let application_id = this.route.snapshot.paramMap.get('id');

      this.provider.get_payment_methods().subscribe((response) => {
        console.log(response);
      });//FIN POST

      this.provider.get_all_creditors().subscribe((response) => {
        //console.log(response);
        load2.dismiss().then(() => {
          if (!response['error']) {
            let allCreditors = response['results'];
            allCreditors.forEach((element, index) => {
              this.ports[index] = {
                id: element.id,
                name: element.name,
                identification_number: element.identification_number
              };
            });

            this.provider.get_applications(application_id).subscribe((response) => {
              load2.dismiss().then(() => {
                if (!response['error']) {
                  this.datosdeudas = response['data'];
                  console.log(JSON.stringify(this.datosdeudas));
                } else {
                  const alert = this.alertCtrl.create({
                    subHeader: "Aviso",
                    message: response['msg'],
                    buttons: [
                      {
                        text: "Ok",
                        role: 'cancel'
                      }
                    ]
                  }).then(alert => alert.present());
                }//Fin else
              });//FIN LOADING DISS
            }, error => {
              load2.dismiss().then(() => {
                const alert = this.alertCtrl.create({
                  subHeader: "Aviso",
                  message: "No pudo conectarse al servidor",
                  buttons: [
                    {
                      text: "Reintentar",
                      role: 'cancel',
                      cssClass: 'ion-aceptar',
                      handler: data => {
                        this.provider_menu.logerroresadd("Pagar cuenta", JSON.stringify(error)).subscribe((response) => { });//FIN LOADING DISS
                        //this.ionViewDidEnter();
                      }
                    }
                  ]
                }).then(alert => alert.present());
              });//FIN LOADING DISS
            });//FIN POST

          } else {
            const alert = this.alertCtrl.create({
              subHeader: "Aviso",
              message: "No pudo conectarse al servidor",
              buttons: [
                {
                  text: "Reintentar",
                  role: 'cancel',
                  cssClass: 'ion-aceptar',
                  handler: data => {
                    this.provider_menu.logerroresadd("Pagar cuenta", JSON.stringify(error)).subscribe((response) => { });//FIN LOADING DISS
                    //this.ionViewDidEnter();
                  }
                }
              ]
            }).then(alert => alert.present());
          }//Fin else
        });//FIN LOADING DISS
      });

    });//FIn LOADING
  }*/
  ngOnInit() {
  }
  quitarresena() {

  }
  enviarformulario() {

  }
  regresar() {
    this.navController.back();
  }

  informacion(var1) {
    if (var1 == 1) {
      this.navController.navigateForward('inforcondiciones');
    } else if (var1 == 2) {
      this.navController.navigateForward('inforpoliticas');
    } else if (var1 == 3) {
      this.navController.navigateForward('loginrecuperar1');
    } else if (var1 == 4) {
      this.navController.navigateForward('loginclave');
    }
  }

  isset(ref) { return typeof ref !== 'undefined' }

  cambiarimagengaleria() {
    this.getPicture(this.camera.PictureSourceType.PHOTOLIBRARY);
    this.activa = '2';
    /*const alert = this.alertCtrl.create({
      subHeader: "Cambiar imagen",
      buttons: [
        {
          text: "Abrir Galeria",
          cssClass: 'ion-aceptar',
          handler: data => {
            this.getPicture(this.camera.PictureSourceType.PHOTOLIBRARY);
          }
        }
      ]
    }).then(alert => alert.present());*/
  }

  cambiarimagencamara() {
    this.getPicture(this.camera.PictureSourceType.CAMERA);
    /*const alert = this.alertCtrl.create({
      subHeader: "Cambiar imagen",
      buttons: [
        {
          text: "Abrir Camara",
          cssClass: 'ion-aceptar',
          handler: data => {
            this.getPicture(this.camera.PictureSourceType.CAMERA);
          }
        }
      ]
    }).then(alert => alert.present());*/
  }

  cambiarimagen() {
    const alert = this.alertCtrl.create({
      subHeader: "Cambiar imagen",
      message: "Seleccione una opcion",
      buttons: [
        {
          text: "Galeria",
          cssClass: 'ion-cancelar-small',
          handler: data => {
            this.getPicture(this.camera.PictureSourceType.PHOTOLIBRARY);
          }
        },
        {
          text: "Camara",
          cssClass: 'ion-aceptar-small',
          handler: data => {
            this.getPicture(this.camera.PictureSourceType.CAMERA);
          }
        }
      ]
    }).then(alert => alert.present());
  }
  getPicture(sourceType) {
    let options: CameraOptions = {
      destinationType: this.camera.DestinationType.FILE_URI,
      //targetWidth: 1080,
      //targetHeight: 1080,
      quality: 100,
      correctOrientation: true,
      cameraDirection: this.camera.Direction.BACK,
      sourceType: sourceType
    }
    this.camera.getPicture(options)
      .then(imageData => {
        this.image1 = 'data:image/jpeg;base64,' + imageData;
        this.image[0] = this.image1;
        //this.p_push
        if (!this.platform.is('mobileweb')) {
          this.cropImage(imageData);
        }
        
        this.currentImagen = 'data:image/jpeg;base64,' + imageData;
        this.currentImagenEmail = 'base64:image.jpeg//' + imageData;
      })
      .catch(error => {
        console.error(error);
      });
  }//FIN FUNCTION
  cropImage(fileUrl) {
    this.crop.crop(fileUrl, this.cropOptions)
      .then(
        newPath => {
          this.showCroppedImage(newPath.split('?')[0])
        },
        error => {
          //alert('Error cropping image' + error);
        }
      );
  }
  showCroppedImage(ImagePath) {
    this.isLoading = true;
    var copyPath = ImagePath;
    var splitPath = copyPath.split('/');
    var imageName = splitPath[splitPath.length - 1];
    var filePath = ImagePath.split(imageName)[0];
    this.file.readAsDataURL(filePath, imageName).then(base64 => {
      this.croppedImagepath = base64;
      this.isLoading = false;
      this.image1 = base64;
      this.image[0] = this.image1;
      //this.updatefoto();

      this.currentImagen = base64;
    }, error => {
      //alert('Error in showing image' + error);
      this.isLoading = false;
    });
  }
  continuar() {
    const loader = this.loadingCtrl.create({
      ////duration: 10000
      //message: "Un momento por favor..."
    }).then(load => {
      load.present();
      let application_id = this.route.snapshot.paramMap.get('id');
      if(this.montoDepositado==""){this.montoDepositado="0";}
      this.numeroTransaccion="0";
      this.banco="0";
      this.provider.depositos(this.usuarioid, this.numeroTransaccion, this.banco, this.montoDepositado, this.customerid, application_id, this.image1).subscribe((response) => {
        this.loadingCtrl.dismiss().then(() => {
          if (response['code'] == 200) {
            this.fotoperfil = response['data']['imagen'];
            this.navController.navigateForward('principal/localizardeuda1_6');
          } else if (response['code'] == 201) {
            const alert = this.alertCtrl.create({
              subHeader: "Aviso",
              message: response['msg'],
              buttons: [
                {
                  text: "Ok",
                  role: 'cancel'
                }
              ]
            }).then(alert => alert.present());
          }//Fin else
        });//FIN LOADING DISS
      }, error => {
        this.loadingCtrl.dismiss().then(() => {
          const alert = this.alertCtrl.create({
            subHeader: "Aviso",
            message: "Disculpe, no se conecto al servidor",
            buttons: [
              {
                text: "Reintentar",
                role: 'cancel',
                cssClass: 'ion-aceptar',
                handler: data => {
                  //this.ionViewDidEnter();
                }
              }
            ]
          }).then(alert => alert.present());
        });//FIN LOADING DISS
      });//FIn LOADING
    });
  }//FIN FUNCTION

}//FIN CLASS
