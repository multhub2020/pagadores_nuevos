import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Verdetallecuenta } from './verdetallecuenta';
import { SuperTabsModule } from '@ionic-super-tabs/angular';
import { NgxMaskIonicModule } from 'ngx-mask-ionic';
import { NgxCurrencyModule } from "ngx-currency";

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SuperTabsModule,
    NgxMaskIonicModule,
    NgxCurrencyModule,
    RouterModule.forChild([{ path: '', component: Verdetallecuenta }])
  ],
  declarations: [Verdetallecuenta]
})
export class VerdetallecuentaModule {}
