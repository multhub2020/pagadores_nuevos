import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NavController, LoadingController, AlertController, Platform } from '@ionic/angular';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Menus } from '../../providers/menus';
import { Usuario } from '../../providers/usuario';
import { SuperTabs } from "@ionic-super-tabs/angular";
import { Variablesglobales } from '../../providers/variablesglobal';
import { ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-verdetallecuenta',
  templateUrl: 'verdetallecuenta.html',
  styleUrls: ['verdetallecuenta.scss'],
  providers: [Menus, Usuario, Variablesglobales]
})

export class Verdetallecuenta {
  //myPage = Mapa;
  @ViewChild(SuperTabs, { static: false }) superTabs: SuperTabs;
  public versionapp = "1.0.5";
  public datosdeudas: any = [''];
  public data: any;
  public datos_publi: any;
  public selectedIndex = 0;
  public usuario: string;
  public usuarioid: string;
  public customerid: string;
  public bodys: string;
  public myForm: FormGroup;
  public imgurl = new Variablesglobales();
  public imgurl2: any;
  public listaContactos: any = [];
  public listaContactos2: any = [];
  //Variables para calcularr monto
  public checkbox_re: any = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
  public checkbox_mon: any = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
  public checkbox_cuo: any = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1];
  public checkbox_pro: any = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1];
  ///////////////////////////////
  public datosMostar: any = [];
  public datosMostar2: any = [];
  public checkbox: any;
  public avatar: string = "";
  public invitados = 0;
  public contador_c = 0;
  public seguidores = 0;
  public color = "";
  public i = 0;
  public value = 0;
  public finalizar = 1;
  public slideOpts = {
    effect: 'flip',
    autoplay: {
      delay: 5000
    }
  };
  public invitado = "";
  public appPages = [
    {
      title: 'Sugerencias',
      url: 'perfilsugerencias',
      icon: 'mail'
    }
  ];
  public cargandoBtn = false;
  public pending_loader = null;
  public inicio = 1;

  public creditor_instruccion = null;

  constructor(private router: Router,
    public formBuilder: FormBuilder,
    private navController: NavController,
    private provider_menu: Menus,
    private provider_usuario: Usuario, 
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    private platform: Platform,
    private rutaActiva: ActivatedRoute
    //public supertabs: SuperTabs
  ) {
    this.usuario = localStorage.getItem('NOMBRESAPELLIDOS');
    this.usuarioid = localStorage.getItem('IDUSER');
    this.customerid = localStorage.getItem('CUSTOMERID');
    this.myForm = this.formBuilder.group({
      usuario: new FormControl(0, Validators.compose([
        Validators.required,
        Validators.pattern('^[0-9 ]+$'),
      ])
      )

    });
  }//FIN FUncTION
  inf(v) {
    this.finalizar = v;
  }
  seleccionradio(a, b) {
    //alert(a+' - '+b);
    this.checkbox_re[a] = b;
  }
  resolver(a, b) {
    if (this.finalizar == 1) {
      this.checkbox_pro[a] = b;
    }
  }
  quitarresena() {
    this.inicio = 1;
  }
  ionViewDidEnter() {

    this.provider_menu.get_customer_accounts_by_customer(this.customerid).subscribe((response) => {
      console.log(response['results']);
    });
    this.checkbox = this.rutaActiva.snapshot.paramMap.get('id');
    const loader = this.loadingCtrl.create({
      message: "Un momento por favor..."
    }).then(load2 => {
      load2.present();
      this.provider_menu.get_applications(this.checkbox).subscribe((response) => {
        console.log(response);
        load2.dismiss().then(() => {
          if (!response['error']) {
                    this.imgurl2 = this.imgurl.getServar();
                    this.data = [response['data']];
                    console.log(this.data);
                    this.provider_menu.get_creditors(response['data']['creditor_id']).subscribe((responseCreditor) => {
                              //console.log(responseCreditor);
                                  this.data[0]['identifica_banco'] = responseCreditor['data']['identification_number'];
                                  this.data[0]['banco_deuda']      = responseCreditor['data']['name'];
                                       
                                         if ((response['data']['status']  == 'CAPOS' || response['data']['status']  == 'A') && response['data']['product_type_id']== 1) {
                                  } else if ((response['data']['status']  == 'CAPOS' || response['data']['status']  == 'A')  && response['data']['product_type_id']== 2) {
                                  } else if (response['data']['status'] == 'CO' || response['data']['status'] == 'COL') {
                                  } else if (response['data']['status'] == 'PP' && response['data']['product_type_id']== 1) {
                                  } else if (response['data']['status'] == 'PP' && response['data']['product_type_id']== 2) {
                                  } else if (response['data']['status'] == 'LC' && response['data']['product_type_id']== 2) {
                                  } else if (response['data']['status'] == 'WSA' && response['data']['product_type_id']== 2) {
                                  } else if (response['data']['status'] == 'C') {
                                  } else if (response['data']['status'] == 'AwaitingSignatureByTheCustomer') {
                                  //} else if (response['data']['status'] == 'M') {
                                  } else {
                                          //this.data[0]['banco_deuda']  = response['data']['comment'];
                                          this.provider_usuario.listaplication(this.checkbox).subscribe((responsedata) => {
                                                console.log('datos: ');
                                                console.log(responsedata);
                                                if(responsedata['contar'].length==0){

                                                }else{

                                                    this.data[0]['banco_deuda'] = responsedata['contar'][0]['banco_deuda'];
                                                }

                                          });
                                  }
                    });//FIN POST

                    if (response['data']['status'] == 'CAPOS' || response['data']['status'] == 'PP' || response['data']['status'] == 'A') {
                      this.provider_menu.localizarinstruccion(response['data']['creditor_id']).subscribe((responseInstrucciones) => {
                        console.log(responseInstrucciones);
                        if (!responseInstrucciones['error']) {
                          this.creditor_instruccion = responseInstrucciones['data']['instruccion'];
                        }
                      });//FIN POST
                    }

                    //if (response['data']['status'] == 'CAPOS' && response['data']['product_type_id'] == 1) {
                    if ((response['data']['status']  == 'CAPOS' || response['data']['status']  == 'A')   &&  response['data']['product_type_id'] == 1) {
                      this.data[0]['status_text'] = 'Pendiente de Pago Convenido';
                    //} else if (response['data']['status'] == 'CAPOS' && response['data']['product_type_id'] == 2) {
                    } else if ((response['data']['status']  == 'CAPOS' || response['data']['status']  == 'A')   &&  response['data']['product_type_id'] == 2) {
                      this.data[0]['status_text'] = 'Aprobada';
                    } else if (response['data']['status'] == 'CO' || response['data']['status'] == 'COL') {
                      this.data[0]['status_text'] = 'Contra Oferta';
                    } else if (response['data']['status'] == 'PP' && response['data']['product_type_id'] == 1) {
                      this.data[0]['status_text'] = 'Pendiente de Pago Convenido';
                    } else if (response['data']['status'] == 'PP' && response['data']['product_type_id'] == 2) {
                      this.data[0]['status_text'] = 'Aprobada';                   
                    } else if (response['data']['status'] == 'LC' && response['data']['product_type_id'] == 2) {
                      this.data[0]['status_text'] = 'Aprobada';                    
                    } else if (response['data']['status'] == 'WSA' && response['data']['product_type_id'] == 2) {
                      this.data[0]['status_text'] = 'Pendiente por firma de contrato';
                     } else if (response['data']['status'] == 'C') {
                      this.data[0]['status_text'] = 'Completado';
                    } else if (response['data']['status'] == 'AwaitingSignatureByTheCustomer') {
                      this.data[0]['status_text'] = 'Pendiente por firma de contrato';
                    // } else if (response['data']['status'] == 'M') {
                     // this.data[0]['status_text'] = 'En proceso';
                    } else {
                      this.data[0]['status_text'] = 'En proceso';
                    }

          } else {
            const alert = this.alertCtrl.create({
              subHeader: "Aviso",
              message: response['message'],
              buttons: [
                {
                  text: "Ok",
                  role: 'cancel'
                }
              ]
            }).then(alert => alert.present());
          }//Fin else
        });//FIN LOADING DISS
      }, error => {
        load2.dismiss().then(() => {
          const alert = this.alertCtrl.create({
            subHeader: "Aviso",
            message: "No pudo conectarse al servidor",
            buttons: [
              {
                text: "Reintentar",
                role: 'cancel',
                cssClass: 'ion-aceptar',
                handler: data => {
                  this.provider_menu.logerroresadd("Ver detalle couenta", JSON.stringify(error)).subscribe((response) => { });//FIN LOADING DISS
                  this.ionViewDidEnter();
                }
              }
            ]
          }).then(alert => alert.present());
        });//FIN LOADING DISS
      });//FIN POST
    });//FIn LOADING

  }//FIN FcuntiN
  regresar() {
    this.navController.back();
  }
}//FIN CLASS