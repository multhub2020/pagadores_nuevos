import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl} from '@angular/forms';

import { Router } from '@angular/router';
import { Platform, NavController, LoadingController, AlertController } from '@ionic/angular';
import { Menus } from '../../providers/menus';
import { Perfils } from '../../providers/perfils';
import { MenuController } from '@ionic/angular';

import { ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-perfilsugerencias',
  templateUrl: 'perfilsugerencias.html',
  styleUrls: ['perfilsugerencias.scss'],
  providers:[Perfils, Menus]
})

export class Perfilsugerencias {
  public myForm: FormGroup;
  public loading: any;
  public usuarioid: string;
  public puntaje = "0";
  public selected = "1";
  public selected2 = "1";
  public nombres: string;
  public apellidos: string;
  public avatar:string="";
  public invitados = "";
  public seguidores: any;
  public seguidores1: any;
  public seguidores2: any;
  public ordena      = "";
  public color     = "";
  public usuario = "";
  public searchTerm: any;
  public allItems: any;
  public items: any;
  public seguidoresid = "";
  public nombressub = "";
  public seleccionado = "0";
  constructor(public navController: NavController,
  			  public formBuilder: FormBuilder,
  			  private router: Router, 
  			  private provider: Perfils, 
          private platform: Platform,
          private menu: MenuController,
  			  public alertCtrl: AlertController,
  			  public loadingCtrl: LoadingController,
          private rutaActiva: ActivatedRoute,
          private provider_menu: Menus
  			  ) {
      this.myForm = this.formBuilder.group({
          puntaje: new FormControl('', Validators.compose([ 
                              Validators.required
                              ])
                   ),
          texto: new FormControl('', Validators.compose([ 
                              Validators.required
                              ])
                   )
      });
      this.usuario      = localStorage.getItem('NOMBRESAPELLIDOS');
  }
  sel(id){
      if(this.seleccionado!='0'){
        let notiEle: HTMLElement = document.getElementById('col'+this.seleccionado);
        notiEle.classList.remove('focus_col');
      }
      this.seleccionado = id;
      this.myForm.value.puntaje = id;
      this.puntaje = id;
      let notiEle2: HTMLElement = document.getElementById('col'+id);
      notiEle2.classList.add('focus_col');
      
  }
  sub(id){
    this.navController.navigateForward('listarseguidoressub/'+id);
  }
  salir(){
      this.navController.navigateRoot('menu');
  }
  linkmenu(var1, i){
      if(var1=="notificacion"){
            this.navController.navigateForward('perfilnotificaciones');
      }else if(var1=="top"){
             this.navController.navigateForward('perfiltop');
      }
  }//fin function
  ionViewDidEnter() {

  }
  enviarformulario(){
  			this.usuarioid = localStorage.getItem('IDUSER');
            const loader = this.loadingCtrl.create({
            	message: "Un momento por favor..."
            }).then(load => {
                    load.present();
                    this.provider.sugerenciasadd(this.usuarioid, this.myForm.value).subscribe((response) => {  
                                this.loadingCtrl.dismiss().then( () => { 
                                        if(response['code']==200){
                                            const alert = this.alertCtrl.create({
                                                subHeader: "Aviso",
                                                message: response['msg'],
                                                buttons: [
                                                  {
                                                      text: "Continuar",
                                                      role: 'cancel',
                                                      cssClass:'ion-aceptar',
                                                      handler: data => {
                                                          this.navController.navigateForward('menu');
                                                      }
                                                  }
                                                ]
                                            }).then(alert => alert.present());
                                        }else if (response['code']==201){
                                                    const alert = this.alertCtrl.create({
                                                      subHeader: "Aviso",
                                                        message: response['msg'],
                                                        buttons: [
                                                          {
                                                            text: "Ok", 
                                                            role: 'cancel'
                                                          }
                                                        ]
                                                    }).then(alert => alert.present());
                                        }//Fin else
                                });//FIN LOADING DISS
                    },error => {
                          this.loadingCtrl.dismiss().then( () => {
                                    const alert = this.alertCtrl.create({
                                        subHeader: "Aviso",
                                        message: "No pudo conectarse al servidor",
                                        buttons: [
                                          {
                                              text: "Reintentar",
                                              role: 'cancel',
                                              cssClass:'ion-aceptar',
                                              handler: data => {
                                                 this.provider_menu.logerroresadd("Perfil sugerencias", JSON.stringify(error)).subscribe((response) => { });//FIN LOADING DISS
                                                  this.ionViewDidEnter();
                                              }
                                          }
                                        ]
                                    }).then(alert => alert.present());
                        });//FIN LOADING DISS
                    });//FIN POST
            });//FIn LOADING
         
  }
}//FIN CLASS
