import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl} from '@angular/forms';

import { Router } from '@angular/router';
import { NavController, LoadingController, AlertController } from '@ionic/angular';
import { GooglePlus } from '@ionic-native/google-plus/ngx';
import { FacebookLoginResponse, Facebook} from "@ionic-native/facebook/ngx";
import { Usuario } from '../../providers/usuario';
import { Menus } from '../../providers/menus';
import { IonicSelectableComponent } from 'ionic-selectable';

class Cuidades {
  public id: string;
  public name: string;
}

class Provincias {
  public id: string;
  public name: string;
}


@Component({
  selector: 'app-localizardeuda2_3_2',
  templateUrl: 'localizardeuda2_3_2.html',
  styleUrls: ['localizardeuda2_3_2.scss'],
  providers:[Usuario, GooglePlus, Facebook, Menus]
})

export class Localizardeuda2_3_2 {
  public myForm: FormGroup;
  public loading: any;
  public email: string;

  public t_push: string;
  public p_push: string;
  public u_push: string;

  public politicas = false;

  public usuarioregistro: string;
  public usuariocedula: string;
  public usuarionombres: string;

  public registroemail: string;
  public registrotelefono: string;
  public registrogmail = "";
  public clavecool = '12345';


  public ciudad: any;
  public provincia: any;
  public industria: any;


  public ciudades: any;
  public provincias: any;
  public industrias: any;


  public salario: string;
  public salario2 = 0;

  public empleo: string;
  public nacionalidad: string;
  public ocupacion: string;

  public vehiculo = "";

  public customerid = "";
  public customerid2 = 0;


  public usuarioid: string;
  public usuario = "";
  public nombres: string;
  public apellidos: string;
  public user: string;
  public national_id: string;
  public cellphone: string;

  constructor(public navController: NavController,
  			  public formBuilder: FormBuilder,
  			  private router: Router,
          private googlePlus: GooglePlus, 
          private fb: Facebook,
  			  private provider_usuario: Usuario, 
  			  public alertCtrl: AlertController,
  			  public loadingCtrl: LoadingController,
          private provider_menu: Menus,
  			  ) {
	  	this.myForm = this.formBuilder.group({
         direccion: new FormControl('', Validators.compose([ 
                              Validators.required,
                               Validators.maxLength(50)
                              ])
                   ),
        ciudad: new FormControl('', Validators.compose([ 
                              Validators.required,
                              ])
                   ),
         provincia: new FormControl('', Validators.compose([ 
                              Validators.required
                              ])
                   ),
          industria: new FormControl('', Validators.compose([ 
                              Validators.required
                              ])
                   ),
          sector: new FormControl('', Validators.compose([ 
                              Validators.required,
                              Validators.maxLength(50)
                              ])
                   ),
          valida: new FormControl('', Validators.compose([ 
                              Validators.required,
                              ])
                   )
      });
  }
  informacion(var1) {
    if (var1 == 1) {
      this.navController.navigateForward('inforcondiciones');
    } else if (var1 == 2) {
      this.navController.navigateForward('inforpoliticas');
    } else if (var1 == 3) {
      this.navController.navigateForward('loginrecuperar1');
    } else if (var1 == 4) {
      this.navController.navigateForward('loginclave');
    }
  }
  getText(e){
     var elementValue = e.srcElement.value;
     if(elementValue){
       var regex = /^[A-Za-z ]+$/;   
        var tempValue = elementValue.substring(0, elementValue.length - 1);
        if (!regex.test(elementValue)) {
          console.log("Entered char is not alphabet");
          e.srcElement.value = tempValue;
        }
     }
   }
  salir(){
      this.navController.navigateRoot('login');
  }
  ionViewDidEnter() {
        this.salario      = localStorage.getItem('SALARIO');
        this.empleo       = localStorage.getItem('EMPLEO');
        this.nacionalidad = localStorage.getItem('NACIONALIDAD');
        this.ocupacion    = localStorage.getItem('OCUPACION');
        this.vehiculo     = localStorage.getItem('VEHICULO');

        this.provider_usuario.api_listciudad().subscribe((response) => {
                this.ciudad = response['datas'];
        },error => {
              this.loadingCtrl.dismiss().then( () => {
                        const alert = this.alertCtrl.create({
                            subHeader: "Aviso",
                            message: "No pudo conectarse al servidor",
                            buttons: [
                              {
                                  text: "Reintentar",
                                  role: 'cancel',
                                  cssClass:'ion-aceptar',
                                  handler: data => {
                                    this.provider_menu.logerroresadd("localizardeuda2 3 2", JSON.stringify(error)).subscribe((response) => { });//FIN LOADING DISS
                                    this.ionViewDidEnter();
                                  }
                              }
                            ]
                        }).then(alert => alert.present());
            });//FIN LOADING DISS
        });//FIN POST


        this.provider_usuario.api_listprovin().subscribe((response) => {
                this.provincia = response['datas'];
        },error => {
              this.loadingCtrl.dismiss().then( () => {
                        const alert = this.alertCtrl.create({
                            subHeader: "Aviso",
                            message: "No pudo conectarse al servidor",
                            buttons: [
                              {
                                  text: "Reintentar",
                                  role: 'cancel',
                                  cssClass:'ion-aceptar',
                                  handler: data => {
                                    this.provider_menu.logerroresadd("localizardeuda2 3 2", JSON.stringify(error)).subscribe((response) => { });//FIN LOADING DISS
                                    this.ionViewDidEnter();
                                  }
                              }
                            ]
                        }).then(alert => alert.present());
            });//FIN LOADING DISS
        });//FIN POST



         this.provider_usuario.api_listindustria().subscribe((response) => {
                this.industria = response['datas'];
        },error => {
              this.loadingCtrl.dismiss().then( () => {
                        const alert = this.alertCtrl.create({
                            subHeader: "Aviso",
                            message: "No pudo conectarse al servidor",
                            buttons: [
                              {
                                  text: "Reintentar",
                                  role: 'cancel',
                                  cssClass:'ion-aceptar',
                                  handler: data => {
                                    this.provider_menu.logerroresadd("localizardeuda2 3 2", JSON.stringify(error)).subscribe((response) => { });//FIN LOADING DISS
                                    this.ionViewDidEnter();
                                  }
                              }
                            ]
                        }).then(alert => alert.present());
            });//FIN LOADING DISS
        });//FIN POST



       

  }

  ciudadChange(event: {
    component: IonicSelectableComponent,
    value: any
  }) {
    console.log('port:', event.value);
  }


  provinciaChange(event: {
    component: IonicSelectableComponent,
    value: any
  }) {
    console.log('port:', event.value);
  }



  enviarformulario(v){
    this.customerid = localStorage.getItem('CUSTOMERID');
    this.usuarioid  = localStorage.getItem('IDUSER');
    const loader = this.loadingCtrl.create({
      message: "Un momento por favor..."
    }).then(load => {

                      load.present(); 
                      console.log('ID: '+this.customerid);
                      this.provider_usuario.perfileditar(this.usuarioid).subscribe((response2) => {  
                            this.nombres     = response2['datos_usuarios'][0]['nombres'];
                            this.apellidos   = response2['datos_usuarios'][0]['apellidos'];
                            this.user        = response2['datos_usuarios'][0]['email'];
                            this.cellphone   = response2['datos_usuarios'][0]['telefono'];
                            this.national_id = response2['datos_usuarios'][0]['ci'];

                            this.customerid2 = parseFloat(this.customerid+'.0');
                            this.salario2    = parseFloat(this.salario);


                            //this.customerid2 = parseFloat(this.customerid2).toFixed(2);

                                          this.provider_usuario.api_pagadores_customers_refi_id(this.customerid2, 
                                                                                                this.salario2, 
                                                                                                this.empleo, 
                                                                                                this.nacionalidad,
                                                                                                this.myForm.value.direccion,
                                                                                                //this.ocupacion,
                                                                                                //"0",
                                                                                                this.myForm.value.ciudad,
                                                                                                this.myForm.value.provincia.denominacion,
                                                                                                this.myForm.value.sector,


                                                                                                this.nombres,
                                                                                                this.apellidos,
                                                                                                this.user,
                                                                                                this.cellphone,
                                                                                                this.national_id,

                                                                                                this.myForm.value.industria.codigo,
                                                                                                this.vehiculo


                                         ).subscribe((response) => {

                                                      this.loadingCtrl.dismiss().then( () => {
                                                                    this.navController.navigateForward('principal/localizardeuda2_3');
                                                                    //console.log(this.myForm.value.industria);
                                                      });//FIN LOADING DISS
                                          },error => {
                                                      this.loadingCtrl.dismiss().then( () => {
                                                                    const alert = this.alertCtrl.create({
                                                                        subHeader: "Aviso",
                                                                        message: "No pudo conectarse al servidor",
                                                                        buttons: [
                                                                          {
                                                                              text: "Reintentar",
                                                                              role: 'cancel',
                                                                              cssClass:'ion-aceptar',
                                                                              handler: data => {
                                                                                this.provider_menu.logerroresadd("localizardeuda2 3 2", JSON.stringify(error)).subscribe((response) => { });//FIN LOADING DISS
                                                                              }
                                                                          }
                                                                        ]
                                                                    }).then(alert => alert.present());
                                                        });//FIN LOADING DISS
                                          });//FIN POST
                        },error => {
                                  this.loadingCtrl.dismiss().then( () => {
                                            const alert = this.alertCtrl.create({
                                                subHeader: "Aviso",
                                                message: "No pudo conectarse al servidor",
                                                buttons: [
                                                  {
                                                      text: "Reintentar",
                                                      role: 'cancel',
                                                      cssClass:'ion-aceptar',
                                                      handler: data => {
                                                        this.provider_menu.logerroresadd("localizardeuda2 3 2", JSON.stringify(error)).subscribe((response) => { });//FIN LOADING DISS
                                                      }
                                                  }
                                                ]
                                            }).then(alert => alert.present());
                                });//FIN LOADING DISS
                         });//FIN POST


                      
        });//FIn LOADING
  }//FIN FUNCTION

}//FIN CLASS
