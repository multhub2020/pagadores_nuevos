import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Localizardeuda2_3_2 } from './localizardeuda2_3_2';
import { IonicSelectableModule } from 'ionic-selectable';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicSelectableModule,
    RouterModule.forChild([{ path: '', component: Localizardeuda2_3_2 }])
  ],
  declarations: [Localizardeuda2_3_2]
})
export class Localizardeuda2_3_2Module {}
