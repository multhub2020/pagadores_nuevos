import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl} from '@angular/forms';

import { Router } from '@angular/router';
import { NavController, LoadingController, AlertController } from '@ionic/angular';

import { Usuario } from '../../providers/usuario';

@Component({
  selector: 'app-loginregistrofin',
  templateUrl: 'loginregistrofin.html',
  styleUrls: ['loginregistrofin.scss'],
  providers:[Usuario]
})

export class Loginregistrofin {
    public nombresapellidos: string;
    constructor(public navController: NavController,
  			  private router: Router, 
  			  private provider_usuario: Usuario, 
  			  public alertCtrl: AlertController,
  			  public loadingCtrl: LoadingController
  			  ){
    }
    ionViewDidEnter() {
        this.nombresapellidos = localStorage.getItem('USUARIONOMBRESAPELLIDOS');
    }
    link(){
	  		this.navController.navigateRoot('menu');
    }
}//FIN CLASS
