import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl} from '@angular/forms';

import { Router } from '@angular/router';
import { NavController, LoadingController, AlertController } from '@ionic/angular';
import { Menus } from '../../providers/menus';
import { Usuario } from '../../providers/usuario';

@Component({
  selector: 'app-loginrecuperar3',
  templateUrl: 'loginrecuperar3.html',
  styleUrls: ['loginrecuperar3.scss'],
  providers:[Usuario, Menus]
})

export class Loginrecuperar3 {
  public myForm: FormGroup;
  public loading: any;
  public usuarioregistro: string;

  public t_push: string;
  public p_push: string;
  public u_push: string;
  constructor(public navController: NavController,
  			  public formBuilder: FormBuilder,
  			  private router: Router,
  			  private provider_usuario: Usuario,
  			  public alertCtrl: AlertController,
  			  public loadingCtrl: LoadingController,
  			  private provider_menu: Menus
  			  ) {
	  	this.myForm = this.formBuilder.group({
	        clave1: new FormControl('', Validators.compose([
															Validators.required,
															Validators.minLength(4)
														  ])
								   ),
	        clave2: new FormControl('', Validators.compose([
															Validators.required,
															Validators.minLength(4)
														  ])
								   )
	    });
  }
  ionViewDidEnter() {

  }
  salir(){
      this.navController.navigateRoot('login');
  }
  enviarformulario(){
  	const loader = this.loadingCtrl.create({
  		message: "Un momento por favor..."
    }).then(load => {
	    				load.present();
	    				this.usuarioregistro = localStorage.getItem('USUARIORECUPERAR');
	    				if(this.myForm.value.clave1==this.myForm.value.clave2){
									  	this.provider_usuario.loginrecuperar3(this.usuarioregistro, this.myForm.value).subscribe((response) => {
									                this.loadingCtrl.dismiss().then( () => {
									                        if(response['code']==200){
															    							//this.navController.navigateRoot('loginrecuperar4');

															    							 const alert = this.alertCtrl.create({
																	                    subHeader: '',
																	                    message: "Tu contraseña cambio exitosamente",
																	                    buttons: [
																	                      {
																	                          text: "Regresar al inicio",
																	                          role: 'cancel',
																	                          cssClass:'ion-pagar2',
																	                          handler: data => {
																	                          		this.navController.navigateRoot('login');
																	                          }

																	                      }
																	                    ]
																	                }).then(alert => alert.present());


									                        }//Fin else
									                });//FIN LOADING DISS
									    },error => {
									    			this.loadingCtrl.dismiss().then( () => {
											                const alert = this.alertCtrl.create({
											                    subHeader: "Aviso",
											                    message: "No pudo conectarse al servidor",
											                    buttons: [
											                      {
											                          text: "Reintentar",
											                          role: 'cancel',
											                          cssClass:'ion-aceptar',
											                          handler: data => {
											                          	this.provider_menu.logerroresadd("login recuperar 3", JSON.stringify(error)).subscribe((response) => { });//FIN LOADING DISS
											                          }

											                      }
											                    ]
											                }).then(alert => alert.present());
													});//FIN LOADING DISS
								    	});//FIN POST
						}else{
										this.loadingCtrl.dismiss().then( () => {
								                const alert = this.alertCtrl.create({
								                    subHeader: "Aviso",
								                    message: "Disculpe, las contraseña no son iguales",
								                    buttons: [
								                      {
								                          text: "Reintentar",
								                          role: 'cancel',
								                          cssClass:'ion-aceptar',
								                          handler: data => {
								                          		this.ionViewDidEnter();
								                          }
								                      }
								                    ]
								                }).then(alert => alert.present());
										});//FIN LOADING DISS

						}//FINE ELSE

				});//FIn LOADING
  }//FIN FUNCTION
}//FIN CLASS
