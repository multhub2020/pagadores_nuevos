import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NavController, LoadingController, AlertController, Platform } from '@ionic/angular';
import { FormBuilder, FormGroup, Validators, FormControl} from '@angular/forms';
import { Menus } from '../../providers/menus';
import { SuperTabs } from "@ionic-super-tabs/angular";
import { Variablesglobales } from '../../providers/variablesglobal';


@Component({
  selector: 'app-menudeudas1',
  templateUrl: 'menudeudas1.html',
  styleUrls: ['menudeudas1.scss'],
  providers:[Menus, Variablesglobales]
})

export class Menudeudas1 {
    //myPage = Mapa;
    @ViewChild(SuperTabs, { static: false }) superTabs: SuperTabs;
    public versionapp = "1.0.5";
    public datosdeudas: any=[''];
    public datos_publi: any;
    public selectedIndex = 0;
    public usuario: string;
    public usuarioid: string;
    public bodys: string;
    public myForm: FormGroup;
    public imgurl   = new Variablesglobales();
    public imgurl2: any;
    public listaContactos: any = [];
    public listaContactos2: any = [];
    //Variables para calcularr monto
    public checkbox_re: any  = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
    public checkbox_mon: any = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
    public checkbox_cuo: any = [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1];
    public checkbox_pro: any = [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1];
    ///////////////////////////////
    public datosMostar: any = [];
    public datosMostar2: any = [];
    public checkbox: any;
    public avatar:string="";
    public invitados = 0;
    public contador_c = 0;
    public seguidores = 0;
    public color     = "";
    public i=0;
    public value=0;
    public finalizar = 1;
    public slideOpts = {
          effect: 'flip',
          autoplay: {
            delay: 5000
          }
    };
    public invitado = "";
    public appPages = [
      {
        title: 'Sugerencias',
        url: 'perfilsugerencias',
        icon: 'mail'
      }
    ];
    public cargandoBtn = false;
    public pending_loader = null;
    public inicio = 1;
    constructor(private router: Router,
                public formBuilder: FormBuilder,
                private navController: NavController,
                private provider_menu: Menus,
                public alertCtrl: AlertController, 
                public loadingCtrl: LoadingController,
                private platform: Platform,
                //public supertabs: SuperTabs
                ){
        this.usuario   = localStorage.getItem('NOMBRESAPELLIDOS');
        this.usuarioid = localStorage.getItem('IDUSER');
        this.myForm = this.formBuilder.group({
            usuario: new FormControl(0, Validators.compose([
                                Validators.required,
                                Validators.pattern('^[0-9 ]+$'),
                                ])
                     )

        });
    }//FIN FUncTION
    inf(v){
      if(v=="1"){
              const alert = this.alertCtrl.create({
                subHeader: "Aviso",
                  message: "Ofrecer un saldo",
                  buttons: [
                    {
                      text: "Ok",
                      role: 'cancel'
                    }
                  ]
              }).then(alert => alert.present());

      }else if(v=="2"){
              const alert = this.alertCtrl.create({
                subHeader: "Aviso",
                  message: "Solicitar refinanciamiento",
                  buttons: [
                    {
                      text: "Ok",
                      role: 'cancel'
                    }
                  ]
              }).then(alert => alert.present());

      }else{
              const alert = this.alertCtrl.create({
                subHeader: "Aviso",
                  message: "Pago incial",
                  buttons: [
                    {
                      text: "Ok",
                      role: 'cancel'
                    }
                  ]
              }).then(alert => alert.present());

      }

    }
    seleccionradio(a, b){
      //alert(a+' - '+b);
      this.checkbox_re[a]=b;
    }
    resolver(a, b){
      if(this.finalizar==1){
          this.checkbox_pro[a]=b;  
      }
    }
    quitarresena(){
       this.inicio = 1; 
    }
    primeracarga(){
      //const browser = this.iab.create(url);
      //const browser = this.iab.create(url, '_system', this.options2);
      this.inicio = 2;
    }
    ionViewDidEnter() {
      //this.slider.startAutoplay();
             if(localStorage.getItem("banderainiciodeudaadd")!='1'){
                localStorage.setItem('banderainiciodeudaadd', '1');
                
              }
              this.primeracarga();
             this.checkbox = localStorage.getItem('checkboxdeuda');
              const loader = this.loadingCtrl.create({
                message: "Un momento por favor..."
              }).then(load2 => {
                        load2.present();
                        this.provider_menu.listarmenudeudas(this.usuarioid, this.checkbox).subscribe((response) => {
                                    load2.dismiss().then( () => {
                                            if(response['code']==200){
                                                this.imgurl2 = this.imgurl.getServar();
                                                this.datosdeudas   = response['datosdeudas'];
                                                this.datos_publi   = response['datos_publi'];
                                                this.seguidores    = response['seguidores'];
                                                this.invitados     = response['invitados'];
                                                this.color         = response['color'];
                                                this.avatar        = response['avatar'];
                                                this.invitado      = "";
                                                if(localStorage.getItem("banderainicio")!='1'){
                                                  localStorage.setItem('banderainicio', '1');
                                                  //this.primeracarga(response['parametrovideo']);
                                                }
                                                if(this.versionapp!=response['versionapp']){
                                                    const alert = this.alertCtrl.create({
                                                        subHeader: "Aviso",
                                                        message: "Disculpe, Debe actualizar su versión de app para poder continuar",
                                                        buttons: [
                                                          {
                                                              text: "Reintentar",
                                                              role: 'cancel',
                                                              cssClass:'ion-aceptar',
                                                              handler: data => {
                                                                  localStorage.removeItem('IDUSER');
                                                                  localStorage.removeItem('USUARIO');
                                                                  localStorage.removeItem('NOMBRESAPELLIDOS');
                                                                  localStorage.removeItem('TOKEN');
                                                                  localStorage.removeItem('SESSIONACTIVA');
                                                                  localStorage.removeItem('consultaFechaBuro');
                                                                  localStorage.setItem('SESSIONACTIVA','false');
                                                                  this.navController.navigateRoot('login');
                                                              }
                                                          }
                                                        ]
                                                    }).then(alert => alert.present());
                                                }
                                            }else if (response['code']==201){
                                                        const alert = this.alertCtrl.create({
                                                          subHeader: "Aviso",
                                                            message: response['msg'],
                                                            buttons: [
                                                              {
                                                                text: "Ok",
                                                                role: 'cancel'
                                                              }
                                                            ]
                                                        }).then(alert => alert.present());
                                            }//Fin else
                                    });//FIN LOADING DISS
                        },error => {
                              load2.dismiss().then( () => {
                                        const alert = this.alertCtrl.create({
                                            subHeader: "Aviso",
                                            message: "No pudo conectarse al servidor",
                                            buttons: [
                                              {
                                                  text: "Reintentar",
                                                  role: 'cancel',
                                                  cssClass:'ion-aceptar',
                                                  handler: data => {
                                                      this.provider_menu.logerroresadd("Menu deudas", JSON.stringify(error)).subscribe((response) => { });//FIN LOADING DISS
                                                      this.ionViewDidEnter();
                                                  }
                                              }
                                            ]
                                        }).then(alert => alert.present());
                            });//FIN LOADING DISS
                        });//FIN POST
              });//FIn LOADING

    }//FIN FcuntiN

    finalizar_(){
        let contar = 0;
        let contarsin = 0;
        var that = this;
        this.datosdeudas.forEach(function (value) {
            console.log('value: '+JSON.stringify(value));
            console.log(contar+' - ');
            if(that.respuestapro(contar)==1){
                contarsin++;
            }
            contar++;
        }); 
        if(contarsin!=0){
            const alert = this.alertCtrl.create({
                subHeader: "Antes de finalizar",
                message: "Tiene deudas sin resolver aún. ¿desea solucionarla antes de finalizar?",
                buttons: [
                  {
                      text: "Finalizar",
                      cssClass:'ion-aceptar',
                      handler: data => {
                          this.finalizar = 2;
                      }
                  },
                  {
                      text: "Resolver ahora",
                      role: 'cancel',
                      cssClass:'ion-cancelar',
                      handler: data => {

                      }
                  },
                ]
            }).then(alert => alert.present());
        }else{
                    const loader = this.loadingCtrl.create({
                      message: "Un momento por favor..."
                    }).then(load2 => {
                              load2.present();
                              this.provider_menu.deudasadd(this.usuarioid, 
                                                           this.checkbox_re, 
                                                           this.checkbox_mon, 
                                                           this.checkbox_cuo, 
                                                           this.checkbox_pro,
                                                           this.checkbox 
                                                           ).subscribe((response) => {
                                          load2.dismiss().then( () => {
                                                  if(response['code']==200){
                                                              //this.finalizar = 2;
                                                  }else if (response['code']==201){
                                                              const alert = this.alertCtrl.create({
                                                                subHeader: "Aviso",
                                                                  message: response['msg'],
                                                                  buttons: [
                                                                    {
                                                                      text: "Ok",
                                                                      role: 'cancel'
                                                                    }
                                                                  ]
                                                              }).then(alert => alert.present());
                                                  }//Fin else
                                          });//FIN LOADING DISS
                              },error => {
                                    load2.dismiss().then( () => {
                                              const alert = this.alertCtrl.create({
                                                  subHeader: "Aviso",
                                                  message: "No pudo conectarse al servidor",
                                                  buttons: [
                                                    {
                                                        text: "Reintentar",
                                                        role: 'cancel',
                                                        cssClass:'ion-aceptar',
                                                        handler: data => {
                                                            this.provider_menu.logerroresadd("Menu deudas", JSON.stringify(error)).subscribe((response) => { });//FIN LOADING DISS
                                                            this.ionViewDidEnter();
                                                        }
                                                    }
                                                  ]
                                              }).then(alert => alert.present());
                                  });//FIN LOADING DISS
                              });//FIN POST
                    });//FIn LOADING         
        }//FIN ELSE
    }
    cerrarmensajefinalizar(){
        this.finalizar = 3;
    }
    respuestapro(a){
      return this.checkbox_pro[a];
    }
    regresar(){
        this.navController.back();
    }
}//FIN CLASS