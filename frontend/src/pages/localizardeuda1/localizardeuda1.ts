import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NavController, LoadingController, AlertController, Platform } from '@ionic/angular';
import { FormBuilder, FormGroup, Validators, FormControl} from '@angular/forms';
import { Menus } from '../../providers/menus';
import { SuperTabs } from "@ionic-super-tabs/angular";
import { Variablesglobales } from '../../providers/variablesglobal';


@Component({
  selector: 'app-localizardeuda1',
  templateUrl: 'localizardeuda1.html',
  styleUrls: ['localizardeuda1.scss'],
  providers:[Menus, Variablesglobales]
})

export class Localizardeuda1 {
    //myPage = Mapa;
    @ViewChild(SuperTabs, { static: false }) superTabs: SuperTabs;
    public versionapp = "1.0.5";
    public datosdeudas: any=[''];
    public datos_publi: any;
    public selectedIndex = 0;
    public usuario: string;
    public usuarioid: string;
    public bodys: string;
    public myForm: FormGroup;
    public imgurl   = new Variablesglobales();
    public imgurl2: any;
    public listaContactos: any = [];
    public listaContactos2: any = [];
    //Variables para calcularr monto
    public checkbox_re: any  = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
    public checkbox_mon: any = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
    public checkbox_cuo: any = [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1];
    public checkbox_pro: any = [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1];
    ///////////////////////////////
    public datosMostar: any = [];
    public datosMostar2: any = [];
    public checkbox: any;
    public avatar:string="";
    public invitados = 0;
    public contador_c = 0;
    public seguidores = 0;
    public color     = "";
    public i=0;
    public value=0;
    public finalizar = 1;
    public slideOpts = {
          effect: 'flip',
          autoplay: {
            delay: 5000
          }
    };
    public invitado = "";
    public appPages = [
      {
        title: 'Sugerencias',
        url: 'perfilsugerencias',
        icon: 'mail'
      }
    ];
    public cargandoBtn = false;
    public pending_loader = null;
    public inicio = 1;
    constructor(private router: Router,
                public formBuilder: FormBuilder,
                private navController: NavController,
                private provider_menu: Menus,
                public alertCtrl: AlertController, 
                public loadingCtrl: LoadingController,
                private platform: Platform,
                //public supertabs: SuperTabs
                ){
        this.usuario   = localStorage.getItem('NOMBRESAPELLIDOS');
        this.usuarioid = localStorage.getItem('IDUSER');
        this.myForm = this.formBuilder.group({
            usuario: new FormControl(0, Validators.compose([
                                Validators.required,
                                Validators.pattern('^[0-9 ]+$'),
                                ])
                     )

        });
    }//FIN FUncTION
    inf(v){
        this.finalizar = v;
        this.inicio = 3;
    }
    inf_ce(v){
        this.finalizar = v;
        this.inicio = 1;
    }
    seleccionradio(a, b){
      //alert(a+' - '+b);
      this.checkbox_re[a]=b;
    }
    resolver(a, b){
      if(this.finalizar==1){
          this.checkbox_pro[a]=b;  
      }
    }
    quitarresena(){
       this.inicio = 1; 
    }
    primeracarga(){
      //const browser = this.iab.create(url);
      //const browser = this.iab.create(url, '_system', this.options2);
      //this.inicio = 2;
      this.inicio = 1;
    }
    ionViewDidEnter() {
      //this.slider.startAutoplay();
             if(localStorage.getItem("banderainiciodeudaadd")!='1'){
                localStorage.setItem('banderainiciodeudaadd', '1');
                this.primeracarga();  
              }
             this.checkbox = localStorage.getItem('checkboxdeuda');
              const loader = this.loadingCtrl.create({
                message: "Un momento por favor..."
              }).then(load2 => {
                        load2.present();
                        this.provider_menu.listarmenudeudas(this.usuarioid, this.checkbox).subscribe((response) => {
                                    load2.dismiss().then( () => {
                                            if(response['code']==200){
                                                this.imgurl2 = this.imgurl.getServar();
                                                this.datosdeudas   = response['datosdeudas'];
                                                this.datos_publi   = response['datos_publi'];
                                                this.seguidores    = response['seguidores'];
                                                this.invitados     = response['invitados'];
                                                this.color         = response['color'];
                                                this.avatar        = response['avatar'];
                                                this.invitado      = "";
                                                if(localStorage.getItem("banderainicio")!='1'){
                                                  localStorage.setItem('banderainicio', '1');
                                                  //this.primeracarga(response['parametrovideo']);
                                                }
                                                console.log(this.datosdeudas);
                                                if(this.versionapp!=response['versionapp']){
                                                    const alert = this.alertCtrl.create({
                                                        subHeader: "Aviso",
                                                        message: "Disculpe, Debe actualizar su versión de app para poder continuar",
                                                        buttons: [
                                                          {
                                                              text: "Reintentar",
                                                              role: 'cancel',
                                                              cssClass:'ion-aceptar',
                                                              handler: data => {
                                                                  localStorage.removeItem('IDUSER');
                                                                  localStorage.removeItem('USUARIO');
                                                                  localStorage.removeItem('NOMBRESAPELLIDOS');
                                                                  localStorage.removeItem('TOKEN');
                                                                  localStorage.removeItem('SESSIONACTIVA');
                                                                  localStorage.removeItem('consultaFechaBuro');
                                                                  localStorage.setItem('SESSIONACTIVA','false');
                                                                  this.navController.navigateRoot('login');
                                                              }
                                                          }
                                                        ]
                                                    }).then(alert => alert.present());
                                                }
                                            }else if (response['code']==201){
                                                        const alert = this.alertCtrl.create({
                                                          subHeader: "Aviso",
                                                            message: response['msg'],
                                                            buttons: [
                                                              {
                                                                text: "Ok",
                                                                role: 'cancel'
                                                              }
                                                            ]
                                                        }).then(alert => alert.present());
                                            }//Fin else
                                    });//FIN LOADING DISS
                        },error => {
                              load2.dismiss().then( () => {
                                        const alert = this.alertCtrl.create({
                                            subHeader: "Aviso",
                                            message: "No pudo conectarse al servidor",
                                            buttons: [
                                              {
                                                  text: "Reintentar",
                                                  role: 'cancel',
                                                  cssClass:'ion-aceptar',
                                                  handler: data => {
                                                      this.provider_menu.logerroresadd("localizardeuda1", JSON.stringify(error)).subscribe((response) => { });//FIN LOADING DISS
                                                      this.ionViewDidEnter();
                                                  }
                                              }
                                            ]
                                        }).then(alert => alert.present());
                            });//FIN LOADING DISS
                        });//FIN POST
              });//FIn LOADING

    }//FIN FcuntiN
    continnuar(){
         var contar = 0;
          this.checkbox_re.forEach(function (value) {
            if(value!=0){
                contar = value;
            }
          }); 
          if(contar==1){
                localStorage.setItem('checkboxdeuda_re',this.checkbox_re);
                this.navController.navigateForward('principal/localizardeuda1_1');         
          }else if(contar==2){
                localStorage.setItem('checkboxdeuda_re',this.checkbox_re);
                this.navController.navigateForward('principal/localizardeuda2_1');          
          }else{
                const alert = this.alertCtrl.create({
                      subHeader: "Aviso",
                      message: "Disculpe, no se tiene opción seleccionada",
                      buttons: [
                        {
                            text: "Continuar",
                            role: 'cancel',
                            cssClass:'ion-aceptar',
                            handler: data => {
                            }
                        }
                      ]
                  }).then(alert => alert.present());
          }
    }
    cerrarmensajefinalizar(){
        this.finalizar = 4;
    }
    respuestapro(a){
      return this.checkbox_pro[a];
    }
    regresar(){
        this.navController.back();
    }
}//FIN CLASS