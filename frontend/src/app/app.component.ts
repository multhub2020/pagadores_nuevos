import { Component, OnInit } from '@angular/core';
import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import { Push, PushObject, PushOptions } from '@ionic-native/push/ngx';
import { Usuario } from '../providers/usuario';

import { IRoot } from '../../plugins/cordova-plugin-iroot/www/iroot';

declare var IRoot: IRoot;

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
  providers:[Usuario]
})
export class AppComponent {
      //public rootPage = "";
      public t_push = "";
      public p_push: string;

      constructor(
        private platform: Platform,
        private splashScreen: SplashScreen,
        private statusBar: StatusBar,
        private router: Router,
        private navController: NavController,
        private push: Push,
        private provider_usuario: Usuario,
      ) {
        this.initializeApp();
        //this.rootPage = 'Login';
         var contarinicio = '';
         contarinicio = localStorage.getItem('contarinicio_1_1');
         if(contarinicio==null){localStorage.setItem('contarinicio_1_1', '0');}
      }

      initializeApp() {
              this.platform.backButton.subscribe(() => {
                      var contarinicio = '';
                         contarinicio = localStorage.getItem('contarinicio_1_1');
                      if(contarinicio==null){
                       // alert(contarinicio);
                        contarinicio='0'; 
                        localStorage.setItem('contarinicio_1_1', '0');
                      }
                      if(this.router.url=='/principal/inicio/'+contarinicio) {
                          navigator['app'].exitApp();  
                      }else{
                          this.navController.back();
                      }
              });
              this.platform.ready().then(() => {
                    this.statusBar.styleDefault();
                    this.splashScreen.hide();
                    if (this.platform.is('ios')) {
                            localStorage.setItem('P_PUSH', 'ios');
                            this.p_push = "ios";
                    }else if(this.platform.is('android')){
                            localStorage.setItem('P_PUSH', 'android');
                            this.p_push = "android";
                            IRoot.isRooted((booleanVal) => {
                              console.log('IRoot.isRooted success: ', booleanVal);
                              //alert('respuesta ='+booleanVal);
                              if(booleanVal==true){
                                  alert('La APP Pagadores no esta disponible en dspositivos rooteados');
                                  navigator['app'].exitApp(); 
                              }
                            }, (err) => {
                              console.log('IRoot.isRooted error:', err);
                              //alert('b');
                            });
                    }else{
                            localStorage.setItem('P_PUSH', 'webapp');
                    }
                    // to check if we have permission
                    this.push.hasPermission()
                      .then((res: any) => {
                        if (res.isEnabled) {
                          console.log('We have permission to send push notifications');
                        } else {
                          console.log('We do not have permission to send push notifications');
                        }
                    });
                    // to initialize push notifications
                    const options: PushOptions = {
                       android: {
                           senderID: '312738230606'
                       },
                       ios: {
                          // senderID: 'SENDER_ID'//si no lo pones, se generará un token para APNS
                           alert: 'true',
                           badge: true,
                           sound: 'false'
                       },
                       browser: {
                           pushServiceURL: 'http://push.api.phonegap.com/v1/push'
                       }
                    };
                    const pushObject: PushObject = this.push.init(options);
                    pushObject.on('notification').subscribe((notification: any) => {
                      // data.message,
                      // data.title,
                      // data.count,
                      // data.sound,
                      // data.image,
                      // data.additionalData
                    });
                    pushObject.on('registration').subscribe((registration: any) => {
                      console.log(registration);
                      //alert(registration);
                      localStorage.setItem('T_PUSH', registration.registrationId);
                    });
                    pushObject.on('error').subscribe(error => {
                      //alert('Error with Push plugin:'+ error);
                    });
                    if(localStorage.getItem('SESSIONACTIVA') == 'true'){
                        //this.navController.navigateRoot('menu');
                    }else{
                        //this.navController.navigateRoot('login');
                    }
              });

      }




}
