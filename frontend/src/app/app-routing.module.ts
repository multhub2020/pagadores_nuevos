import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AclGuard } from '../guards/acl.guard';
import { Menu } from '../pages/menu/menu';


const routes: Routes = [
    {
        path: 'login',
        loadChildren: () => import('../pages/login/login.module').then(m => m.LoginModule)
    },
    {
        path: 'loginclave',
        loadChildren: () => import('../pages/loginclave/loginclave.module').then(m => m.LoginclaveModule)
    },
    /*{
        path: 'loginregistro',  
        loadChildren: () => import('../pages/loginregistro/loginregistro.module').then(m => m.LoginregistroModule)
    },
    {
        path: 'loginregistro2',  
        loadChildren: () => import('../pages/loginregistro2/loginregistro2.module').then(m => m.Loginregistro2Module)
    },*/

    {
        path: 'loginregistro3',
        loadChildren: () => import('../pages/loginregistro3/loginregistro3.module').then(m => m.Loginregistro3Module)
    },
    {
        path: 'loginregistro4',
        loadChildren: () => import('../pages/loginregistro4/loginregistro4.module').then(m => m.Loginregistro4Module)
    },
    {
        path: 'loginregistro6',
        loadChildren: () => import('../pages/loginregistro6/loginregistro6.module').then(m => m.Loginregistro6Module)
    },
    {
        path: 'loginregistro7',
        loadChildren: () => import('../pages/loginregistro7/loginregistro7.module').then(m => m.Loginregistro7Module)
    },
    {
        path: 'loginregistrofin',
        loadChildren: () => import('../pages/loginregistrofin/loginregistrofin.module').then(m => m.LoginregistrofinModule)
    },
    {
        path: 'loginrecuperar1',
        loadChildren: () => import('../pages/loginrecuperar1/loginrecuperar1.module').then(m => m.Loginrecuperar1Module)
    },
    {
        path: 'loginrecuperar2',
        loadChildren: () => import('../pages/loginrecuperar2/loginrecuperar2.module').then(m => m.Loginrecuperar2Module)
    },
    {
        path: 'loginrecuperar3',
        loadChildren: () => import('../pages/loginrecuperar3/loginrecuperar3.module').then(m => m.Loginrecuperar3Module)
    },
    {
        path: 'loginrecuperar4',
        loadChildren: () => import('../pages/loginrecuperar4/loginrecuperar4.module').then(m => m.Loginrecuperar4Module)
    },
    {
        path: 'inforpoliticas',
        loadChildren: () => import('../pages/inforpoliticas/inforpoliticas.module').then(m => m.InforpoliticasModule)
    },
    {
        path: 'inforcondiciones',
        loadChildren: () => import('../pages/inforcondiciones/inforcondiciones.module').then(m => m.InforcondicionesModule)
    },
    {
        path: 'menu',
        loadChildren: () => import('../pages/menu/menu.module').then(m => m.MenuModule),
        canActivate: [AclGuard]
    },
    {
        path: 'principal',
        loadChildren: () => import('../pages/principal/principal.module').then(m => m.PrincipalModule),
        canActivate: [AclGuard]
    },
    {
        path: 'perfileditar',
        loadChildren: () => import('../pages/perfileditar/perfileditar.module').then(m => m.PerfileditarModule),
        canActivate: [AclGuard]
    },
    {
        path: 'perfilclaveeditar',
        loadChildren: () => import('../pages/perfilclaveeditar/perfilclaveeditar.module').then(m => m.PerfilclaveeditarModule),
        canActivate: [AclGuard]
    },
    {
      path: 'perfilconfiguracion',
      loadChildren: () => import('../pages/perfilconfiguracion/perfilconfiguracion.module').then(m => m.PerfilconfiguracionModule),
      canActivate:[AclGuard]
    },    
    {
        path: 'perfilsobrenosotros',
        loadChildren: () => import('../pages/perfilsobrenosotros/perfilsobrenosotros.module').then(m => m.PerfilsobrenosotrosModule),
        canActivate: [AclGuard]
    },
    {
        path: 'perfilmejorando',
        loadChildren: () => import('../pages/perfilmejorando/perfilmejorando.module').then(m => m.PerfilmejorandoModule),
        canActivate: [AclGuard]
    },
    {
        path: 'perfilayuda',
        loadChildren: () => import('../pages/perfilayuda/perfilayuda.module').then(m => m.PerfilayudaModule),
        canActivate: [AclGuard]
    },
    {
        path: 'perfilsugerencias',
        loadChildren: () => import('../pages/perfilsugerencias/perfilsugerencias.module').then(m => m.PerfilsugerenciasModule),
        canActivate: [AclGuard]
    },
     {
      path: 'perfilayudasugerencia',
      loadChildren: () => import('../pages/perfilayudasugerencia/perfilayudasugerencia.module').then(m => m.PerfilayudasugerenciaModule),
      canActivate:[AclGuard]
    },
    {
        path: 'perfilinvitado',
        loadChildren: () => import('../pages/perfilinvitado/perfilinvitado.module').then(m => m.PerfilinvitadoModule),
        canActivate: [AclGuard]
    },
    {
        path: 'listarseguidores',
        loadChildren: () => import('../pages/listarseguidores/listarseguidores.module').then(m => m.ListarseguidoresModule),
        canActivate: [AclGuard]
    },
    {
        path: 'listarseguidoressub/:id',
        loadChildren: () => import('../pages/listarseguidoressub/listarseguidoressub.module').then(m => m.ListarseguidoressubModule),
        canActivate: [AclGuard]
    },
    {
        path: 'listarinvitados',
        loadChildren: () => import('../pages/listarinvitados/listarinvitados.module').then(m => m.ListarinvitadosModule),
        canActivate: [AclGuard]
    },
    {
      path: 'ayuda',
      loadChildren: () => import('../pages/ayuda/ayuda.module').then(m => m.AyudaModule),
      canActivate:[AclGuard]
    },
    {
        path: 'agregadeuda1',
        loadChildren: () => import('../pages/agregadeuda1/agregadeuda1.module').then(m => m.Agregadeuda1Module),
        canActivate: [AclGuard]
    },

    {
        path: 'verdetallecontraofertaaprobar/:nombre',
        loadChildren: () => import('../pages/verdetallecontraofertaaprobar/verdetallecontraofertaaprobar.module').then(m => m.VerdetallecontraofertaaprobarModule),
        canActivate: [AclGuard]
    },

    {
        path: 'verdetallecontraoferta/:id',
        loadChildren: () => import('../pages/verdetallecontraoferta/verdetallecontraoferta.module').then(m => m.VerdetallecontraofertaModule),
        canActivate: [AclGuard]
    },

    {
        path: 'principal/verdetallecontraoferta/:id',
        loadChildren: () => import('../pages/verdetallecontraoferta/verdetallecontraoferta.module').then(m => m.VerdetallecontraofertaModule),
        canActivate: [AclGuard]
    },

    {
        path: 'verdetallecontraofertacontinua/:id',
        loadChildren: () => import('../pages/verdetallecontraofertacontinua/verdetallecontraofertacontinua.module').then(m => m.VerdetallecontraofertacontinuaModule),
        canActivate: [AclGuard]
    },

    {
        path: 'verdetallecontraofertapagar/:id',
        loadChildren: () => import('../pages/verdetallecontraofertapagar/verdetallecontraofertapagar.module').then(m => m.VerdetallecontraofertapagarModule),
        canActivate: [AclGuard]
    },


    {
        path: 'verdetallecontraofertapagarmsj',
        loadChildren: () => import('../pages/verdetallecontraofertapagarmsj/verdetallecontraofertapagarmsj.module').then(m => m.VerdetallecontraofertapagarmsjModule),
        canActivate: [AclGuard]
    },

    {
        path: 'verdetallecuenta/:id',
        loadChildren: () => import('../pages/verdetallecuenta/verdetallecuenta.module').then(m => m.VerdetallecuentaModule),
        canActivate: [AclGuard]
    },

    {
        path: 'pagarcuenta/:contract_id/:application_id',
        loadChildren: () => import('../pages/pagarcuenta/pagarcuenta.module').then(m => m.PagarcuentaModule),
        canActivate: [AclGuard]
    },
    {
        path: 'pagarcuentadeposito/:id',
        loadChildren: () => import('../pages/pagarcuentadeposito/pagarcuentadeposito.module').then(m => m.PagarcuentadepositoModule),
        canActivate: [AclGuard]
    },
    {
        path: 'vercuenta/:id',
        loadChildren: () => import('../pages/vercuenta/vercuenta.module').then(m => m.VercuentaModule),
        canActivate: [AclGuard]
    },
    {
        path: 'principal/verdetalleaceptar/:id',
        loadChildren: () => import('../pages/verdetalleaceptar/verdetalleaceptar.module').then(m => m.VerdetalleaceptarModule),
        canActivate: [AclGuard]
    },
    {
        path: 'enviarpago/:application_id/:account_number',
        loadChildren: () => import('../pages/enviarpago/enviarpago.module').then(m => m.EnviarpagoModule),
        canActivate: [AclGuard]
    },
    ////////////////////////////////////////////////////////////

    /* {
         path: 'verdetalleaprobar/:nombre',  
         loadChildren: () => import('../pages/verdetalleaprobar/verdetalleaprobar.module').then(m => m.VerdetalleaprobarModule),
         canActivate:[AclGuard]
     },
   
     {
         path: 'verdetalle/:id',  
         loadChildren: () => import('../pages/verdetalle/verdetalle.module').then(m => m.VerdetalleModule),
         canActivate:[AclGuard]
     },*/

    {
        path: 'verdetallecontinua/:id',
        loadChildren: () => import('../pages/verdetallecontinua/verdetallecontinua.module').then(m => m.VerdetallecontinuaModule),
        canActivate: [AclGuard]
    },

    /* {
         path: 'verdetallepagar/:id',  
         loadChildren: () => import('../pages/verdetallepagar/verdetallepagar.module').then(m => m.VerdetallepagarModule),
         canActivate:[AclGuard]
     },
   
     
     {
         path: 'verdetallepagarmsj',  
         loadChildren: () => import('../pages/verdetallepagarmsj/verdetallepagarmsj.module').then(m => m.VerdetallepagarmsjModule),
         canActivate:[AclGuard]
     },*/

    {
        path: '',
        redirectTo: 'principal/inicio/0', // redirección a este componente en el primer acceso
        pathMatch: 'full'
    }
];
@NgModule({
    imports: [
        //RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
        RouterModule.forRoot(routes)
    ],
    exports: [RouterModule]
})
export class AppRoutingModule { }
