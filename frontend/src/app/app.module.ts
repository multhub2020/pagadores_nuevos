import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { AlertController } from '@ionic/angular';
import { MenuModule } from '../pages/menu/menu.module';
import { SuperTabsModule } from '@ionic-super-tabs/angular';
import { Push } from '@ionic-native/push/ngx';
import { FormsModule } from '@angular/forms';
import { NgxMaskIonicModule } from 'ngx-mask-ionic';
import { NgxCurrencyModule } from "ngx-currency";
import { Contacts } from '@ionic-native/contacts';
import { GooglePlus } from '@ionic-native/google-plus/ngx';
import { IonicRatingModule } from 'ionic4-rating';

import { Usuario } from '../providers/usuario';
import { Menus } from '../providers/menus';
import { Listacontactos } from '../providers/listacontactos';
import { Informacion } from '../providers/informacion';
import { Perfils } from '../providers/perfils';
import { Notificaciones } from '../providers/notificaciones';
import { Mapas } from '../providers/mapas';
import { Variablesglobales } from '../providers/variablesglobal';

import { IonicSelectableModule } from 'ionic-selectable';
import { EmailComposer } from '@ionic-native/email-composer/ngx';
import { HTTP } from '@ionic-native/http/ngx';
import { UniqueDeviceID } from '@ionic-native/unique-device-id/ngx';

@NgModule({ 
  declarations: [
                  AppComponent,
                  Usuario,
                  Menus,
                  Listacontactos,
                  Informacion,
                  Perfils,
                  Notificaciones,
                  Mapas,
                  Variablesglobales
                ],
  entryComponents: [],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    SuperTabsModule.forRoot(),
    AppRoutingModule,
    HttpClientModule,
    MenuModule,
    FormsModule,
    IonicRatingModule,
    NgxCurrencyModule,
    IonicSelectableModule,
    NgxMaskIonicModule.forRoot()
  ],
  exports: [
    
  ],
  providers: [
    StatusBar,
    SplashScreen,
    AlertController,
    Push,
    Informacion,
    Contacts,
    GooglePlus,
    EmailComposer,
    UniqueDeviceID,
    HTTP,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
