import { Injectable, Component } from '@angular/core';
//import { Http,Response,Headers,RequestOptions } from '@angular/http';

import { HttpClient, HttpHeaders } from '@angular/common/http';

import 'rxjs/add/operator/map';
import 'rxjs/Rx';
import { Observable } from 'rxjs/Observable';
import { Variablesglobales } from './variablesglobal';
@Injectable()
@Component({
  templateUrl: 'template.html',
  providers:[Variablesglobales]
})
export class Perfils{ //Se define la clase
        public v:string;
        public url     = new Variablesglobales();
        public httpHeader = {
                                headers: new HttpHeaders({  'Content-Type': 'application/json', 
                                                                  'Accept': 'application/json',
                                                          'remember_token': this.url.getKeyvar()
                                                        })
                            };
        public direccionGoogle : string = 'https://maps.googleapis.com/maps/api/geocode/json?latlng=';
        constructor(
            public _http:HttpClient,
        ){}
        listarperfil(var1){
                return  this._http.post(
                                this.url.getApivar()+"listarperfil",  
                                JSON.stringify({
                                    'usuarioid':var1,
                                }),
                                this.httpHeader
                            );
        }
        sobrenosotros(){
                return  this._http.post(
                                this.url.getApivar()+"sobrenosotros",  
                                JSON.stringify({
                                }),
                                this.httpHeader
                            );
        }
        addayudas(var1, var2){
                return  this._http.post(
                                this.url.getApivar()+"addayudas",  
                                JSON.stringify({
                                    'usuario_id':var1,
                                    'denominacion':var2
                                }),
                                this.httpHeader
                            );
        }
        addcalificaciones(var1, var2){
                return  this._http.post(
                                this.url.getApivar()+"addcalificaciones",  
                                JSON.stringify({
                                    'usuario_id':var1,
                                    'puntos':var2
                                }),
                                this.httpHeader
                            );
        }
        guardarperfil(var1, var2){
                return  this._http.post(
                                this.url.getApivar()+"guardarperfil",  
                                JSON.stringify({
                                    'usuarioid':var1,
                                    'clave':var2.clave,
                                    'nombre':var2.nombre,
                                    'apellido':var2.apellido,
                                }),
                                this.httpHeader
                            );
        }
        sugerenciasadd(var1, var2){
                return  this._http.post(
                                this.url.getApivar()+"sugerenciasadd",  
                                JSON.stringify({
                                    'usuarioid':var1,
                                    'puntaje':var2.puntaje,
                                    'texto':var2.texto,
                                }),
                                this.httpHeader
                            );
        }
        miperfilfotoupdate(dato1, dato2){
                return  this._http.post(
                                this.url.getApivar()+"miperfilfotoupdate",
                                JSON.stringify({
                                    'usuarioid':dato1,
                                    'imagenes': dato2
                                }),
                                this.httpHeader
                        );
        }
}

    