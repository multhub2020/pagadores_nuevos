import { Injectable, Component } from '@angular/core';
//import { Http,Response,Headers,RequestOptions } from '@angular/http';

import { HttpClient, HttpHeaders } from '@angular/common/http';

import 'rxjs/add/operator/map';
import 'rxjs/Rx';
import { Observable } from 'rxjs/Observable';
import { Variablesglobales } from './variablesglobal';
@Injectable()
@Component({
  templateUrl: 'template.html',
  providers:[Variablesglobales]
})
export class Notificaciones{ //Se define la clase
        public v:string;
        public url     = new Variablesglobales();
        public httpHeader = {
                                headers: new HttpHeaders({  'Content-Type': 'application/json', 
                                                                  'Accept': 'application/json',
                                                          'remember_token': this.url.getKeyvar()
                                                        })
                            };
        constructor(
            public _http:HttpClient,
        ){}
        listarnotificaciones(var1){
                return  this._http.post(
                                this.url.getApivar()+"listarnotificaciones",  
                                JSON.stringify({
                                    'usuarioid':var1,
                                }),
                                this.httpHeader
                            );
        }

        encuestaadd(var1, var2, var3){
                return  this._http.post(
                                this.url.getApivar()+"encuestaadd",  
                                JSON.stringify({
                                    'usuarioid':var1,
                                    'encuestaid':var2,
                                    'encuestaresid':var3.encuentasrespuesta_id,
                                    'encuestaresob':var3.observacion,
                                }),
                                this.httpHeader
                            );
        }
        encuestalist(var1){
                return  this._http.post(
                                this.url.getApivar()+"encuestalist",  
                                JSON.stringify({
                                    'encuestaid':var1
                                }),
                                this.httpHeader
                            );
        }


         eventoadd(var1, var2, var3){
                return  this._http.post(
                                this.url.getApivar()+"eventoadd",  
                                JSON.stringify({
                                    'usuarioid':var1,
                                    'eventoid':var2,
                                    'eventoresid':var3.eventosrespuesta_id,
                                    'eventoresob':var3.observacion,
                                }),
                                this.httpHeader
                            );
        }
        eventolist(var1){
                return  this._http.post(
                                this.url.getApivar()+"eventolist",  
                                JSON.stringify({
                                    'eventoid':var1
                                }),
                                this.httpHeader
                            );
        }
        eventoconf(var1, var2){
                return  this._http.post(
                                this.url.getApivar()+"eventoconf",  
                                JSON.stringify({
                                    'usuarioid':var1,
                                    'eventoid':var2,
                                    'confirma':'2'
                                }),
                                this.httpHeader
                            );
        }
}

    