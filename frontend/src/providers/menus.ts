import { Injectable, Component } from '@angular/core';
//import { Http,Response,Headers,RequestOptions } from '@angular/http';

import { HttpClient, HttpHeaders } from '@angular/common/http';

import 'rxjs/add/operator/map';
import 'rxjs/Rx';
import { Observable } from 'rxjs/Observable';
import { Variablesglobales } from './variablesglobal';

import * as CryptoJS from 'crypto-js';
import * as forge from 'node-forge';
import { UniqueDeviceID } from '@ionic-native/unique-device-id/ngx';


@Injectable()
@Component({
    templateUrl: 'template.html',
    providers: [Variablesglobales, UniqueDeviceID]
})
export class Menus { //Se define la clase
    public encryptedData: string;
    public decryptedData: string;
    public textPhrase: string;
    public phrase: string;

    public textPhraseRSA: string;
    public encryptedDataRSA: string;
    public certificateContentRSA: any = `-----BEGIN CERTIFICATE-----
MIIDsjCCApqgAwIBAgIMJBWimQU7u/8pMQO/MA0GCSqGSIb3DQEBCwUAMGIxCzAJ
BgNVBAYTAkRPMRYwFAYDVQQIEw1TYW50byBEb21pbmdvMRAwDgYDVQQHEwdmYXNs
ZnNhMRIwEAYDVQQLEwlmc2thZmtzYWYxFTATBgNVBAMTDGZzYWZzYWZzYWZzYTAe
Fw0yMTEwMDgwMzA4MjhaFw0yMjEwMDgwMzA4MjhaMGIxCzAJBgNVBAYTAkRPMRYw
FAYDVQQIEw1TYW50byBEb21pbmdvMRAwDgYDVQQHEwdmYXNsZnNhMRIwEAYDVQQL
Ewlmc2thZmtzYWYxFTATBgNVBAMTDGZzYWZzYWZzYWZzYTCCASIwDQYJKoZIhvcN
AQEBBQADggEPADCCAQoCggEBAGc0LICF92cfwL4lMJVo3sB8T+rSYLMkeLFjN4id
HhjFMSvl1xoFkcPoXf0jIPZ+aHvcart3QuFF2GOylyO2mO+6MrJlu9a/h7V3V1Hm
P9cn+f1kgZrwiz6M/nDvdBieAzy09yaYpsCQ8DbRykAL8EcqK17oykEpLtX/rVNt
O3ZlfflAGeQEKOLquOTpG8FahfctLGP2ZQb22R/59piww5uv4U/W3hdnjBKpms5F
ejftXoNHFz4tZ3s273L7v9JklN+Rm5VC6LfqS5jj59gtrJOWWl6NmB8iHrsrf+km
hy0ZBTCRhdFj5+mgI1lqHgnZ2DH3kci7rZ/jeJQvpMev+X0CAwEAAaNoMGYwKQYD
VR0OBCIEINAHhXFVY2PT2avz9VjHsnnwEH0N8nL3QSCo5JSzVcgJMCsGA1UdIwQk
MCKAINAHhXFVY2PT2avz9VjHsnnwEH0N8nL3QSCo5JSzVcgJMAwGA1UdEwQFMAMB
AQAwDQYJKoZIhvcNAQELBQADggEBAGSTOGTA02udhMJmtzZeQQ0rneIjpDROvO0C
NWsK0CNK05MNoWNzxmP/LXA6l5+bMchin9xsVK76de94ix662NZTnlcE3WFlflmi
EF7pi55mq9M2j0UFOH77J4VwTK81JuEZW6+/e3Ca6FRkOXzPdszyDjre/06lQbzP
omK+aT/bMOYdP1HFmlLY7rZoCgqg8sE9pMS2VEiGTqekUFBYmk6XFGxCV/xQbFqk
E4vo/IWpJ/U5d3To0YQexaR7fXcn+m3aUCmJa2iu2MJh4NwhaIoqHQDDmL3aR7iS
cqYBIcIN/RLSg1BBhyITl4q1+pLxQJ+OXIrVel2t0DL7Z1IgSIQ=
-----END CERTIFICATE-----
`;
    public v:string;
    public UniqueDeviceIDVar: string;

    public url = new Variablesglobales();
    public httpHeader = {
        headers: new HttpHeaders({
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'remember_token': this.url.getKeyvar()
        })
    };
    public httpHeader_apipagadores = {
        headers: new HttpHeaders({
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        })
    };
    public httpHeader_apipagadores_token = {
        headers: new HttpHeaders({
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'Authorization': 'Bearer ' + localStorage.getItem('TOKEAPIPAGADORES')
        })
    };
    public direccionGoogle: string = 'https://maps.googleapis.com/maps/api/geocode/json?latlng=';
    constructor(
        public _http: HttpClient,
        public uniqueDeviceID: UniqueDeviceID
    ) { }
    logerroresadd(var1, var2) {
        return this._http.post(
            this.url.getApivar() + "logerroresadd",
            JSON.stringify({
                'ruta': var1,
                'error': var2,
            }),
            this.httpHeader
        );
    }
    
    inicio(var1, var2) {
        return this._http.post(
            this.url.getApivar() + "inicio",
            JSON.stringify({
                'usuarioid': var1,
                'customerId': var2,
                'customer': localStorage.getItem('TOKEAPIPAGADORES'),
                'tipoaccion':this.url.getTipoaccion()
            }),
            this.httpHeader
        );
    }
    agregadeuda1(var1, var2) {
        return this._http.post(
            this.url.getApivar() + "agregadeuda1",
            JSON.stringify({
                'usuarioid': var1,
                //'creditor_id': var2.banco.id,
                //'banco': var2.banco.name,
                //'identifica_banco': var2.banco.identification_number,
                'banco': var2.banco2,
                'creditor_id': var2.banco,                
                'identifica_banco': var2.banco,

                'moneda': var2.moneda.id,
                'producto': var2.producto.id,
                'deuda': var2.deuda
            }),
            this.httpHeader
        );
    }
    localizardeudaid(var1, var2) {
        return this._http.post(
            this.url.getApivar() + "localizardeudaid",
            JSON.stringify({
                'usuarioid': var1,
                'checkbox': var2
            }),
            this.httpHeader
        );
    }
    deudaslista(var1, var2) {
        return this._http.post(
            this.url.getApivar() + "deudaslista",
            JSON.stringify({
                'usuarioid': var1,
                'checkbox': var2
            }),
            this.httpHeader
        );
    }
    deudasadd(var1, var2, var3, var4, var5, var6) {
        return this._http.post(
            this.url.getApivar() + "deudasadd",
            JSON.stringify({
                'usuarioid': var1,
                'checkbox_re': var2,
                'checkbox_mon': var3,
                'checkbox_cuo': var4,
                'checkbox_pro': var5,
                'checkbox': var6
            }),
            this.httpHeader
        );
    }
    deudassolicitudenviarcuotas(var1, var2, var3, var4, var5, var6) {
        return this._http.post(
            this.url.getApivar() + "deudassolicitudenviarcuotas",
            JSON.stringify({
                'usuarioid': var1,
                'id': var2,
                'checkbox_re': var3,
                'checkbox_mon': var4,
                'checkbox_cuo': var5,
                'pago_inicial': var6
            }),
            this.httpHeader
        );

    }
    deudassolicitudenviar(var1, var2, var3) {
        return this._http.post(
            this.url.getApivar() + "deudassolicitudenviar",
            JSON.stringify({
                'usuarioid': var1,
                'checkbox_id': var2,
                'checkbox_mon': var3
            }),
            this.httpHeader
        );

    }
    reenviarinvitacion(var1, var2) {
        return this._http.post(
            this.url.getApivar() + "reenviarinvitacion",
            JSON.stringify({
                'usuarioid': var1,
                'usuarioidpuntual': var2,
            }),
            this.httpHeader
        );
    }
    reenviarinvitacionall(var1) {
        return this._http.post(
            this.url.getApivar() + "reenviarinvitacionall",
            JSON.stringify({
                'usuarioid': var1
            }),
            this.httpHeader
        );
    }
    listarseguidores(var1) {
        return this._http.post(
            this.url.getApivar() + "listarseguidores",
            JSON.stringify({
                'usuarioid': var1,
            }),
            this.httpHeader
        );
    }
    listarseguidoresfiltro(var1, var2, var3, var4) {
        return this._http.post(
            this.url.getApivar() + "listarseguidoresfiltro",
            JSON.stringify({
                'usuarioid': var1,
                'puntaje': var2,
                'nivel': var3,
                'compara': var4,
            }),
            this.httpHeader
        );
    }
    listarinvitados(var1) {
        return this._http.post(
            this.url.getApivar() + "listarinvitados",
            JSON.stringify({
                'usuarioid': var1,
            }),
            this.httpHeader
        );
    }
    listamenu(var1, consultarBuro) {
        return this._http.post(
            this.url.getApivar() + "listarmenu",
            JSON.stringify({
                'usuarioid': var1,
                'consultarBuro': consultarBuro,
            }),
            this.httpHeader
        );
    }
    listarmenudeudas(var1, var2) {
        return this._http.post(
            this.url.getApivar() + "listarmenudeudas",
            JSON.stringify({
                'usuarioid': var1,
                'checkbox': var2
            }),
            this.httpHeader
        );
    }
    contraoferta(var1, var2) {
        return this._http.post(
            this.url.getApivar() + "contraoferta",
            JSON.stringify({
                'usuarioid': var1,
                'deudaid': var2
            }),
            this.httpHeader
        );
    }
    statusaprobacion(var1, var2) {
        return this._http.post(
            this.url.getApivar() + "statusaprobacion",
            JSON.stringify({
                'usuarioid': var1,
                'deudaid': var2
            }),
            this.httpHeader
        );
    }
    aprobarcontraoferta(var1, var2) {
        return this._http.post(
            this.url.getApivar() + "aprobarcontraoferta",
            JSON.stringify({
                'usuarioid': var1,
                'deudaid': var2
            }),
            this.httpHeader
        );
    }

    firmardeuda(var1, var2) {
        return this._http.post(
            this.url.getApivar() + "firmardeuda",
            JSON.stringify({
                'usuarioid': var1,
                'deudaid': var2
            }),
            this.httpHeader
        );
    }

    pagardeuda(var1, var2) {
        return this._http.post(
            this.url.getApivar() + "pagardeuda",
            JSON.stringify({
                'usuarioid': var1,
                'deudaid': var2
            }),
            this.httpHeader
        );
    }


    menuinvitacion(var1, var2) {
        return this._http.post(
            this.url.getApivar() + "menuinvitacion",
            JSON.stringify({
                'usuarioid': var1,
                'invitado': var2.usuario,
            }),
            this.httpHeader
        );
    }
    listartop() {
        return this._http.post(
            this.url.getApivar() + "listartop",
            JSON.stringify({
            }),
            this.httpHeader
        );
    }
    authorizetoken(AccountNumber, ExpirationMonth, ExpirationYear, CVV, AccountType, ZipCode, CustomerID, CustomerName, CustomerEmail, Amount, Currency, Customeraddress, CustomerCity, CustomerState) {
        return this._http.post(
            this.url.getApivar() + "authorizetoken",
            JSON.stringify({
                'AccountNumber': AccountNumber,
                'ExpirationMonth': ExpirationMonth,
                'ExpirationYear': ExpirationYear,
                'CVV': CVV,
                'AccountType': AccountType,
                'ZipCode': ZipCode,
                'CustomerId': CustomerID,
                'CustomerName': CustomerName,
                'CustomerEmail': CustomerEmail,
                'Amount': Amount,
                'Currency': Currency,
                'Customeraddress': Customeraddress,
                'CustomerCity': CustomerCity,
                'CustomerState': CustomerState
            }),
            this.httpHeader
        );
    }
    get_pending_applications_core(customerId) {
        return this._http.get(
            this.url.getApivar() + "get_pending_applications_core",
            this.httpHeader
        );
    }
    depositos(usuarioid, numero_transaccion, banco, monto, customerid, applicationid, imagen) {
        return this._http.post(
            this.url.getApivar() + "depositos",
            JSON.stringify({
                'usuarioid': usuarioid,                
                'numero_transaccion': numero_transaccion,
                'banco': banco,
                'monto': monto,
                'customer_id': customerid,
                'application_id': applicationid,
                'imagen': imagen
            }),
            this.httpHeader
        );
    }
    
    localizarinstruccion(creditor_id, idiomas = 'ES') {
        return this._http.post(
            this.url.getApivar() + "localizarinstruccion",
            JSON.stringify({
                'creditor_id': creditor_id,
                'idiomas': idiomas
            }),
            this.httpHeader
        );
    }
    credolabcollect(customer_id, credolabcollect){
        return this._http.post(
            this.url.getApivar()+"credolabcollect",
            JSON.stringify({
                'customer_id':customer_id,
                'credolabcollect':credolabcollect
            }),
            this.httpHeader
        );
    }

    credolabDatasets(customer_id){
        return this._http.post(
            this.url.getApivar()+"credolabDatasets",
            JSON.stringify({
                'customer_id':customer_id
            }),
            this.httpHeader
        );
    }

    /////////////////////////////ENCRIPTACION/////////////////////////////////////////
    encrypt_m(textPhrase, paraPhrase) {
              var secret = paraPhrase;
              var encrypted = CryptoJS.AES.encrypt(textPhrase, secret); 
              this.encryptedData = encrypted.toString();
              //console.log("Cipher text: " + this.encryptedData);
              return this.encryptedData;
    }
    decrypt_m(encryptedText, paraPhrase) {
              var secret = paraPhrase;
              this.encryptedData = ''
              var decrypted = CryptoJS.AES.decrypt(encryptedText, secret).toString(CryptoJS.enc.Utf8);
              this.decryptedData = decrypted;
              //console.log("decrypted text: " + this.decryptedData);
              return this.decryptedData;
    }
    encryptRSA(uuid) {
                  //this.UniqueDeviceIDVar =  "{6D127FBA-EC87-4C8E-9FA5-3FBF14B42EA4}";
                  var text = uuid;
                  var cert = forge.pki.certificateFromPem(this.certificateContentRSA);
                  var publicKey = cert.publicKey;
                  let encrypt = publicKey.encrypt(forge.util.encodeUtf8(text), 'RSA-OAEP', {
                    md: forge.md.sha256.create(),
                    mgf1: {
                      md: forge.md.sha1.create()
                    },
                  });
                  this.encryptedDataRSA = forge.util.encode64(encrypt)
                  return this.encryptedDataRSA;
                  
    }
    ///////////////////////////////////////////////////////////////////////////////////
    prueba_encriptacion_m(){
            localStorage.setItem('ENCRIPTARDATA' ,this.encrypt_m('Hola Mundo', 'jfsa9j43223kscfasnc'));
            alert(localStorage.getItem('ENCRIPTARDATA'));
    }
    prueba_desencriptacion_m(){
            alert(this.decrypt_m(localStorage.getItem('ENCRIPTARDATA'), 'jfsa9j43223kscfasnc'));
    }


    get_pending_applications(customerId) {
        this.uniqueDeviceID.get().then((uuid: any) => {
                 this.UniqueDeviceIDVar = uuid;
                 localStorage.setItem('TOKEAPICORE_UniqueDeviceIDVar', this.UniqueDeviceIDVar);
        }).catch((error: any) => {
                 this.UniqueDeviceIDVar = localStorage.getItem('TOKEAPIPAGADORES').substr(1, 15);
                 localStorage.setItem('TOKEAPICORE_UniqueDeviceIDVar', this.UniqueDeviceIDVar);
                 
        });
        this.httpHeader_apipagadores_token = {
                        headers: new HttpHeaders({ 'Content-Type': 'application/json', 
                                                   'Authorization': 'Bearer ' + localStorage.getItem('TOKEAPIPAGADORES'),
                                                   'X-keyvalue':this.encryptRSA(localStorage.getItem('TOKEAPICORE_UniqueDeviceIDVar'))
                                               })
        };
        return this._http.get(
            this.url.getApivarcore() + "get_pending_applications/" + customerId + "?limit=1000",
            this.httpHeader_apipagadores_token
        );
    }    


    get_creditors(creditor_id) {
        this.uniqueDeviceID.get().then((uuid: any) => {
                 this.UniqueDeviceIDVar = uuid;
                 localStorage.setItem('TOKEAPICORE_UniqueDeviceIDVar', this.UniqueDeviceIDVar);
        }).catch((error: any) => {
                 this.UniqueDeviceIDVar = localStorage.getItem('TOKEAPIPAGADORES').substr(1, 15);
                 localStorage.setItem('TOKEAPICORE_UniqueDeviceIDVar', this.UniqueDeviceIDVar);
                 
        });
        this.httpHeader_apipagadores_token = {
                        headers: new HttpHeaders({ 'Content-Type': 'application/json', 
                                                   'Authorization': 'Bearer ' + localStorage.getItem('TOKEAPIPAGADORES'),
                                                   'X-keyvalue':this.encryptRSA(localStorage.getItem('TOKEAPICORE_UniqueDeviceIDVar'))
                                               })
        };
        return this._http.get(
            this.url.getApivarcore() + "creditors/" + creditor_id,
            this.httpHeader_apipagadores_token
        );
    }


    get_product_types(product_type_id) {
        this.uniqueDeviceID.get().then((uuid: any) => {
                 this.UniqueDeviceIDVar = uuid;
                 localStorage.setItem('TOKEAPICORE_UniqueDeviceIDVar', this.UniqueDeviceIDVar);
        }).catch((error: any) => {
                 this.UniqueDeviceIDVar = localStorage.getItem('TOKEAPIPAGADORES').substr(1, 15);
                 localStorage.setItem('TOKEAPICORE_UniqueDeviceIDVar', this.UniqueDeviceIDVar);
                 
        });
        this.httpHeader_apipagadores_token = {
                        headers: new HttpHeaders({ 'Content-Type': 'application/json', 
                                                   'Authorization': 'Bearer ' + localStorage.getItem('TOKEAPIPAGADORES'),
                                                   'X-keyvalue':this.encryptRSA(localStorage.getItem('TOKEAPICORE_UniqueDeviceIDVar'))
                                               })
        };
        return this._http.get(
            this.url.getApivarcore() + "product_types/" + product_type_id,
            this.httpHeader_apipagadores_token
        );
    }


    get_applications(application_id) {
        this.uniqueDeviceID.get().then((uuid: any) => {
                 this.UniqueDeviceIDVar = uuid;
                 localStorage.setItem('TOKEAPICORE_UniqueDeviceIDVar', this.UniqueDeviceIDVar);
        }).catch((error: any) => {
                 this.UniqueDeviceIDVar = localStorage.getItem('TOKEAPIPAGADORES').substr(1, 15);
                 localStorage.setItem('TOKEAPICORE_UniqueDeviceIDVar', this.UniqueDeviceIDVar);
                 
        });
        this.httpHeader_apipagadores_token = {
                        headers: new HttpHeaders({ 'Content-Type': 'application/json', 
                                                   'Authorization': 'Bearer ' + localStorage.getItem('TOKEAPIPAGADORES'),
                                                   'X-keyvalue':this.encryptRSA(localStorage.getItem('TOKEAPICORE_UniqueDeviceIDVar'))
                                               })
        };
        return this._http.get(
            this.url.getApivarcore() + "applications/" + application_id,
            this.httpHeader_apipagadores_token
        );
    }


    get_customer_accounts(contract_id) {
        this.uniqueDeviceID.get().then((uuid: any) => {
                 this.UniqueDeviceIDVar = uuid;
                 localStorage.setItem('TOKEAPICORE_UniqueDeviceIDVar', this.UniqueDeviceIDVar);
        }).catch((error: any) => {
                 this.UniqueDeviceIDVar = localStorage.getItem('TOKEAPIPAGADORES').substr(1, 15);
                 localStorage.setItem('TOKEAPICORE_UniqueDeviceIDVar', this.UniqueDeviceIDVar);
                 
        });
        this.httpHeader_apipagadores_token = {
                        headers: new HttpHeaders({ 'Content-Type': 'application/json', 
                                                   'Authorization': 'Bearer ' + localStorage.getItem('TOKEAPIPAGADORES'),
                                                   'X-keyvalue':this.encryptRSA(localStorage.getItem('TOKEAPICORE_UniqueDeviceIDVar'))
                                               })
        };
        return this._http.get(
            this.url.getApivarcore() + "customer_accounts/" + contract_id,
            this.httpHeader_apipagadores_token
        );
    }


    get_payments_contract(contract_id) {
        this.uniqueDeviceID.get().then((uuid: any) => {
                 this.UniqueDeviceIDVar = uuid;
                 localStorage.setItem('TOKEAPICORE_UniqueDeviceIDVar', this.UniqueDeviceIDVar);
        }).catch((error: any) => {
                 this.UniqueDeviceIDVar = localStorage.getItem('TOKEAPIPAGADORES').substr(1, 15);
                 localStorage.setItem('TOKEAPICORE_UniqueDeviceIDVar', this.UniqueDeviceIDVar);
                 
        });
        this.httpHeader_apipagadores_token = {
                        headers: new HttpHeaders({ 'Content-Type': 'application/json', 
                                                   'Authorization': 'Bearer ' + localStorage.getItem('TOKEAPIPAGADORES'),
                                                   'X-keyvalue':this.encryptRSA(localStorage.getItem('TOKEAPICORE_UniqueDeviceIDVar'))
                                               })
        };
        return this._http.get(
            this.url.getApivarcore() + "customer_accounts/" + contract_id + "/payments",
            this.httpHeader_apipagadores_token
        );
    }


    get_payment_methods() {
        this.uniqueDeviceID.get().then((uuid: any) => {
                 this.UniqueDeviceIDVar = uuid;
                 localStorage.setItem('TOKEAPICORE_UniqueDeviceIDVar', this.UniqueDeviceIDVar);
        }).catch((error: any) => {
                 this.UniqueDeviceIDVar = localStorage.getItem('TOKEAPIPAGADORES').substr(1, 15);
                 localStorage.setItem('TOKEAPICORE_UniqueDeviceIDVar', this.UniqueDeviceIDVar);
                 
        });
        this.httpHeader_apipagadores_token = {
                        headers: new HttpHeaders({ 'Content-Type': 'application/json', 
                                                   'Authorization': 'Bearer ' + localStorage.getItem('TOKEAPIPAGADORES'),
                                                   'X-keyvalue':this.encryptRSA(localStorage.getItem('TOKEAPICORE_UniqueDeviceIDVar'))
                                               })
        };
        return this._http.get(
            this.url.getApivarcore() + "payment_methods",
            this.httpHeader_apipagadores_token
        );
    }


    post_payments(contract_id, account_number, payment_method, payment_gateway, payment_source, transaction_amount, currency, type, authorization) {
        this.uniqueDeviceID.get().then((uuid: any) => {
                 this.UniqueDeviceIDVar = uuid;
                 localStorage.setItem('TOKEAPICORE_UniqueDeviceIDVar', this.UniqueDeviceIDVar);
        }).catch((error: any) => {
                 this.UniqueDeviceIDVar = localStorage.getItem('TOKEAPIPAGADORES').substr(1, 15);
                 localStorage.setItem('TOKEAPICORE_UniqueDeviceIDVar', this.UniqueDeviceIDVar);
                 
        });
        this.httpHeader_apipagadores_token = {
                        headers: new HttpHeaders({ 'Content-Type': 'application/json', 
                                                   'Authorization': 'Bearer ' + localStorage.getItem('TOKEAPIPAGADORES'),
                                                   'X-keyvalue':this.encryptRSA(localStorage.getItem('TOKEAPICORE_UniqueDeviceIDVar'))
                                               })
        };
        return this._http.post(
            this.url.getApivarcore() + "payments",
            JSON.stringify({
                'contract_id': contract_id,
                'account_number': account_number,
                'payment_method': payment_method,
                'payment_gateway': payment_gateway,
                'payment_source': payment_source,
                'transaction_amount': transaction_amount,
                'currency': currency,
                'type': type,
                'authorization': authorization
                //"data":""+this.encrypt_m('{"authorization": "'+authorization+'", "transaction_amount": "'+transaction_amount+'", "currency": "'+currency+'", "type": "'+type+'", "payment_method": "'+payment_method+'", "payment_gateway": "'+payment_gateway+'", "payment_source": "'+payment_source+'", "contract_id": "'+contract_id+'","account_number": "'+account_number+'"}', localStorage.getItem('TOKEAPICORE_UniqueDeviceIDVar'))+"",
            }),
            this.httpHeader_apipagadores_token
        );
    }


    post_customer_decision(application_id) {
        this.uniqueDeviceID.get().then((uuid: any) => {
                 this.UniqueDeviceIDVar = uuid;
                 localStorage.setItem('TOKEAPICORE_UniqueDeviceIDVar', this.UniqueDeviceIDVar);
        }).catch((error: any) => {
                 this.UniqueDeviceIDVar = localStorage.getItem('TOKEAPIPAGADORES').substr(1, 15);
                 localStorage.setItem('TOKEAPICORE_UniqueDeviceIDVar', this.UniqueDeviceIDVar);
                 
        });
        this.httpHeader_apipagadores_token = {
                        headers: new HttpHeaders({ 'Content-Type': 'application/json', 
                                                   'Authorization': 'Bearer ' + localStorage.getItem('TOKEAPIPAGADORES'),
                                                   'X-keyvalue':this.encryptRSA(localStorage.getItem('TOKEAPICORE_UniqueDeviceIDVar'))
                                               })
        };
        return this._http.post(
            this.url.getApivarcore() + "customer_decision/" + application_id,
            JSON.stringify({
                "final_decision": "A"
                //"data":""+this.encrypt_m('{"final_decision": "A"}', localStorage.getItem('TOKEAPICORE_UniqueDeviceIDVar'))+"",
            }),
            this.httpHeader_apipagadores_token
        );
    }


    get_all_creditors() {
        this.uniqueDeviceID.get().then((uuid: any) => {
                 this.UniqueDeviceIDVar = uuid;
                 localStorage.setItem('TOKEAPICORE_UniqueDeviceIDVar', this.UniqueDeviceIDVar);
        }).catch((error: any) => {
                 this.UniqueDeviceIDVar = localStorage.getItem('TOKEAPIPAGADORES').substr(1, 15);
                 localStorage.setItem('TOKEAPICORE_UniqueDeviceIDVar', this.UniqueDeviceIDVar);
                 
        });
        this.httpHeader_apipagadores_token = {
                        headers: new HttpHeaders({ 'Content-Type': 'application/json', 
                                                   'Authorization': 'Bearer ' + localStorage.getItem('TOKEAPIPAGADORES'),
                                                   'X-keyvalue':this.encryptRSA(localStorage.getItem('TOKEAPICORE_UniqueDeviceIDVar'))
                                               })
        };
        return this._http.get(
            this.url.getApivarcore() + "creditors?limit=1000",
            this.httpHeader_apipagadores_token
        );
    }


    get_customer_accounts_by_customer(customer_id) {
        this.uniqueDeviceID.get().then((uuid: any) => {
                 this.UniqueDeviceIDVar = uuid;
                 localStorage.setItem('TOKEAPICORE_UniqueDeviceIDVar', this.UniqueDeviceIDVar);
        }).catch((error: any) => {
                 this.UniqueDeviceIDVar = localStorage.getItem('TOKEAPIPAGADORES').substr(1, 15);
                 localStorage.setItem('TOKEAPICORE_UniqueDeviceIDVar', this.UniqueDeviceIDVar);
                 
        });
        this.httpHeader_apipagadores_token = {
                        headers: new HttpHeaders({ 'Content-Type': 'application/json', 
                                                   'Authorization': 'Bearer ' + localStorage.getItem('TOKEAPIPAGADORES'),
                                                   'X-keyvalue':this.encryptRSA(localStorage.getItem('TOKEAPICORE_UniqueDeviceIDVar'))
                                               })
        };
        return this._http.get(
            this.url.getApivarcore() + "customer_accounts/" + customer_id + "/accounts",
            this.httpHeader_apipagadores_token
        );
    }


    get_all_product_resolutions() {
        this.uniqueDeviceID.get().then((uuid: any) => {
                 this.UniqueDeviceIDVar = uuid;
                 localStorage.setItem('TOKEAPICORE_UniqueDeviceIDVar', this.UniqueDeviceIDVar);
        }).catch((error: any) => {
                 this.UniqueDeviceIDVar = localStorage.getItem('TOKEAPIPAGADORES').substr(1, 15);
                 localStorage.setItem('TOKEAPICORE_UniqueDeviceIDVar', this.UniqueDeviceIDVar);
                 
        });
        this.httpHeader_apipagadores_token = {
                        headers: new HttpHeaders({ 'Content-Type': 'application/json', 
                                                   'Authorization': 'Bearer ' + localStorage.getItem('TOKEAPIPAGADORES'),
                                                   'X-keyvalue':this.encryptRSA(localStorage.getItem('TOKEAPICORE_UniqueDeviceIDVar'))
                                               })
        };
        return this._http.get(
            this.url.getApivarcore() + "product_resolutions",
            this.httpHeader_apipagadores_token
        );
    }    


    put_customers_id(customer_id, nombres, apellidos, email, national_id, cellphone, birthdate = '1/1/1900', country = 'DOM', gender = 'Masculino', marital_status = 'Soltero') {
        this.uniqueDeviceID.get().then((uuid: any) => {
                 this.UniqueDeviceIDVar = uuid;
                 localStorage.setItem('TOKEAPICORE_UniqueDeviceIDVar', this.UniqueDeviceIDVar);
        }).catch((error: any) => {
                 this.UniqueDeviceIDVar = localStorage.getItem('TOKEAPIPAGADORES').substr(1, 15);
                 localStorage.setItem('TOKEAPICORE_UniqueDeviceIDVar', this.UniqueDeviceIDVar);
                 
        });
        this.httpHeader_apipagadores_token = {
                        headers: new HttpHeaders({ 'Content-Type': 'application/json', 
                                                   'Authorization': 'Bearer ' + localStorage.getItem('TOKEAPIPAGADORES'),
                                                   'X-keyvalue':this.encryptRSA(localStorage.getItem('TOKEAPICORE_UniqueDeviceIDVar'))
                                               })
        };
        return this._http.put(
            this.url.getApivarcore() + "customers/" + customer_id,
            JSON.stringify({
                'first_name': nombres,
                'last_name': apellidos,
                'email': email,
                'cellphone': cellphone,
                'national_id': national_id,
                'birthdate': birthdate,
                'country': country.substr(0, 3),
                'gender': gender,
                'marital_status': marital_status
                //"data":""+this.encrypt_m('{"birthdate": "'+birthdate+'", "country": "'+country.substr(0, 3)+'", "email": "'+email+'", "cellphone": "'+cellphone+'", "national_id": "'+national_id+'", "first_name": "'+nombres+'", "last_name": "'+apellidos+'", "marital_status": "'+marital_status+'", "gender": "'+gender+'"}', localStorage.getItem('TOKEAPICORE_UniqueDeviceIDVar'))+"",
            }),
            this.httpHeader_apipagadores_token
        );
    }
    

}

