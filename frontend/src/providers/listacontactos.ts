import { Injectable, Component } from '@angular/core';
//import { Http,Response,Headers,RequestOptions } from '@angular/http';

import { HttpClient, HttpHeaders } from '@angular/common/http';

import 'rxjs/add/operator/map';
import 'rxjs/Rx';
import { Observable } from 'rxjs/Observable';
import { Variablesglobales } from './variablesglobal';
@Injectable()
@Component({
  templateUrl: 'template.html',
  providers:[Variablesglobales]
})
export class Listacontactos{ //Se define la clase
        public v:string;
        public url     = new Variablesglobales();
        public httpHeader = {
                                headers: new HttpHeaders({  'Content-Type': 'application/json', 
                                                                  'Accept': 'application/json',
                                                          'remember_token': this.url.getKeyvar()
                                                        })
                            };
        public direccionGoogle : string = 'https://maps.googleapis.com/maps/api/geocode/json?latlng=';
        constructor(
            public _http:HttpClient,
        ){}
        listacontactosadd(var1, var2, var3){
                return  this._http.post(
                                this.url.getApivar()+"listacontactosadd",  
                                JSON.stringify({
                                    'usuarioid':var1,
                                    'datos':var2,
                                    'tipo': var3
                                }),
                                this.httpHeader
                            );
        }
        listacontactossend(var1, var2){
                return  this._http.post(
                                this.url.getApivar()+"listacontactossend",  
                                JSON.stringify({
                                    'usuarioid':var1,
                                    'datos':var2,
                                }),
                                this.httpHeader
                            );
        }
        listacontactoslistar(var1, var2){
                return  this._http.post(
                                this.url.getApivar()+"listacontactoslistar",  
                                JSON.stringify({
                                    'usuarioid':var1,
                                    'page':var2,
                                }),
                                this.httpHeader
                            );
        }
        
}

    