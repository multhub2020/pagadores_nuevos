import { Injectable } from '@angular/core';
@Injectable()

export class Helpers {

    number_format(number, decimals, dec_point, thousands_point) {

        if (!decimals) {
            var len = number.toString().split('.').length;
            decimals = len > 1 ? len : 0;
        }

        if (!dec_point) {
            dec_point = '.';
        }

        if (!thousands_point) {
            thousands_point = ',';
        }

        return number = parseFloat(number).toFixed(decimals);

        number = number.replace(".", dec_point);

        var splitNum = number.split(dec_point);
        splitNum[0] = splitNum[0].replace(/\B(?=(\d{3})+(?!\d))/g, thousands_point);
        number = splitNum.join(dec_point);

        return Number(number);
    }

    replaceLeters(str) {
        str = str.replace(/a/gi, "");
        str = str.replace(/b/gi, "");
        str = str.replace(/c/gi, "");
        str = str.replace(/d/gi, "");
        str = str.replace(/e/gi, "");
        str = str.replace(/f/gi, "");
        str = str.replace(/g/gi, "");
        str = str.replace(/h/gi, "");
        str = str.replace(/i/gi, "");
        str = str.replace(/j/gi, "");
        str = str.replace(/k/gi, "");
        str = str.replace(/l/gi, "");
        str = str.replace(/m/gi, "");
        str = str.replace(/n/gi, "");
        str = str.replace(/o/gi, "");
        str = str.replace(/p/gi, "");
        str = str.replace(/q/gi, "");
        str = str.replace(/r/gi, "");
        str = str.replace(/s/gi, "");
        str = str.replace(/t/gi, "");
        str = str.replace(/u/gi, "");
        str = str.replace(/v/gi, "");
        str = str.replace(/w/gi, "");
        str = str.replace(/x/gi, "");
        str = str.replace(/y/gi, "");
        str = str.replace(/z/gi, "");

        return str;
    }

    async credolabcollect(recordNumber) {alert('collect');
        var cordova: any;
        var exec = cordova.require("cordova/exec");

        let url = 'https://scoring-demo.credolab.com/';
        let auth = '290a5866-293e-4956-8bf5-993fdb2751f1';

        exec(async function (succes) {
            alert("Data has been successfully uploaded:" + succes)
        },
            async function (error) {
                alert("Error: " + error)
            },
            "CredoAppSdk",
            "collect",
            [{
                url: url,
                authKey: auth,
                recordNumber: recordNumber
            }]
        );
    }

}