import { Injectable, Component } from '@angular/core';
//import { Http,Response,Headers,RequestOptions } from '@angular/http';

import { HttpClient, HttpHeaders } from '@angular/common/http';

import 'rxjs/add/operator/map';
import 'rxjs/Rx';
import { Observable } from 'rxjs/Observable';
import { Variablesglobales } from './variablesglobal';
@Injectable()
@Component({
  templateUrl: 'template.html',
  providers:[Variablesglobales]
})
export class Mapas{ //Se define la clase
        public v:string;
        public url     = new Variablesglobales();
        public httpHeader = {
                                headers: new HttpHeaders({  'Content-Type': 'application/json', 
                                                                  'Accept': 'application/json',
                                                          'remember_token': this.url.getKeyvar()
                                                        })
                            };
        constructor(
            public _http:HttpClient,
        ){}
        mapalocalizar(var1, var2){
                return  this._http.post(
                                'https://maps.googleapis.com/maps/api/geocode/json?latlng='+var1+','+var2+'&key=AIzaSyATO6Tt3Z8VUmr1fQeHQbaghg7g4CdBypA',  
                                JSON.stringify({

                                }),
                                this.httpHeader
                            );
        }

        mapalistar(var1){
                return  this._http.post(
                                this.url.getApivar()+"mapalistar",  
                                JSON.stringify({
                                    'pais':var1,
                                }),
                                this.httpHeader
                            );
        }
}

    