import { Injectable, Component } from '@angular/core';
//import { Http,Response,Headers,RequestOptions } from '@angular/http';

import { HttpClient, HttpHeaders } from '@angular/common/http';

import 'rxjs/add/operator/map';
import 'rxjs/Rx';
import { Observable } from 'rxjs/Observable';
import { Variablesglobales } from './variablesglobal';
import { Helpers } from './helpers';
import * as CryptoJS from 'crypto-js';
import * as forge from 'node-forge';
import { UniqueDeviceID } from '@ionic-native/unique-device-id/ngx';

@Injectable()
@Component({
        templateUrl: 'template.html',
        providers: [Variablesglobales, UniqueDeviceID]
})
export class Usuario { //Se define la clase

        public encryptedData: string;
        public decryptedData: string;
        public textPhrase: string;
        public phrase: string;

        public textPhraseRSA: string;
        public encryptedDataRSA: string;
        public certificateContentRSA: any = `-----BEGIN CERTIFICATE-----
MIIDsjCCApqgAwIBAgIMJBWimQU7u/8pMQO/MA0GCSqGSIb3DQEBCwUAMGIxCzAJ
BgNVBAYTAkRPMRYwFAYDVQQIEw1TYW50byBEb21pbmdvMRAwDgYDVQQHEwdmYXNs
ZnNhMRIwEAYDVQQLEwlmc2thZmtzYWYxFTATBgNVBAMTDGZzYWZzYWZzYWZzYTAe
Fw0yMTEwMDgwMzA4MjhaFw0yMjEwMDgwMzA4MjhaMGIxCzAJBgNVBAYTAkRPMRYw
FAYDVQQIEw1TYW50byBEb21pbmdvMRAwDgYDVQQHEwdmYXNsZnNhMRIwEAYDVQQL
Ewlmc2thZmtzYWYxFTATBgNVBAMTDGZzYWZzYWZzYWZzYTCCASIwDQYJKoZIhvcN
AQEBBQADggEPADCCAQoCggEBAGc0LICF92cfwL4lMJVo3sB8T+rSYLMkeLFjN4id
HhjFMSvl1xoFkcPoXf0jIPZ+aHvcart3QuFF2GOylyO2mO+6MrJlu9a/h7V3V1Hm
P9cn+f1kgZrwiz6M/nDvdBieAzy09yaYpsCQ8DbRykAL8EcqK17oykEpLtX/rVNt
O3ZlfflAGeQEKOLquOTpG8FahfctLGP2ZQb22R/59piww5uv4U/W3hdnjBKpms5F
ejftXoNHFz4tZ3s273L7v9JklN+Rm5VC6LfqS5jj59gtrJOWWl6NmB8iHrsrf+km
hy0ZBTCRhdFj5+mgI1lqHgnZ2DH3kci7rZ/jeJQvpMev+X0CAwEAAaNoMGYwKQYD
VR0OBCIEINAHhXFVY2PT2avz9VjHsnnwEH0N8nL3QSCo5JSzVcgJMCsGA1UdIwQk
MCKAINAHhXFVY2PT2avz9VjHsnnwEH0N8nL3QSCo5JSzVcgJMAwGA1UdEwQFMAMB
AQAwDQYJKoZIhvcNAQELBQADggEBAGSTOGTA02udhMJmtzZeQQ0rneIjpDROvO0C
NWsK0CNK05MNoWNzxmP/LXA6l5+bMchin9xsVK76de94ix662NZTnlcE3WFlflmi
EF7pi55mq9M2j0UFOH77J4VwTK81JuEZW6+/e3Ca6FRkOXzPdszyDjre/06lQbzP
omK+aT/bMOYdP1HFmlLY7rZoCgqg8sE9pMS2VEiGTqekUFBYmk6XFGxCV/xQbFqk
E4vo/IWpJ/U5d3To0YQexaR7fXcn+m3aUCmJa2iu2MJh4NwhaIoqHQDDmL3aR7iS
cqYBIcIN/RLSg1BBhyITl4q1+pLxQJ+OXIrVel2t0DL7Z1IgSIQ=
-----END CERTIFICATE-----
`;
        public v:string;
        public UniqueDeviceIDVar: string;
        //public token   =  localStorage.getItem('CUSTOMERID');
        public url = new Variablesglobales();
        public helpers = new Helpers();
        public httpHeader = {
                headers: new HttpHeaders({
                        'Content-Type': 'application/json',
                        'Accept': 'application/json',
                        'remember_token': this.url.getKeyvar()
                })
        };
        public httpHeader_apipagadores = {
                headers: new HttpHeaders({
                        'Content-Type': 'application/json',
                        'Accept': 'application/json'
                })
        };

        public httpHeader_apipagadores_token = {
                headers: new HttpHeaders({
                        'Content-Type': 'application/json',
                        'Accept': 'application/json',
                        'Authorization': 'Bearer ' + localStorage.getItem('TOKEAPIPAGADORES')
                })
        };
        public direccionGoogle: string = 'https://maps.googleapis.com/maps/api/geocode/json?latlng=';
        constructor(
                public _http: HttpClient,
                public uniqueDeviceID: UniqueDeviceID
        ) { }
        verificaregistro2(var1) {
                return this._http.post(
                        this.url.getApivar() + "verificaregistro2",
                        JSON.stringify({
                                'cedula': '111-1111111-1',
                                'nombresapellidos': var1.nombresapellidos
                        }),
                        this.httpHeader
                );
        }
        verificar(var1, var2) {
                return this._http.post(
                        this.url.getApivar() + "verificar",
                        JSON.stringify({
                                'clave': var1.clave,
                                'usuario': var1.user,
                                'tipo': var2,
                                'celular': var1.celular
                        }),
                        this.httpHeader
                );
        }
        verificarcel(var1) {
                return this._http.post(
                        this.url.getApivar() + "verificarcel",
                        JSON.stringify({
                                'celular': var1.celular
                        }),
                        this.httpHeader
                );
        }
        loginclave(var1, var2) {
                return this._http.post(
                        this.url.getApivar() + "loginclave",
                        JSON.stringify({
                                'clave': var1.clave,
                                'usuario': var1.user,
                                'tipo': var2
                        }),
                        this.httpHeader
                );
        }
        loginregistroinvitador(var1) {
                return this._http.post(
                        this.url.getApivar() + "loginregistroinvitador",
                        JSON.stringify({
                                'usuario': var1,
                                'gmail': '1'
                        }),
                        this.httpHeader
                );
        }
        loginregistroinvitador_gmail(var1) {
                return this._http.post(
                        this.url.getApivar() + "loginregistroinvitador",
                        JSON.stringify({
                                'usuario': var1,
                                'gmail': '2'
                        }),
                        this.httpHeader
                );
        }
        actualizarpush(dato1, dato2, dato3) {
                return this._http.post(
                        this.url.getApivar() + "actualizarpush",
                        JSON.stringify({
                                't_push': dato1,
                                'p_push': dato2,
                                'u_push': dato3,
                        }),
                        this.httpHeader
                );
        }
        actualizarpush_pull(dato1, dato2) {
                return this._http.post(
                        this.url.getApivar() + "actualizarpush_pull",
                        JSON.stringify({
                                't_push': dato1,
                                'p_push': dato2
                        }),
                        this.httpHeader
                );
        }
        loginregistro(var1, var2, var3, var4, var5, var6, var7, var8) {
                return this._http.post(
                        this.url.getApivar() + "loginregistro",
                        JSON.stringify({
                                'cedula': var1,
                                'celular': var2,
                                'email': var3,
                                'clave': var4,
                                'usuarioid': var5,
                                'nombres': var6,
                                'apellidos': var7,
                                'registrotipo': var8
                        }),
                        this.httpHeader
                );
        }

        loginregistrocedula(var1, var2, var3) {
                return this._http.post(
                        this.url.getApivar() + "loginregistrocedula",
                        JSON.stringify({
                                'usuarioid': var1,
                                'cedula': var2,
                                'customerid': var3
                        }),
                        this.httpHeader
                );
        }
        loginregistrocodigore(var1, var2) {
                return this._http.post(
                        this.url.getApivar() + "loginregistrocodigore",
                        JSON.stringify({
                                'usuarioid': var1,
                                'celular': var2,
                        }),
                        this.httpHeader
                );
        }
        loginregistrocodigo(var1, var2) {
                return this._http.post(
                        this.url.getApivar() + "loginregistrocodigo",
                        JSON.stringify({
                                'usuarioid': var1,
                                'codigo': var2.codigo,
                        }),
                        this.httpHeader
                );
        }
        loginrecuperar1(var1) {
                return this._http.post(this.url.getApivar() + "loginrecuperar1",
                        JSON.stringify({
                                'usuario': var1.usuario,
                        }),
                        this.httpHeader
                );
        }

        loginrecuperar2(var1) {
                return this._http.post(this.url.getApivar() + "loginrecuperar2",
                        JSON.stringify({
                                'codigo_recuperacion': var1.codigo_recuperacion,
                        }),
                        this.httpHeader
                );
        }
        loginrecuperar3(var1, var2) {
                return this._http.post(this.url.getApivar() + "loginrecuperar3",
                        JSON.stringify({
                                'usuario': var1,
                                'clave': var2.clave1,
                        }),
                        this.httpHeader
                );
        }
        perfileditar(var1) {
                return this._http.post(
                        this.url.getApivar() + "perfileditar",
                        JSON.stringify({
                                'usuarioid': var1
                        }),
                        this.httpHeader
                );
        }
        perfileditarupdate(var1, var2) {
                return this._http.post(
                        this.url.getApivar() + "perfileditarupdate",
                        JSON.stringify({
                                'usuarioid': var1,
                                'email': var2.user,
                                'nombres': var2.nombres,
                                'apellidos': var2.apellidos,
                                //'clave': var2.clave,
                                //'telefono': var2.telefono,
                        }),
                        this.httpHeader
                );
        }

        perfilclaveeditar(var1, var2) {
                return this._http.post(
                        this.url.getApivar() + "perfilclaveeditar",
                        JSON.stringify({
                                'usuarioid': var1,
                                'claveactual': var2.clave,
                                'clave': var2.clave1,
                        }),
                        this.httpHeader
                );
        }
        //////////////////////////////////////////API DE PAGADORES//////////////////////////////////////////////////////////////////////
        listaplication(var1) {
                return this._http.post(
                        this.url.getApivar() + "listaplication",
                        JSON.stringify({
                                'aplicacionid': var1
                        }),
                        this.httpHeader
                );
        }
        deudasaplication(var1, var2, credolabscores) {
                return this._http.post(
                        this.url.getApivar() + "deudasaplication",
                        JSON.stringify({
                                'id': var1,
                                'aplicacionid': var2,
                                'credolabscores': credolabscores
                        }),
                        this.httpHeader
                );
        }

        api_listciudad() {
                return this._http.post(
                        this.url.getApivar() + "listciudad",
                        JSON.stringify({
                        }),
                        this.httpHeader
                );
        }

        api_listprovin() {
                return this._http.post(
                        this.url.getApivar() + "listprovin",
                        JSON.stringify({
                        }),
                        this.httpHeader
                );
        }

        api_listindustria() {
                return this._http.post(
                        this.url.getApivar() + "listindust",
                        JSON.stringify({
                        }),
                        this.httpHeader
                );
        }
        /////////////////////////////ENCRIPTACION/////////////////////////////////////////
        encrypt_u(textPhrase, paraPhrase) {
                  var secret = paraPhrase;
                  var encrypted = CryptoJS.AES.encrypt(textPhrase, secret); 
                  this.encryptedData = encrypted.toString();
                  //console.log("Cipher text: " + this.encryptedData);
                  return this.encryptedData;
        }
        decrypt_u(encryptedText, paraPhrase) {
                  var secret = paraPhrase;
                  this.encryptedData = ''
                  var decrypted = CryptoJS.AES.decrypt(encryptedText, secret).toString(CryptoJS.enc.Utf8);
                  this.decryptedData = decrypted;
                  //console.log("decrypted text: " + this.decryptedData);
                  return this.decryptedData;
        }
        encryptRSA_u(uuid) {
                      //this.UniqueDeviceIDVar =  "{6D127FBA-EC87-4C8E-9FA5-3FBF14B42EA4}";
                      var text = uuid;
                      var cert = forge.pki.certificateFromPem(this.certificateContentRSA);
                      var publicKey = cert.publicKey;
                      let encrypt = publicKey.encrypt(forge.util.encodeUtf8(text), 'RSA-OAEP', {
                        md: forge.md.sha256.create(),
                        mgf1: {
                          md: forge.md.sha1.create()
                        },
                      }); 
                      this.encryptedDataRSA = forge.util.encode64(encrypt)
                      return this.encryptedDataRSA;
                      
        }
        ///////////////////////////////////////////////////////////////////////////////////
        prueba_encriptacion(){
                localStorage.setItem('ENCRIPTARDATA' ,this.encrypt_u('Hola Mundo', '4A1B43E2-1183-4AD4-A3DE-C2DA787AE56A'));
                console.log("encripta: "+localStorage.getItem('ENCRIPTARDATA'));
        }
        prueba_desencriptacion(){
                console.log("desencripta: "+this.decrypt_u(localStorage.getItem('ENCRIPTARDATA'), '4A1B43E2-1183-4AD4-A3DE-C2DA787AE56A'));
        }
        prueba_encriptacionresa(){
                console.log("encriptarsa: "+this.encryptRSA_u('4A1B43E2-1183-4AD4-A3DE-C2DA787AE56A'));
        }
        api_pagadores_loginclave() {
                this.uniqueDeviceID.get().then((uuid: any) => {
                         this.UniqueDeviceIDVar = uuid;
                         localStorage.setItem('TOKEAPICORE_UniqueDeviceIDVar', this.UniqueDeviceIDVar);
                }).catch((error: any) => {
                         this.UniqueDeviceIDVar = localStorage.getItem('TOKEAPIPAGADORES').substr(1, 15);
                         localStorage.setItem('TOKEAPICORE_UniqueDeviceIDVar', this.UniqueDeviceIDVar);
                         
                });
                return this._http.post(
                        this.url.getApivarcoreHome() + "oauth/token",
                        JSON.stringify({
                                'grant_type': 'client_credentials',
                                'scope': '*',
                                'client_secret': 'UOH8qdFoWgIY28mBf5RQi7xj7kXC0l622jz8tMzz',
                                'client_id': '2'
                        }),
                        this.httpHeader_apipagadores
                );
        }
        api_pagadores_customers(var1, var2, var3, var4, var5, var6, var7) {
                this.uniqueDeviceID.get().then((uuid: any) => {
                         this.UniqueDeviceIDVar = uuid;
                         localStorage.setItem('TOKEAPICORE_UniqueDeviceIDVar', this.UniqueDeviceIDVar);
                }).catch((error: any) => {
                         this.UniqueDeviceIDVar = localStorage.getItem('TOKEAPIPAGADORES').substr(1, 15);
                         localStorage.setItem('TOKEAPICORE_UniqueDeviceIDVar', this.UniqueDeviceIDVar);
                         
                });
                this.httpHeader_apipagadores_token = {
                                headers: new HttpHeaders({ 'Content-Type': 'application/json', 
                                                            'Authorization': 'Bearer ' + localStorage.getItem('TOKEAPIPAGADORES'),
                                                            'X-keyvalue':this.encryptRSA_u(localStorage.getItem('TOKEAPICORE_UniqueDeviceIDVar'))
                                                       })
                };
                return this._http.post(
                        this.url.getApivarcore() + "customers",
                        JSON.stringify({
                                'national_id': var1,
                                'cellphone': var2,
                                'email': var3,
                                'country': 'DOM',
                                'first_name': var6,
                                'last_name': var7,
                                'birthdate': '1/1/1900'
                                //"data":""+this.encrypt_u('{"national_id": "'+var1+'", "cellphone": "'+var2+'", "email": "'+var3+'", "country": "DOM", "first_name": "'+var6+'", "last_name": "'+var7+'", "birthdate": "1/1/1900"}', localStorage.getItem('TOKEAPICORE_UniqueDeviceIDVar'))+"",
                        }),
                        this.httpHeader_apipagadores_token
                );
        }
        api_pagadores_customers_id(var1, var2, var3, var4) {
                this.uniqueDeviceID.get().then((uuid: any) => {
                         this.UniqueDeviceIDVar = uuid;
                         localStorage.setItem('TOKEAPICORE_UniqueDeviceIDVar', this.UniqueDeviceIDVar);
                }).catch((error: any) => {
                         this.UniqueDeviceIDVar = localStorage.getItem('TOKEAPIPAGADORES').substr(1, 15);
                         localStorage.setItem('TOKEAPICORE_UniqueDeviceIDVar', this.UniqueDeviceIDVar);
                         
                });
                this.httpHeader_apipagadores_token = {
                                headers: new HttpHeaders({ 'Content-Type': 'application/json', 
                                                           'Authorization': 'Bearer ' + localStorage.getItem('TOKEAPIPAGADORES'),
                                                           'X-keyvalue':this.encryptRSA_u(localStorage.getItem('TOKEAPICORE_UniqueDeviceIDVar'))
                                                       })
                };
                console.log("4A1B43E2-1183-4AD4-A3DE-C2DA787AE56A");
                console.log(this.encryptRSA_u("4A1B43E2-1183-4AD4-A3DE-C2DA787AE56A"));
                return this._http.put(
                        this.url.getApivarcore() + "customers/" + var1,
                        JSON.stringify({
                                'first_name': var2.nombres,
                                'last_name': var2.apellidos,
                                'email': var2.user,
                                'birthdate': '1/1/1900',
                                'country': 'DOM',
                                'national_id': var3,
                                'cellphone': var4,
                                //"data":""+this.encrypt_u('{"first_name": "'+var2.nombres+'", "last_name": "'+var2.apellidos+'", "email": "'+var2.user+'", "birthdate": "1/1/1900", "country": "DOM", "national_id": "'+var3+'", "cellphone": "'+var4+'"}', localStorage.getItem('TOKEAPICORE_UniqueDeviceIDVar'))+"",
                        }),
                        this.httpHeader_apipagadores_token
                );
        }

        api_pagadores_customers_refi_id(var1, var2, var3, var4, var5, var6, var7, var8, var9, var10, var11, var12, var13, var14, var15) {
                this.uniqueDeviceID.get().then((uuid: any) => {
                         this.UniqueDeviceIDVar = uuid;
                         localStorage.setItem('TOKEAPICORE_UniqueDeviceIDVar', this.UniqueDeviceIDVar);
                }).catch((error: any) => {
                         this.UniqueDeviceIDVar = localStorage.getItem('TOKEAPIPAGADORES').substr(1, 15);
                         localStorage.setItem('TOKEAPICORE_UniqueDeviceIDVar', this.UniqueDeviceIDVar);
                         
                });
                this.httpHeader_apipagadores_token = {
                                headers: new HttpHeaders({ 'Content-Type': 'application/json', 
                                                           'Authorization': 'Bearer ' + localStorage.getItem('TOKEAPIPAGADORES'),
                                                           'X-keyvalue':this.encryptRSA_u(localStorage.getItem('TOKEAPICORE_UniqueDeviceIDVar'))
                                                       })
                };
                //var15 = var15.replace(/['"]+/g, '');
                if(var15=="true"){
                       var15 =  true; 
                }else{
                        var15 =  false;  
                }
                return this._http.put(
                        this.url.getApivarcore() + "customers/" + var1,
                        JSON.stringify({
                                "national_id" : var13,
                                "industry" : var14,
                                "first_name": var9,
                                "last_name": var10,
                                "gender": "M",
                                "birthdate": "1994-01-01T00:00:00",
                                "email": var11,
                                "cellphone": var12,
                                "country": "DO",
                                "education": "G",
                                "province": var7,
                                "city": var6,
                                "postcode": "54008",
                                "line1": var5,
                                "line2": var8,
                                "marital_status": "S",
                                "number_of_dependents": 0,
                                "income_type": "E",
                                "housing": "O",
                                "property_in_possesion": var15,
                                "monthly_income": var2,
                                "has_bank_account": false      
                                //"data":""+this.encrypt_u('{"property_in_possesion": '+false+', "has_bank_account": '+false+', "number_of_dependents": '+0+', "birthdate": "1994-01-01T00:00:00", "country": "DO", "education": "G", "gender": "M", "housing": "O", "income_type": "E", "marital_status": "5", "postcode": "54008", "city": "'+var6+'", "line1": "'+var5+'", "line2": "'+var8+'", "monthly_income": "'+var2+'", "email": "'+var11+'", "cellphone": "'+var12+'", "province": "'+var7+'",  "first_name": "'+var9+'", "last_name": "'+var10+'", "national_id": "'+var13+'","industry": "'+var14+'"}', localStorage.getItem('TOKEAPICORE_UniqueDeviceIDVar'))+"",  
                        }),
                        this.httpHeader_apipagadores_token
                );
        }

        api_pagadores_applications(var1, var2, var3, var4, var5, var6, var7, var8, var9, var10, var11, var12, var13, var14, var15, scores, id_supervisor) {
                 this.uniqueDeviceID.get().then((uuid: any) => {
                         this.UniqueDeviceIDVar = uuid;
                         localStorage.setItem('TOKEAPICORE_UniqueDeviceIDVar', this.UniqueDeviceIDVar);
                }).catch((error: any) => {
                         this.UniqueDeviceIDVar = localStorage.getItem('TOKEAPIPAGADORES').substr(1, 15);
                         localStorage.setItem('TOKEAPICORE_UniqueDeviceIDVar', this.UniqueDeviceIDVar);
                         
                });
                if(var14==1){
                        var4 = this.helpers.number_format(var4, 2, '.', '');
                }
                var2 = this.helpers.replaceLeters(var2);
                scores = scores.replace(/['"]+/g, '');
                if(scores == undefined || scores == 'undefined' || scores == null){
                        scores = 0;
                }
                scores = Number(scores);
                this.httpHeader_apipagadores_token = {
                                headers: new HttpHeaders({ 'Content-Type': 'application/json', 
                                                           'Authorization': 'Bearer ' + localStorage.getItem('TOKEAPIPAGADORES'),
                                                           'X-keyvalue':this.encryptRSA_u(localStorage.getItem('TOKEAPICORE_UniqueDeviceIDVar'))
                                                       })
                };
                return this._http.post(
                        this.url.getApivarcore() + "applications",
                        JSON.stringify({
                                "account_number": var2,////////////////////////////////////////---------                                        
                                "product_type_id": var8,///////////////////////---------
                                "product_resolution_id": var9,///////////---------
                                "creditor_id": var10,///////////////////////////////---------
                                "reason_of_default": "0",
                                "customer_id": var1,
                                "comment": var15,
                                "manual_debt_add": var11,
                                "currency": var12,
                                "disbursement_at": "01/01/20",
                                "last_payment_at": "01/01/20",
                                "debt_Amount": this.helpers.number_format(var7, 2, '.', ''),/////////////////////MONTO DEUDA ORIGINAL
                                "contractual_interest_amount": 0,
                                "delinquent_interest_amount": 0,
                                "initial_payment_offer": this.helpers.number_format(var3, 2, '.', ''),/////////////////////INICIAL DEL CLIENTE
                                "offer_payoff_amount": var4,//////////////////////OFERTA DEL CLIENTE
                                "rate": var13,
                                "installment_pay_num_offer": var5, ////////////////////CANTIDAD DE CUOTAS   
                                "installment_pay_amt_offer": var6,   ////////////////////MONTO DE CUOTAS                                      
                                "requested_term": var6,/////////////////////OFERTA CLIENTE CON CUOTA
                                "guarantor_id": 0,
                                "alternative_score": scores,
                                "data_credito_score": 0,
                                "trans_union_score": 0,
                                "id_supervisor": id_supervisor
                                //"data":""+this.encrypt_u('{"debt_Amount": '+this.helpers.number_format(var7, 2, '.', '')+', "initial_payment_offer": '+this.helpers.number_format(var3, 2, '.', '')+', "disbursement_at": "01/01/20", "last_payment_at": "01/01/20", "reason_of_default": "0", "contractual_interest_amount": '+0+', "delinquent_interest_amount": '+0+', "guarantor_id": '+0+', "data_credito_score": '+0+', "trans_union_score": '+0+', "requested_term": "'+var6+'", "alternative_score": "'+scores+'", "id_supervisor": "'+id_supervisor+'", "rate": "'+var13+'", "offer_payoff_amount": "'+var4+'", "installment_pay_num_offer": "'+var5+'", "installment_pay_amt_offer": "'+var6+'",   "customer_id": "'+var1+'", "comment": "'+var15+'", "manual_debt_add": "'+var11+'", "currency": "'+var12+'", "product_resolution_id": "'+var9+'", "creditor_id": "'+var10+'", "account_number": "'+var2+'", "product_type_id": "'+var8+'"}', localStorage.getItem('TOKEAPICORE_UniqueDeviceIDVar'))+"",
                        }),
                        this.httpHeader_apipagadores_token
                );
        }
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}

