import * as CryptoJS from 'crypto-js';
import * as forge from 'node-forge';

import { UniqueDeviceID } from '@ionic-native/unique-device-id/ngx';
import { Injectable, Component } from '@angular/core';
@Injectable()
@Component({
  templateUrl: 'template.html',
  providers:[UniqueDeviceID] 
})
export class Variablesglobales {

  public Apivar: string;
  public Apivarcore: string;
  public Apivarcorehome: string;
  public ApivarcorehomeI: string;
  public Tipoaccion: string;
  public Keyvar: string;
  public Servar: string;

  public credolabApi: string;
  public credolabUser: string;
  public credolabPassword: string;
  public credolabAuth: string;
  public credolabUrl: string;

  public encryptedData: string;
  public decryptedData: string;
  public textPhrase: string;
  public phrase: string;

  public textPhraseRSA: string;
  public encryptedDataRSA: string;
  public certificateContentRSA: any = `-----BEGIN CERTIFICATE-----
MIIDsjCCApqgAwIBAgIMJBWimQU7u/8pMQO/MA0GCSqGSIb3DQEBCwUAMGIxCzAJ
BgNVBAYTAkRPMRYwFAYDVQQIEw1TYW50byBEb21pbmdvMRAwDgYDVQQHEwdmYXNs
ZnNhMRIwEAYDVQQLEwlmc2thZmtzYWYxFTATBgNVBAMTDGZzYWZzYWZzYWZzYTAe
Fw0yMTEwMDgwMzA4MjhaFw0yMjEwMDgwMzA4MjhaMGIxCzAJBgNVBAYTAkRPMRYw
FAYDVQQIEw1TYW50byBEb21pbmdvMRAwDgYDVQQHEwdmYXNsZnNhMRIwEAYDVQQL
Ewlmc2thZmtzYWYxFTATBgNVBAMTDGZzYWZzYWZzYWZzYTCCASIwDQYJKoZIhvcN
AQEBBQADggEPADCCAQoCggEBAGc0LICF92cfwL4lMJVo3sB8T+rSYLMkeLFjN4id
HhjFMSvl1xoFkcPoXf0jIPZ+aHvcart3QuFF2GOylyO2mO+6MrJlu9a/h7V3V1Hm
P9cn+f1kgZrwiz6M/nDvdBieAzy09yaYpsCQ8DbRykAL8EcqK17oykEpLtX/rVNt
O3ZlfflAGeQEKOLquOTpG8FahfctLGP2ZQb22R/59piww5uv4U/W3hdnjBKpms5F
ejftXoNHFz4tZ3s273L7v9JklN+Rm5VC6LfqS5jj59gtrJOWWl6NmB8iHrsrf+km
hy0ZBTCRhdFj5+mgI1lqHgnZ2DH3kci7rZ/jeJQvpMev+X0CAwEAAaNoMGYwKQYD
VR0OBCIEINAHhXFVY2PT2avz9VjHsnnwEH0N8nL3QSCo5JSzVcgJMCsGA1UdIwQk
MCKAINAHhXFVY2PT2avz9VjHsnnwEH0N8nL3QSCo5JSzVcgJMAwGA1UdEwQFMAMB
AQAwDQYJKoZIhvcNAQELBQADggEBAGSTOGTA02udhMJmtzZeQQ0rneIjpDROvO0C
NWsK0CNK05MNoWNzxmP/LXA6l5+bMchin9xsVK76de94ix662NZTnlcE3WFlflmi
EF7pi55mq9M2j0UFOH77J4VwTK81JuEZW6+/e3Ca6FRkOXzPdszyDjre/06lQbzP
omK+aT/bMOYdP1HFmlLY7rZoCgqg8sE9pMS2VEiGTqekUFBYmk6XFGxCV/xQbFqk
E4vo/IWpJ/U5d3To0YQexaR7fXcn+m3aUCmJa2iu2MJh4NwhaIoqHQDDmL3aR7iS
cqYBIcIN/RLSg1BBhyITl4q1+pLxQJ+OXIrVel2t0DL7Z1IgSIQ=
-----END CERTIFICATE-----
`;
  public v:string;
  public UniqueDeviceIDVar: string;

  constructor() {
            this.Keyvar          = atob("JDJ5JDEwJERLZkV3QTRndTIvaW8sd2hnbGVicDIyM1ZNNUhHc2FhZmMubEFaZkJsZ1FxcG9uNzc2bW0=");

            /* 
              Desarrollo encriptado 

            this.Apivar          = "http://40.114.39.91/api/public/api/";
            this.Servar          = "http://40.114.39.91/backend/app/webroot/";
            this.ApivarcorehomeI = "http://40.114.39.91/";
            this.Apivarcore      = "http://40.121.45.204/api/";
            this.Apivarcorehome  = "http://40.121.45.204/";
            this.Tipoaccion      = '2';  
            
            */

            /* 
              Desarrollo normal
            */
            
            this.Apivar          = "http://23.96.3.66/api/public/api/";
            this.Servar          = "http://23.96.3.66/backend/app/webroot/";
            this.ApivarcorehomeI = "http://23.96.3.66/";
            this.Apivarcore      = "http://23.96.19.56/api/";
            this.Apivarcorehome  = "http://23.96.19.56/";            
            this.Tipoaccion = '2'; 
            
            

            /*    PRODUCCION      
            
            this.Apivar          = atob("aHR0cHM6Ly93ZWJhcHAucGFnYWRvcmVzLmNvbS9hcGkvcHVibGljL2FwaS8=");
            this.Servar          = atob("aHR0cHM6Ly93ZWJhcHAucGFnYWRvcmVzLmNvbS9iYWNrZW5kL2FwcC93ZWJyb290Lw==");
            this.Apivarcore      = atob("aHR0cHM6Ly9hcGkxLnBhZ2Fkb3Jlcy5jb20vYXBpLw==");
            this.Apivarcorehome  = atob("aHR0cHM6Ly9hcGkxLnBhZ2Fkb3Jlcy5jb20v");
            this.ApivarcorehomeI = atob("aHR0cHM6Ly93ZWJhcHAucGFnYWRvcmVzLmNvbS8");
            this.Tipoaccion = '1'; 
           
            */

            /*
            this.Apivar          = "http://40.114.39.91/api/public/api/";
            this.Servar          = "http://40.114.39.91/backend/app/webroot/";
            this.ApivarcorehomeI = "http://40.114.39.91/";            
            this.Apivarcore      = "http://23.96.19.56/api/";
            this.Apivarcorehome  = "http://23.96.19.56/";            
            this.Tipoaccion      = '2';   
            */


            //this.Apivar = "http://localhost/pagadores/api/public/api/";


            //////////////////////////////////////////////////////////////////////////////////////////
            this.credolabAuth = atob("YzQ0Yjc5ZTEtYzE0YS00NzM1LTkxN2EtMjM0ZGEyNDBhNjhk");
            this.credolabUrl  = atob("aHR0cHM6Ly9zY29yaW5nLWJyLmNyZWRvbGFiLmNvbS8=");
  }
/////////////////////////////ENCRIPTACION/////////////////////////////////////////
  encrypt(textPhrase, paraPhrase) {
            var secret = paraPhrase;
            var encrypted = CryptoJS.AES.encrypt(textPhrase, secret);
            this.encryptedData = encrypted.toString();
            console.log("Cipher text: " + this.encryptedData);
            return this.encryptedData;
  }
  decrypt(encryptedText, paraPhrase) {
            var secret = paraPhrase;
            this.encryptedData = ''
            var decrypted = CryptoJS.AES.decrypt(encryptedText, secret).toString(CryptoJS.enc.Utf8);
            this.decryptedData = decrypted;
            console.log("decrypted text: " + this.decryptedData);
            return this.decryptedData;
  }
  encryptRSA() {
                /*this.UniqueDeviceID.get().then((uuid: any) => {
                       this.UniqueDeviceIDVar = uuid;
                }).catch((error: any) => {
                       this.UniqueDeviceIDVar = localStorage.getItem('TOKEAPICORE');
                       
                });*/
                this.UniqueDeviceIDVar = localStorage.getItem('TOKEAPIPAGADORES');
                var text = this.UniqueDeviceIDVar;
                var cert = forge.pki.certificateFromPem(this.certificateContentRSA);
                var publicKey = cert.publicKey;
                let encrypt = publicKey.encrypt(forge.util.encodeUtf8(text), 'RSA-OAEP', {
                  md: forge.md.sha256.create(),
                  mgf1: {
                    md: forge.md.sha1.create()
                  },
                });
                this.encryptedDataRSA = forge.util.encode64(encrypt)
                return this.encryptedDataRSA;
                
  }
///////////////////////////////////////////////////////////////////////////////////
  getKeyvar() {
    return this.Keyvar;
  }
  getApivar() {
    return this.Apivar;
  }
  getServar() {
    return this.Servar;
  }
  getApivarcore() {
    return this.Apivarcore;
  }
  getApivarcoreHome() {
    return this.Apivarcorehome;
  }
  getApivarcoreHomeI() {
    return this.ApivarcorehomeI;
  }
  getTipoaccion(){
    return this.Tipoaccion;
  }

  getCredolabAuth(){
    return this.credolabAuth;
  }
  getCredolabUrl(){
    return this.credolabUrl;
  }

}