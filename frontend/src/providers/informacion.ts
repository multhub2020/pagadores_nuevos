import { Injectable, Component } from '@angular/core';
//import { Http,Response,Headers,RequestOptions } from '@angular/http';
import { HTTP } from '@ionic-native/http/ngx';

import { HttpClient, HttpHeaders } from '@angular/common/http';

import 'rxjs/add/operator/map';
import 'rxjs/Rx';
import { Observable } from 'rxjs/Observable';
import { Variablesglobales } from './variablesglobal';
@Injectable()
@Component({
  templateUrl: 'template.html',
  providers:[Variablesglobales, HTTP]
})
export class Informacion{ //Se define la clase
        public v:string;
        public url     = new Variablesglobales();
        public httpHeader = {
                                headers: new HttpHeaders({  'Content-Type': 'application/json', 
                                                                  'Accept': 'application/json',
                                                          'remember_token': this.url.getKeyvar()
                                                        })
                            };
        public httpHeader_token = {
            headers: new HttpHeaders({
                'Content-Type': 'aplicación/vnd.api+json'
            })
        };
        public direccionGoogle : string = 'https://maps.googleapis.com/maps/api/geocode/json?latlng=';
        constructor(
            public _http:HttpClient,
            public http: HTTP
        ){}
        inforpoliticas(var1){
                return  this._http.post(
                                this.url.getApivar()+"inforpoliticas",  
                                JSON.stringify({
                                    'categoria':var1,
                                }),
                                this.httpHeader
                            );
        }
        inforcondiciones(var1){
                return  this._http.post(
                                this.url.getApivar()+"inforcondiciones",  
                                JSON.stringify({
                                    'categoria':var1,
                                }),
                                this.httpHeader
                        );
        }


        prueba_apo() {
                return this.http.get("https://api.caribbeancinemas.co/app/v3/carrusel/1", this.httpHeader_token, {});
        }



        
}

    